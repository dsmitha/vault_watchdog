# File: Awards utils
#
# Filename:
# >awards_utils.py
#
# Description:
# This file will carry the support functions that are used by the NAV functions
# that are specific to the Awards application. Those functions usually are not
# going to generate report and their main objective is just to modularize
# and clanup some code in the other functions.

# Framework imports
from framework.reporting import *
from framework.awards.defaults import *
from framework.utils import *

import framework.awards.utils as utils

# Python imports
import os
import shutil
import datetime
import re
import pdb

# Function: delete_list_of_files
# Will delete a list of files passed as parameter (as a list) at the root
# directory passed as parameter.
#
# Author:
# Danilo Guimaraes
def delete_list_of_files(files, root_directory = "c:\\"):

    # Go to the root folder
    utils.send_keys("cd %s [enter]" % root_directory)

    # Start deleting everything without prompt
    for file_to_delete in files:
        utils.send_keys("del /F /Q %s [enter]" % file_to_delete)
        time.sleep(0.5)

def delete_local_list_of_files(files):

    for file_to_delete in files:
        os.remove(file_to_delete)

# Function: validate_list_of_deleted_files
# Will validate a list of files passed as parameter (as a list) have been
# deleted at the directory passed as parameter.
#
# Author:
# Danilo Guimaraes
def validate_list_of_deleted_files(files, exceptions, root_directory = "c:\\"):

    # Go to the root folder
    utils.send_keys("cd %s [enter]" % root_directory)

    # Validate that things have been deleted sucessfully
    for file_to_delete in files:
        utils.send_keys("cls[enter]")
        utils.send_keys("dir %s [enter]" % file_to_delete)

        if not utils.wait_for_text_in_console("0 File\(s\)|The system cannot find the file specified", True, 3):

            # Check if thats not an exception:
            good = False
            for pattern in exceptions:
                if utils.find_text_in_console(pattern, True):
                    file_from_screen = utils.match_text_in_console(pattern)
                    add_sub_step("The following file has not been deleted but that is ok: %s." % file_from_screen)
                    utils.take_screenshot()
                    good = True
                    break

            if not good:
                add_sub_step("Validate that %s has been deleted sucessfully." % file_to_delete)
                fail_sub_step("[validate_list_of_deleted_files] The process to delete %s has not been sucessfull. Files have been found." % file_to_delete)
                utils.take_screenshot()

# def copy_list_of_files(files, destination):
#
#     # Copy all the files to the destination
#     for file_to_copy in files:
#
#         utils.send_keys("cls[enter]")
#         utils.send_keys("copy %s %s [enter]" % (file_to_copy,destination))
#
#         if not utils.wait_for_text_in_console("file(s) copied", False, DEFAULT_TIMEOUT):
#             add_sub_step("Wait for '%s' to be copied." % file_to_delete)
#             fail_sub_step("[copy_list_of_files] The process to copy '%s' has not been sucessfull or has not finished after %d seconds." % (file_to_copy, DEFAULT_TIMEOUT))

# Function: get_triggered_coupon_amount
# Will press [f4] and will retrieve the list of coupons that have been printed
# in the system so far.
#
# Author:
# Danilo Guimaraes
def get_triggered_coupon_amount():

    # Show the coupon statistics on screen
    utils.send_keys("[f4]")

    # Get the amount of coupons from screen
    end_time = et(DEFAULT_TIMEOUT)
    coupon_amount = utils.match_text_in_console(r"Triggered +\d+")
    while ct() < end_time and coupon_amount is None:
        time.sleep(1)
        utils.send_keys("[f4]")
        coupon_amount = utils.match_text_in_console(r"Triggered +\d+")

    # Check if it has found
    if ct() >= end_time:
        fail_step("[get_triggered_coupon_amount] Could not retrieve the amount of triggered coupons.")
        utils.take_screenshot()
        utils.finish_execution()

    # Get the number of coupons from the string
    coupon_amount = re.findall(r"\d+", coupon_amount)[0]

    # Return the coupon amount
    return int(coupon_amount)

# Function: prepare_sut_temp_directory
# Will cleanup the automation temp directory in the SUT and will create a new
# one. This process is used for the file copy process.
#
# Author:
# Danilo Guimaraes
def prepare_sut_temp_directory(dir_path):

    # Send the dir keys
    utils.send_keys(r"cd C:\ [enter]")
    utils.send_keys("cls[enter]")
    utils.send_keys("dir %s [enter]" % dir_path)

    # In case the directory doesn't exists, create it
    if not utils.wait_for_text_in_console("File Not Found", False, 1):
        utils.send_keys("rm -rf %s [enter]" % dir_path)

    utils.send_keys("mkdir %s [enter]" % dir_path)

# Function: prepare_local_temp_directory
# Will cleanup the automation temp directory in the local system and will create
# a new one. This process is used for the file copy process.
#
# Author:
# Danilo Guimaraes
def prepare_local_temp_directory(dir_path):

    # Delete the folder with contents if it exist
    if os.path.exists(dir_path):
        shutil.rmtree(dir_path)

    # Create the new folder
    os.makedirs(dir_path)

# Function: prepare_evidence_storage_directory
# Will create a new folder in the evidence storage with the name/information of
# the current execution. This process is used for the file copy process.
#
# Author:
# Danilo Guimaraes
def prepare_evidence_storage_directory(storage_path):

    # Build the name of the file
    timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H_%M_%S')
    folder_name = "Awards execution at %s" % timestamp

    # Delete the folder with contents if it exist
    if not os.path.exists(storage_path):
        os.makedirs(storage_path)

    # Check if the results folder has been created
    storage_path += r"\%s" % folder_name
    if not os.path.exists(storage_path):
        os.makedirs(storage_path)

    # Return the name of the directory
    return storage_path

# Function: zip_folder_in_sut
# Zip a folder in the SUT. This process is used for the file copy process.
#
# Author:
# Danilo Guimaraes
def zip_folder_in_sut(dir_path, output_file):

    # Generate the file
    utils.send_keys("pkzip -rp %s %s [enter]" % (output_file, dir_path) )
    utils.wait_for_prompt(r"C:\\")

# Function: clean_sut_temp_directory
# Cleanup the automation temp directory in the SUT. This process is used for the
# file copy process.
#
# Author:
# Danilo Guimaraes
def clean_sut_temp_directory(dir_path):

    # Send the dir keys

    utils.send_keys(r"cd C:\ [enter]")
    utils.send_keys("cls[enter]")
    utils.send_keys("dir %s [enter]" % dir_path)
    #
    # In case the directory doesn't exists, create it
    if not utils.wait_for_text_in_console("File Not Found", False, 1):
        utils.send_keys("rm -rf %s [enter]" % dir_path)

# Function: clean_sut_temp_directory
# Cleanup the automation temp directory in the local machine. This process is
# used for the file copy process.
#
# Author:
# Danilo Guimaraes
def clean_local_temp_directory(dir_path):

    # Delete the folder with contents if it exist
    if os.path.exists(dir_path):
        shutil.rmtree(dir_path)

# Function: delay_after
# Will sleep for some time and add a log entryspecifically for "delay after".
#
# Author:
# Danilo Guimaraes
def delay_after(params):

    # Check if this is supposed to be run
    if "delay after" not in params:
        return

    # Sleep for some time
    delay = int(params["delay after"])
    add_sub_step("Wait %d seconds before moving on to the next keyword." % delay)
    time.sleep(delay)

# Function: delay_before
# Will sleep for some time and add a log entryspecifically for "delay before".
#
# Author:
# Danilo Guimaraes
def delay_before(params):

    # Check if this is supposed to be run
    if "delay before" not in params:
        return

    # Sleep for some time
    delay = int(params["delay before"])
    add_sub_step("Wait %d seconds before executing this keyword." % delay)
    time.sleep(delay)

# Function: verify_ini_file_value
# Will verify if a given value in an .ini file in the SUT matches the expected.
#
# Author:
# Danilo Guimaraes
def verify_ini_file_value(filename, section, entry, value):

    # Store the ini file value in the file
    file_value = utils.get_ini_file_value(filename, section, entry)
    if file_value is None:
        fail_sub_step("The entry '%s\%s' in the file '%s' is invalid." % (section, entry, filename) )

    # Check if the value is correct.
    add_sub_step("Verify the entry '%s\%s' in the file '%s' equals to '%s'." % (section, entry, filename, value) )
    if tu(file_value) != tu(value):
        fail_sub_step("Expected '%s' but found '%s'." % (value, file_value) )


#Function Name: verify_value_not_in_ini_file
#Description: Verify section value i not found in inin files
#Author: Smitha D

def verify_value_not_in_ini_file(filename, section, entry, value):

    # Store the ini file value in the file
    file_value = utils.get_ini_file_value(filename, section, entry)
    if file_value is  None:
        pass_sub_step("The entry '%s\%s' in the file '%s' is not found." % (section, entry, filename))
    else:

        fail_sub_step("The entry '%s\%s' in the file '%s' is present" % (section, entry, filename) )


# Function: verify_section_not_in_ini_file
# Will verify that a given section is not present in the an .ini file in the SUT.
#
# Author:
# Danilo Guimaraes
def verify_section_not_in_ini_file(filename, section):

    # Get the file contents
    ini_file = utils.get_file_contents(filename)
    if ini_file is None:
        return False

    # Search for [section] in it
    if ini_file.lower().find("[%s]" % section) != -1:
        return False

    return True

# Function: run_dmc
# > #################################################################
# > #################################################################
# > #################################################################
# >
# > ###############      #######      ####### ############     ######
# > #################    #######      ####### #############    ######
# > ######     #######   #######      ####### ###### #######   ######
# > ######      #######  #######      ####### ######  #######  ######
# > ######     #######   #######      ####### ######   ####### ######
# > #################    #######      ####### ######    ###### ######
# > ###############      #######      ####### ######     ##### ######
# > ######     ######    #######      ####### ######      ###########
# > ######      ######   #################### ######       ##########
# > ######       ######   ##################  ######        #########
# > ######        ######    ##############    ######         ########
# >
# > #################    #######     #######    ###################
# > ##################   ########   ########   #####################
# > ######      #######  ########   ########  #######         #######
# > ######       ####### ######### #########  ######           ######
# > ######        ###### ######### #########  ######
# > ######        ###### ###### ##### ######  ######
# > ######        ###### ######  ###  ######  ######
# > ######       ####### ######       ######  ######           ######
# > ######      #######  ######       ######  #######         #######
# > ##################   ######       ######   #####################
# > #################    ######       ######    ###################
# >
# > #################################################################
# > #################################################################
# > #################################################################
#
# (will run the command DMC in the awards application for a given award number)
# Author:
# Danilo Guimaraes (not the ascii, I it grabbed from the internet :P)
def run_dmc(mclu):

    utils.send_keys("dmc %s [enter]" % tu(mclu))

    # Find the CMC is now operational on screen
    if not utils.wait_for_text_in_console("MCLU Number", 5):
        fail_step("[validate_mclu] 'MCLU number has not been found on screen.")
        utils.take_screenshot()
        utils.finish_execution()

# Function: run_da
# Will run the command DA in the awards appication for the given award number.
#
# Author:
# Danilo Guimaraes
def run_da(award_number):

    utils.send_keys("da %s [enter]" % tu(mclu))

    # Find the CMC is now operational on screen
    if not utils.wait_for_text_in_console("Treshold Seq", 5):
        fail_step("[run_da] Seems like the 'da' information has not been displayed on screen as requested.")
        utils.take_screenshot()
        utils.finish_execution()

# Function: check_printer_type
# Will press F5 and will check if the printer type displayed on screen matches
# the printer type passed as parameter.
#
# Author:
# Danilo Guimaraes
def check_printer_type(expected_printer_type):

    utils.send_keys("[f5]")
    printer_type = utils.get_console_screen_area(37,22,47,23)
    #
    if tu(printer_type) != tu(expected_printer_type):
        return False

    return True

def load_display_application(start_display="YES"):

    # Get the in the display system
    #If start_display=No then no need to type display from command prompt
    #This is added because if newstore is called then display will be loaded automatically
    if tu(start_display) == "YES":
        utils.send_keys("display [enter]")
        if not utils.wait_for_text_in_console("Display Program", False, 60):
            fail_step("[load_display_application] Could not load the display program.")
            utils.take_screenshot()
            utils.finish_execution()

        utils.take_screenshot()

    # Log into the system
    utils.send_keys("%s[enter]" % DEFAULT_DISPLAY_PASSWORD)
    if not utils.wait_for_text_in_console("MAIN MENU", False, 15):
        fail_step("[load_display_application] Could not log into the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
