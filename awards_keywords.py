﻿# File: Awards Keywords
#
# Filename:
# > awards_keywords.py

#
# Description:
# This file holds the keywords for the awards automation. Those keywords are
# divided in groups:
# - High Level keywords: Those keywords execute and validate their own process,
# without the need of other keywords. They are used for reusable pieces of
# process that are not going to change across tests.
# - Low Level Keywords: Those keywords allow the tester to write a test in ALM
# to cover a very specific process without having to create a new keyword. Those
# keywords allow lower-level interaction with the Awards box, like typing stuff
# on screen, sending keys, etc.
# - Legacy Keywords: Those keywords might be a mix between high level and Low
# level keywords with the main characteristic that those are the keywords used
# by the old awards automation. Those need to be migrated in order to add
# compatibility to the previous solution.
#
# How to create new keywords:
# A few steps are needed:
# - Check if you already have everything you need in terms of actions,
#   navigation and validations in their respective files. If not, create the new
#   functions that do the steps needed. See Awards Navigation, Awards Actions
#   and Awards Validations for more information.
# - Having those functions done, create a new function in this file, under the
#   proper section (High Level or Low Level keywords). Use an existing keyword
#   as example or the template below.
# - Add documentation to your keyword. Again, use the comments from on of the
#   other functions as example. The comments header needs to be in that specific
#   format so that the HTML documentation is generated properly.
# - Go to the file framework/awards/register_keywords.py and rgeister your new
#   keyword in there. See instructions in the file.
# - Make sure you test your keyword before commiting your code. The best way to
#   do that without having to launch the test from ALM is to create a test file
#   at the unit/keyword folder.
#
# Difference between High Level and Low Level keywords:
# - *High Level Keywords*: Will run a given process entirely (ex.: Print
#   Coupon). A High Level keyword will take care of navigating to the right
#   place, running the actions needed and then running the validations. They
#   are built using functions from the Action, Navigation and Validations
#   modules. It is recommended that all High Level keywords also run "cleanup"
#   processes after they run in order to leave the system in a state that the
#   next keywords are able to run without issues.
# - *Low Level keywords*: Will run a command, individual action or validation.
#    The main purpose of the low level keywords is to allow the user to build
#    new scenarios in ALM by "coding" the test directly in the test case.
#    While a High Level keyword runs an entire process, the low level will do
#    something simples like sending keystrokes to the Awards box or navigating
#    to a certain screen. Those keywords can use the functions from the Actions,
#    Navigation and Validation modules but not necessairly will do it.
#
# Briefly speaking, the High Level keyword is almost like a macro, that
# "bundles" a set of actions and validations while the Low Level keywords would
# be like the components that integrate the macro.
#
# Some recommendations:
# - Although is not recommended to add a lot of comments in between your code
#   (headers are ok), please remember to always document your code properly.
#   If is seems obvious, no need to write comments. If someone might question
#   about why something is the way it is, some comment would be good.
# - Try to always leave the "real code" on the NAV files (Navigation, Actions,
#   Validations), building just the keywords here. That keeps the code way more
#   organized.
# - Atom's shortcut for folding the code is *Ctrl+Shift+P* then type *Fold 1
#   [enter]*. That turns the navigation through the code way easier.
#
# Keyword Template:
# (start code)
# # Comments header here
# def keyword_name(params):
#
#   # Comment stating the expected parameters for this action
#
#   start_action("Keyword: <Keyword name>")
#
#   # Function calls here
#
#   end_action()
# (end code)


#Updated comments for functions
#Date- 2-May-2017/ 3-May-2017
#Mindtree-QA

import pdb

# Framework Imports
from framework.utils import *
from framework.reporting import *
from framework.alm import *
from framework.awards.defaults import *
import framework.awards.utils as utils
from framework.awards.settings import machine_settings as ms

# NAV imports
import awards_actions
import awards_navigation
import awards_validations
import awards_utils

# Python imports
import datetime
import ftplib
import shutil

# ##############################################################################
# Section: High Level Keywords
# Those keywords execute and validate their own process, without the need of
# other keywords. They are used for reusable pieces of process that are not
# going to change across tests. High level keywords are supposed to generate
# Actions in the test report, so they need to begin with start_action()
# ##############################################################################

# Keyword: A A A Initialize Store System
#
# Snippet:
# > [Action Word] A A A Initialize Store System
# > [Data Source Name]
# > [Need Database?] YES
# > [CDS Enabled?] Yes
# > [Expected CLU] 379
# > [Printer Type] CMC4
#
# Description:
# Initializes the awards box so that it is ready for running a awards test case.
# The process consists of:
# - Stoping awards
# - Cleaning up the log files
# - Copying the proper store.ini and shuttle.ini
# - If "Need Database?" is "Yes": cleanup the database files also
# - Verifies that the awards and OS version are correct accordingly to
# config.py
# - Unzip the CDS Database
# - Start awards
# - Validate the CLUS, store and chain numbers
# - Force a CDS batch load
#
# Function Prototype:
# > a_a_a_initialize_store_system(params)
#
# Test Strings:
# - For CDS flow:
# > awards_keywords.a_a_a_initialize_store_system({"data source name": "",
# >                                                "need database?": "yes",
# >                                                "cds enabled?": "yes",
# >                                                "expected clu": "379"})
#
# - For SO flow:
# > awards_keywords.a_a_a_initialize_store_system({"data source name": "",
# >                                                "need database?": "yes",
# >                                                "cds enabled?": "no",
# >                                                "expected clu": "1152"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def a_a_a_initialize_store_system(params):

    # The parameters we expect for this keyword are
    #[Data Source Name]
    #[Need Database?]
    #[Turn All Coupons (ON/OFF)]
    #[CDS Enabled?]
    #[Expected CLU]

    start_action("Keyword: A A A Initialize Store System",params)

    # Set default values
    if "store number" not in params or tu(params["store number"]) == "":
        store_number = "0250"
    else:
        store_number = str(params["store number"])

    if "chain number" not in params or tu(params["chain number"]) == "":
        chain_number = "376"
    else:
        chain_number = str(params["chain number"])

    if "big database?" not in params or tu(params["big database?"]) == "":
        params["big database?"] = "No"
    #Added by Smitha
    if "need databse cleanup?" not in params or tu(params["need databse cleanup?"])=="":
        params["need databse cleanup?"] ="YES"

    #Added by Smitha
    if "run rollover" not in params or tu(params["run rollover"])=="":
        params["run rollover"]="NO"
    #Added by Smitha
    if "store file" not in params or tu(params["store file"])=="":
        params["store file"]=""
    if "printer type" not in params or tu(params["printer type"])=="":
        params["printer type"] ="cmc6"

    # 1. Validate all running groups(Dos Sessions) are closed with the exception
    # of the "Multitasking Store System" are closed.
    awards_navigation.to_running_groups_screen()

    awards_actions.close_all_running_groups()
    awards_navigation.to_multitastking_store_system()
    awards_actions.shutdown_awards()

    # Open a new dos session to clean up the files
    awards_navigation.to_new_dos_session()

    # Cleanup thereports, backups, logs and etc.
    awards_actions.log_cleanup()

    # Leave the DOS session
    awards_navigation.leave_dos_session()
    awards_navigation.to_multitastking_store_system()
    awards_actions.start_awards()
    #

    # Only run the first steps if awards is running already
    if not utils.check_process_is_running("mainexec.exe"):

        # Try to open a new awards session
        awards_navigation.to_new_dos_session()
        awards_actions.start_awards(False)

    #
    if utils.check_process_is_running("mainexec.exe"):

        # Go to the multitasking store system for the next few validations
        awards_navigation.to_multitastking_store_system()

        # Check if we already have the right database by looking at the
        # CLUs. If so, leave the keyword and move on.

        if tu(params["need database?"]) == "NO" and awards_validations.validate_clus( params["expected clu"], False ):

            # If we have the right clus, validate the system properties and
            # leave the keyword.

            awards_validations.validate_awards_version()
            awards_validations.validate_os_version()
            awards_validations.validate_build_date()

            # If CDS is YES, validate it is enabled
            if tu(params["cds enabled?"]) == "YES":
                awards_validations.validate_cds_is_enabled("Yes")
                end_action()
                return
                # @Workaround - Printer Sim issue
                # When CDS is enabled, always check for "CMC4", as that is the
                # printer type that will be setup after loading a CDS database.
                #if awards_utils.check_printer_type("CMC4"):

                    # Turn all coupons on before running the script
                    #awards_navigation.to_multitastking_store_system()
                    #awards_actions.run_cmcset_for_all_coupons()
                    #awards_validations.validate_system_is_running()

            else:
                #
                awards_validations.validate_cds_is_enabled("No")
                #awards_actions.start_awards()
                if awards_utils.check_printer_type(params["printer type"]):

                    # Turn all coupons on before running the script
                    awards_navigation.to_multitastking_store_system()
                    #awards_actions.run_cmcset_for_all_coupons()
                    #awards_validations.validate_system_is_running()

                    end_action()
                    #return

        # 2. Shutdown Awards(Type “E” [enter])
        awards_actions.shutdown_awards()

    # 1. Validate all running groups(Dos Sessions) are closed with the exception
    # of the "Multitasking Store System" are closed.
    awards_navigation.to_running_groups_screen()
    awards_actions.close_all_running_groups()

    # If a new database is needed, cleanup the database files
    #if tu(params["need database?"]) == "YES":
    if tu(params["need databse cleanup?"])=="YES":

        awards_actions.new_database_file_cleanup()
    #
    if tu(params["run rollover"])=="YES":
        awards_actions.run_rollover()
    #
    #23. Copy over baseline store.ini file to the \strsystm\data directory
    #    Copy the SO INI file in case this is not a SO file.
    #24. Copy over baseline shuttle.ini to the \strsystm\data directory
    #26. Verify store and chain number- for our old test data it was store 198, chain 198(that could change)
    # Also, set the date and validate the CDS flag

    if tu(params["cds enabled?"]) == "YES":

        # Finally, load the CDS database
        if ie(tu(params["data source name"])):
            #

            awards_actions.load_cds_database()
        else:
            #
            awards_actions.load_cds_database(params["data source name"])

        #awards_actions.copy_store_ini("store.aaa")
        #awards_actions.copy_shuttle_ini()
        awards_actions.set_date(datetime.date.today())
        awards_validations.validate_cds_is_enabled("Yes")
        #
        # Validate the store and chain number
        awards_validations.validate_store_number(str(store_number))
        awards_validations.validate_chain_number(str(chain_number))

        # @Workaround - Printer Sim issue
        # Change the printer type to the printer type we want to load the database
        #wards_actions.set_printer_type(params["printer type"])

    else:
        #
        #awards_actions.copy_store_ini("STR20X.INI")

        if "store file" in params or tu(params["store file"]) != "":
            awards_navigation.to_new_dos_session()
            #awards_actions.copy_store_ini(params["store file"])
        #awards_actions.copy_shuttle_ini()

        # By default, set the date for 2003/11/11 in the SO process
        #awards_actions.set_date(datetime.date(2003, 11, 11))
        awards_validations.validate_cds_is_enabled("No")

        # Load the SO database

        if ie(tu(params["data source name"])):
            #
            awards_actions.load_so_database()
        else:
            awards_actions.load_so_database(params["data source name"])

        # Validate the store and chain number
        awards_validations.validate_store_number(str(store_number))
        awards_validations.validate_chain_number(str(chain_number))
        #
        # Set the printer type to the right type
        if "printer type" in params:
            if tu(params["printer type"]) != "":
                awards_actions.set_printer_type(params["printer type"])

    # 32. Start Awards
    #
    awards_actions.start_awards()
    ##
    # 29. Verify and log Awards version
    #awards_validations.validate_awards_version()
    #
    # 30. Verify and log OS version
    #awards_validations.validate_os_version()
    #
    # 31. Verify database build date: 01-01-2010:00:00:00(default)
    #awards_validations.validate_build_date()
    #
    # 33. Verify system is now operational
    awards_validations.validate_system_is_running()

    # 34. Open another Dos Session
    # 42. Move to the CDS\mirror\related_file directory,
    # i.e. cd\CDS\mirror\related_file [enter]
    # 43. Create file, force_load.job
    # 44. Exit dos Session
    # 45. Return to Awards Session
    # 46. Execute online command- Batchname CDSforceload force [enter]
    # 47. Open a Dos session
    # 48. Verify it has been loaded sucessfully
    # 27. If no database was required, validate CLU=???(not sure until we get
    # our golden data set)
    # For the SO process, use the CLU passed as parameter.
    # All of this in the keyword below
    #

    force_load(params)
    # Removed code for setting printer type CMC4 is not applicble
    # Date - 4/28/2017
    end_action()

# Keyword: Print Coupon
#
# Snippet:
# > [Action Word] Print Coupon
# > [Include File Name] CDS_deac.txt
# > [Coupon Number] 1289567
# > [Print Count] 1
#
# Description:
# - Prints a coupon using TS and an include file.
# - After that, check in the awards system if the coupon has been printed.
# - Then, run a rollover, stop awards and generate the app log.
# - Look in the app log for an entry that shows the coupon has been printed
#   sucessfully.
# - Finally, start awards.
#
# Function Prototype:
# > print_coupon(params)
#
# Test String:
# > awards_keywords.print_coupon({"include file name": "cds_aft.txt",
# >                               "coupon number": "1303278",
# >                               "print count": "2"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def print_coupon(params):

    # The parameters we expect for this keyword are
    # [Include File Name]
    # [Coupon Number]
    # [Print Count]
    #
    # Get the amount of coupons expected to be printed
    #
    print_count = int(tu(params["print count"]))
    #Removed print statements added for debug purpose
    #Date 4/28/2017
    #
    #Added by Smitha
    if "validate coupon" not in params or tu(params["validate coupon"]) == "":
        params["validate coupon"]="No"
    #Added by Smitha
    if "validate barcode" not in params or tu(params["validate barcode"]) == "":
        params["validate barcode"]="No"
    validate_bc = tu(params["validate barcode"])
    #For printing coupns in vault environment
    if "vault" not in params or tu(params["vault"])=="":
        params["vault"]="No"
    print ("valut is"+params["vault"])

    #
    start_action("Keyword: Print Award using TS include file '%s'." % params["include file name"],params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()
    if tu(params["vault"])=="YES":
        awards_actions.clear_print_statistics()

    # Go to the multitasking store system to check the amount of triggered coupons
    awards_navigation.to_multitastking_store_system()
    coupon_amount = awards_actions.get_triggered_coupon_amount()
    #
    # Run TS with the file passed as parameter
    awards_navigation.to_new_dos_session()
    awards_actions.run_ts_with_inc_file(params["include file name"],params["vault"])
    awards_navigation.leave_dos_session()
    #time.sleep(10)
    # Go to the multitasking store system to validate the coupon has been
    # triggered
    awards_navigation.to_multitastking_store_system()
    #
    awards_validations.validate_triggered_coupon_amount( print_count)


    if tu(params["vault"])=="NO":
        # Generate the report file
        awards_actions.run_rollover()

        # Shutown awards
        awards_actions.shutdown_awards()

    # Look in the files generated by the rollover that it has the coupon
    # information that it has been printed correctly
        awards_navigation.to_new_dos_session()
        awards_actions.generate_app_log_file(params["include file name"].replace(".txt", ".log"))
        if "coupon number" in params and params["coupon number"] != "":
            awards_validations.validate_coupon_print_in_log(params["include file name"].replace(".txt", ".log"), params["coupon number"])
        #Added by Smitha
        if tu(params["validate coupon"])=="YES":
            awards_validations.Validate_coupon_number_in_image(params["coupon number"], validate_bc)
        awards_navigation.leave_dos_session()
        awards_navigation.to_multitastking_store_system()
        awards_actions.start_awards()
    else:
        awards_validations.validate_vaut_coupon_print_in_log()

    #commented by ajay as this was not needed here
    # Start awards and make sure the system is running
    #

    #awards_validations.validate_system_is_running()

    end_action()

def print_blind_coupon(params):

    # The parameters we expect for this keyword are
    # [Include File Name]
    # [Coupon Number]
    # [Print Count]

    # Get the amount of coupons expected to be printed
    print_count = int(tu(params["print count"]))

    start_action("Keyword: Print Blind Award using TS include file '%s'." % params["include file name"],params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    # Go to the multitasking store system to check the amount of triggered coupons
    awards_navigation.to_multitastking_store_system()
    coupon_amount = awards_actions.get_triggered_coupon_amount()

    # Run TS with the file passed as parameter
    awards_navigation.to_new_dos_session()
    awards_actions.run_ts_with_inc_file(params["include file name"])
    awards_navigation.leave_dos_session()

    # Go to the multitasking store system to validate the coupon has been
    # triggered
    awards_navigation.to_multitastking_store_system()
    #
    awards_validations.validate_triggered_coupon_amount(coupon_amount + print_count)

    # Generate the report file
    awards_actions.run_rollover()

    # Shutown awards
    awards_actions.shutdown_awards()
    awards_navigation.to_new_dos_session()

    # Look in the files generated by the rollover that it has the coupon
    # information that it has been printed correctly
    awards_actions.generate_app_log_file(params["include file name"].replace(".txt", ".log"))
    awards_validations.validate_blind_coupon_print_in_log(params["include file name"].replace(".txt", ".log"), params["coupon number"])

    #commented by ajay as this was not needed here
    # Start awards and make sure the system is running
    awards_actions.start_awards()
    #awards_validations.validate_system_is_running()

    end_action()

# Keyword: Print Coupon - Negative
#
# Snippet:
# > [Action Word] Print Coupon - Negative
# > [Include File Name] CDS_deac_neg.txt
# > [Coupon Number] 1289567
#
# Description:
# - Tries to print a coupon using TS and an include file.
# - After that, check in the awards system that no coupon has been printed.
# - Then, run a rollover, stop awards and generate the app log.
# - Make sure there is no sucessful print in the print log.
# - Finally, start awards.
#
# Function Prototype:
# > print_coupon_negative(params)
#
# Test String:
# > awards_keywords.print_coupon_negative({"include file name": "cds_aft.txt",
# >                                        "coupon number": "1303278"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def print_coupon_negative(params):

    # The parameters we expect for this keyword are
    # [Include File Name]
    # [Coupon Number]

    # Get the amount of coupons expected to be printed
    print_count = int(tu(params["print count"]))

    start_action("Keyword: Make sure the award '%s' does not print." % params["include file name"],params)

    # Workaround for TRAPI.exe meo ry leak
    awards_actions.reconnect()

    # Go to the multitasking store system to check the amount of triggered coupons
    awards_navigation.to_multitastking_store_system()
    coupon_amount = awards_actions.get_triggered_coupon_amount()

    # Run TS with the file passed as parameter
    awards_navigation.to_new_dos_session()
    awards_actions.run_ts_with_inc_file(params["include file name"])
    awards_navigation.leave_dos_session()

    # Go to the multitasking store system to validate the coupon has been
    # not been triggered
    awards_navigation.to_multitastking_store_system()
    awards_validations.validate_triggered_coupon_amount(coupon_amount)

    # Generate the report file
    awards_actions.run_rollover()

    # Shutown awards
    awards_actions.shutdown_awards()

    # Look in the files generated by the rollover that it has the coupon
    # information that it has been printed correctly
    awards_actions.generate_app_log_file(params["include file name"].replace(".txt", ".log"))
    awards_validations.validate_coupon_not_printed_in_log(params["include file name"].replace(".txt", ".log"), params["coupon number"])

    #commented by ajay as this was not needed here
    # Start awards and make sure the system is running
    #awards_actions.start_awards()
    #awards_validations.validate_system_is_running()

    end_action()

# Keyword: Extract and report system logs
#
# Snippet:
# > [Action Word] Extract and report system Logs
# > [CDS Enabled?] Yes
#
# Description:
# - Zips a list of directories into a File.
# - Downloads this zip file to the local machine.
# - Copies this zip file to a shared repository folder.
# - If the test is running from ALM, adds a link to the test results to the ALM
#   test results.
# - If running CDS, it will also zip the CDS folder and copy it to the
#   repository. /Under Discussion: It takes a LOT of time to download the CDS
#   database./
#
# Function Prototype:
# > extract_and_report_system_logs(params)
#
# Test String:
# > awards_keywords.extract_and_report_system_logs({"cds enabled?": "yes"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def extract_and_report_system_logs(params):

    # The parameters we expect for this keyword are
    #[CDS Enabled?]

    start_action("Keyword: Extract system logs.",params)

    machine_name = ms.awards_machine_name.replace(" ", "_")

    directories = {
        r"C:\strsystm\BACKUP\*.*":  "BACKUP_%s.ZIP"    % machine_name,
        r"C:\strsystm\LOG\*.*":     "LOG_%s.ZIP"       % machine_name,
        r"C:\strsystm\REPORTS\*.*": "REPORTS_%s.ZIP"   % machine_name,
        r"C:\strsystm\CAPTURE\*.*": "CAPTURE_%s.ZIP"   % machine_name,
        r"C:\strsystm\DATA\*.*":    "DATA_%s.ZIP"      % machine_name,
        r"C:\strsystm\DB\*.*":      "DB_%s.ZIP"        % machine_name,
        r"C:\strsystm\SHUTTLE\*.*": "SHUTTLE_%s.ZIP"   % machine_name,
    }
    #
    if tu(params["cds enabled?"]) == "YES":
        directories[r"C:\CDS\*.*"] = "CDS_DB_%s.ZIP"   % machine_name
    #
    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    # Shutdown awards
    awards_navigation.to_multitastking_store_system()
    awards_actions.shutdown_awards()

    # Create the temp directory we'll use for storing the temp files
    # Need to create a temp path with the name of the machine that is currently
    # running the scripts to avoid issues when having concurrent tests running
    # in the same machine.
    local_temp_path = "%s_%s" % (DEFAULT_LOCAL_TEMP_PATH, machine_name)
    awards_utils.prepare_sut_temp_directory(DEFAULT_SUT_TEMP_PATH)
    awards_utils.prepare_local_temp_directory(local_temp_path)

    # Generate the output file for them all
    start_step("Generate The zip files in the awards box.")
    for directory in directories:
        orig_file = "%s\%s" % (DEFAULT_SUT_TEMP_PATH, directories[directory])
        utils.send_keys("rm %s[enter]" % orig_file)
        awards_utils.zip_folder_in_sut(directory, orig_file)

    # Build a list of files with their directory path for the FTP file transfer
    # functions
    files_for_transfer = {}
    for filename in directories:
        files_for_transfer[directories[filename]] = "%s\%s" % (DEFAULT_SUT_TEMP_PATH, directories[filename])

    # Download the files from the SUT to the FTP server
    awards_actions.upload_from_sut_to_ftp_server(files_for_transfer)

    # Download the files from the FTP server to the local machine
    awards_actions.download_from_ftp_to_local(files_for_transfer, local_temp_path)

    # Zip the directory
    start_step("Zip the directory.")
    zip_file_path = "%s_%s" % (DEFAULT_ZIP_LOG_PATH, machine_name)
    utils.zip_folder(local_temp_path, zip_file_path)

    # Copy the file to the repository
    start_step("Copy the system logs zip to the repository at %s." % DEFAULT_EVIDENCE_STORAGE_PATH)

    zip_file_name = "syslogs_%s.zip" % machine_name
    folder_path = awards_utils.prepare_evidence_storage_directory(DEFAULT_EVIDENCE_STORAGE_PATH)
    file_path = "%s\%s" % (folder_path, zip_file_name)

    utils.copy_local_file(zip_file_path + ".zip", file_path)

    # Add the file for linking in ALM
    add_file_for_alm_linking(file_path, "System logs for test execution.")

    # ALso, add the information to the report header
    report.add_relevant_data("System Logs Path", file_path)

    # Cleanup
    start_step("Cleanup - Delete temporary files in the local machine and in the SUT.")

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_utils.clean_sut_temp_directory(DEFAULT_SUT_TEMP_PATH)
    awards_utils.clean_local_temp_directory(local_temp_path)

    awards_actions.start_awards()
    awards_validations.validate_system_is_running()

    #commented by ajay as this would have already left DOS
    awards_navigation.leave_dos_session()

    end_action()

# Keyword: Validate Coupon Not Printed
#
# Snippet:
# > [Action Word] Validate Coupon Not Printed
# > [Coupon Number] 1289570
# > [Log File Name] CDS_deac.001
#
# Description:
# - Opens the log file passed as parameter. It always looks for this log file in
#   the folder "C:\strsystm\LOG\".
# - Looks for a sucessful print log entry. It will fail in case it finds it.
#
# *Note:* When the system restarts it will rename any log file with extension
# .LOG in the logs folder to filename.001. The 001 is sequential, so the next
# next one is 002 and so on. (unless the logs folder is cleanup up)
#
# Function Prototype:
# > validate_coupon_not_printed(params)
#
# Test String:
# > awards_keywords.validate_coupon_not_printed({"coupon number": "yes",
# >                                              "log file name": "CDS_deac.001"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def validate_coupon_not_printed(params):

    # The parameters we expect for this keyword are
    # [coupon number]
    # [log file name]

    start_action("Keyword: Validate Coupon not Printed",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_validations.validate_coupon_not_printed_in_log(params["log file name"], params["coupon number"])
    end_action()

# Keyword: Validate Coupon Printed
#
# Snippet:
# > [Action Word] Validate Coupon Printed
# > [Coupon Number] 1289570
# > [Log File Name] CDS_deac.001
#
# Description:
# - Opens the log file passed as parameter. It always looks for this log file in
#   the folder "C:\strsystm\LOG\".
# - Looks for a sucessful print log entry.
#
# *Note:* When the system restarts it will rename any log file with extension
# .LOG in the logs folder to filename.001. The 001 is sequential, so the next
# next one is 002 and so on. (unless the logs folder is cleanup up)
#
# Function Prototype:
# > validate_coupon_printed(params)
#
# Test String:
# > awards_keywords.validate_coupon_printed({"coupon number": "yes",
# >                                          "log file name": "CDS_deac.001"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def validate_coupon_printed(params):

    # The parameters we expect for this keyword are
    # [coupon number]
    # [log file name]
    start_action("Keyword: Validate Coupon Printed",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_validations.validate_coupon_print_in_log(params["log file name"], params["coupon number"])
    end_action()

# Keyword: Force Load
#
# Snippet:
# > [Action Word] Force Load
# > [CDS Enabled?] Yes
# > [Expected CLU] 379
#
# Description:
# - Generates the force batch job file.
# - Runs the force load.
# - Validates the database has been loaded properly.
#
# Function Prototype:
# > force_load(params)
#
# Test String:
# > awards_keywords.force_load({"cds enabled?": "yes",
# >                             "expected clu": "379"})
#
# > awards_keywords.force_load({"cds enabled?": "no",
# >                             "expected clu": "1152"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def force_load(params):

    # The parameters we expect for this keyword are
    #[CDS Enabled?]
    #[Expected CLU]

    start_action("Keyword: Execute a force load job.",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    # Create the force load file
    awards_navigation.to_new_dos_session()

    awards_actions.create_force_load_job("force_load.job")
    awards_navigation.leave_dos_session()
    awards_navigation.to_multitastking_store_system()
    #
    # Run the batch load for CDS in case the parameter "cds enabled" is YES
    if tu(params["cds enabled?"]) == "YES":
        #

        awards_actions.cds_force_batch_load()
        #
        time.sleep(90)
        # Wait 1 hour in case this is a big database
        if tu(params["big database?"]) == "YES":
            time.sleep(3600)

        awards_validations.validate_cds_database_load()
        # Edited by Smitha - Waiting for loading CLU counts
        time.sleep(30)
        awards_validations.validate_system_is_running()

        #awards_validations.validate_clus( params["expected clu"] )

    # Else, run it for the regular SO process
    else:#
        #
        awards_actions.so_force_batch_load()

    # Wait 1 hour in case this is a big database
        if tu(params["big database?"]) == "YES":
            time.sleep(3600)
        #
        awards_validations.validate_so_database_load()
        awards_validations.validate_system_is_running()
        time.sleep(30)
        #
        if "expected clu" in params:
            awards_validations.validate_clus( params["expected clu"] )

    end_action()

# Keyword: Force Load - Negative
#
# Snippet:
# > [Action Word] Force Load - Negative
# > [CDS Enabled?] Yes
# > [Expected Message] CDS IS NOT ENABLED IN THE STORE.INI
#
# Description:
# - Generates the force batch job file.
# - Runs the force load.
# - Validates the database has not been loaded properly by looking up into the
#   database report whatever is in the parameter [Expected Message].
#
# Function Prototype:
# > force_load_negative(params)
#
# Test String:
# > awards_keywords.force_load_negative({"cds enabled?": "yes",
# >                                      "expected message": "CDS IS NOT ENABLED IN THE STORE.INI"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def force_load_negative(params):

    # The parameters we expect for this keyword are
    #[CDS Enabled?]
    #[Expected Message]
    #[Regex? (True or False)] True

    start_action("Keyword: Execute a force load job for negative results.",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    # Handle some parameters
    params["regex? (true/false)"] = True if tu(params["regex? (true/false)"]) == 'TRUE' else False

    # Create the force load file
    awards_navigation.to_new_dos_session()
    awards_actions.create_force_load_job("force_load.job")
    awards_navigation.leave_dos_session()
    awards_navigation.to_multitastking_store_system()

    # Run the batch load for CDS in case the parameter "cds enabled" is YES
    if tu(params["cds enabled?"]) == "YES":

        awards_actions.cds_force_batch_load()
        awards_validations.validate_error_in_cds_database_log(params["expected message"], params["regex? (true/false)"])

    # Else, run it for the regular SO process
    else:
        awards_actions.so_force_batch_load()
        awards_validations.validate_error_in_so_database_log(params["expected message"], params["regex? (true/false)"])

# Keyword: Run Batch: dailytask
#
# Snippet:
# > [Action Word] Run Batch: dailytask
# > [CDS Enabled?] Yes
#
# Description:
# - Opens the awards online prompt and force start a dailytask batch.
#
# Function Prototype:
# > run_batch_dailytask(params)
#
# Test String:
# > awards_keywords.run_batch_dailytask({"cds enabled?": "yes"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def run_batch_dailytask(params):

    # The parameters we expect for this keyword are
    # [CDS Enabled?]

    start_action("Keyword: Run Batch: dailytask.",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_navigation.to_multitastking_store_system()
    awards_actions.force_batch_run("dailytask")
    time.sleep(600)
    if tu(params["cds enabled?"]) == "YES":
        awards_validations.validate_cds_database_load()
    else:
        awards_validations.validate_clus( params["expected clu"] )

    end_action()

# Keyword: Run Batch: database
#
# Snippet:
# > [Action Word] Run Batch: database
# > [CDS Enabled?] Yes
#
# Description:
# - Opens the awards online prompt and force start a database batch.
#
# Function Prototype:
# > run_batch_database(params)
#
# Test String:
# > awards_keywords.run_batch_database({"cds enabled?": "yes"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def run_batch_database(params):

    # The parameters we expect for this keyword are
    # [CDS Enabled?]

    start_action("Keyword: Execute batch: database.",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_navigation.to_multitastking_store_system()
    awards_actions.force_batch_run("database")

    end_action()

# Keyword: Replace Store INI
#
# Snippet:
# > [Action Word] Replace Store INI
# > [Store INI Name] Store.CDSNEG
#
# Description:
# - Deletes the existing store ini at C:\strsystm\data and copy a new store.ini
#   matching the name given as parameter. The INI file must be located at
#   'C:\strsystm\comm\a'
#
# Function Prototype:
# > replace_store_ini(params)
#
# Test String:
# > awards_keywords.replace_store_ini({"store ini name": "Store.CDSNEG"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def replace_store_ini(params):

    # The parameters we expect for this keyword are
    # [Store INI Name]

    start_action("Keyword: Replace the store.ini",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    # Run the action
    awards_actions.delete_store_ini()
    awards_actions.copy_store_ini(params["store ini name"])

    end_action()

# Keyword: Validate CDS section in INI file
#
# Snippet:
# > [Action Word] Validate CDS section in INI file
# > [INI File Path] C:\strsystm\install\store.USA
# > [Enabled] Yes
# > [Mirror] \cds\mirror
# > [AwardDir] awardfile
# > [JobDir] related_file
# > [CatalinaDir] to_catalina
# > [CCDir] cc_file
# > [CCFileExtension] xml
# > [CCFileBasePattern] *
# > [AwardFileExtension] xml
# > [AwardFileBasePattern] *
# > [JobFileExtension] job
# > [JobFileBasePattern] *
# > [AfariaLockFileName] TransferLock.FLG
#
# Description:
# - Gets the store.ini passed as parameter and validates each field of the CDS
#   section accordingly to the parameters passed.
#
# Function Prototype:
# > validate_cds_section_in_ini_file(params)
#
#
# Test String:
# > awards_keywords.validate_cds_section_in_ini_file({"ini file path": "C:\strsystm\install\store.USA",
# >                                                   "enabled": "Yes",
# >                                                   "mirror": "\cds\mirror",
# >                                                   "awarddir": "awardfile",
# >                                                   "jobdir": "related_file",
# >                                                   "catalinadir": "to_catalina",
# >                                                   "ccdir": "cc_file",
# >                                                   "ccfileextension": "xml",
# >                                                   "ccfilebasepattern": "*",
# >                                                   "awardfileextension": "xml",
# >                                                   "awardfilebasepattern": "*",
# >                                                   "jobfileextension": "job",
# >                                                   "jobfilebasepattern": "*",
# >                                                   "afarialockfilename": "TransferLock.FLG"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def validate_cds_section_in_ini_file(params):

    start_action("Keyword: Validate CDS section in INI files.",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    # Remove some undesired stuff from the parameters to build the list of Values
    values = params.copy()
    values.pop("ini file path")

    # Run the action
    awards_validations.validate_cds_section_in_ini_file(params["ini file path"], values)

    end_action()

# Keyword: Validate CDS section not in INI file
#
# Snippet:
# > [Action Word] Validate CDS section not in INI file
# > [INI File Path] C:\strsystm\install\store.FRA
#
# Description:
# - Gets the ini file passed as parameter and validates it has no [CDS] section
#   in it.
#
# Function Prototype:
# > validate_cds_section_not_in_ini_file(params)
#
# Test String:
# > awards_keywords.validate_cds_section_not_in_ini_file({"ini file pathe": "C:\strsystm\install\store.FRA"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def validate_cds_section_not_in_ini_file(params):

    start_action("Keyword: Validate CDS section is NOT in INI file.",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    # Run the action
    awards_validations.validate_cds_section_not_in_ini_file(params["ini file path"])

    end_action()

# Keyword: Validate realtime section in INI file
#
# Snippet:
# > [Action Word] Validate realtime section in INI file
# > [INI File Path] C:\strsystm\data\store.ini
# > [Enabled] No
# > [ZeroMQ_Tran_Address] tcp://10.192.25.100:3000
#
# Description:
# - Gets the ini file passed as parameter and validates the values for the
#   for the realtime section accordingly to the parameters passed.
#
# Function Prototype:
# > validate_realtime_section_in_ini_file(params)
#
# Test String:
# > awards_keywords.validate_realtime_section_in_ini_file({"INI File Path": "C:\strsystm\data\store.ini",
# >                                                        "Enabled": "No",
# >                                                        "ZeroMQ_Tran_Address": "tcp://10.192.25.100:3000"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def validate_realtime_section_in_ini_file(params):

    start_action("Keyword: Validate Realtime section in INI files.",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    # Remove some undesired stuff from the parameters to build the list of Values
    values = params.copy()
    values.pop("ini file path")

    # Run the action
    awards_validations.validate_realtime_section_in_ini_file(params["ini file path"], values)

    end_action()

# Keyword: Validate QuickCash DW log
#
# Snippet:
# > [Action Word] Validate QuickCash DW log
# > [Coupon]
# > [UPC]
#
# Description:
# - Rollover the report.
# - Generate a DW log.
# - Validate the quickcash print in the DW log.
#
# Function Prototype:
# > validate_quickcash_dw_log(params)
#
# Test String:
# > awards_keywords.validate_quickcash_dw_log({"coupon": "",
# >                                            "upc": ""})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def validate_quickcash_dw_log(params):

    start_action("Validate QuickCash coupon: Points Award",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_navigation.to_multitastking_store_system()
    awards_actions.run_rollover()

    awards_navigation.to_new_dos_session()
    awards_actions.generate_dw_log_file(DEFAULT_DW_LOG_PATH)
    awards_navigation.leave_dos_session()

    awards_validations.validate_quickcash_in_dw_log(DEFAULT_DW_LOG_PATH, params['coupon'], params['upc'])

    end_action()

# Keyword: Validate Images Folder
#
# Snippet:
# > [Action Word] Validate Images Folder
# > [Expected amount of files] 131
#
# Description:
# - Runs a dir in the folder C:\strsystm\images and makes sure the total amount
#   of files in the directory matches what has been passed as parameter.
#
# Function Prototype:
# > validate_images_folder(params)
#
# Test String:
# > awards_keywords.validate_images_folder({"expected amount of files": "131"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def validate_images_folder(params):

    start_action("Validate Images folder.",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_navigation.to_new_dos_session()
    awards_validations.validate_jpgs_in_images_folder(params['expected amount of files'])
    awards_navigation.leave_dos_session()

    end_action()

# Keyword: Validate XMLDB Folder
#
# Snippet:
# > [Action Word] Validate XMLDB Folder
# > [Expected amount of files] 324
#
# Description:
# - Runs a dir in the folder C:\strsystm\XMLDB and makes sure the total amount
#   of files in the directory matches what has been passed as parameter.
#
# Function Prototype:
# > validate_xmldb_folder(params)
#
# Test String:
# > awards_keywords.validate_xmldb_folder({"expected amount of files": "324"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def validate_xmldb_folder(params):

    start_action("Validate XMLdb folder.",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_navigation.to_new_dos_session()
    awards_validations.validate_xmls_in_xmldb_folder(params['expected amount of files'])
    awards_navigation.leave_dos_session()

    end_action()

# Keyword: Validate coupon data - DMC
#
# Snippet:
# > [Action Word] Validate coupon data - DMC
# > [Coupon]
# > [Coupon Class]
# > [Coupon Type]
# > [Print Priority]
#
# Description:
# - Runs the *dmc* command in the online prompt.
# - When the coupon data is displayed, start validating all fields accordingly
#   to the parameters.
#
# Function Prototype:
# > validate_coupon_data_dmc(params)
#
# Test String:
# > awards_keywords.validate_coupon_data_dmc({"coupon": "",
# >                                           "coupon class": "",
# >                                           "coupon type": "",
# >                                           "print priority": ""})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def validate_coupon_data_dmc(params):

    start_action("Validate coupon data at DMC for coupon '%s'."%params['coupon'],params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_navigation.to_multitastking_store_system()
    awards_validations.validate_coupon_data_dmc(params['coupon'], params['coupon class'], params['coupon type'], params['print priority'])

    end_action()

# Keyword: Validate coupon data - DA
#
# Snippet:
# > [Action Word] Validate coupon data - DA
# > [Award Number]
# > [Coupon]
# > [Treshold Seq]
# > [Coup Seq]
# > [Description]
#
# Description:
# - Runs the *da* command in the online prompt.
# - When the coupon data is displayed, start validating all fields accordingly
#   to the parameters.
#
# Function Prototype:
# > validate_coupon_data_da(params)
#
# Test String:
# > awards_keywords.validate_coupon_data_da({"award number": "",
# >                                          "coupon": "",
# >                                          "treshold seq": "",
# >                                          "coup seq": "",
# >                                          "description": ""})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def validate_coupon_data_da(params):

    start_action("Validate coupon data at DA for coupon '%s'."%params['award number'],params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_navigation.to_multitastking_store_system()
    awards_validations.validate_coupon_data_da(params['award number'], params['treshold seq'], params['coup seq'], params['description'], params['coupon'])

    end_action()

# Keyword: Validate NBIC Condition
#
# Snippet:
# > [Action Word] Validate NBIC Condition
# > [Award Number]
# > [Expected Condition]
#
# Description:
# - Runs the *da* command in the online prompt.
# - Validates the expected NBIC condition is being displayed on screen.
#
# Function Prototype:
# > validate_nbic_condition(params)
#
# Test String:
# > awards_keywords.validate_nbic_condition({"award number": "",
# >                                          "expected condition": ""})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def validate_nbic_condition(params):

    start_action("Validate the Award '%s' has a NBIC condition.",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_navigation.to_multitastking_store_system()
    awards_validations.validate_nbic_condition(params['award number'], params['expected condition'])

    end_action()

# Keyword: New Store
#
# Snippet:
# > [Action Word] New Store
# > [Country] United States
# > [Printer] CMC6
# > [Locale] US
#
# Description:
# - Runs the *newstore* command using the parameters.
#
# Function Prototype:
# > new_store(params)
#
# Test String:
# > awards_keywords.new_store({"country": "united states",
# >                            "printer": "cmc6",
# >                            "locale": "US"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def new_store(params):

    start_action("Go thru the new store process.",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_actions.run_newstore(params["country"], params["printer"], params["locale"])

    end_action()

# Keyword: Set Printer Type
#
# Snippet:
# > [Action Word] Set Printer Type
# > [Printer Type] CMC4
#
# Description:
# - Go to the awards session and shut it down.
# - Open the display application.
# - Go to the lane configuration screen and change the printer type.
# - Leave the display program.
# - Start the awards system.
#
# Function Prototype:
# > set_printer_type(params)
#
# Test String:
# > awards_keywords.set_printer_type({"printer type": "cmc4"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def set_printer_type(params):

    start_action("Set printer type.",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_navigation.to_multitastking_store_system()
    awards_actions.shutdown_awards()
    awards_actions.set_printer_type(params["printer type"])
    awards_actions.start_awards()

    end_action()

# Keyword: Replace Batch.ini
#
# Snippet:
# > [Action Word] Replace batch.ini
# > [File Name] C:\strsystm\comm\a\batch.ini.aaa
#
# Description:
# - Go to a new DOS session and delete the current batch.ini
# - Copy a new batch.ini in place of it. This file is passed as parameter.
# - Leave the dos session
#
# Function Prototype:
# > replace_batch_ini(params)
#
# Test String:
# > awards_keywords.replace_batch_ini({"file name": "C:\strsystm\comm\a\batch.ini.aaa"})
#
# Author:
# Danilo Guimaraes (Code) / Barry Hauhe (Process)
def replace_batch_ini(params):

    start_action("Keyword: Replace batch.ini by %s." % params["file name"],params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_navigation.to_new_dos_session()
    awards_actions.replace_batch_ini(params["file name"])
    awards_navigation.leave_dos_session()

    end_action()

# Keyword: Delete File
#
# Snippet:
# > [Action Word] Delete File
# > [File Name] C:\example.txt
#
# Description:
# - Will delete the file passed as parameter.
#
# Function Prototype:
# > delete_file(params)
#
# Test String:
# > awards_keywords.delete_file({"file name": "C:\\temp\\example.txt"})
#
# Author:
# Danilo Guimaraes
def delete_file(params):

    # The parameters we expect for this keyword are
    #[File Name]

    if "file name" not in params or tu(params["file name"]) == "":
        start_step("Keyword: Delete file.")
        fail_step("Parameter 'file name' not found or no value has been associated to it.")

    start_action("Keyword: Delete the file %s." % params["file name"],params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    # Open the DOS session
    awards_navigation.to_new_dos_session()

    # Delete the file
    awards_actions.delete_file(params["file name"])

    # Leave the DOS session
    awards_navigation.leave_dos_session()

    end_action()

# Keyword: Set Printer Type
#
# Snippet:
# > [Action Word] Set Printer Type
# > [Printer Type] CMC6
#
# Description:
# - Will move to the awards session
# - After that, is going to shutown awards and launch display
# - It is going to set the printer to the type set in the parameters
# - After that, it will return to DOS and start awards
#
# Function Prototype:
# > set_printer_type(params)
#
# Test String:
# > awards_keywords.set_printer_type({"printer type": "CMC6"})
#
# Author:
# Danilo Guimaraes
def set_printer_type(params):

    # The parameters we expect for this keyword are
    #[File Name]

    if "printer type" not in params or tu(params["printer type"]) == "":
        start_step("Keyword: Set Printer Type.")
        fail_step("Parameter 'printer type' not found or no value has been associated to it.")

    start_action("Keyword: Set the printer type to %s." % params["printer type"],params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_actions.shutdown_awards()

    # Go to the multitasking store system and shutdown awards
    awards_navigation.to_multitastking_store_system()
    awards_actions.shutdown_awards()
    awards_navigation.leave_dos_session() # do not leave this session open, we'll not use it anymore

    # Open the DOS session and set the printer type
    awards_navigation.to_new_dos_session()
    awards_actions.set_printer_type(params["printer type"])

    # Use the same sesison to start awards
    awards_actions.start_awards()

    end_action()

def copy_ic_file(params):

    if "original file name" not in params or tu(params["original file name"]) == "":
        start_step("Keyword: Copy IC file.")
        fail_step("Parameter 'original file name' not found or no value has been associated to it.")

    if "destination file name" not in params or tu(params["destination file name"]) == "":
        start_step("Keyword: Copy IC file.")
        fail_step("Parameter 'destination file name' not found or no value has been associated to it.")

    start_action("Keyword:Copy the IC file %s." % params["destination file name"],params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    # Open the DOS session
    awards_navigation.to_new_dos_session()

    # Delete the file
    origin = "%s\%s" % (DEFAULT_IC_FILE_PATH, params["original file name"])
    destination = "%s\%s" % (DEFAULT_IC_FILE_DESTINATION, params["destination file name"])
    awards_actions.delete_file(destination)
    awards_actions.copy_file(origin, destination)

    # Leave the DOS session
    awards_navigation.leave_dos_session()

    end_action()

def validate_amount_of_clus(params):

    start_action("Keyword: Validate Amount of CLUs",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_navigation.to_multitastking_store_system()
    awards_validations.validate_clus(params["expected clus"])

    end_action()

def clean_logs(params):

    start_action("Keyword: Clean Logs",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_navigation.to_new_dos_session()
    awards_actions.log_cleanup()
    awards_navigation.leave_dos_session()

    end_action()

def validate_shuttle_ini(params):

    start_action("Keyword: Validate shuttle.ini",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    awards_navigation.to_multitastking_store_system()

    # Get the values from screen
    expected_clus = awards_actions.capture_clus_from_screen()
    expected_messages = awards_actions.capture_messages_from_screen()
    expected_pictures = awards_actions.capture_pictures_from_screen()
    expected_triggers = awards_actions.capture_triggers_from_screen()
    expected_last_shipment_processed = params["last shipment processed"]

    # Validate the Shuttle.ini
    awards_validations.validate_shuttle_ini(
        expected_last_shipment_processed,
        str(expected_clus),
        expected_messages,
        expected_pictures,
        expected_triggers
    )

    end_action()

def set_store_and_chain_number(params):

    start_action("Keyword: Set Store and Chain number in the Store.ini file",params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    # Shutdown awards and then set the store and chain numbers
    awards_navigation.to_multitastking_store_system()
    awards_actions.shutdown_awards()
    awards_actions.set_store_and_chain_number(params)
    awards_actions.start_awards()
    awards_validations.validate_system_is_running()

    end_action()

# Keyword: Validate system is operational
#
# Snippet:
# > [Action Word] Validate system is operational
#
# Description:
# - Will move to the awards session
# - Will wait until <-----CMC IS NOW OPERATIONAL----> is displayed on screen.
# - In case it is not displayed after the DEFAULT_TIMEOUT, it will press P and
#   check if "1  Ready" can be found on screen.
#
# Function Prototype:
# > validate_system_is_operational(params)
#
# Test String:
# > awards_keywords.validate_system_is_operational({})
#
# Author:
# Danilo Guimaraes
def validate_system_is_operational(params):

    start_action("Keyword: Validate the system is operational.",params)

    awards_navigation.to_multitastking_store_system()
    awards_validations.validate_system_is_running()

    end_action()

def update_awards(params):

    # Expected parameters:
    # - expected clus
    # - version
    # - build date
    # - os version

    # By default, wait 5 minutes for the VM to reboot
    reboot_timeout = 300

    # Wait up to 30 minutes between reboots
    interval_timeout = 3600
    #
    start_action("Keyword: Update Awards",params)
    #
    # Before anything, capture the current awards version
    utils.send_keys("[ctrl!][esc!]")
    awards_navigation.to_multitastking_store_system()

    #old_awards_version="16.3.185.0"
    old_awards_version = awards_actions.capture_awards_version()
    #
    #if old_awards_version==awards_version:
        #pass_step("The system is already updated.")
        #sys.exit(0)

    # Clean the logs before running the upgrade process
    awards_navigation.to_new_dos_session()
    #awards_actions.log_cleanup()
    awards_navigation.leave_dos_session()

    # Upload the build to the FTP server and then download
    # it into the Awards machine
    #
    #pdb.set_trace()
    #if tu(params["version"]) == "DAILY":
    expected_version = awards_actions.download_daily_build_from_server(params,params["build date"], params["build folder"])


    '''else:
        expected_version = tu(params["version"])
        sys.exit(0) '''# @TODO Need to find the process to download the file
                    # for a given version. This is not done yet.
    #

    #awards_actions.upload_from_local_to_ftp({DEFAULT_UPDATE_FILENAME:DEFAULT_UPDATE_LOCAL_FILEPATH})
    #
    if tu(params["version"])=="DAILY" or tu(params["version"])=="NIGHTLY":
        filename = DEFAULT_UPDATE_FILE_SUT

        restart_count=4
    if   tu(params["version"])=="FIRSTUPD":
        filename=DEFAULT_UPD_FILENAME

        restart_count=2
    if tu(params["version"])=="STORELAB":

        print (params["patch"])
        if tu(params["patch"])=="YES":
            restart_count=4

            awards_actions.download_from_ftp_to_sut({ms.machine_name +"\\"+ DEFAULT_UPDATE_FILENAME:DEFAULT_UPDATE_FILEPATH})
            utils.send_keys(r"copy %s %s\%s [enter]"%(DEFAULT_UPDATE_FILENAME,DEFAULT_IC_FILE_DESTINATION,DEFAULT_UPDATE_FILE_SUT))
            utils.send_keys("y[enter]")
            utils.send_keys(r"pkzipc -ext -dir  %s\%s  c: [enter]"%(DEFAULT_IC_FILE_DESTINATION,DEFAULT_UPDATE_FILE_SUT))
            time.sleep(2)
            utils.send_keys("y[enter]")
            time.sleep(2)
            utils.send_keys("y[enter]")
            time.sleep(2)
            utils.send_keys("y[enter]")
            time.sleep(2)
            utils.send_keys("y[enter]")
            time.sleep(4)
            utils.send_keys(r"copy c:\strsystm\comm\storelab.zip  c:\strsystm\comm\firstupd.zip[enter]")
            awards_navigation.leave_dos_session

            #awards_utils.delete_local_list_of_files([DEFAULT_UPDATE_LOCAL_FILEPATH +"\\"+ ms.machine_name +"\\"+DEFAULT_UPDATE_FILENAME])
            #awards_navigation.to_new_dos_session()


        else:
            filename=DEFAULT_STORE_FILENAME
            restart_count=4
    awards_navigation.to_new_dos_session()
    #
    if tu(params["version"])=="DAILY" or tu(params["version"])=="NIGHTLY":
        awards_actions.download_from_ftp_to_sut({ms.machine_name +"\\"+ DEFAULT_UPDATE_FILENAME:DEFAULT_UPDATE_FILEPATH})
        utils.send_keys(r"copy %s %s\%s [enter]"%(DEFAULT_UPDATE_FILENAME,DEFAULT_IC_FILE_DESTINATION,filename))
        utils.send_keys("Y[enter]")
        time.sleep(4)


#
        awards_navigation.leave_dos_session()

        # cleanup
        awards_utils.delete_local_list_of_files([DEFAULT_UPDATE_LOCAL_FILEPATH +"\\"+ ms.machine_name +"\\"+DEFAULT_UPDATE_FILENAME])
    elif tu(params["version"])=="STORELAB" and tu(params["patch"])=="YES":

        pass
    else:

        awards_actions.download_from_ftp_to_sut({ms.machine_name +"\\"+ filename:DEFAULT_UPDATE_FILEPATH})


        utils.send_keys(r"copy %s %s\%s [enter]"%(filename,DEFAULT_IC_FILE_DESTINATION,filename))
        utils.send_keys("Y[enter]")
        time.sleep(4)


#
        awards_navigation.leave_dos_session()

        # cleanup
        awards_utils.delete_local_list_of_files([DEFAULT_UPDATE_LOCAL_FILEPATH +"\\"+ ms.machine_name +"\\"+filename])

    #
    # Start the update process
    awards_navigation.to_multitastking_store_system()
    if tu(params["version"])=="DAILY" or tu(params["version"])=="STORELAB":

        awards_actions.force_batch_run("dailytask")
    if tu(params["version"])=="NIGHTLY":
        awards_navigation.to_new_dos_session()
        awards_actions. set_time(datetime.datetime.strptime("11:45","%H:%M"))
        time.sleep(1800)
    if tu(params["version"])=="FIRSTUPD":

        awards_actions.force_batch_run("firstupd")
        awards_navigation.to_multitastking_store_system()
        end_time = et(REBOOT_INIT_TIME)
        while not utils.find_text_in_console("firstupd is complete",True) and ct() < end_time:
            time.sleep(3)
        utils.send_keys("restart[enter]")


    for i in range (restart_count):
    #time.sleep(1600)
    # Commented by Smitha as reboot was not happening
    # Reboot 1
        awards_actions.wait_for_no_connection(interval_timeout)
        awards_actions.wait_for_connection(reboot_timeout)

    # Reboot 2
    #awards_actions.wait_for_no_connection(interval_timeout)
    #awards_actions.wait_for_connection(reboot_timeout)

    # Reboot 3
    #awards_actions.wait_for_no_connection(interval_timeout)
    #awards_actions.wait_for_connection(reboot_timeout)

    interval_timeout=4800
    # Reboot 4
    #awards_actions.wait_for_no_connection(interval_timeout)
    #awards_actions.wait_for_connection(reboot_timeout)

    # Go to the multitasking store system and make sure we wait
    # until the system is operational

    utils.send_keys("[ctrl!][esc!]")
    awards_navigation.to_multitastking_store_system()
    awards_validations.validate_system_is_running(reboot_timeout)

    #



    # In case this is a daily build, we can`t predict the version that is going
    # be present in the system after the update in case this is a daily build.\
    # For that reason, we only capture values and make sure we are not stuck
    # in the same version.


        #awards_validations.validate_awards_update()

    new_awards_version = awards_actions.capture_awards_version()

    new_os_version = awards_actions.capture_os_version()

    new_build_date = awards_actions.capture_build_date()

    new_build_version = "A - %s" %new_awards_version

    # Make sure we are not stuck in the same version of the system.
    if tu(old_awards_version) == tu(new_awards_version):
        fail_step("The system version remains the same. It has not been updated.")
        sys.exit(0)

    # Add the information to the header
    add_header_info("New Awards Version", new_awards_version)
    add_header_info("New OS Version", new_os_version)
    add_header_info("New Build Date", new_build_date)

# Else, we just run the regular validation capturing the fields.

    #awards_validations.validate_awards_update()
    #new_awards_version_validate = awards_validations.validate_awards_version()
    #new_os_version = awards_validations.validate_os_version()
    #new_build_date = awards_validations.validate_build_date()



    # Update the config file with the new awards_version

    # Make sure we are in the awards session and validate the amount of CLUs
    # is the right one
    awards_navigation.to_multitastking_store_system()
    #awards_validations.validate_clus(tu(params["expected clus"]))


    #awards_actions.update_config_file("124.56","45.5","12-34-55","345")
    if new_build_version!=daily_build_ver:
        awards_actions.update_config_file(new_awards_version, new_os_version, new_build_date, new_build_version)

    end_action()
# ##############################################################################
# Section: Low Level Keywords
# Those keywords allow the tester to write a test in ALM to cover a very
# specific process without having to create a new keyword. Those keywords allow
# lower-level interaction with the Awards box, like typing stuff on screen,
# sending keys, etc. Low level keywords are supposed to generate steps in the
# test report, so they need to begin with start_step()
# ##############################################################################

# Keyword: Send Keys
#
# Snippet:
# > [Action Word] Send Keys
# > [Keys] cd C:\ [enter]
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Send keys to the Awards box using the TRapi. Special keys must be enclosed
#   by [brackets]. Adding a '!' to the keystroke will hold the key and adding a
#   '.' will release it.
#
# Examples:
# > dir C:\ [enter]
# > [alt!][ctrl!][del!]
# > [shift!][f2]
# > escaping what is not a special \[key]
#
# *Note 1*: Anything enclosed by [brackets] is going to be considered a special
# key. In case the function detects it as being an invalid special key, this
# entire sequence of characters is going to be ignored. In order to type stuff
# enclose by [brackets], use \ (backslash) to escape the special key:
# > escaping what is not a special \[key]
#
# *Note 2*: It is always recommended to have a blank space ' ' before a special
# key in case you have more text before it, to avoid escaping valid special
# keys by accident (when typing a directory path, for example). Example:
# > better do this way [enter]
# > to avoid issues \when\using\system\paths\finishing\with\backslash\ [enter]
#
# Function Prototype:
# > send_keys(params)
#
# Test String:
# > awards_keywords.send_keys({"keys": r"cd C:\ [enter]"})
#
# Author:
# Danilo Guimaraes
def send_keys(params):

    # Some error handling
    if "keys" not in params or tu(params["keys"]) == "":
        start_step("Keyword: Send Keys")
        fail_step("Parameter 'keys' not found or no key has been associated to it.")

    start_action("Keyword: Send Keys '%s'" % params["keys"],params)

    # Wait Some time after running the command
    awards_utils.delay_before(params)

    # Send the keys
    utils.send_keys(params["keys"])

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Validate Text is on Screen
#
# Snippet:
# > [Action Word] Validate Text is on Screen
# > [Text] .*Done.*
# > [Regex? (True or False)] True
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Captures the screen text using the TRapi.
# - Tries to find or match what has been passed in the parameter [Text]
# - Logs the result in the report.
#
# Function Prototype:
# > validate_text_is_on_screen(params)
#
# Test String:
# > awards_keywords.validate_text_is_on_screen({"text": ".*Done.*",
# >                                             "regex? (true or false)": "True"})
#
# Author:
# Danilo Guimaraes
def validate_text_is_on_screen(params):

    # Error handling
    if "text" not in params or tu(params["text"]) == "":
        start_step("Keyword: Validate text is on screen")
        fail_step("Parameter 'text' not found or no value has been associated to it.")

    if "regex? (true/false)" not in params or tu(params["regex? (true/false)"]) == "":
        start_step("Keyword: Validate text is on screen")
        fail_step("Parameter 'regex? (true/false)' not found or no value has been associated to it.")

    elif tu(params["regex? (true/false)"]) not in ['TRUE', 'FALSE']:
        start_step("Keyword: Validate text is on screen")
        fail_step("Parameter 'regex? (true/false)' should be 'True' or 'False'. Value found: '%s'." % params["regex? (true/false)"])

    start_action("Keyword: Validate text is on screen: '%s'" % params["text"],params)

    # Handle some parameters
    params["regex? (true/false)"] = True if tu(params["regex? (true/false)"]) == 'TRUE' else False

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Run the action
    if not utils.find_text_in_console(params["text"], params["regex? (true/false)"]):
        fail_step("Could not find the text on screen.")

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Wait for Text on Screen
#
# Snippet:
# > [Action Word] Wait for Text on Screen
# > [Text] .*Done.*
# > [Regex? (True or False)] True
# > [Timeout] 60 (optional)
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Captures the screen text using the TRapi.
# - Tries to find or match what has been passed in the parameter [Text]
# - In case it has not been matched/found, wait one second and repeat the
#   process.
# - In case the timeout has been exceeded, logs a fail in the report.
# - In case the test has been found/matched, logs sucess in the report.
#
# Function Prototype:
# > wait_for_text_on_screen(params)
#
# Test String:
# > awards_keywords.wait_for_text_on_screen({"text": ".*Done.*",
# >                                          "regex? (true or false)": "True",
# >                                          "timeout": "60"})
#
# Author:
# Danilo Guimaraes
def wait_for_text_on_screen(params):

    # Error handling
    if "text" not in params or tu(params["text"]) == "":
        start_step("Keyword: Wait for text on screen")
        fail_step("Parameter 'text' not found or no value has been associated to it.")

    if "regex? (true/false)" not in params or tu(params["regex? (true/false)"]) == "":
        start_step("Keyword: Wait for text on screen")
        fail_step("Parameter 'regex? (true/false)' not found or no value has been associated to it.")

    elif tu(params["regex? (true/false)"]) not in ['TRUE', 'FALSE']:
        start_step("Keyword: Wait for text on screen")
        fail_step("Parameter 'regex? (true/false)' should be 'True' or 'False'. Value found: '%s'." % params["regex? (true/false)"])

    start_action("Keyword: Wait for text on screen: '%s'" % params["text"],params)

    # Handle some parameters
    params["regex? (true/false)"] = True if tu(params["regex? (true/false)"]) == 'TRUE' else False

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Run the action
    if not utils.wait_for_text_in_console(params["text"], params["regex? (true/false)"]):
        fail_step("Could not find the text on screen.")

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Go to multitasking store system
#
# Snippet:
# > [Action Word] Go to multitasking store system
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# Tries to navigate to the Awards system screen:
# - Press ctrl + esc until it can find the running groups screen.
# - When it is at the running groups screen, select the right pane, press home
#   and enter.
#
# Function Prototype:
# > go_to_multitasking_store_system(params)
#
# Test String:
# > awards_keywords.go_to_multitasking_store_system({})
#
# Author:
# Danilo Guimaraes
def go_to_multitasking_store_system(params):

    start_action("Keyword: Go to the multitasking store system session.",params)

    # Wait Some time after running the command
    awards_utils.delay_before(params)

    # Run the Action
    awards_navigation.to_multitastking_store_system()

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Open a new DOS session
#
# Snippet:
# > [Action Word] Open a new DOS session
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# Tries to navigate to a new DOS session:
# - Press ctrl + esc until it can find the running groups screen.
# - When it is at the running groups screen, select the left pane, press down
#   and enter.
#
# Function Prototype:
# > open_a_new_dos_session(params)
#
# Test String:
# > awards_keywords.open_a_new_dos_session({})
#
# Author:
# Danilo Guimaraes
def open_a_new_dos_session(params):

    start_action("Keyword: Open a new DOS session.",params)

    # Wait Some time after running the command
    awards_utils.delay_before(params)

    # Run the Action
    awards_navigation.to_new_dos_session()

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Leave DOS session
#
# Snippet:
# > [Action Word] Leave DOS session
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# Tries to navigate to a new DOS session:
# - Types 'exit' and press enter.
#
# *Note*: This keyword assumes you are already in a DOS session. It will not
# work if triggered from another screen.
#
# Function Prototype:
# > leave_dos_session(params)
#
# Test String:
# > awards_keywords.leave_dos_session({})
#
# Author:
# Danilo Guimaraes
def leave_dos_session(params):

    start_action("Keyword: Leave DOS session.",params)

    # Wait Some time after running the command
    awards_utils.delay_before(params)

    # Run the Action
    awards_navigation.leave_dos_session()

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Go to Running Groups screen
#
# Snippet:
# > [Action Word] Go to Running Groups screen
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Press ctrl + esc and check if the text "Running Groups" can be found in the
#   screen.
#
# Function Prototype:
# > go_to_running_groups_screen(params)
#
# Test String:
# > awards_keywords.go_to_running_groups_screen({})
#
# Author:
# Danilo Guimaraes
def go_to_running_groups_screen(params):

    start_action("Keyword: Go to Running Groups Screen.",params)

    # Wait Some time after running the command
    awards_utils.delay_before(params)

    # Run the Action
    awards_navigation.to_running_groups_screen()

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Close all running groups
#
# Snippet:
# > [Action Word] Close all running groups
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Go to the running groups screen.
# - Checks what is the last row from the left pane.
# - While the last row from the right pane is not "Multitasking Store System",
#   It will:
# - Focus the right pane, press end and enter to get into the dos session.
#   Then, it will type exit and enter to finish the session.
# - This is going to repeat this process while we have only the "Multitasking
#   Store System" in the running groups.
#
# Function Prototype:
# > close_all_running_groups(params)
#
# Test String:
# > awards_keywords.close_all_running_groups({})
#
# Author:
# Danilo Guimaraes
def close_all_running_groups(params):

    start_action("Keyword: Close all running groups.",params)

    # Wait Some time after running the command
    awards_utils.delay_before(params)

    # Run the Action
    awards_actions.close_all_running_groups()

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Validate CLUs
#
# Snippet:
# > [Action Word] Validate CLUs
# > [expected clu] 1152
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Will press F6 in the awards screen in order to bring the store stats on
#   screen.
# - Then, it will validate that the amount of CLUs displayed on screen matches
#   what has been passed as parameters.
#
# *Note*: This keyword assumes you are already in the Awards session screen.
#
# Function Prototype:
# > validate_clus(params)
#
# Test String:
# > awards_keywords.validate_clus({"expected clu": "1152"})
#
# Author:
# Danilo Guimaraes
def validate_clus(params):

    # Some error handling
    if "expected clu" not in params or tu(params["expected clu"]) == "":
        start_step("Keyword: Validate CLUs")
        fail_step("Parameter 'Expected CLU' not found or no key has been associated to it.")

    start_action("Keyword: Validate CLUs '%s'" % params["expected clu"],params)

    # Wait Some time after running the command
    awards_utils.delay_before(params)

    # Send the keys
    awards_validations.validate_clus( int(tu(params["expected clu"])) )

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Shutdown Awards
#
# Snippet:
# > [Action Word] Shutdown Awards
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Will type E and press enter so that the awards system starts to shutdown.
# - Will wait until "FYI - AWARDS: ONLINE SYSTEM IS SHUTDOWN" is displayed on
#   screen.
#
# *Note 1*: This keyword assumes you are already in the Awards session screen.
# *Note 2*: After running this keyword, the DOS prompt will be available for
# regular usage. Not running Start Awards from this very same prompt will result
# on having the multitasking store system in a different place at the Running
# Groups Screen, what may break navigation functions.
#
# Function Prototype:
# > shutdown_awards(params)
#
# Test String:
# > awards_keywords.shutdown_awards({})
#
# Author:
# Danilo Guimaraes
def shutdown_awards(params):

    start_action("Keyword: Shutdown Awards.",params)

    # Wait Some time after running the command
    awards_utils.delay_before(params)

    # Run the Action
    awards_actions.shutdown_awards()

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Start Awards
#
# Snippet:
# > [Action Word] Start Awards
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Will type Awards and press enter so that the awards system starts.
# - Will wait until "CMC IS NOW OPERATIONAL" is displayed on screen.
#
# *Note 1*: This keyword assumes you are in a DOS session.
# *Note 2*: After running this keyword, the DOS prompt will not be available for
# use anymore. This session will become a Multitasking Store System section.
#
# Function Prototype:
# > start_awards(params)
#
# Test String:
# > awards_keywords.start_awards({})
#
# Author:
# Danilo Guimaraes
def start_awards(params):

    start_action("Keyword: Start Awards.",params)

    # Wait Some time after running the command
    awards_utils.delay_before(params)

    awards_navigation.to_multitastking_store_system()
    # Run the Action
    awards_actions.start_awards()
    awards_validations.validate_system_is_running()

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Validate INI File Value
#
# Snippet:
# > [Action Word] Validate INI File Value
# > [File] C:\strsystm\data\store.ini
# > [Section] CDS
# > [Value] Enabled
# > [Expected] No
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Retrieves the INI file passed as parameter and validate that the value at
#   the section that have been passed as parametere match the [Expected] value.
#
# Function Prototype:
# > validate_ini_file_value(params)
#
# Test String:
# > awards_keywords.validate_ini_file_value({"file": "C:\strsystm\data\store.ini",
# >                                          "dection": "CDS",
# >                                          "value": "Enabled",
# >                                          "expected": "No"})
#
# Author:
# Danilo Guimaraes
def validate_ini_file_value(params):

    # Error handling
    if "file" not in params or tu(params["file"]) == "":
        start_step("Keyword: Validate INI File Value")
        fail_step("Parameter 'file' not found or no value has been associated to it.")

    if "section" not in params or tu(params["section"]) == "":
        start_step("Keyword: Validate INI File Value")
        fail_step("Parameter 'section' not found or no value has been associated to it.")

    if "value" not in params or tu(params["value"]) == "":
        start_step("Keyword: Validate INI File Value")
        fail_step("Parameter 'value' not found or no value has been associated to it.")

    if "expected" not in params or tu(params["expected"]) == "":
        start_step("Keyword: Validate INI File Value")
        fail_step("Parameter 'expected' not found or no value has been associated to it.")

    start_action("Keyword: Validate INI File Value: '%s' ['%s', '%s']. Expected '%s'." % (params["file"], params["section"], params["value"], params["expected"]) ,params)

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Run the action
    value = utils.get_ini_file_value(params["file"], params["section"], params["value"])
    if tu(value) != tu(params["expected"]):
        fail_step("Found '%s' while the expected was '%s'." % (value, params["expected"]))

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Tail File
#
# Snippet:
# > [Action Word] Tail File
# > [File Name]
# > [Expected Value]
# > [Regex? (true/false)]
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Retrieves a file from the Awards box and checks if the [Expected Value] can
#   be found in the file.
# - If it could not be found, repeat the process for a certain time.
# - In case the value has been found, log a sucess in the report.
# - In case the value has not been found and the maximum search time has been
#   exceeded, log a fail.
#
# Function Prototype:
# > tail_file(params)
#
# Test String:
# > awards_keywords.tail_file({"file name": "",
# >                            "expected value": ""})
#
# Author:
# Danilo Guimaraes
def tail_file(params):

    # Error handling
    if "file name" not in params or tu(params["file name"]) == "":
        start_step("Keyword: Tail File")
        fail_step("Parameter 'file name' not found or no value has been associated to it.")
        utils.finish_execution()

    if "expected value" not in params or tu(params["expected value"]) == "":
        start_step("Keyword: Tail File")
        fail_step("Parameter 'expected value' not found or no value has been associated to it.")
        utils.finish_execution()

    if "regex? (true/false)" not in params or tu(params["regex? (true/false)"]) == "":
        start_step("Keyword: Tail File")
        fail_step("Parameter 'regex? (true/false)' not found or no value has been associated to it.")
        utils.finish_execution()

    elif tu(params["regex? (true/false)"]) not in ['TRUE', 'FALSE']:
        start_step("Keyword: Tail File")
        fail_step("Parameter 'regex? (true/false)' should be 'True' or 'False'. Value found: '%s'." % params["regex? (true/false)"])
        utils.finish_execution()

    start_action("Keyword: Tail File '%s' for %s." % (params["file name"], params["expected value"]) ,params)

    # Handle some parameters
    params["regex? (true/false)"] = True if tu(params["regex? (true/false)"]) == 'TRUE' else False

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Run the action
    if not utils.tail(params["file name"], params["expected value"], params["regex? (true/false)"]):
        fail_step("Could not find the value in the file.")
        utils.finish_execution()

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Run TS using Include File
#
# Snippet:
# > [Action Word] Run TS using Include File
# > [Include File Name]
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Copy the file from C:\strsystm\comm\a to C:\
# - Opens a new TS shell in a dos session.
# - Run the include file.
# - Close the TS shell.
# - Delete the file from C:\
#
# Function Prototype:
# > run_ts_using_include_file(params)
#
# Test String:
# > awards_keywords.run_ts_using_include_file({"include file name": ""})
#
# Author:
# Danilo Guimaraes
def run_ts_using_include_file(params):

    if "include file name" not in params or tu(params["include file name"]) == "":
        start_step("Keyword: Run TS using include file." )
        fail_step("Parameter 'include file name' not found or no value has been associated to it.")

    start_action("Keyword: Run TS using include file: '%s'." % params["include file name"],params)

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Run the action
    awards_actions.run_ts_with_inc_file(params["include file name"])

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Set Date
#
# Snippet:
# > [Action Word] Set Date
# > [Date] 11/25/2003 (can be "Today" for today's date)
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Will run the *date set* command in the DOS prompt using the data passed as
#   parameter.
# - After that, runs the *date* command to make sure the date has been setup
#   properly.
#
# Function Prototype:
# > set_date(params)
#
# Test String:
# > awards_keywords.set_date({"date": "today"})
#
# Author:
# Danilo Guimaraes
def set_date(params):

    if "date" not in params or tu(params["date"]) == "":
        start_step("Keyword: Set Date.")
        fail_step("Parameter 'date' not found or no value has been associated to it.")

    start_action("Keyword: Set Date: '%s'." % params["date"],params)

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Convert the date to a datetime object
    if tu(params["date"]) == "TODAY":
        date = datetime.date.today()
    else:
        date = datetime.datetime.strptime(params["date"], "%d/%m/%Y")

    # Run the action
    awards_actions.set_date(date)

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Set Time
#
# Snippet:
# > [Action Word] Set Time
# > [Time] 15:40
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Will run the *time* command in the DOS prompt
# - Then, will input the date passed as parameter
# - After that, will echo the date
#
# Function Prototype:
# > set_time(params)
#
# Test String:
# > awards_keywords.set_time({"time": "15:40"})
#
# Author:
# Danilo Guimaraes
def set_time(params):

    if "time" not in params or tu(params["time"]) == "":
        start_step("Keyword: Set Time.")
        fail_step("Parameter 'time' not found or no value has been associated to it.")

    start_action("Keyword: Set Time: '%s'." % params["time"],params)

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Convert the date to a datetime object
    time = datetime.datetime.strptime(params["time"], "%H:%M")

    # Run the action
    awards_actions.set_time(time)

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Validate File Exists
#
# Snippet:
# > [Action Word] Validate File Exists
# > [File Name] C:\filename.txt
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Will send a special message to the awards box thru the TRapi that should
#   return if the file exists or not.
#
# Function Prototype:
# > validate_file_exists(params)
#
# Test String:
# > awards_keywords.validate_file_exists({"file name": "C:\filename.txt"})
#
# Author:
# Danilo Guimaraes
def validate_file_exists(params):

    if "file name" not in params or tu(params["file name"]) == "":
        start_step("Keyword: Validate file exists.")
        fail_step("Parameter 'filename' not found or no value has been associated to it.")

    start_action("Keyword: Validate file exists: '%s'." % params["file name"],params)

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Run the action
    awards_validations.validate_file_exists(params["file name"])

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Validate File Not Exists
#
# Snippet:
# > [Action Word] Validate File Not Exists
# > [File Name] C:\filename.txt [enter]
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Will send a special message to the awards box thru the TRapi that should
#   return if the file exists or not.
#
# Function Prototype:
# > validate_file_not_exists(params)
#
# Test String:
# > awards_keywords.validate_file_not_exists({"file name": "C:\filename.txt"})
#
# Author:
# Danilo Guimaraes
def validate_file_not_exists(params):

    if "file name" not in params or tu(params["file name"]) == "":
        start_step("Keyword: Validate file does not exists.")
        fail_step("Parameter 'filename' not found or no value has been associated to it.")

    start_action("Keyword: Validate file does not exists: '%s'." % params["file name"],params)

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Run the action
    awards_validations.validate_file_does_not_exists(params["file name"])

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Copy Store INI
#
# Snippet:
# > [Action Word] Copy Store INI
# > [Store INI Name] STORE.AAA
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Copy a store ini from the C:\strsystm\comm\a to C:\strsystm\data accordingly
#   to the store.ini name that has been provided.
#
# Function Prototype:
# > copy_store_ini(params)
#
# Test String:
# > awards_keywords.copy_store_ini({"file name": "C:\filename.txt"})
#
# Author:
# Danilo Guimaraes
def copy_store_ini(params):

    # The parameters we expect for this keyword are
    #[Copy Store INI]

    if "store ini name" not in params or tu(params["store ini name"]) == "":
        start_step("Keyword: Copy Store INI.")
        fail_step("Parameter 'store ini name' not found or no value has been associated to it.")

    start_action("Keyword: Copy Store INI: '%s'" % params["store ini name"],params)

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Run the action
    awards_actions.copy_store_ini(params["store ini name"])

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Delete Store INI
#
# Snippet:
# > [Action Word] Delete Store INI
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Delete the store.ini that can be currently found at C:\strsystm\data.
#
# Function Prototype:
# > delete_store_ini(params)
#
# Test String:
# > awards_keywords.delete_store_ini({})
#
# Author:
# Danilo Guimaraes
def delete_store_ini(params):

    start_action("Keyword: Delete Store INI.",params)

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Run the action
    awards_actions.delete_store_ini()

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Load SO File
#
# Snippet:
# > [Action Word] Load SO File
# > [SO File Name] STR27X.SO
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# - Copy the so file passed as parameter from C:\strsystm\comm\a to
#   C:\strsystm\data.
#
# Function Prototype:
# > load_so_file(params)
#
# Test String:
# > awards_keywords.load_so_file({"so file name": "STR27X.SO"})
#
# Author:
# Danilo Guimaraes
def load_so_file(params):

    # The parameters we expect for this keyword are
    #[So File Name]

    if "so file name" not in params or tu(params["so file name"]) == "":
        start_step("Keyword: Load So File.")
        fail_step("Parameter 'so file name' not found or no value has been associated to it.")

    start_action("Keyword: Load SO File: '%s'" % params["so file name"],params)

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Run the action
    awards_actions.load_so_database(params["so file name"])

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Validate MCLU
#
# Snippet:
# > [Action Word] Validate MCLU
# > [MCLU] 12345667
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# Validate that an MCLU exists in the system:
# - Will run dmc for the MCLU passed as parameter.
# - Will log sucess in case the MCLU is displayed on screen.
#
# Function Prototype:
# > validate_mclu(params)
#
# Test String:
# > awards_keywords.validate_mclu({"mclu": "12345667"})
#
# Author:
# Danilo Guimaraes
def validate_mclu(params):

    # The parameters we expect for this keyword are
    #[MCLU]

    if "mclu" not in params or tu(params["mclu"]) == "":
        start_step("Keyword: Validate MCLU.")
        fail_step("Parameter 'mclu' not found or no value has been associated to it.")

    start_action("Keyword: Validate MCLU: '%s'" % params["mclu"],params)

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Run the action
    awards_validations.validate_mclu(params["mclu"])

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Validate MCLU - Negative
#
# Snippet:
# > [Action Word] Validate MCLU - Negative
# > [MCLU] 12345667
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# Validate that an MCLU exists in the system:
# - Will run dmc for the MCLU passed as parameter.
# - Will log sucess in case the MCLU is not displayed on screen.
#
# Function Prototype:
# > validate_mclu_negative(params)
#
# Test String:
# > awards_keywords.validate_mclu_negative({"mclu": "12345667"})
#
# Author:
# Danilo Guimaraes
def validate_mclu_negative(params):

    # The parameters we expect for this keyword are
    #[MCLU]

    if "mclu" not in params or tu(params["mclu"]) == "":
        start_step("Keyword: Validate MCLU - Negative.")
        fail_step("Parameter 'mclu' not found or no value has been associated to it.")

    start_action("Keyword: Validate MCLU - Negative: '%s'" % params["mclu"],params)

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Run the action
    awards_validations.validate_mclu_negative(params["mclu"])

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Validate process is running
#
# Snippet:
# > [Action Word] Validate process is running
# > [Process Name] COUPAPP.EXE
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# Will send an special TRAPI message in order to see if a given process is
# running.
#
# Function Prototype:
# > validate_process_is_running(params)
#
# Test String:
# > awards_keywords.validate_process_is_running({"process name": "COUPAPP.exe"})
#
# Author:
# Danilo Guimaraes
def validate_process_is_running(params):

    if "process name" not in params or tu(params["process name"]) == "":
        start_step("Keyword: Validate process is running")
        fail_step("Parameter 'process name' not found or no value has been associated to it.")

    start_action("Keyword: Validate process is running: '%s'" % params["process name"],params)

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Run the action
    awards_validations.validate_process_is_running(params["process name"])

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Validate process is not running
#
# Snippet:
# > [Action Word] Validate process is not running
# > [Process Name] COUPAPP.EXE
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# Will send an special TRAPI message in order to see if a given process is not
# running.
#
# Function Prototype:
# > validate_process_is_not_running(params)
#
# Test String:
# > awards_keywords.validate_process_is_not_running({"process name": "COUPAPP.exe"})
#
# Author:
# Danilo Guimaraes
def validate_process_is_not_running(params):

    if "process name" not in params or tu(params["process name"]) == "":
        start_step("Keyword: Validate process is NOT running")
        fail_step("Parameter 'process name' not found or no value has been associated to it.")

    start_action("Keyword: Validate process is NOT running: '%s'" % params["process name"],params)

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Run the action
    awards_validations.validate_process_is_not_running(params["process name"])

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Validate process is running in Awards
#
# Snippet:
# > [Action Word] Validate process is running in Awards
# > [Process Name] COUPAPP
# > [Delay Before] 1 (optional)
# > [Delay After] 1 (optional)
#
# Description:
# Will press F10 in the Awards prompt and then will verify that the process name
# passed as parameter is displayed on screen.
#
# Function Prototype:
# > validate_process_is_running_in_awards(params)
#
# Test String:
# > awards_keywords.validate_process_is_running_in_awards({"process name": "COUPAPP"})
#
# Author:
# Danilo Guimaraes
def validate_process_is_running_in_awards(params):

    if "process name" not in params or tu(params["process name"]) == "":
        start_step("Keyword: Validate process is running in Awards")
        fail_step("Parameter 'process name' not found or no value has been associated to it.")

    start_action("Keyword: Validate process is running in Awards: '%s'" % params["process name"],params)

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Run the action
    awards_validations.validate_process_is_running_in_awards(params["process name"])

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Extended Wait
#
# Snippet:
# > [Action Word] Extended Wait
# > [Time] 5
# > [Reason] Wait for the nightly staging process.
#
# Description:
# Will wait a certain amount of numbers.
#
# Function Prototype:
# > extended_wait(params)
#
# Test String:
# > awards_keywords.extended_wait({"time": "5",
#                                  "reason": "Wait for the nightly staging process."})
#
# Author:
# Danilo Guimaraes
def extended_wait(params):

    if "time" not in params or tu(params["time"]) == "":
        start_step("Keyword: Extended Wait")
        fail_step("Parameter 'time' not found or no value has been associated to it.")

    if "reason" not in params or tu(params["reason"]) == "":
        start_step("Keyword: Extended Wait")
        fail_step("Parameter 'reason' not found or no value has been associated to it.")

    start_action("Keyword: Extended Wait: '%s' minutes. Reason: '%s'." % (params["time"], params["reason"]),params)

    # Run the action
    print("Waiting %s minutes..." % params["time"])
    time.sleep( int(tu(params["time"])) * 60 )
    print("Done.")

    end_action()

# Keyword: Load CDS database
#
# Snippet:
# > [Action Word] Load CDS database
# > [Database Name]
#
# Description:
# Will unzip a new file over the CDS folder in C:\
#
# Function Prototype:
# > load_cds_database(params)
#
# Test String:
# > awards_keywords.load_cds_database({"database name": ""})
#
# Author:
# Danilo Guimaraes
def load_cds_database(params):

	if "database name" not in params or tu(params["database name"]) == "":
		start_step("Keyword: Load CDS database")
		fail_step("Parameter 'database name' not found or no value has been associated to it.")

	start_action("Keyword: Load CDS database: '%s'" % params["database name"],params)

	# Wait Some time before running the command
	awards_utils.delay_before(params)
	awards_navigation.to_new_dos_session()

	awards_actions.load_cds_database(params["database name"])

	# Wait Some time after running the command
	awards_utils.delay_after(params)
	#
	end_action()

# Keyword: Validate list of files exist
#
# Snippet:
# > [Action Word] Validate list of files exist
# > [Folder] C:\strsystm\images\
# > [Files] 12345.jpg, 123456.jpg
#
# Description:
# Will verify that a list of files (separated by ",") exists in the folder
# passed as parameter.
#
# Function Prototype:
# > validate_list_of_files_exist(params)
#
# Test String:
# > awards_keywords.validate_list_of_files_exist({"folder": "C:\strsystm\images\",
# >                                               "files": "12345.jpg, 123456.jpg"})
#
# Author:
# Danilo Guimaraes
def validate_list_of_files_exist(params):

    if "folder" not in params or tu(params["folder"]) == "":
        start_step("Keyword: Verify list of files exist")
        fail_step("Parameter 'folder' not found or no value has been associated to it.")

    if "files" not in params or tu(params["files"]) == "":
        start_step("Keyword: Verify list of files exist")
        fail_step("Parameter 'files' not found or no value has been associated to it.")

    start_action("Keyword: Verify list of files exist in folder: '%s'" % params["folder"],params)

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    awards_validations.validate_list_of_files_exist(params["folder"], params["files"])

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Validate file contains
#
# Snippet:
# > [Action Word] Validate file contains
# > [File Name] ???
# > [Text to Validate] ???
# > [Case Sensitive?] False
# > [Regex?] False
#
# Description:
# Will verify that an specific file contains a value that has been passed as
# parameter.
#
# Function Prototype:
# > validate_file_contains(params)
#
# Test String:
# > awards_keywords.validate_file_contains({"file name": "",
# >                                         "text to validate": "",
# >                                         "case sensitive?": "False",
# >                                         "regex?": "False"})
#
# Author:
# Danilo Guimaraes
def validate_file_contains(params):

    if "filename" not in params or tu(params["filename"]) == "":
        start_step("Keyword: Validate file contains")
        fail_step("Parameter 'filename' not found or no value has been associated to it.")

    if "text to validate" not in params or tu(params["text to validate"]) == "":
        start_step("Keyword: Validate file contains")
        fail_step("Parameter 'text to validate' not found or no value has been associated to it.")

    if "case sensitive?" not in params or tu(params["case sensitive?"]) == "":
        start_step("Keyword: Validate file contains")
        fail_step("Parameter 'case sensitive flag' not found or no value has been associated to it.")

    if "regex?" not in params or tu(params["regex?"]) == "":
        start_step("Keyword: Validate text is on screen")
        fail_step("Parameter 'regex?' not found or no value has been associated to it.")
    if "Extract log" not in params or tu(params["Extract log"])=="":
        params["Extract log"]="No"
    file_name=params["filename"]

    start_action("Keyword: Validate file '%s' contains '%s'." % (params["filename"],params["text to validate"]),params)
    if tu(params["Extract log"])=="YES":
        utils.send_keys("dblist -m %s >%s"%(params["filename"],"log.txt"))
        file_name="log.txt"
    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Create the flags
    if tu(params["case sensitive?"]) == "TRUE":
        cs_flag = True
    else:
        cs_flag = False

    if tu(params["regex?"]) == "TRUE":
        regex_flag = True
    else:
        regex_flag = False
    #
    awards_validations.validate_file_contains(file_name,params["text to validate"],cs_flag,regex_flag)

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Validate file does not contains
#
# Snippet:
# > [Action Word] Validate file does not contains
# > [File Name] ???
# > [Text to Validate] ???
# > [Case Sensitive?] False
# > [Regex?] False
#
# Description:
# Will verify that an specific file does not contains a value that has been
# passed as parameter.
#
# Function Prototype:
# > validate_file_not_contains(params)
#
# Test String:
# > awards_keywords.validate_file_not_contains({"file name": "",
# >                                             "text to validate": "",
# >                                             "case sensitive?": "False",
# >                                             "regex?": "False"})
#
# Author:
# Danilo Guimaraes
def validate_file_not_contains(params):

    if "file name" not in params or tu(params["file name"]) == "":
        start_step("Keyword: Validate file contains")
        fail_step("Parameter 'filename' not found or no value has been associated to it.")

    if "text to validate" not in params or tu(params["text to validate"]) == "":
        start_step("Keyword: Validate file contains")
        fail_step("Parameter 'text to validate' not found or no value has been associated to it.")

    if "case sensitive? (true/false)" not in params or tu(params["case sensitive? (true/false)"]) == "":
        start_step("Keyword: Validate file contains")
        fail_step("Parameter 'case sensitive flag (true/false)' not found or no value has been associated to it.")

    if "regex? (true/false)" not in params or tu(params["regex? (true/false)"]) == "":
        start_step("Keyword: Validate text is on screen")
        fail_step("Parameter 'regex? (true/false)' not found or no value has been associated to it.")

    start_action("Keyword: Validate file '%s' contains '%s'." % (params["file name"],params["text to validate"],params))

    # Wait Some time before running the command
    awards_utils.delay_before(params)

    # Create the flags
    if tu(params["case sensitive? (true/false)"]) == "TRUE":
        cs_flag = True
    else:
        cs_flag = False

    if tu(params["regex? (true/false)"]) == "TRUE":
        regex_flag = True
    else:
        regex_flag = False

    awards_validations.validate_file_not_contains(params["file name"],params["text to validate"],cs_flag,regex_flag)

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Reconnect
#
# Snippet:
# > [Action Word] Reconnect
#
# Description:
# Will close the current connection to the Awards box and start a new one.
# This function needs to be called once in a while when only using low-level
# keywords.
# The TRAPI.exe (that runs in the Awards box) has an issue with memory management,
# that causes the awards box system memory to be completely filled after some
# time running the scripts. This is reseted when closing the connection, so this
# function comes to use in this situation. In case this is not done, the TRAPI.exe
# will start to take a LOT of time to reply send the acks or to reply to the
# requests (because the system has started using the paging memory) up to the
# point in which the system hangs. (Awards box)
#
# Function Prototype:
# > reconnect(params)
#
# Test String:
# > awards_keywords.reconnect({})
#
# Author:
# Danilo Guimaraes
def reconnect(params):

    start_action("Keyword: Reconnect.",params)

    awards_actions.reconnect()

    # Wait Some time after running the command
    awards_utils.delay_after(params)

    end_action()

# Keyword: Wait for connection
#
# Snippet:
# > [Action Word] Wait for connection
#
# Description:
# Will wait until the TRAPI can be reached through the network.
#
# Function Prototype:
# > wait_for_connection(params)
#
# Test String:
# > awards_keywords.wait_for_connection({})
#
# Author:
# Danilo Guimaraes
def wait_for_connection(params):

    if "timeout" in params and not ie(params["timeout"]):
        timeout = int(params["timeout"])
    else:
        timeout = DEFAULT_TIMEOUT
    start_action("Keyword: Wait for Connection",params)
    awards_actions.wait_for_connection(timeout)
    end_action()

# ##############################################################################
# Section: Legacy keywords below
# Those keywords might be a mix between high level and Low level keywords with
# the main characteristic that those are the keywords used by the old awards
# automation. Those need to be migrated in order to add compatibility to the
# previous solution.
# ##############################################################################

# Keyword: Stop Awards
# Shuts down the awards system.
#
# Snippet:
# > [Action Word] Stop Awards
#
# Author:
# ? / Migrated by Danilo Guimaraes
def stop_awards(params):

    start_action("Keyword: Stop Awards (Legacy)",params)
    awards_navigation.to_multitastking_store_system()
    awards_actions.shutdown_awards()
    end_action()

# Keyword: Start Awards
# Initializes the awards system.
#
# Snippet:
# > [Action Word] Start Awards
#
# Author:
# ? / Migrated by Danilo Guimaraes
def start_awards(params):

    start_action("Keyword: Start Awards (Legacy)",params)
    awards_actions.start_awards()
    end_action()

# Keyword: OS2 Session
# Initializes a new DOS session.
#
# Snippet:
# > [Action Word] OS/2 session
#
# Author:
# ? / Migrated by Danilo Guimaraes
def os2_session(params):
    start_action("Keyword: OS/2 Session (Legacy)",params)
    awards_navigation.to_new_dos_session()
    end_action()

# Keyword: Execute TS
# Executes a set of commands in the TS terminal
#
# Snippet:
# > [Action Word] Execute TS
# > [Row 1]
# > [Row 2]
# > [Row 3]
# > [Row 4]
# > [Row 5]
#
# Author:
# ? / Migrated by Danilo Guimaraes
def execute_ts(params):
    start_action("Keyword: Execute TS (Legacy)",params)
    awards_actions.run_ts_with_parameters(params)
    end_action()

# Keyword: Awards Session
# Shows up the awards sesion on screen
#
# Snippet:
# > [Action Word] Awards Session
#
# Author:
# ? / Migrated by Danilo Guimaraes
def awards_session(params):
    start_action("Keyword: Awards Session",params)

    awards_navigation.to_multitastking_store_system()

    end_action()

# Keyword: Report Results
# Generate the log extraction.
#
# Snippet:
# > [Action Word] Report Results
#
# Author:
# ? / Migrated by Danilo Guimaraes
def report_results():

    start_action("Keyword: Awards Session",params)
    extract_and_report_system_logs
    end_action()

# Keyword: CMCSet Update
# Change the CMCset print type to Yes or No
#
# Snippet:
# > [Action Word] CMCSet Update
#
# Author: Ajay Keerthi G
def cmcset_update(params):
    start_action("Keyword: CMCSet Update",params)
    awards_navigation.to_new_dos_session()
    awards_utils.clean_sut_temp_directory(r'C:\Strsystm\PrnSimImageDump')
    awards_navigation.leave_dos_session()
    #
    awards_actions.clear_print_statistics()
    time.sleep(20)

    if "oclu" not in params or params["oclu"] == "":
        params["oclu"]="No"
    if "clu" not in params or tu(params["clu"])=="":
        params["clu"]="No"
    if "clu" in params and tu(params["clu"]) == "ALL":
        awards_actions.run_cmcset_off_for_all_coupons()
        awards_actions.clear_print_statistics()
        end_action()
    awards_navigation.to_multitastking_store_system()
    awards_actions.run_cmcset_off_for_all_coupons()
    if "oclu" in params and tu(params["oclu"]) == "OFF":
        #awards_actions.run_cmcset_off_for_all_coupons()
        #time.sleep(10)
        awards_actions.clear_print_statistics()
        end_action()
    #

    if tu(params["oclu"])!="NO" and tu(params["oclu"]) != "OFF":
        awards_actions.run_cmcset_for_specific_oclu(params)
    else:
        #
        awards_actions.shutdown_awards()
        awards_actions.set_print_yes(params)
        awards_actions.start_awards()

    #
    time.sleep(30)
    awards_actions.clear_print_statistics()
    end_action()

def validate_xmllog(params):

    start_action("Keyword: Validate XML log",params)
    awards_navigation.to_new_dos_session()

    awards_validations.validate_xmllog(params)

    end_action()

# Keyword: Validate CDS enabled Yes/No on screen
#
# Snippet:
# > [Action Word] Validate CDS section on screen
#
# Description:
# - Verifies if the CDS enabled Yes/No on screen when we hit a F1
#
# Function Prototype:
# > validate_cds_section_on_screen(params)
#
#
# Test String:
# > awards_keywords.validate_cds_section_on_screen({"enabled": "Yes"})
# Author:
# Ajay Keerthi (Code) / Barry Hauhe (Process)
def validate_cds_section_on_screen(params):
    start_action("Keyword: Validate cds section on awards screen",params)
    #
    awards_navigation.to_multitastking_store_system()
    awards_validations.validate_cdssection_screen(params)
    end_action()
#Function Name validate_barcode
# Validaes the bar code in the coupon image

def validate_bar_code(params):
    #
    start_action("Keyword:Validate Bar Code",params)
    #

    awards_navigation.to_new_dos_session()
    barcode_file=params['include barcode file name']
    barcodetype = params['barcode type']
    awards_validations.validate_barcode(barcode_file, barcodetype,params)
    end_action()

def verify_storefix_single_section(params):
    reboot_timeout = 300
    #start_action("Keyword:verify store.ini file")
    # Wait up to 30 minutes between reboots
    interval_timeout = 1800

    start_action("Keyword:verify store single section file",params)
    awards_actions.shutdown_awards()
    #'''
    section_update=awards_validations.parse_single_section_ini_file(params)
    #
    awards_validations.update_store_ini_single_section(params)


    #

    awards_actions.upload_ini_to_sut()
    #
    run_batch_dailytask(params)
    #
    # Reboot 1
    #
    awards_actions.wait_for_no_connection(interval_timeout)
    awards_actions.wait_for_connection(reboot_timeout)
    #
    # Reboot 2
    awards_actions.wait_for_no_connection(interval_timeout)
    awards_actions.wait_for_connection(reboot_timeout)
    #'''
    # Reboot 3
    '''awards_actions.wait_for_no_connection(interval_timeout)
    awards_actions.wait_for_connection(reboot_timeout)'''

    # Reboot 4
    ''''awards_actions.wait_for_no_connection(interval_timeout)
    awards_actions.wait_for_connection(reboot_timeout)'''

    # Go to the multitasking store system and make sure we wait
    # until the system is operational
    '''awards_navigation.to_multitastking_store_system()
    awards_validations.validate_system_is_running(reboot_timeout)'''

    '''awards_navigation.to_new_dos_session()
    #'''
    '''utils.send_keys("Find ZEROCOUPON err*[enter]")
    if not utils.find_text_in_console("end:ZEROCOUPONCOUNTS",True):
        fail_step("[validate_dailytask_is_done] 'end:ZEROCOUPONCOUNTS' has not been found on screen.")
        utils.take_screenshot()
        utils.finish_execution()'''

    #utils.send_keys("exit [enter]")
    #awards_actions.shutdown_awards()
    #
    awards_validations.compare_store_ini_single_section(params,section_update)
    end_action()


#function_name verfy storefix.ini

def verify_storefix_ini(params):
    # By default, wait 5 minutes for the VM to reboot
#
    reboot_timeout = 300
    start_action("Keyword:verify store.ini file",params)
    # Wait up to 30 minutes between reboots
    interval_timeout = 1800

    if "big database?" not in params or tu(params["big database?"]) == "":
        params["big database?"] = "No"
    awards_actions.shutdown_awards()
    #
    #commented by ajay
    updated_setions=awards_validations.parse_ini_file(params)
    #
    awards_validations.update_store_ini_file()


#

    awards_actions.upload_ini_to_sut()
    #
    awards_navigation.to_multitastking_store_system()
    time.sleep(3)
    utils.send_keys("batchname dailytask force [enter]")
    #force_load(params)
    #run_batch_dailytask(params)
    #
    # Reboot 1
    awards_actions.wait_for_no_connection(interval_timeout)
    awards_actions.wait_for_connection(reboot_timeout)
    #
    # Reboot 2
    awards_actions.wait_for_no_connection(interval_timeout)
    awards_actions.wait_for_connection(reboot_timeout)
    #'''
    # Reboot 3
    '''awards_actions.wait_for_no_connection(interval_timeout)
    awards_actions.wait_for_connection(reboot_timeout)'''

    # Reboot 4
    ''''awards_actions.wait_for_no_connection(interval_timeout)
    awards_actions.wait_for_connection(reboot_timeout)'''

    # Go to the multitasking store system and make sure we wait
    # until the system is operational
    #
    '''awards_navigation.to_multitastking_store_system()
    awards_validations.validate_system_is_running(reboot_timeout)'''

    '''awards_navigation.to_new_dos_session()
    '''
    '''utils.send_keys("Find ZEROCOUPON err*[enter]")
    if not utils.find_text_in_console("end:ZEROCOUPONCOUNTS",True):
        fail_step("[validate_dailytask_is_done] 'end:ZEROCOUPONCOUNTS' has not been found on screen.")
        utils.take_screenshot()
        utils.finish_execution()'''

    #utils.send_keys("exit [enter]")
    #awards_actions.shutdown_awards()
    #
    awards_validations.compare_store_ini_file(updated_setions)
    end_action()



# Function Name : update_ini_file_before_print_coupon
#
##Description: Update ini ile for barcodr color and scale
#
#Input parameters:ini_filename, section, value
#
#Author
#Smitha D

def  update_ini_file_before_print_coupon(params):
    #awards_actions.shutdown_awards()
    if "copy file" not in params or tu(params["copy file"])=="":
        params["copy file"]="Yes"
    if "upload file" not in params or tu(params["upload file"])=="":
        params["upload file"]="Yes"
    #
    awards_actions.update_ini_file(params['ini filename'],params["sectionname"],params["value"],params["copy file"],params["upload file"])
    awards_navigation.to_multitastking_store_system()
    #pdb.set_trace()
    end_action()
    #awards_actions.start_awards()
    #force_load(params)

# Keyword: Start a specified Virtual machine
# Snippet:
# > [Action Word] start VM
# Description:
# - starts a mentioned virtual machine
# Function Prototype:
# > start_vm(params)
# Author:
# Ajay Keerthi (Code)
def start_vm(params):
    #
    strvmname = ms.awards_machine_name#params["vm name"]
    print (strvmname)

    import subprocess
    subprocess.call(["C:\Program Files\Oracle\VirtualBox\VBoxManage.exe", "startvm", strvmname])

#Get the printertype to set it
#Function Name: add_printer
#Description:
# Get the printer name from the testsut name
#Add printer to the award machine
#
#Author:
# Smitha D

def add_printer(params):
    awards_actions.shutdown_awards()
    print(ms.awards_printer_type[0])
    #
    awards_actions.add_printer_type(ms.awards_printer_type[0])
    #
    awards_actions.start_awards()

# Function Name: Validate_address2()
#
#
# Description: Validate address2 related
# strings are there in log file

#Input parameters: Text_to_validate
#
#Author:
#Smitha D
def validate_address2(params):

    start_action("Validate Address2 messages in file ",params)
    file_name=r"c:\strsystm\Log\log"+params["option"]+".txt"
    if "text to validate" not in params or tu(params["text to validate"]) == "":
        start_step("Keyword: Validate file contains")
        fail_step("Parameter 'text to validate' not found or no value has been associated to it.")

    if "case sensitive?" not in params or tu(params["case sensitive?"]) == "":
        start_step("Keyword: Validate file contains")
        fail_step("Parameter 'case sensitive flag' not found or no value has been associated to it.")

    if "regex?" not in params or tu(params["regex?"]) == "":
        start_step("Keyword: Validate text is on screen")
        fail_step("Parameter 'regex?' not found or no value has been associated to it.")

    if tu(params["case sensitive?"]) == "TRUE":
        cs_flag = True
    else:
        cs_flag = False

    if tu(params["regex?"]) == "TRUE":
        regex_flag = True
    else:
        regex_flag = False

    awards_actions.generate_db_log_file(params["option"],file_name)
    print (params["text to validate"])

    if tu(params["search option"]) == "FILE CONTAINS":
        awards_validations.validate_file_contains(file_name,params["text to validate"],cs_flag,regex_flag,xmllog=False)
    if tu(params["search option"]) == "FILE NOT CONTAINS":
        awards_validations.validate_file_not_contains(file_name,params["text to validate"],cs_flag,regex_flag,xmllog=False)





# Function Name: Satrt _ vault()
#
#
# Description: Verify node process is running or not
# Kill all the running process
# start run.cmd#
#
#
#Author:
#Smitha D

def start_vault(params):
    start_action("Keyword: Start vault",params)
    awards_navigation.to_running_groups_screen()
    text_console=utils.get_console_text()
    awards_actions.start_vault()
    #awards_navigation.to_vault_runing()
    awards_validations.validate_vault_is_running()


#Function Name: Install_vault
#
#Description:
# Copy vault.zip in c:\strsystm\command
#restart awards
#
#
#Author: Smitha D

def install_vault(params):
    start_action("Keyword: Install vault",params)

    awards_actions.upload_from_local_to_ftp({"vault.zip":DEFAULT_VAULT_BUILDPATH})
    #

    awards_navigation.to_new_dos_session()

    awards_actions.download_from_ftp_to_sut({"vault.zip":DEFAULT_IC_FILE_DESTINATION})
    utils.send_keys(r"copy vault.zip c:\strsystm\comm[enter]")
    time.sleep(1)
    utils.send_keys("exit[enter]")
    awards_actions.shutdown_awards()
    awards_actions.start_awards()

#Function NAme:
# update_vault_config_file()
#Update the config file for vault
#Author:
#Smitha D
def update_vault_config_file(params):
    #awards_actions.shutdown_awards()

    start_action("Keyword: Update vault Config File",params)
    if "copy file" not in params or tu(params["copy file"])=="":
        params["copy file"]="Yes"
    if "upload file" not in params or tu(params["upload file"])=="":
        params["upload file"]="Yes"
    #
    awards_actions.update_ini_file(params['ini filename'],params["sectionname"],params["value"],params["copy file"],params["upload file"])
    awards_navigation.to_multitastking_store_system()
#Function Name:validate_logrec
#Description: Validate the logrec entry in the app log after coupon print
#Author: Smitha D

def validate_logrec(params):
    start_action("Keyword: validate logrec")
    awards_actions.reconnect()

    # Go to the multitasking store system to check the amount of triggered coupons
    awards_navigation.to_multitastking_store_system()
    coupon_amount = awards_actions.get_triggered_coupon_amount()
    #
    # Run TS with the file passed as parameter
    awards_navigation.to_new_dos_session()
    awards_actions.run_ts_with_inc_file(params["include file name"])
    awards_navigation.leave_dos_session()
    awards_navigation.to_multitastking_store_system()

    # Generate the report file
    awards_actions.run_rollover()
    # Shutown awards
    awards_actions.shutdown_awards()
    awards_navigation.to_new_dos_session()
    awards_actions.generate_app_log_file("app_log.txt")
    awards_validations.validate_logrec_in_applog("app_log.txt",params["coupon number"],params["clu"])
    awards_actions.start_awards()

#Function Name: run_before_cmd
#
#Description: Function will run the before.cmd before upgrade  command and capture logs
#
#Author: Smitha D

def run_before_cmd(params):
    start_action("Keyword:Run Before.cmd",params)

    local_path=local_path = DEFAULT_UPDATE_LOCAL_FILEPATH + "\%s" %ms.machine_name
    awards_utils.prepare_local_temp_directory(local_path)
    file_path=DEFAULT_CMD_FILEPATH+"\\before.cmd"
    shutil.copy(file_path, local_path)
    #awards_actions.upload_from_local_to_ftp({"before.cmd":DEFAULT_CMD_FILEPATH})

    awards_navigation.to_new_dos_session()

    awards_actions.download_from_ftp_to_sut({"before.cmd":DEFAULT_CMD_FILE_DESTINATION})

    utils.send_keys(r"copy C:\\before.cmd c:\strsystm\before.cmd [enter]")
    utils.send_keys("Y [enter]")
    utils.send_keys("exit[enter]")
    awards_navigation.to_multitastking_store_system()
    awards_actions.run_rollover()
    awards_actions.run_rollover("errorlog")

    utils.send_keys("terminate cmc[enter]")
    time.sleep(20)

    utils.send_keys("call before[enter]")
    time.sleep(60)
    utils.send_keys("start cmc[enter]")
    time.sleep(20)
    end_action()


#Function Name: run_after_cmd
#
#Description: Function will run the after.cmd command and capture logs
#
#Author: Smitha D

def run_after_cmd(params):
    start_action("Keyword:Run After.cmd",params)
    local_path=local_path = DEFAULT_UPDATE_LOCAL_FILEPATH + "\%s" %ms.machine_name
    awards_utils.prepare_local_temp_directory(local_path)
    file_path=DEFAULT_CMD_FILEPATH+"\\after.cmd"
    shutil.copy(file_path, local_path)

    #awards_actions.upload_from_local_to_ftp({"after.cmd":DEFAULT_CMD_FILEPATH})
    awards_navigation.to_new_dos_session()
    awards_actions.download_from_ftp_to_sut({"after.cmd":DEFAULT_CMD_FILE_DESTINATION})
    utils.send_keys(r"copy C:\\after.cmd c:\strsystm\after.cmd [enter]")
    utils.send_keys("Y [enter]")
    utils.send_keys("exit[enter]")
    awards_navigation.to_multitastking_store_system()
    utils.send_keys("DMC[enter]")
    utils.send_keys("DMC[enter]")
    utils.send_keys("DT[enter]")
    utils.send_keys("coup all[enter]")
    awards_actions.run_rollover()
    awards_actions.run_rollover("errorlog")
    utils.send_keys("terminate cmc[enter]")
    time.sleep(20)
    utils.send_keys("call after[enter]")

    time.sleep(60)
    utils.send_keys("start cmc[enter]")
    time.sleep(20)
    end_action()

#Function Name: run_extend_cmd
#
#Description: Function will run the before.cmd command and capture logs
#
#Author: Smitha D
def run_extend_cmd(params):
    start_action("keyord:Run Extend Command",params)
    local_path=local_path = DEFAULT_UPDATE_LOCAL_FILEPATH + "\%s" %ms.machine_name
    awards_utils.prepare_local_temp_directory(local_path)
    file_path=DEFAULT_CMD_FILEPATH+"\\extend.cmd"
    shutil.copy(file_path, local_path)

    awards_navigation.to_new_dos_session()
    awards_actions.download_from_ftp_to_sut({"extend.cmd":DEFAULT_CMD_FILE_DESTINATION})

    utils.send_keys(r"copy C:\\extend.cmd c:\strsystm\extend.cmd [enter]")
    awards_navigation.to_multitastking_store_system()
    utils.send_keys("Terminate CMC[enter]")
    time.sleep(20)
    utils.send_keys("call cmcset -a -n[enter]")
    utils.send_keys("call cmcset -a -d[enter]")
    utils.send_keys("call cmcset -a -v -s[enter]")
    utils.send_keys("Start cmc[enter]")
    time.sleep(20)
    awards_navigation.to_new_dos_session()
    utils.send_keys("type NUL> c:\strsystm\capture\binfile.b01[enter]")
    utils.send_keys("exit[enter]")
    awards_navigation.to_multitastking_store_system()
    utils.send_keys("DUMP read[enter]")

    utils.send_keys("CAPT EXTEND[enter]")

    utils.send_keys(" run rollover[enter]")

    utils.send_keys("REPLAY IGNORE[enter]")

    utils.send_keys("REPLAY C:\strsystm\CAPTURE\BINFILE.B01")

    utils.send_ckeys(" run rollover[enter]")

    utils.send_keys("DUMP OFF[enter]")

    utils.send_keys("CAPT OFF[enter]")

    utils.send_keys(" run rollover errorlog[enter]")

    utils.send_keys("terminate cmc[emter]")

    time.sleep(20)
    utils.send_keys("CALL Extend[enter]")

    time.sleep(60)
    utils.send_keys("start cmc [enter]")


def restore_awards_previous(params):
    start_action("keyord:Restore awards",params)
    awards_navigation.to_new_dos_session()
    utils.send_keys(r"copy c:\strsystm\before.zip c:\strsystm\comm\Restore.FLG[enter]")
    awards_navigation.to_multitastking_store_system()
    utils.send_keys("restart[enter]")
    awards_actions.wait_for_no_connection(1800)
    awards_actions.wait_for_connection(1800)
    utils.send_keys("[ctrl!][esc!]")
    awards_navigation.to_multitastking_store_system()
    awards_validations.validate_system_is_running(1800)
    time.sleep(20)
    end_action()

# Function Name: validate_upgrade
###
#Description: Validation of logs nd DB files after upgrade
###
#Author: Smitha D
def validate_upgrade(params):
    start_action("Keyword: Validate files after upgrade",params)
    local_temp_path="%s_%s_upgrade" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    awards_utils.prepare_local_temp_directory(local_temp_path)
    #local_temp_path=
    files_for_transfer={r"before.zip":r"C:\strsystm\before.zip",r"after.zip":r"C:\strsystm\after.zip"}
    #awards_actions.upload_from_sut_to_ftp_server(files_for_transfer)
    print(local_temp_path)

    awards_actions.download_from_ftp_to_local(files_for_transfer,local_temp_path)
    #awards_actions.upload_from_sut_to_ftp_server({r"after.zip":r"C:\strsystm\after.zip"})
    #awards_actions.download_from_ftp_to_sut({local_temp_path:"after.zip"})
    #
    if os.path.exists(local_temp_path+r"\before.zip"):
        utils.unzip_folder(local_temp_path+r"\before.zip",local_temp_path+r"\before")
    else:
        fail_sub_step("Before.zip file is not downloaded")
    if os.path.exists(local_temp_path+r"\after.zip"):
        utils.unzip_folder(local_temp_path+r"\after.zip",local_temp_path+r"\after")
    else:
        fail_sub_step("After.zip file is not downloaded")
    before_size=utils.get_file_size(local_temp_path+r"\before\snapshot")
    after_size=utils.get_file_size(local_temp_path+r"\after\snapshot")

    if before_size != after_size:
        fail_step("Size of the snapshots are not same")
    else:
        pass_step("Snapshot size is same")

    awards_actions.comparefile(local_temp_path+r"\before\dblist.txt",local_temp_path+r"\after\dblist.txt")
    awards_actions.verify_file_content(local_temp_path+r"\after\gsee.txt")
    awards_actions.comparefile(local_temp_path+r"\before\pure.txt",local_temp_path+r"\after\pure.txt")
    awards_validations.validate_ini_file(local_temp_path+r"\before\store.ini",local_temp_path+r"\after\store.ini")
    end_action()

# Function Name: restore_awards
###
#Description: Restore awards usinf before.zip file
###
#Author: Smitha D
def restore_awards(params):
    start_action("Keywaord:Restore_award",params)
    run_before_cmd(params)
    awards_navigation.to_multitastking_store_system()

    #old_awards_version="16.3.185.0"
    old_awards_version = awards_actions.capture_awards_version()
    #old_awards_version=

    print (params["build date"])
    update_awards({"version":params["version"],"build date":params["build date"],"build folder":params["build folder"]})

    run_after_cmd(params)

    restore_awards_previous(params)

    awards_navigation.to_multitastking_store_system()


    #old_awards_version="16.3.185.0"
    new_awards_version = awards_actions.capture_awards_version()
    if old_awards_version!=new_awards_version:
        fail_step("restore command did not restore awards to verion %s "%old_awards_version)
    else:
        pass_step("Awrds restored properly to old version %s"%old_awards_version)
    end_action()


# Function Name: restore_awards
###
#Description: Restore awards usinf before.zip file
###
#Author: Smitha D

def upgrade_BAT(params):
    start_action("Keywaord:Restore_award",params)
    if "patch" not in params or params["patch"]=="":
        params["patch"]="No"
    awards_actions.validate_print_coupon()
    run_before_cmd(params)
    time.sleep(20)
    awards_navigation.to_multitastking_store_system()
    awards_navigation.to_multitastking_store_system()


    #old_awards_version="16.3.185.0"
    old_awards_version = awards_actions.capture_awards_version()
    #old_awards_version=

    print (params["build date"])
    print (params["patch"])

    update_awards({"version":params["version"],"build date":params["build date"],"build folder":params["build folder"],"patch":params["patch"]})

    awards_actions.validate_print_coupon()
    run_after_cmd(params)

    #run_extend_cmd(params)
    restore_awards_previous(params)

    awards_actions.validate_print_coupon()
    awards_navigation.to_multitastking_store_system()


    #old_awards_version="16.3.185.0
    #old_awards_version="17.1.12.0"

    new_awards_version = awards_actions.capture_awards_version()
    validate_upgrade(params)
    if old_awards_version==new_awards_version:
        pass_step("Awards restored successfully")
    else:
        fail_step("Awards did not restored properly")

    end_action()


# Function Name: setup_cds_vault
###
#Description: copy and load CDS DB and vault
###
#Author: Smitha D

def setup_cds_vault(params):
    start_action("Setup CDS and vault install")

    awards_actions.upload_from_local_to_ftp({"vault.zip":DEFAULT_VAULT_BUILDPATH})
    #
    awards_actions.load_cds_database(params["data source name"])

    awards_navigation.to_new_dos_session()

    awards_actions.download_from_ftp_to_sut({"vault.zip":DEFAULT_IC_FILE_DESTINATION})
    utils.send_keys(r"copy vault.zip c:\strsystm\comm[enter]")
    utils.send_keys("Y [enter]")
    utils.send_keys("vaultinst[enter]")
    end_action()
# Function Name: setup_cds_vault
###
#Description: copy and load CDS DB and vault
###
#Author: Smitha D

def setup_so_vault(params):
    start_action("Setup CDS and vault install")

    awards_actions.upload_from_local_to_ftp({"vault.zip":DEFAULT_VAULT_BUILDPATH})
    #
    awards_actions.load_so_database(params["data source name"])

    awards_navigation.to_new_dos_session()

    awards_actions.download_from_ftp_to_sut({"vault.zip":DEFAULT_IC_FILE_DESTINATION})
    utils.send_keys(r"copy vault.zip c:\strsystm\comm[enter]")
    utils.send_keys("Y [enter]")
    #utils.send_keys("vaultinst[enter]")
    end_action()
# Function Name: setup_WEBPOS
###
#Description: Configure WebPOs ethernet card from display
###
#Author: Smitha D
def setup_WEBPOS(params):
    start_action("setup WPOS")
    awards_actions.config_board()
    awards_actions.start_awards()
    end_action()

# Function Name: validate_section_in_ini_file
###
#Description: Get the section name and value s paramentes
#Validate all the values are as expected
###
#Author: Smitha D

def validate_section_in_ini_file(params):
    #

    start_action("Keyword: Validate %s section in INI files."%params["section name"],params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    # Remove some undesired stuff from the parameters to build the list of Values
    values = params.copy()
    values.pop("ini file path")

    values.pop("section name")

    if "validate_step" in values:
        values.pop("validate_step")

    # Run the action
    awards_validations.validate_section_in_ini_file(params["ini file path"], params["section name"],values)

    end_action()

# Function Name: validate_section_not_in_ini_file
###
#Description: Get the section name and values paramentes
#Validate all the values are not there in ini file
###
#Author: Smitha D

def validate_section_not_in_ini_file(params):
    start_action("Keyword: Validate %s section not in INI files."%params["section name"],params)

    # Workaround for TRAPI.exe meory leak
    awards_actions.reconnect()

    # Remove some undesired stuff from the parameters to build the list of Values
    values = params.copy()
    values.pop("ini file path")

    values.pop("section name")

    if "validate_step" in values:
        values.pop("validate_step")

    # Run the action
    awards_validations.validate_section_value_not_in_ini_file(params["ini file path"], params["section name"],values)

    end_action()


# Function Name: validate_processs_running
###
#Description: Run PS command and check process is runnning

###
#Author: Smitha D
def validate_processs_running(params):
    start_action("Validate %s process is running"%params["process name"],params)
    awards_actions.get_process_from_ps_command(params["process name"])
    end_action()

def run_POS_command(params):
    start_action("Start POS app",params)
    utils.send_keys(r"cd C:\POS[enter]")
    utils.send_keys("StartPOSSimulator.cmd[enter]")
    time.sleep(400)



# Function Name: WEBPOS_Upgrade
###
#Description: Copy firstUPd and webpos.zip from build folder to award machine
#Run batchname firstupd force on award machine
# Check WEBPOS is running ater upgrade

###
#Author: Smitha D
def WEBPOS_Upgrade(params):
    start_action("Update WEBPOS ",params)

    awards_navigation.to_new_dos_session()
    expected_version = awards_actions.download_daily_build_from_server(params,params["build date"], params["build folder"])
    #expected_version = awards_actions.download_daily_build_from_server(params,params["build date"], params["build folder"])

    awards_actions.download_from_ftp_to_sut({ms.machine_name +"\\"+ DEFAULT_UPDATE_FILENAME:DEFAULT_UPDATE_FILEPATH})

    awards_navigation.to_new_dos_session()
    utils.send_keys(r"copy %s %s\%s [enter]"%(DEFAULT_UPDATE_FILENAME,DEFAULT_IC_FILE_DESTINATION,DEFAULT_UPD_FILENAME))
    time.sleep(50)

    params["version"]="strsystm"
    expected_version = awards_actions.download_daily_build_from_server(params,params["build date"], params["build folder"])
    awards_actions.download_from_ftp_to_sut({ms.machine_name +"\\"+ DEFAULT_STR_FILENAME:DEFAULT_UPDATE_FILEPATH})
    utils.send_keys(r"copy %s %s\%s [enter]"%(DEFAULT_STR_FILENAME,DEFAULT_IC_FILE_DESTINATION,DEFAULT_STR_FILENAME))

    utils.send_keys("Exit[enter]")
    awards_navigation.to_multitastking_store_system()
    utils.send_keys("Batchname firstupd force[enter]")
    time.sleep(60)
    utils.send_keys("restart[enter]")

    awards_actions.wait_for_no_connection(1800)
    awards_actions.wait_for_connection(1800)

    awards_actions.wait_for_no_connection(1800)
    awards_actions.wait_for_connection(1800)

    utils.send_keys("[ctrl!][esc!]")
    awards_navigation.to_multitastking_store_system()
    awards_validations.validate_system_is_running(1800)

    end_action()


# Function Name: WEBPOS_start_capture_log
###
#Description: Start Capture log by running command CAPT ALL
# Dump all the messages by running Dump ALL

###
#Author: Smitha D

def WEBPOS_start_capture_log(params):
    #
    start_action("Start capturing the logs",params)
    #
    awards_navigation.to_multitastking_store_system()
    #
    utils.send_keys("CAPT ALL[enter]")
    time.sleep(2)
    utils.send_keys("DUMP ALL[enter]")
    end_action()


#Function Name: WEBPOS_start_capt_debug_log
###
#Description: Start Capture debug log by running command CAPT On2
# Dump all the messages by running Dump ALL

###
#Author: Smitha D
def WEBPOS_start_capt_debug_log(params):
    start_action("Start capturing the logs",params)
    awards_navigation.to_multitastking_store_system()
    utils.send_keys("CAPT ON2[enter]")
    time.sleep(2)
    utils.send_keys("DUMP ALL[enter]")
    end_action()
#Function Name: WEBPOS_Stop_capture_log
###
#Description: Stop Capture log by running command CAPT OFF
###
#Author: Smitha D
def WEBPOS_Stop_capture_log(params):
    start_action("stop CAPT LOG",params)
    awards_navigation.to_multitastking_store_system()
    utils.send_keys("CAPT OFF[enter]")
    end_action()

#Function Name: WEBPOS_message_caputre_from_screen
###
#Description: Capture all the messages displayed on award screen
###
#Author: Smitha D
def WEBPOS_message_caputre_from_screen(params):
    start_action("Capture messages from AWARDS screen",params)
    awards_navigation.to_multitastking_store_system()
    if "messages" not in params or params["messages"] =="":
        params["messages"]=["Start order","Weighted item","Item description","Item Codes","Priced item","Department purchase","Order Total","Order tender","Customer identification","Order cancel","Sign on","Sign off","Item price adjustment","Coupon Reward","End of order"]

    awards_validations.validate_messages_CAPT_LOG(params["messages"])
    end_action()



#Function Name: validate_WEBPOS_Log
###
#Description: Validate all the messages displayed in the log are in proper order
###
#Author: Smitha D
def validate_WEBPOS_Log(params):
    start_action("Verify log messages")
    awards_validations.validate_order_CAPT_LOG()
    end_action()

#Function Name: validate_messages_in_log
###
#Description: Validate messages displayed on the screen are logged in CAPT log
###
#Author: Smitha D

def validate_messages_in_log(params):
    #
    start_action("Veriy messages in log",params)
    if "messages" not in params or params["messages"] =="":
        params["messages"]=["Start order","Weighted item","Item description","Item Codes","Priced item","Department purchase","Order total","Order tender","Customer identification","Order cancel","Sign on","Sign off","Item price adjustment","Coupon Reward","End of order"]
    #
    awards_validations.validate_messages_CAPT_LOG(params["messages"])
    end_action()


#Function Name: validate_messages_in_log
###
#Description: Validate messages are not in CAPT log when WEBPOS is off
###
#Author: Smitha D


def validate_messages_not_in_log(params):
    #
    start_action("Veriy messages not in log",params)
    #pdb.set_trace()
    if "messages" not in params or params["messages"] =="":
        params["messages"]=["Start order","Weighted item","Item description","Item Codes","Priced item","Department purchase","Order total","Order tender","Customer identification","Order cancel","Sign on","Sign off","Item price adjustment","Coupon Reward","End of order"]
    #
    #pdb.set_trace()
    awards_validations.validate_messages_Not_in_CAPT_LOG(params["messages"])
    end_action()
#Function Name: start_POS_APP
###
#Description: Start POS APP from local machine
###
#Author: Smitha D

def start_POS_APP(params):
    start_action("Start POSSimulator APP",params)
    import subprocess
    pos_file=r"C:\POS_%s\PosSimulator\StartPosSimulator.cmd"%ms.machine_name
    subprocess.call([pos_file])
    time.sleep(60)
    end_action()


#Function Name: get_WEBPOS_from_screen
###
#Description: Press F1 on award machine and check WEBPOS is displayed
# when WEBPOS is enabled in store.ini
###
#Author: Smitha D
def get_WEBPOS_from_screen(params):
    start_action("WEBPOS is displayed on Screen",params)
    awards_actions.capture_WEBPOS()
    end_action()


#Function Name: compare_log_files
###
#Description:Compare 2 logfiles
###
#Author: Smitha D
def compare_log_files(params):
    start_action("Compare log files",params)
    local_temp_path = "%s_%s" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    print(local_temp_path)

    awards_utils.prepare_sut_temp_directory(local_temp_path)

    awards_actions.copy_file_from_SUT_local()
    file1=local_temp_path+"\\ALL.CAP"
    file2=r"C:\POS\PosSimulator\PosMessage.log"
    awards_validations.validate_log_files(file1,file2)
    end_action()

#Function Name: kill_process
###
#Description:Cskill the process
###
#Author: Smitha D


def kill_process(params):
    start_action("Kill the procee %s"%params["process name"],params)
    awards_actions.kill_process(params["process name"])
    end_action()

#Function Name: add_firewall_exception
##
## Description: Add Firewall exception
##
##Autor Smitha D
def add_firewall_exception(params):
    start_action("Add Firewall exception",params)
    awards_actions.rest_firewall_setup()
    awards_actions.set_firewall_setup(params["protocol"])
    end_action()
#Function_naeme: def validate_trans_applog()
#
## descriptin: Validate app log for transaction ID

#Author: Smitha D
def validate_trans_applog(params):
    start_action("Validate app log for transaction ID in orders",params)
    awards_navigation.to_multitastking_store_system()
    awards_actions.shutdown_awards()
    awards_navigation.to_new_dos_session()
    awards_actions.generate_app_log_file("WEBPOS.txt")
    awards_validations.validate_trans_applog(r"c:\strsystm\Log\WEBPOS.txt")
    end_action()

#Function_naeme: validate_POS_logfile
#
## descriptin: Vallidate POS logfile for the message

#Author: Smitha D
def validate_POS_logfile(params):
    start_action("Validate POS Simulator log file ",params)
    awards_validations.validate_messges_POS_Simulator_log(params["filename"],parms["message"])
    end_ation()

#Function Name: TS_department_purchase
#
#Description: Run department purchase commands from TS
#
#Author: Smitha D

def TS_department_purchase(params):
    Start_action("Run depatrment purchase commands from TS",params)
    awards_actons.run_ts_with_inc_file(params["filename"])
    end_action()

#Function Name: compare_transid
#
#Description: Compare all transaction IDs in POS log are there in CAPT log
#
#Author: Smitha D


def compare_transid(params):
    start_action("Comapre transaction id in POS log and CAPTlog",params)
    tranid=awards_actions.get_tranid_from_Simulatorlog(params["filename"])
    awards_validations.validate_tranid_captlog(tranid)
    #print(tranid)
    end_action()

#Function Name: WEBPOS_get_active_status
#
#DEscription: Send command L to the awards screen to get the WEBPOS acive status
#
#Author: Smitha D

def WEBPOS_get_active_status(params):
    start_action("Send Command L to award screen and get WEBPOS active",params)
    awards_navigation.to_multitastking_store_system()
    utils.send_keys("%s[enter]"%params["command"])
    end_time=et(REBOOT_INIT_TIME)
    while not utils.find_text_in_console("WEBPOS ACTIVE",True) and ct() < end_time:
        time.sleep(3)
    if ct()>end_time:
        fail_sub_step("[WEBPOS active] Could not find 'WEBPOS ACTIVE' on screen after %d seconds." % (REBOOT_INIT_TIME))
        utils.take_screenshot()
        utils.finish_execution()
    else:
        pass_sub_step("[WEBPOS active] WEBPOS ACTIVE is found on screen ")
    end_action()
#Function Name: WEBPOS_get_inactive_status
#
#DEscription: Send command L to the awards screen to get the WEBPOS inacive status
#
#Author: Smitha D

def WEBPOS_get_inactive_status(params):
    start_action("Send Command L to award screen and get WEBPOS Inactive",params)
    awards_navigation.to_multitastking_store_system()
    utils.send_keys("%s[enter]"%params["command"])
    while not utils.find_text_in_console("WEBPOS INACTIVE",True) and ct() < end_time:
        time.sleep(3)
    if ct()>end_time:
        fail_sub_step("[WEBPOS active] Could not find 'WEBPOS INACTIVE' on screen after %d seconds." % (REBOOT_INIT_TIME))
        utils.take_screenshot()
        utils.finish_execution()
    else:
        pass_sub_step("[WEBPOS active] WEBPOS INACTIVE is found on screen ")
    end_action()
    end_action()

####################################################################
#Function Name: remove_webpos_board
######################################################
#Description: Delete WEBPOS board
######################################################
#Author: Smitha D
####################################################################

def remove_webpos_board(params):
    stat_acion("Remove the WEBPOS board",params)
    awards_navigation.to_multitastking_store_system()
    awards_actions.shutdown_awards()
    awards_actions.config_WEBPOSboard()
    end_action()

####################################################################
#Function Name: No_error_in_errorlog
######################################################
#Description: Check eroors are not there in errorlog
#######################################################
#Author: Smitha D
####################################################################

def no_error_in_errorlog(params):
    start_action("Validate errors are not there in Error",params)
    awards_validations.validate_Eror_in_Errorlog()
    end_action()

#############################################################
# Funtion Name:validate_config_json
# DEscription: validate message in config.josn
#Author Smitha D
#####################################################################
def validate_config_json(params):
    start_action("Validate configuration in config.json file",params)
    awards.validations.validate_message_config(params["messages"])
    end_action()

############################################################
# Funtion Name:validate_webpos_log
# DEscription: validate message in webpos.log
#Author Smitha D
#####################################################################
def validate_webpos_log(params):
    start_action("Validate configuration in config.json file",params)
    awards.validations.validate_message_in_webposlog(params["messages"])
    end_action()
############################################################
# Funtion Name:validate_webpos_log_neg
# DEscription: validate message not  in webpos.log
#Author Smitha D
#####################################################################
def validate_webpos_log_neg(params):
    start_action("Validate configuration in config.json file",params)
    awards.validations.validate_message_not_in_webposlog(params["messages"])
    end_action()
############################################################
# Funtion Name:run_batch_prepcds
# DEscription: run prepcds batchfile
#Author Smitha D
#####################################################################
def run_batch_prepcds(params):
    start_action("Copy DB file")
    awards_actions.load_cds_database(params["database name"])
    awards_ctions.create_force_load_job()
    awards_actions.force_batch_run("prepcdszip")
    time.sleep(900)
    if tu(params["cds enabled?"]) == "YES":
        awards_validations.validate_cds_database_load()
    else:
        awards_validations.validate_clus( params["expected clu"] )

    end_action()


############################################################
# Funtion Name:upgrade_firstupd
# DEscription: upgrade Awards using firstupd
#Author Smitha D
#####################################################################
def upgrade_firstupd(params):
    start_action("Update DB using firstup file",params)
    expected_version = awards_actions.download_daily_build_from_server(params,params["build date"], params["build folder"])
    #expected_version = awards_actions.download_daily_build_from_server(params,params["build date"], params["build folder"])

    awards_actions.download_from_ftp_to_sut({ms.machine_name +"\\"+ DEFAULT_UPDATE_FILENAME:DEFAULT_UPDATE_FILEPATH})

    awards_navigation.to_new_dos_session()
    utils.send_keys(r"copy %s %s\%s [enter]"%(DEFAULT_UPDATE_FILENAME,DEFAULT_IC_FILE_DESTINATION,DEFAULT_UPD_FILENAME))
    time.sleep(50)

    params["version"]="strsystm"
    expected_version = awards_actions.download_daily_build_from_server(params,params["build date"], params["build folder"])
    awards_actions.download_from_ftp_to_sut({ms.machine_name +"\\"+ DEFAULT_STR_FILENAME:DEFAULT_UPDATE_FILEPATH})
    utils.send_keys(r"copy %s %s\%s [enter]"%(DEFAULT_STR_FILENAME,DEFAULT_IC_FILE_DESTINATION,DEFAULT_STR_FILENAME))

    utils.send_keys("Exit[enter]")
    awards_navigation.to_multitastking_store_system()
    utils.send_keys("Batchname firstupd force[enter]")
    time.sleep(60)
    utils.send_keys("restart[enter]")

    awards_actions.wait_for_no_connection(1800)
    awards_actions.wait_for_connection(1800)

    awards_actions.wait_for_no_connection(1800)
    awards_actions.wait_for_connection(1800)

    utils.send_keys("[ctrl!][esc!]")

    awards_navigation.to_multitastking_store_system()
    awards_validations.validate_system_is_running(1800)

    end_action()
##############################################################################
#Function Name:nightly_refresh_db
#Description: wait for nightly update CDS load
#Author: Smitha D
##############################################################################


def nightly_refresh_db(params):
    start_action("Wait fr nighty refresh to load DB")


    awards_actions.load_cds_database(params["database name"])
    awards_navigation.to_new_dos_session()
    awards_actions. set_time(datetime.datetime.strptime("11:45","%H:%M"))
    time.sleep(1800)
    if tu(params["cds enabled?"]) == "YES":
        awards_validations.validate_cds_database_load()
    else:
        awards_validations.validate_clus( params["expected clu"] )

    end_action()

##############################################################################
#Function Name:validate_cds_dblog
#Description: wait for nightly update CDS load
#Author: Smitha D
##############################################################################
def validate_cds_dblog(params):
    start_action("Validate messages in CDS forceload log file")
    utils.send_keys("cd strsystm\log[enter]")
    utils.sendkeys("copy CDSforceloadstatus-??_??_????.log cdslog.txt[enter]")
    awards_validations.validate_messages_Not_in_CAPT_LOG(params["messages"],r"c:\strsystm\log\cdslog.txt")
    end_action()
######################################################################
#Function Name: webpos_Repay
# Description: run repay in award screen
# Author Smitha D
##################################################################
def WEBPOS_repay(params):
    start_action("Repay file to get all WEBPOS messages on screen")
    awards_navigation.to_multitastking_store_system()
    utils.send_keys("repay %s"%params["filename"])
    end_action()

def run_batch_prepcds(params):
    start_action("Copy DB file")
    awards_actions.load_cds_database(params["database name"])
    end_action()
#Function Name: upgrade_vault
#
#Description:
# Copy vault.zip in c:\strsystm\command
#restart awards
#
#
#Author: Smitha D

def upgrade_vault(params):
    start_action("Keyword: Upgrade vault",params)

    awards_actions.upload_from_local_to_ftp({"vaultupd.zip":DEFAULT_VAULT_BUILDPATH})
    #
    awards_navigation.to_running_groups_screen()
    awards_navigation.to_new_dos_session()

    awards_actions.download_from_ftp_to_sut({"vaultupd.zip":DEFAULT_IC_FILE_DESTINATION})
    utils.send_keys(r"copy vault.zip c:\strsystm\comm[enter]")
    time.sleep(1)
    utils.send_keys("exit[enter]")
    utils.send_keys("Restart[enter]")
#Function Name: vault_nightly_update
#
#Description:
# Copy vault.zip in c:\strsystm\command
#Wait till 12:00 clock to automatically install vault
#
#
#Author: Smitha D

def vault_nightly_update(params):
    start_action("Keyword: Upgrade vault",params)

    awards_actions.upload_from_local_to_ftp({"vault.zip":DEFAULT_VAULT_BUILDPATH})
    #
    awards_navigation.to_running_groups_screen()
    awards_navigation.to_new_dos_session()

    awards_actions.download_from_ftp_to_sut({"vault.zip":DEFAULT_IC_FILE_DESTINATION})
    awards_actions. set_time(datetime.datetime.strptime("11:45","%H:%M"))

    utils.send_keys(r"copy vault.zip c:\strsystm\comm[enter]")
    utils.send_keys("exit[enter]")
    time.sleep(1800)
    time.sleep(1)

    awards_navigation.to_running_groups_screen()
#Function Name: vault_nightly_update
#
#Description:
# Copy vault.zip in c:\strsystm\command
#Wait till 12:00 clock to automatically install vault
#
#
#Author: Smitha D

def create_new_lock_files(params):
    start_action("Create new locak files",params)
    awards_actions.create_lock_file(parnms["CDS Lock"],params["CDS LOCK FILEPATH"])
    awards_actions.create_force_create_lock_file(parnms["TRANSFERLOCK"],params["TRANSFERLOCK FILE PATH"])
    end_action()
#Function Name: copy_database
#
#Description:
# Copy DB file to in c:\strsystm\comm
#
#
#
#Author: Smitha D
def copy_database(params):
    start_action("copy databse %s"%params["databasename"],params)
    utils.send_keys(r"copy %s\%s %s"%(DEFAULT_AWARDS_CDS_DATABASE_PATH,params["databasename"],DEFAULT_IC_FILE_DESTINATION))
    utils.wait_for_text_in_console(r"file(s) copied", False, DEFAULT_TIMEOUT)
    end_action()
#Function Name: add_lane_display
#
#Description:
# Add Lane to display
#
#
#
#Author: Smitha D

def add_lane_display(params):
    start_action("Add new lane via display",params)
    #
    end_time = et(REBOOT_INIT_TIME)
    awards_actions.shutdown_awards()
    #
    if "lane max" not in params or params["lane max"]=="":
        params["lane max"]=False

    awards_actions.add_lane_display(params["printer type"],params["lane num"],params["term id"],params["lane max"])
    awards_actions.start_awards()
    utils.send_keys("[F3]")
    while not utils.find_text_in_console("(1 *Ready)",True) and ct() < end_time:
        time.sleep(3)
    end_action()
#Function Name:delete_lane_display
#Descrption: Delete lane from display
#Author: Smitha D
def del_lane_display(parms):
    start_action("Add new lane via display",params)
    end_time = et(REBOOT_INIT_TIME)
    awards_actions.shutdown_awards()
    awards_actions.delete_lane_display()
    awards_actions.start_awards()
    utils.send_keys("[F3]")
    while not utils.find_text_in_console("(No Response*)",True) and ct() < end_time:
        time.sleep(3)
    end_action()
#Functon  Name: get_mac_address
#Description: Get the mac Address of the machine
#Author: Smitha D
def get_mac_address():
    #start_action("Get MAC Address")
    awards_navigation.to_new_dos_session()
    utils.send_keys("getmac[enter]")
    time.sleep(2)
    text_console=utils.get_console_text()
    mac_add= utils.match_text_in_console(r"[\d,A-z,a-z][\d,A-z,a-z]-[\d,A-z,a-z][\d,A-z,a-z]-[\d,A-z,a-z][\d,A-z,a-z]+.*","Yes")[0].split(" ")[0]
    return mac_add

    #end_action()
#Function Name:get_pritnter_types_from_dispaly
#Descrption: Get printer types from the display mode
#Author: Smitha D
def get_pritnter_types_from_dispaly(params):
    start_action("Get all the printer types from display",params)
    #awards_actions.shutdown_awards()
    awards_actions.get_printertype_display(params["printers"],params["lanenum"],params["termid"])
    awrads_actions.capture_WEBPOS_messages(params["printers"])
    awards_actions.start_awards()
    end_action()


#Function Name:get_pritnter_types_from_preload
#Descrption: Get printer types from the preload - Newstore
#Author: Smitha D
def get_pritnter_types_from_preload(params):
    start_action("Get all the printer types from display",params)
    awards_actions.shutdown_awards()
    awards_actions.get_printertype_preload(params["printers"])
    awards_actions.start_awards()
    end_action()

#Function Name:capture_driver_version
#Descrption: capture the driver vesion from
#Author: Smitha D
def capture_driver_version(parms):
    start_action("Get the printer driver verson ",params)
    awards_actions.capture_driver_version()
    end_action()
#Function Name:capture_lane_mapping_scren
#Descrption: capture the Lane mapping from display
#Author: Smitha D
def capture_lane_mapping_scren(params):
    start_action("Get info of the lane mapping Screen",params)
    awards_actions.shutdown_awards()
    awards_actions.get_lane_mapping_screen_info(params["lane info"])
    awards_actions.start_awards()
    ens_action()

#Function_name :Copy_CMC9
#Description: Copy CMC9 file to comm folder
#Author: Smitha D

def copy_zip_to_comm(params):
    start_action("Keyword:Copy zip file to comm folder",params)
    awards_actions.shutdown_awards()

    awards_actions.upload_from_local_to_ftp({params["file name"]:params["file path"]})
    #
    awards_navigation.to_running_groups_screen()
    awards_navigation.to_new_dos_session()

    awards_actions.download_from_ftp_to_sut({params["file name"]:DEFAULT_IC_FILE_DESTINATION})
    utils.send_keys(r"copy %s c:\strsystm\comm[enter]"%params["file name"])
    time.sleep(1)
    utils.send_keys("exit[enter]")
    awards_navigation.to_multitastking_store_system()

    awards_actions.start_awards()



#Function name: validaate_firewall_port_open
#Descrption:Verify  firewall ports are opened
#Author : Smitha D

def validate_firewall_port_opened(params):
    start_action("Verify ports are opened ",params)
    #pdb.set_trace()
    awards_actions.show_firewal_port_opened(params["firewall port"])
    end_action()
#Function:copy ini from copy_ini_from_comm
#Author: Smitha D
def copy_ini_from_comm(params):
    start_action("Copy store.ini file")
    awards_navigation.to_multitastking_store_system()
    awards_actions.shutdown_awards()
    awards_navigation.to_new_dos_session()
    utils.send_keys(r"copy c:\strsystm\comm\a\store.ini c:\strsystm\data[enter]")
    time.sleep(2)
    utils.send_keys("y[enter]")
    awards_navigation.to_multitastking_store_system()
    awards_actions.start_awards()

#Function_name: newstore_setup()
#Description: configure Store,controller,Board and printer
#Author: Smitha D

def newstore_setup(params):
    start_action("Start newstore setup",params)
    #pdb.set_trace()
    awards_actions.shutdown_awards()
    awards_actions.run_newstore(params["country"],params["printer"],params["locale"],display="yes",printer_type=params["printer_type"])
    awards_acions.start_awards()
    end_action()
#Function_name: validate_memory_utils
#Description: Run command Status system to get CPU and virtual memory utilazation
#Author: Smitha D

def validate_memory_utils(params):
    start_action("Validate memory utils",params)
    awards_navigation.to_multitastking_store_system()
    system_memory=awards_actions.get_system_details()
    if int(system_memory) > 80:
        #pdb.set_trace()
        awards_actions.generate_app_log_file("app_log.txt")
        awards_validations.verify_swapfile_error(filename=r"app_log.txt",Error_msg=params["Error_msg"],copy_text="Yes")
        fail_step("Memory utilization is more than 80%")
    pass_step("Memory utilization is stable")


# Copy data to text file
#Function Name: validate_zapperfile_Errorlog
#Description: Verify zapperfile error message is there on the Error log file
#Copy the error message to text file .
#Author: Smitha D

def validate_swapfile_Applog(params):
    start_action("Get the Swapfile error message from  applog",params)
    awards_navigation.to_new_dos_session()
    awards_actions.generate_app_log_file("app_log.txt")
    awards_validations.verify_swapfile_error(filename="app_log.txt",Error_msg=params["Error_msg"],copy_text="Yes")
#Function  Name: install_latest_vault()
#Description:Install latest vault build
#Author: Smitha D

def install_latest_vault(params):
    start_action("Get the latest build and install",params)
    awards_actions.download_latest_vault(params)

    end_action()
