from cryptography.fernet import Fernet
import base64

__something = 'A0uGhrtn7xUXrzxx_9b6GwZpYIzPeCmCvxJ4dcgYcI4='

def encrypt(value):
	global __something

	if value.__class__ != "<class 'bytes'>":
		value = bytes(value, "utf-8")

	e = Fernet(__something)
	token = e.encrypt(value)
	return bytes.decode(token)

def decrypt(value):
	global __something

	if value.__class__ != "<class 'bytes'>":
		value = bytes(value, "utf-8")

	e = Fernet(__something)
	value = e.decrypt(value)
	return bytes.decode(value)
