table_html  =   """
                <div class="row">
                    <h3>[TABLENAME]</h3>
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Entry</th>
                            <th>Value</th>
                        </tr>
                        [ROWHTML]
                    </table>
                </div>
                """

row_html =      """
                <tr>
                    <td>[SEQ]</td>
                    <td>[ENTRY]</td>
                    <td>[VALUE]</td>
                </tr>
                """

iteration_html = """
                 <div class='row'>
		   		 	<div class='panel panel-primary'>
	             		<div class='panel-heading'><strong>Iteration [ITERATION] - Scenario Details</strong></div>
	             		<div class='panel-body'>
                 			[SCENARIO]
                 		</div>
                 	</div>
                 </div>
                 <div class='row'>
                 	<ul class='list-unstyled'>
                 		[CONTENT]
                 	</ul>
                 </div>
                 """

passed_sub_step_html =  """
                        <li>
                        	<div class='panel panel-default'>
                        		<div class='panel-body'>
                        			<span class='badge'>SubStep [STEPNUM]</span>
                        			[STEPDESC]
                        			<span class='badge pull-right'><span class='glyphicon glyphicon-ok'></span> <span class='substep-status'>Passed</span></span>
                        		</div>
                        		[FOOTERAREA]
                        	</div>
                        </li>
                        """

failed_sub_step_html =  """
                        <li>
                        	<div class='panel panel-danger fail-substep'>
                        		<div class='panel-body'>
                        			<span class='badge'>SubStep [STEPNUM]</span>
                        			[STEPDESC]
                        			<span class='badge pull-right fail-badge'><span class='glyphicon glyphicon-remove'></span> <span class='substep-status'>Failed</span></span>
                        		</div>
                        		[FOOTERAREA]
                        	</div>
                        </li>
                        """

blocked_sub_step_html =  """
                        <li>
                        	<div class='panel panel-danger block-substep'>
                        		<div class='panel-body'>
                        			<span class='badge'>SubStep [STEPNUM]</span>
                        			[STEPDESC]
                        			<span class='badge pull-right block-badge'><span class='glyphicon glyphicon-minus'></span> <span class='substep-status'>Blocked</span></span>
                        		</div>
                        		[FOOTERAREA]
                        	</div>
                        </li>
                        """
html_panel_footer = """		<div class='panel-footer'>[CONTENT]</div>"""

passed_panel_html = """
                    <li>
                    	<div class='panel panel-success results-panel'>
                    		<div class='panel-heading'>
                    			<span class='btn btn-success btn-xs btn-toggle'><span class='glyphicon glyphicon-triangle-[ARROWTYPE]'></span></span>
                    			&nbsp;&nbsp;&nbsp;
                    			<span class='badge'>[STEPTYPE] [STEPNUM]</span>
                    			<strong>[STEPDESC]</strong>
                    			<span class='badge pull-right'><span class='glyphicon glyphicon-ok'></span> <span class='step-status'>Passed</span></span>
                    		</div>
                    		<div class='panel-body [HIDETAG] hideable-panel'>
                    			<ul class='list-unstyled'>
                    				[CONTENT]
                    			</ul>
                    		</div>
                    		[FOOTERAREA]
                    	</div>
                    </li>
                    """

failed_panel_html = """
                    <li>
                    	<div class='panel panel-danger results-panel'>
                    		<div class='panel-heading'>
                    			<span class='btn btn-danger btn-xs btn-toggle'><span class='glyphicon glyphicon-triangle-[ARROWTYPE]'></span></span>
                    			&nbsp;&nbsp;&nbsp;
                    			<span class='badge'>[STEPTYPE] [STEPNUM]</span>
                    			<strong>[STEPDESC]</strong>
                    			<span class='badge pull-right'><span class='glyphicon glyphicon-remove'></span> <span class='step-status'>Failed</span></span>
                    		</div>
                    		<div class='panel-body [HIDETAG] hideable-panel'>
                    			<ul class='list-unstyled'>
                    				[CONTENT]
                    			</ul>
                    		</div>
                    		[FOOTERAREA]
                    	</div>
                    </li>
                    """

blocked_panel_html = """
                    <li>
                    	<div class='panel panel-warning results-panel'>
                    		<div class='panel-heading'>
                    			<span class='btn btn-danger btn-xs btn-toggle'><span class='glyphicon glyphicon-triangle-[ARROWTYPE]'></span></span>
                    			&nbsp;&nbsp;&nbsp;
                    			<span class='badge'>[STEPTYPE] [STEPNUM]</span>
                    			<strong>[STEPDESC]</strong>
                    			<span class='badge pull-right'><span class='glyphicon glyphicon-minus'></span> <span class='step-status'>Blocked</span></span>
                    		</div>
                    		<div class='panel-body [HIDETAG] hideable-panel'>
                    			<ul class='list-unstyled'>
                    				[CONTENT]
                    			</ul>
                    		</div>
                    		[FOOTERAREA]
                    	</div>
                    </li>
                    """
image_panel =   """
        		<div class='panel-footer'>
        			<p>
        				<span class='btn btn-warning btn-screenshot'>Show Screenshot</span>
        			</p>
        			<a href='[IMG]' target='_blank'><img class='error-screenshot hide' src='[IMG]'></a>
        		</div>
                """

console_screen_panel =      """
                    		<div class='panel-footer'>
                    			[SCR]
                    		</div>
                            """


passed_header_html = "<div class='alert alert-success' role='alert'><strong>Test Status:</strong> <span class='test-status'>Passed</span></div>"

failed_header_html = "<div class='alert alert-danger' role='alert'><strong>Test Status:</strong> <span class='test-status'>Failed</span></div>"
blocked_header_html = "<div class='alert alert-warning' role='alert'><strong>Test Status:</strong> <span class='test-status'>Blocked</span></div>"
