from framework.element import Element
from framework.utils import tu

import sys

# Class: ElementRepository
# Serves as a repository for the UI elements used by the automation.
# It loads an external file passed as parameter for the initializer.
# The file must have the following format:
# > element-name,type,description
#
# Example:
# > # Comments can be ignored
# > txt-Username,xpath,//input[@name="userName"]
# > txt-Password,xpath,//input[@name="password"]
# > btn-Login,xpath,//input[@name="login"]
#
# You can also use # for comments.
#
# Internally, the class stores a dictionary that will carry objects from
# the class Element. The file path will be loaded from the config file.
#
# Author:
# Danilo Guimaraes <danilo.guimares@catalinamarketing.com>
class ElementRepository:

	elements = {}

	# Contructor: ElementRepository
	# Read the repository file and store all the objects into a dictionary.
	# Each element is represented by an element from the class Element.
	#
	# Parameters:
	# - filename: name of the object repository file.
	def __init__(self, filename):

		# Load the file and read all the entries
		with open(filename) as f:

			for line in f:
				current_statement = line

				# Ignore if this is a comment and store the valid lines
				if current_statement[0] != "#" and tu(current_statement) != "":
					try:
						attrs = current_statement.split(",",2)
						self.elements[tu(attrs[0].strip())] = Element(attrs[0].strip(),attrs[1].strip(),attrs[2].strip())
					except:
						print("Error parsing the repository file:")
						print(current_statement)
						sys.exit(1)

	# Method: get_element
	# Returns an instance from the Element class containing the element
	# descript that matches the element name passed as parameter.
	#
	# Parameters:
	# - element_name: name of the element that will be retrieved from the repository.
	def get_element(self, element_name):
		return self.elements[tu(element_name)]
