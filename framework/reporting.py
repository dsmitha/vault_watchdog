from config import *
from framework.utils import *
#import awards_actions
from framework.reporting_html import *
from framework.utils import take_screenshot
from framework.awards.settings import machine_settings as ms
from datetime import datetime
from time import strftime
import os
import platform
import time
import sys
import pdb
import re

class ReportStep:

    def __init__(self, step_type, step_number, step_description, parent_step, screenshot_path):
        self.sub_step_list = []
        self.step_type = step_type
        self.step_number = step_number
        self.step_description = step_description
        self.parent_step = parent_step

        self.step_status = 'Passed'
        self.comment = ''
        self.screenshot = ''
        self.screenshot_path = screenshot_path
        self.screenshot_type = ""

    def new_step(self, step_type, step_description, screenshot_path):

        step_number = len(self.sub_step_list) + 1

        if step_type != 'Iteration':
            step_number = str(self.step_number) + "." + str(step_number)

        step = ReportStep(step_type, step_number, step_description, self, self.screenshot_path)
        self.sub_step_list.append(step)

        return step
    def get_parent_step(self):
        return self.parent_step

    def get_action_name(self,parent_step):
        ##
        if self.parent_step is not None:
            parent_step_type=self.parent_step.step_type
            while(self.parent_step.step_type!='Action'):
                #print(self.parent_step.step_type)
                self.close_step()
            #print (self.parent_step.step_description)
            ##
            if len(re.findall("A A A",self.parent_step.step_description))>0:

                return True
        return False


    def block_test(self,comment):
        ###
        self.step_status= 'Blocked'
        ###
        self.comment=comment
        if self.parent_step is not None:
            self.parent_step.block_test('Test case is blocked.')

    def pass_step(self, comment):
        self.step_status = 'Passed'
        self.comment = comment

    def fail_step(self, comment):

        ##
        self.step_status = 'Failed'
        self.comment = comment
        #self.take_screenshot()

        if self.parent_step is not None:
            if self.parent_step.step_type=='Action' :
                #print(self.parent_step.step_description)
                if len(re.findall("A A A",self.parent_step.step_description))>0 :
                    self.parent_step.block_test("Initialization Failed")
                else:
                    ##
                    self.parent_step.fail_step('Expand for more details.')
            else:
                ##
                self.parent_step.fail_step('Expand for more details.')


    def close_step(self):
        return self.parent_step

    def count_failed(self):

        count = 0

        if self.step_status == 'Failed' and self.step_type in ['Step']:
            count += 1

        if len(self.sub_step_list) > 0:
            for s in self.sub_step_list:
                count += s.count_failed()

        return count

    def count_blocked(self):
        ####
        count = 0

        if self.step_status == 'Blocked' and self.step_type in ['Step']:
            count += 1

        if len(self.sub_step_list) > 0:
            for s in self.sub_step_list:
                count += s.count_blocked()
        #print (count)
        ####
        return count

    def count_passed(self):

        count = 0

        if self.step_status == 'Passed' and self.step_type in ['Step']:
            count += 1

        if len(self.sub_step_list) > 0:
            for s in self.sub_step_list:
                count += s.count_passed()

        return count

    def get_html(self):
            #

            if self.step_type == 'SubStep':
                if self.step_status == 'Passed':
                    current_html = passed_sub_step_html
                elif self.step_status == 'Blocked':

                    current_html = blocked_sub_step_html

                else:
                    current_html = failed_sub_step_html

                if self.comment != '':
                    current_html = current_html.replace("[FOOTERAREA]", \
                                   html_panel_footer.replace("[CONTENT]", self.comment) + "[FOOTERAREA]")

                if self.screenshot != '':
                    if self.screenshot_type == "console":
                        current_html = current_html.replace("[FOOTERAREA]", console_screen_panel.replace("[SCR]", self.screenshot))
                    else:
                        current_html = current_html.replace("[FOOTERAREA]", image_panel.replace("[IMG]", self.screenshot))

                current_html = current_html.replace("[FOOTERAREA]", "")

                current_html = current_html.replace("[STEPNUM]", str(self.step_number))
                current_html = current_html.replace("[STEPDESC]", self.step_description)
                current_html = current_html.replace("[ERRORIMG]", "")

            elif self.step_type == 'Iteration':

                current_html = iteration_html
                current_html = current_html.replace("[ITERATION]", str(self.step_number))
                current_html = current_html.replace("[SCENARIO]", self.step_description)

            else:

                if self.step_status == 'Passed':
                    current_html = passed_panel_html
                elif self.step_status == "Blocked":

                    current_html = blocked_panel_html
                else:
                    current_html = failed_panel_html

                if self.comment == '':
                    current_html = current_html.replace("[FOOTERAREA]", "")
                else:
                    current_html = current_html.replace("[FOOTERAREA]",
                                                        html_panel_footer.replace("[CONTENT]", self.comment))

                current_html = current_html.replace("[ARROWTYPE]", "right")
                current_html = current_html.replace("[STEPTYPE]", self.step_type)
                current_html = current_html.replace("[STEPNUM]", str(self.step_number))
                current_html = current_html.replace("[STEPDESC]", self.step_description)
                current_html = current_html.replace("[HIDETAG]", "hide")

            sub_html = ''
            if len(self.sub_step_list) > 0:
                for s in self.sub_step_list:
                    sub_html = sub_html + s.get_html()

            current_html = current_html.replace("[CONTENT]", sub_html)

            return current_html

    def add_screenshot(self, value, scr_type):
        self.screenshot_type = scr_type
        self.screenshot = value

    def take_screenshot(self):
        path = self.screenshot_path + "\\" + str(self.step_number) + "_scr.png"

        if not os.path.exists(self.screenshot_path):
            os.makedirs(self.screenshot_path)

        take_screenshot(path)
        self.screenshot = path

class Report:

    def __init__(self):
        self.iteration_list = []
        self.relevant_data = {}
        self.header_info = {}
        self.current_step = None
        self.start_time = datetime.now()

        self.set_report_path()

    def set_report_path(self):

        file_path = report_path
        if not os.path.exists(file_path):
            os.makedirs(file_path)

        file_path = file_path + '\\' + os.getlogin()
        if not os.path.exists(file_path):
            os.makedirs(file_path)

        report_name = app_name + '_' + os.path.basename(sys.argv[0]).split(".")[0] + time.strftime("%Y-%m-%d_%H-%M-%S", datetime.now().timetuple())
        self.report_path = file_path
        self.screenshot_path = file_path + '\\' + report_name
        self.html_path = file_path + '\\' + report_name + ".html"

    def get_report_path(self):
        return self.html_path

    def new_step(self, step_type, step_description):

        step_number = len(self.iteration_list) + 1

        if self.current_step is None:
            self.current_step = ReportStep(step_type, step_number, step_description, None, self.screenshot_path)
            self.iteration_list.append(self.current_step)
        else:
            if self.current_step.step_type == 'SubStep':
                self.close_step()

            self.current_step = self.current_step.new_step(step_type, step_description, self.screenshot_path)

    def close_step(self):
        if self.current_step is not None:
            self.current_step = self.current_step.close_step()

    def add_iteration(self, description):
        self.close_iteration()
        self.new_step('Iteration', description)

    def close_iteration(self):
        while self.current_step is not None:
            self.close_step()

    def add_action(self, description):
        if self.current_step is None:
            self.new_step('Iteration', description)
        else:
            self.new_step('Action', description)

    def close_action(self):
        while (self.current_step is not None) and (self.current_step.step_type != 'Action') and (self.current_step.step_type != 'Iteration'):
            self.close_step()

        self.close_step()

    def add_step(self, description):
        if self.current_step is None:
            self.add_iteration(description)
        else:
            if self.current_step.step_type == 'SubStep':
                self.close_step()

            if self.current_step.step_type == 'Step':
                self.close_step()


        self.new_step('Step', description)

    def block_test(self,description):
        ##
        #print(self.current_step.step_description)

        if self.current_step is not None:
            ##
            if self.current_step.step_type == 'SubStep':
                self.close_step()

            if self.current_step.step_type == 'Step':
                ##
                self.add_sub_step(description)
                ##
                self.block_sub_step(description)

    def pass_step(self, description):
        if self.current_step is not None:

            if self.current_step.step_type == 'Step':
                self.current_step.pass_step(description)

            elif self.current_step.parent_step.step_type == 'Step':
                self.current_step.parent_step.pass_step(description)

    def fail_step(self, description):
        ##
        if self.current_step is not None:
            if self.current_step.step_type == 'SubStep':
                ##
                if self.current_step.parent_step.step_type=='Action':
                    ##
                    #print(self.current_step.parent_step.step_description)
                    if len(re.findall("A A A",self.current_step.parent_step.step_description))>0:
                        ##
                        self.close_step()
                    else:
                        self.close_step()
                        #self.block_test("Initialization Failed")


                else:
                    self.close_step()


            if self.current_step.step_type == 'Step':
                if self.current_step.parent_step.step_type=='Action':
                    #print(self.current_step.parent_step.step_description)
                    ##
                    if len(re.findall("A A A",self.current_step.parent_step.step_description))>0:
                        self.add_sub_step(description)
                        self.block_sub_step("Initialization Failed")
                    else:
                        #
                        self.add_sub_step(description)
                        self.fail_sub_step(description)

                else:
                    self.add_sub_step(description)
                    self.fail_sub_step(description)

    def add_sub_step(self, description):
        if self.current_step is None:
            self.add_step(description)
        else:
            if self.current_step.step_type == 'SubStep':
                self.close_step()

        self.new_step('SubStep', description)

    def fail_sub_step(self, description):
        #
        if self.current_step is not None:
            if self.current_step.step_type == 'SubStep':
                self.current_step.fail_step(description)

    def pass_sub_step(self, description):
        if self.current_step is not None:
            if self.current_step.step_type == 'SubStep':
                self.current_step.pass_step(description)

    def block_sub_step(self,description):

        if self.current_step is not None:
            ##
            if self.current_step.step_type == 'SubStep':
                ##
                self.current_step.block_test(description)

    def finish_report(self):
        if self.current_step is not None:

            if self.current_step.step_type == 'Iteration':
                while self.current_step is not None:
                    self.close_step()
            else:
               self.close_action()

    def finish_report_real(self):
        while self.current_step is not None:
            self.close_step()

    def generate_report_file(self):
        self.finish_report_real()

        report_file = open(self.get_report_path(), 'w')
        report_file.write(self.get_html())
        report_file.close()

    def get_overall_status(self):
        ###
        return 'Passed'
        for s in self.iteration_list:
            #print (s.step_status)
            ###
            if s.step_status == 'Blocked':
                return 'Blocked'
            elif s.step_status == 'Failed':
                return 'Failed'

        return 'Passed'

    def count_overall_step_status(self):
        self.count_passed = 0
        self.count_failed = 0
        self.count_blocked = 0

        for s in self.iteration_list:
            self.count_passed += s.count_passed()
            self.count_failed += s.count_failed()
            self.count_blocked+=s.count_blocked()

    def add_relevant_data(self, name, value):
        self.relevant_data[name] = value

    def add_header_info(self, name, value):
        self.header_info[name] = value

    def get_html(self):

        self.count_overall_step_status()
        test_status = self.get_overall_status()
        end_time = datetime.now()

        html = open(report_template).read()

        if test_status == 'Passed':
            html = html.replace("[STATUSHEADER]", passed_header_html)
        elif test_status == 'Blocked':

            html = html.replace("[STATUSHEADER]", blocked_header_html)
        else:
            html = html.replace("[STATUSHEADER]", failed_header_html)

        html = html.replace("[HEAD TITLE]", "Automated Test Execution at " + strftime('%Y/%m/%d - %H:%M:%S', self.start_time.timetuple()))
        html = html.replace("[TESTNAME]","ALM: " + sys.argv[0])
        html = html.replace("[TESTSTATUS]", test_status)
        html = html.replace("[EXEC START]", strftime('%Y/%m/%d - %H:%M:%S',self.start_time.timetuple()))
        html = html.replace("[EXEC END]", strftime('%Y/%m/%d - %H:%M:%S',end_time.timetuple()))
        html = html.replace("[EXEC TIME SEC]", '%d' % (end_time - self.start_time).total_seconds() )
        html = html.replace("[EXEC TIME MIN]", '%.02f' % ((end_time - self.start_time).total_seconds()/60) )
        html = html.replace("[APP NAME]", app_name)
        html = html.replace("[APP ENV]", app_environment)
        html = html.replace("[OS SYSTEM]", platform.system() + ' ' + platform.release())
        html = html.replace("[USERNAME]", os.getlogin())
        html = html.replace("[HOSTNAME]", ms.machine_name)
        html = html.replace("[PASSEDSTEPS]", str(self.count_passed))
        html = html.replace("[FAILEDSTEPS]", str(self.count_failed))
        html = html.replace("[REPORTPATH]", report_shared_lib_path)
        html=html.replace("[PLATFORMNAME]",str(ms.awards_printer_type))

        details_html = ''
        header = ''
        relevant_data_header = ''

        # Create the information header
        if len(self.header_info) > 0:
            header = table_html
            rows = ""

            for i, value in enumerate(self.header_info):
                row = row_html
                row = row.replace("[SEQ]", str(i+1))
                row = row.replace("[ENTRY]", str(value))
                row = row.replace("[VALUE]", str(self.header_info[value]))
                rows += row

            header = header.replace("[ROWHTML]", rows)
            header = header.replace("[TABLENAME]", "Header Information")

        # Create the relevant data header
        if len(self.relevant_data) > 0:
            relevant_data_header = table_html
            rows = ""

            for i, value in enumerate(self.relevant_data):
                row = row_html
                row = row.replace("[SEQ]", str(i+1))
                row = row.replace("[ENTRY]", str(value))
                row = row.replace("[VALUE]", str(self.relevant_data[value]))
                rows += row

            relevant_data_header = relevant_data_header.replace("[ROWHTML]", rows)
            relevant_data_header = relevant_data_header.replace("[TABLENAME]", "Relevant Data")

        for i in self.iteration_list:
            details_html += i.get_html()


        details_html = header + relevant_data_header + details_html

        html = html.replace("[DATACONTENT]", details_html)

        return html

    def add_screenshot(self, value, scr_type):
        if self.current_step is not None:
            if scr_type == "console":
                self.add_sub_step("Screenshot:")

            self.current_step.add_screenshot(value, scr_type)

################################################################################

def start_test(description):
    start_iteration(description)

def end_test():
    end_iteration()

def start_iteration(description):
    global report

    report.add_iteration(description)

def end_iteration():
    global report

    report.close_iteration()

def start_action(description,params=None):
    global report
    #

    if params == None or "validate_step" not in params or params["validate_step"]=="" :

            validate_step="No"
    else:
        print (params["validate_step"])
        #
        validate_step=params["validate_step"]
        update_stepinfo_in_config(validate_step)

    report.add_action(description)
    #awards_navigation.to_running_groups_screen()
def end_action():
    global report

    report.close_action()

def start_step(description):
    global report

    report.add_step(description)

def fail_step(description):
    global report

    report.fail_step(description)
def block_test(description):
    global report
    ###
    report.block_test(description)

def pass_step(description):
    global report

    report.pass_step(description)

def add_sub_step(description):
    global report

    report.add_sub_step(description)
def block_sub_step(description):
    global report

    report.block_sub_step(description)
def fail_sub_step(description):
    global report

    report.fail_sub_step(description)

def add_relevant_data(name, value):
    global report

    report.add_relevant_data(name, value)

def add_header_info(name, value):
    global report

    report.add_header_info(name, value)
################################################################################
report = None

if report is None:
    report = Report()
def update_stepinfo_in_config(validate_step,remove_var="NO"):
    config_file_path = "%s\%s" % (base_path, "config.py")
    print (config_file_path)
    print (remove_var)
    #pdb.set_trace()

    if tu(remove_var)=="YES":
        fh=open(config_file_path,"r+")
        lines=fh.readlines()
        for i in range(len(lines)):
            varname="validate_step_%s"%ms.machine_name
            print (varname )
            #pdb.set_trace()
            if len(re.findall(varname,lines[i]))>0:
                lines[i]=""
        fh.close()
        fhw=open(config_file_path,"w+")
        for line in lines:
            fhw.write(line)
        fhw.close()
    else:
        fh=open(config_file_path,"a")
        fh.write('validate_step_%s="%s"\n'%( ms.machine_name,validate_step))
        fh.close()
