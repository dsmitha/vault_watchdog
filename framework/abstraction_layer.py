# Title: Abstraction layer
# Core library for the Abstraction Layer framework. It manages everything
# related to the framework itself. It needs to be imported by all tests that use
# the abstraction layer.
# Import needs to be done as follows:
# > from framework.abstraction_layer import *

# Framework imports
from config import *
from framework.utils import *
from framework.element_repository import ElementRepository
from framework.reporting import *
from framework.alm import ALMConnection
from framework.messages import msg_report
import framework.encryption as e

# Selenium imports
from selenium import webdriver
from selenium.common.exceptions import *
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ExpectedConditions
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from selenium.common.exceptions import NoSuchElementException

# Python lib imports
import time
import atexit
import sys
from datetime import datetime
from datetime import timedelta

# File-Scope variables
repository = ''
current_url = ''

max_try = 5
framework_default_timeout = 60
default_timeout = framework_default_timeout

driver = None
report = None

################################################################################
# Section: Core Funtions
# Those are the core abstraction layer functions. They handle the inner workings
# of the framework. Usually not needed while writing test scripts.
################################################################################

# Function: initialize_abs_layer
# Loads the repository and make the initial setup.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def initialize_abs_layer():

	global driver
	global repository
	global report

	# Load the repository
	repository = ElementRepository(element_repository_path)

	# Load the webdriver
	# For IE, we need to add the executable path to the system path so the
	# webdriver can work properly.
	if tu(browser_type) == 'IE':
		os.environ['PATH'] += ';' + base_path + "\\framework\\bin\\"
		driver = webdriver.Ie()
	elif tu(browser_type) == 'CHROME':
		driver = webdriver.Chrome(base_path + '\\framework\\bin\\chromedriver.exe')
	elif tu(browser_type) == 'FIREFOX':
		driver = webdriver.Firefox()

	# Initialize the report
	report = get_report()

	# Register the function to close the webdriver at the end of the execution
	atexit.register(finalize_abs_layer)

# Function: finalize_abs_layer
# Finishes the test report and also closes the webdriver (if needed)
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def finalize_abs_layer():
	if not remain_open:
		driver.quit()
		print('Driver finished.')

	report.generate_report_file()

	if alm_post_results:
		update_alm()

# Function: update_alm
# Updates ALM with the test execution information and also with the test report.
# Requires the win32com module to work. You can find it at sourceforge.net/projects/pywin32/
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def update_alm():
	if len(sys.argv) > 1:
		test_set_id = sys.argv[1]
		test_name = sys.argv[2]
		test_run = sys.argv[3]
		test_instance = sys.argv[4]

		almc = ALMConnection(alm_server_path, alm_domain, alm_project, alm_username, alm_password)
		almc.update_test_instance(test_set_id, test_name, report.get_overall_status(), test_run, test_instance, report.get_report_path())
		almc.close_connection()

# Function: get_element_from_page
# Get an element from the current page, consists if it exits and try again on
# error. It will try as many times as max_try is defined. It is a recursive
# function, so it receives the amount of times it has tried to find the element
# so we can have a stop condition for it.
# In case the parent_element_name is provided, it will look for the element
# as being a child of the parent_element. This argument is optional, though.
#
# Paramaters:
# - element_name: name of the element that needs to be found in the screen.
# - try_count: how many times it has been tried to find the element so far.
# - parent_element_name: (optional) name of the parent element in case you need
#   to use a nested element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def get_element_from_page(element_name, elapsed_secs, parent_element_name = None):

	global driver

	# Set the initial execution time for this call and also get the element
	start_time = ct()
	parent_element = None
	element_obj = None

	element = repository.get_element(element_name)

	# If we have a parent element specified, also get this element from the repo
	if parent_element_name is not None:
		parent_element = repository.get_element(parent_element_name)

	# Fail if the element or parent_element is None
	if element is None:
		fail_step(msg_report['msg_element_repository'] % (element_name))
	elif parent_element is None and parent_element_name is not None:
		fail_step(msg_report['msg_element_repository'] % (parent_element_name))
	else:

		try:

			# If the parent_element_name has been specified, search for the element within the
			# parent element's children. Else, get only the element.
			if parent_element is not None:
				parent_element_obj = driver.find_element(parent_element.get_by(), parent_element.get_description())
				element_obj = parent_element_obj.find_element(element.get_by(), element.get_description())
				print("Current Element: " + element_name)
			else:
				print("Current Element: " + element_name)
				element_obj = driver.find_element(element.get_by(), element.get_description())

		except NoSuchElementException as e:

			# If the timeout has expired, return None.
			# Else, wait a second and try again with the elapsed time updated.
			if elapsed_secs >= default_timeout:
				element_obj = None
			else:
				time.sleep(1)
				elapsed_secs += (ct() - start_time).total_seconds()
				element_obj = get_element_from_page(element_name, elapsed_secs, parent_element_name)

	return element_obj

# Function: get_elements_from_page
# Get an element array from the current page, consists if it exits and try again
# on error. It will try as many times as max_try is defined. It is a recursive
# function, so it receives the amount of times it has tried to find the element
# so we can have a stop condition for it.
# In case the parent_element_name is provided, it will look for the elements
# as being children of the parent_element. This argument is optional, though.
#
# Paramaters:
# - element_name: name of the element that needs to be found in the screen.
# - try_count: how many times it has been tried to find the element so far.
# - parent_element_name: (optional) name of the parent element in case you need
#   to use a nested element.
#
# Returns:
# Returns an array of elements found in the page.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def get_elements_from_page(element_name, elapsed_secs, parent_element_name = None):

	global driver

	# Set the initial execution time for this call and also get the element
	start_time = ct()
	parent_element = None
	elements = None

	element = repository.get_element(element_name)

	# If we have a parent element specified, also get this element from the repo
	if parent_element_name is not None:
		parent_element = repository.get_element(parent_element_name)

	# Fail if the element or parent_element is None
	if element is None:
		fail_step(msg_report['msg_element_repository'] % (element_name))
	elif parent_element is None and parent_element_name is not None:
		fail_step(msg_report['msg_element_repository'] % (parent_element_name))
	else:

		try:

			# If the parent_element_name has been specified, search for the element within the
			# parent element's children. Else, get only the element.
			if parent_element is not None:
				parent_element_obj = driver.find_element(parent_element.get_by(), parent_element.get_description())
				elements = parent_element_obj.find_elements(element.get_by(), clear_xpath(element.get_description()))
				print("Current Element: " + element_name)
			else:
				print("Current Element: " + element_name)
				elements = driver.find_elements(element.get_by(), clear_xpath(element.get_description()))

		except NoSuchElementException as e:

			# If the timeout has expired, return None.
			# Else, wait a second and try again with the elapsed time updated.
			if elapsed_secs >= default_timeout:
				elements = None
			else:
				time.sleep(1)
				elapsed_secs += (ct() - start_time).total_seconds()
				elements = get_elements_from_page(element_name, elapsed_secs, parent_element_name)

	return elements

# Function: get_driver
# Returns the Selenium driver currently being used by the abstraction layer. use
# this function to interact directly with Selenium, without using the abstraction
# layer.
#
# Returns:
# An instance of the WebDriver class.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def get_driver():
	global driver
	return driver

# Function: get_repository
# Returns the element repository object currently being used by the abstraction
# layer. Use that to retrieve the description of the element (xpath, css
# selector, etc.).
# The element repository is an instance of the ElementRepository class.
#
# Returns:
# An instance of the ElementRepository class.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def get_repository():
	global repository
	return repository

def build_element_name(element):

	if isinstance(element, WebElement):
		element_name = "Element: [%s, %s, %s]" % (element.tag_name, element.text, element.get_attribute("class"))
	else:
		element_name = element

	return element_name

################################################################################
# Section: Interaction Funtions
# The functions below handle the interaction with elements within the browser
# window. Those are the functions used to click, select and get elements from
# the screen.
################################################################################

# Function: navigate_to
# Navigate to a page. Always use "http://" for external URLs.
#
# Parameters:
# url: url that the browser must navigate to.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def navigate_to(url):
	global driver
	global current_url

	current_url = url
	report.add_sub_step(msg_report['navigate_to'] % (current_url))
	try:
		driver.get(url)
	except:
		report.fail_sub_step(msg_report['couldnt_navigate_to'] % (current_url, str(sys.exc_info()) ))

def refresh_page():
	global driver

	report.add_sub_step(msg_report['refresh_page'])
	try:
		driver.refresh()
	except:
		report.fail_sub_step(msg_report['couldnt_refresh'] % (str(sys.exc_info()) ))

# Function: get_element
# Get an element from the current page. A wrapper for the recursive function
# <get_element_from_page>.
#
# Parameters:
# - element_name: Name of the element to be retrieved from the current page.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def get_element(element_name, parent_element_name = None):

	if isinstance(element_name, WebElement):
		return element_name
	else:
		return get_element_from_page(element_name, 0, parent_element_name)

def get_elements(element_name, parent_element_name = None):
	return get_elements_from_page(element_name, 0, parent_element_name)

# Function: get_value
# Gets the value from an element in the current page. Depending on the element
# type, the internal handling of the element might change. Some will use the
# .text() method from selenium while others might need to use HTML properties
# like "innerHTML".
#
# Obs.: If you are struggling to get a value from an specific type of element,
# it might be necessary to add some special handling to this function.
#
# Parameters:
# - element_name: Name of the element to be retrieved from the current page.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Returns:
# The value contained in the element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def get_value(element_name, parent_element_name = None):

	element = get_element(element_name, parent_element_name)

	if element is not None:
		if tu(element.text) == '' and tu(element.tag_name) in ['SPAN']:
			return element.get_attribute('innerHTML')

		elif tu(element.text) == '' and tu(element.tag_name) in ['INPUT']:
			return element.get_attribute('value')

		return element.text

	else:
		report.add_sub_step(msg_report['get_value'] % (build_element_name(element_name)) )
		report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )

# Function: get_attribute
# Gets a HTML attribe from an element in the current page.
# Exmaples:
# - innertext
# - id
# - style
# - name
#
# Parameters:
# - element_name: Name of the element in which the value will be tested.
# - value: Value to be compared.
# - attribute_name: Name of the attribute to be captured from the element.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def get_attribute(element_name, attribute_name, parent_element_name = None):

	element = get_element(element_name, parent_element_name)

	if element is not None:
		return element.get_attribute(attribute_name)
	else:
		return None

# Function: click
# Clicks on an element.
# Use it for any element you want to click.
#
# Parameters:
# - element_name: Name of the element to be clicked.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def click(element_name, parent_element_name = None):

	report.add_sub_step(msg_report['click'] % (build_element_name(element_name)) )

	end_time = et(default_timeout)
	element = get_element(element_name, parent_element_name)

	if element is not None:
		while ct() <= end_time:
			try:
				element.click()
				return
			except:
				if ct() >= end_time:
					highlight(element_name)
					report.fail_sub_step(msg_report['click_fail'] % (build_element_name(element_name), str(sys.exc_info()) ))
					return

			time.sleep(1)
	else:
		report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )

# Function: hover
# Simulates an element being hovered by the mouse cursor.
# Use it for any element you want to hover.
#
# Parameters:
# - element_name: Name of the element to be hovered.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def hover(element_name, parent_element_name = None):

	global driver

	report.add_sub_step(msg_report['hover'] % (build_element_name(element_name)) )

	end_time = et(default_timeout)
	element = get_element(element_name, parent_element_name)

	if element is not None:
		while ct() <= end_time:
			try:
				action = ActionChains(driver)
				action.move_to_element(element)
				action.perform()
				return
			except:
				if ct() >= end_time:
					highlight(element_name)
					report.fail_sub_step(msg_report['hover_fail'] % (build_element_name(element_name), str(sys.exc_info()) ))
					return

			time.sleep(1)

	else:
		report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )

# Function: set_value
# Sets a value on an element.
# Use it for:
# - Text fields (string)
# - Text areas (string)
# - Checkboxes (True or False)
#
# - element_name: Name of the element in which the value will be changed.
# - value: Value that will be set into the element. Use text string for text
#   areas and text fields. For checkboxes it must be True or False.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def set_value(element_name, value, parent_element_name = None):

	report.add_sub_step(msg_report['set_value'] % (value, build_element_name(element_name)))

	end_time = et(default_timeout)
	element = get_element(element_name, parent_element_name)

	if element is not None:
		while ct() <= end_time:
			try:
				# Handle the specifics for each type of element.
				if tu(element.tag_name) == 'INPUT' and tu(element.get_attribute('type')) == 'CHECKBOX':

					# Only click the checkbox if the value is not the one passed as parameter
					if (element.is_selected() != value):
						element.click()

				elif tu(element.tag_name) == 'INPUT' and tu(element.get_attribute('type')) == 'PASSWORD':
					try:
						element.click()
						element.send_keys(e.decrypt(value))
					except:
						element.click()
						element.send_keys(value)

				# For any other element, just click on it (to activate) and send the keys
				else:
					element.click()
					element.send_keys(value)

				return
			except:
				if ct() >= end_time:
					highlight(element_name)
					report.fail_sub_step(msg_report['set_value_fail'] % (value, element_name, str(sys.exc_info()) ))
					return

			time.sleep(1)
	else:

		report.fail_sub_step(msg_report['element_not_found'] % (element_name) )

# Function: select_value
# Selects a value on an element.
# Use it for:
# - Dropdowns (string with exact text from the option or index)
# - Radio Buttons (string with the radio value or index)
#
# Using index:
# You can use an index. To do it so, just set the value as "#1" or "#2". Any
# value starting with "#" will be considered an index.
#
# - element_name: Name of the element in which the value will be changed.
# - value: Value that will be selected in the element. You can also set an index
#   so that instead of selecting an specific option, the automation can select
#   the first value or the second value, for example.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def select_value(element_name, value, parent_element_name = None):

	report.add_sub_step(msg_report['select_value'] % (value, build_element_name(element_name)))

	end_time = et(default_timeout)
	element = get_element(element_name, parent_element_name)

	if element is not None:
		while ct() <= end_time:
			try:
			# Handle the specifics for each type of element.
				if tu(element.tag_name) == 'SELECT':

					dropdown = Select(get_element(element_name, parent_element_name))

					# If the value begins with #, that means we are selecting by index
					if value[0] == "#":
						dropdown.select_by_index( int(value[1:]) )

					# Else, we select by visible text
					else:
						dropdown.select_by_visible_text(value)

				return
			# Handle the correct exception to output te correct message
			except NoSuchElementException as e:

				report.fail_sub_step(msg_report['select_value_fail'] % (value, build_element_name(element_name), e ))
				return
			except:
				if ct() >= end_time:
					highlight(element_name)
					report.fail_sub_step(msg_report['select_value_fail'] % (value, element_name, str(sys.exc_info()) ))
					return

			time.sleep(1)

	else:
		report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )

# Function: type_value
# Similar to set_value, but it simulates the user typing a value instead. This
# function allows the speed in which the value is typed to be defined using an
# optional parameter. This is useful when having to type values in a slower pace
# than usual.
#
# Use it for:
# - Text fields (string)
# - Text areas (string)
#
# - element_name: Name of the element in which the value will be typed.
# - value: Value that will be tyoed into the element.
# - interval: (optional) The time that will be waited between keystrokes in
#   seconds. By default, it waits 0.1 second before sending each key. This value
#   can be a decimal. (0.1, 0.5, etc.)
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def type_value(element_name, value, interval=0.1, parent_element_name = None):

	report.add_sub_step(msg_report['type_value'] % (value, build_element_name(element_name)))

	element = get_element(element_name, parent_element_name)

	if element is not None:
		element.click()
		for c in value:
			try:
				element.send_keys(c)
				time.sleep(interval)
			except:
				highlight(element_name)

				if isinstance(element_name, WebElement):
					element_name = build_element_name(element_name)

				report.fail_sub_step(msg_report['type_value_fail'] % (value, build_element_name(element_name), str(sys.exc_info()) ))

	else:
		report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )

# Function: send_keystrokes
# Send one or more keystrokes to an element in the page. Those keystrokes follow
# the same naming as the Selenium default keystrokes. There is also an alternate
# , simpler name that can be used.
#
# Selenium names can be used normally, passed as strings.
# The complete reference can be found here:
# http://selenium-python.readthedocs.org/en/latest/api.html#module-selenium.webdriver.common.keys
#
# The list of keystrokes have to be sent in a list, even if it is only one.
# Examples:
# > type_value("element_name", 0.5, ["down", "up"])
# > type_value("element_name", 0.5, ["return"])
#
# Accepted Values:
# - "RETURN", "ENTER": Keys.RETURN
# - "ARROW_DOWN", "DOWN": Keys.ARROW_DOWN
# - "ARROW_UP", "UP": Keys.ARROW_UP
# - "ARROW_LEFT", "LEFT": Keys.ARROW_LEFT
# - "ARROW_RIGHT", "RIGHT": Keys.ARROW_RIGHT
# - "TAB": Keys.TAB
# - "ADD": Keys.ADD
# - "ALT": Keys.ALT
# - "BACK_SPACE", "BACKSPACE": Keys.BACK_SPACE
# - "CANCEL": Keys.CANCEL
# - "CLEAR": Keys.CLEAR
# - "COMMAND", "CMD": Keys.COMMAND
# - "CONTROL", "CTRL": Keys.CONTROL
# - "DECIMAL", ".": Keys.DECIMAL
# - "DELETE", "DEL": Keys.DELETE
# - "DIVIDE", "/": Keys.DIVIDE
# - "END": Keys.END
# - "EQUALS", "=": Keys.EQUALS
# - "ESCAPE", "ESC": Keys.ESCAPE
# - "F1": Keys.F1
# - "F2": Keys.F2
# - "F3": Keys.F3
# - "F4": Keys.F4
# - "F5": Keys.F5
# - "F6": Keys.F6
# - "F7": Keys.F7
# - "F8": Keys.F8
# - "F9": Keys.F9
# - "F10": Keys.F10
# - "F11": Keys.F11
# - "F12": Keys.F12
# - "HELP": Keys.HELP
# - "HOME": Keys.HOME
# - "INSERT", "INS": Keys.INSERT
# - "LEFT_ALT", "LALT": Keys.LEFT_ALT
# - "LEFT_CONTROL", "LCTRL": Keys.LEFT_CONTROL
# - "LEFT_SHIFT", "LSHIFT": Keys.LEFT_SHIFT
# - "META", "SUPER", "WIN": Keys.META
# - "MULTIPLY", "*": Keys.MULTIPLY
# - "NULL": Keys.NULL
# - "NUMPAD0", "0": Keys.NUMPAD0
# - "NUMPAD1", "1": Keys.NUMPAD1
# - "NUMPAD2", "2": Keys.NUMPAD2
# - "NUMPAD3", "3": Keys.NUMPAD3
# - "NUMPAD4", "4": Keys.NUMPAD4
# - "NUMPAD5", "5": Keys.NUMPAD5
# - "NUMPAD6", "6": Keys.NUMPAD6
# - "NUMPAD7", "7": Keys.NUMPAD7
# - "NUMPAD8", "8": Keys.NUMPAD8
# - "NUMPAD9", "9": Keys.NUMPAD9
# - "PAGE_DOWN", "PGDWN": Keys.PAGE_DOWN
# - "PAGE_UP", "PGUP": Keys.PAGE_UP
# - "PAUSE": Keys.PAUSE
# - "SEMICOLON", ";": Keys.SEMICOLON
# - "SEPARATOR": Keys.SEPARATOR
# - "SHIFT": Keys.SHIFT
# - "SPACE", " ": Keys.SPACE
# - "SUBTRACT", "-": Keys.SUBTRACT
#
# - element_name: Name of the element in which the keystroke will happen. Some
#   keystrokes are global and act within the browser window, in that case any
#   element can be used for that.
# - value: A list of key names that can be sent to the application. They need to
#          be passed within an array []
# - interval: (optional) The time that will be waited between keystrokes in
#   seconds. By default, it waits 0.1 second before sending each key. This value
#   can be a decimal. (0.1, 0.5, etc.)
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def send_keystrokes(element_name, keys, interval = 0.3, parent_element_name = None):

	report.add_sub_step(msg_report['send_keystrokes'] % (str(value), build_element_name(element_name)))

	element = get_element(element_name, parent_element_name)

	if element is not None:
		try:
			for k in keys:
				if tu(k) in ["RETURN", "ENTER"]:
					element.send_keys(Keys.RETURN)
				if tu(k) in ["ARROW_DOWN", "DOWN"]:
					element.send_keys(Keys.ARROW_DOWN)
				if tu(k) in ["ARROW_UP", "UP"]:
					element.send_keys(Keys.ARROW_UP)
				if tu(k) in ["ARROW_LEFT", "LEFT"]:
					element.send_keys(Keys.ARROW_LEFT)
				if tu(k) in ["ARROW_RIGHT", "RIGHT"]:
					element.send_keys(Keys.ARROW_RIGHT)
				if tu(k) in ["TAB"]:
					element.send_keys(Keys.TAB)
				if tu(k) in ["ADD"]:
					element.send_keys(Keys.ADD)
				if tu(k) in ["ALT"]:
					element.send_keys(Keys.ALT)
				if tu(k) in ["BACK_SPACE", "BACKSPACE"]:
					element.send_keys(Keys.BACK_SPACE)
				if tu(k) in ["CANCEL"]:
					element.send_keys(Keys.CANCEL)
				if tu(k) in ["CLEAR"]:
					element.send_keys(Keys.CLEAR)
				if tu(k) in ["COMMAND", "CMD"]:
					element.send_keys(Keys.COMMAND)
				if tu(k) in ["CONTROL", "CTRL"]:
					element.send_keys(Keys.CONTROL)
				if tu(k) in ["DECIMAL", "."]:
					element.send_keys(Keys.DECIMAL)
				if tu(k) in ["DELETE", "DEL"]:
					element.send_keys(Keys.DELETE)
				if tu(k) in ["DIVIDE", "/"]:
					element.send_keys(Keys.DIVIDE)
				if tu(k) in ["END"]:
					element.send_keys(Keys.END)
				if tu(k) in ["EQUALS", "="]:
					element.send_keys(Keys.EQUALS)
				if tu(k) in ["ESCAPE", "ESC"]:
					element.send_keys(Keys.ESCAPE)
				if tu(k) in ["F1"]:
					element.send_keys(Keys.F1)
				if tu(k) in ["F2"]:
					element.send_keys(Keys.F2)
				if tu(k) in ["F3"]:
					element.send_keys(Keys.F3)
				if tu(k) in ["F4"]:
					element.send_keys(Keys.F4)
				if tu(k) in ["F5"]:
					element.send_keys(Keys.F5)
				if tu(k) in ["F6"]:
					element.send_keys(Keys.F6)
				if tu(k) in ["F7"]:
					element.send_keys(Keys.F7)
				if tu(k) in ["F8"]:
					element.send_keys(Keys.F8)
				if tu(k) in ["F9"]:
					element.send_keys(Keys.F9)
				if tu(k) in ["F10"]:
					element.send_keys(Keys.F10)
				if tu(k) in ["F11"]:
					element.send_keys(Keys.F11)
				if tu(k) in ["F12"]:
					element.send_keys(Keys.F12)
				if tu(k) in ["HELP"]:
					element.send_keys(Keys.HELP)
				if tu(k) in ["HOME"]:
					element.send_keys(Keys.HOME)
				if tu(k) in ["INSERT", "INS"]:
					element.send_keys(Keys.INSERT)
				if tu(k) in ["LEFT_ALT", "LALT"]:
					element.send_keys(Keys.LEFT_ALT)
				if tu(k) in ["LEFT_CONTROL", "LCTRL"]:
					element.send_keys(Keys.LEFT_CONTROL)
				if tu(k) in ["LEFT_SHIFT", "LSHIFT"]:
					element.send_keys(Keys.LEFT_SHIFT)
				if tu(k) in ["META", "SUPER", "WIN"]:
					element.send_keys(Keys.META)
				if tu(k) in ["MULTIPLY", "*"]:
					element.send_keys(Keys.MULTIPLY)
				if tu(k) in ["NULL"]:
					element.send_keys(Keys.NULL)
				if tu(k) in ["NUMPAD0", "0"]:
					element.send_keys(Keys.NUMPAD0)
				if tu(k) in ["NUMPAD1", "1"]:
					element.send_keys(Keys.NUMPAD1)
				if tu(k) in ["NUMPAD2", "2"]:
					element.send_keys(Keys.NUMPAD2)
				if tu(k) in ["NUMPAD3", "3"]:
					element.send_keys(Keys.NUMPAD3)
				if tu(k) in ["NUMPAD4", "4"]:
					element.send_keys(Keys.NUMPAD4)
				if tu(k) in ["NUMPAD5", "5"]:
					element.send_keys(Keys.NUMPAD5)
				if tu(k) in ["NUMPAD6", "6"]:
					element.send_keys(Keys.NUMPAD6)
				if tu(k) in ["NUMPAD7", "7"]:
					element.send_keys(Keys.NUMPAD7)
				if tu(k) in ["NUMPAD8", "8"]:
					element.send_keys(Keys.NUMPAD8)
				if tu(k) in ["NUMPAD9", "9"]:
					element.send_keys(Keys.NUMPAD9)
				if tu(k) in ["PAGE_DOWN", "PGDWN"]:
					element.send_keys(Keys.PAGE_DOWN)
				if tu(k) in ["PAGE_UP", "PGUP"]:
					element.send_keys(Keys.PAGE_UP)
				if tu(k) in ["PAUSE"]:
					element.send_keys(Keys.PAUSE)
				if tu(k) in ["SEMICOLON", ";"]:
					element.send_keys(Keys.SEMICOLON)
				if tu(k) in ["SEPARATOR"]:
					element.send_keys(Keys.SEPARATOR)
				if tu(k) in ["SHIFT"]:
					element.send_keys(Keys.SHIFT)
				if tu(k) == "SPACE" or k == " ":
					element.send_keys(Keys.SPACE)
				if tu(k) in ["SUBTRACT", "-"]:
					element.send_keys(Keys.SUBTRACT)

				time.sleep(interval)
		except:
			highlight(element_name)
			report.fail_sub_step(msg_report['send_keystrokes_fail'] % (build_element_name(element_name), str(value), str(sys.exc_info()) ))
	else:
		report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )

# Function: clear_field
# Clears a field. (Set as empty)
#
# Use it for:
# - Text fields (string)
# - Text areas (string)
#
# - element_name: Name of the element to be cleared.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def clear_field(element_name, parent_element_name = None):

	report.add_sub_step(msg_report['clear_field'] % (build_element_name(element_name)))
	element = get_element(element_name, parent_element_name)

	if element is not None:
		try:
			element.clear()
		except:
			highlight(element_name)
			report.fail_sub_step(msg_report['clear_field_fail'] % (build_element_name(element_name), str(sys.exc_info()) ))

	else:
		report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )

################################################################################
# Section: "Check" functions
# The "check" functions are going to verify something and will either return
# True or False. They will not log any values in the report.
################################################################################

# Function: check_exist
# Verifies that an element exist in the current page. It waits for the current
# timeout, that can be set by <set_timeout> before considering that the element
# doesn't exist in the screen.
#
# - element_name: Name of the element in which the value will be tested.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Returns:
# True in case the element exists in the page. False in case it doesn't.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def check_exist(element_name, parent_element_name = None):

	end_time = datetime.now() + timedelta(seconds=default_timeout)
	status = False

	while datetime.now() < end_time:
		element = get_element(element_name, parent_element_name)
		if element is not None:
			return True
		time.sleep(1)

	return False

# Function: check_not_exist
# Verifies that an element does not exist in the current page. It waits for the
# current timeout, that can be set by <set_timeout> before considering that the
# element does exist in the screen.
#
# - element_name: Name of the element in which the value will be tested.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Returns:
# True in case the element doesn't exists in the page. False in case it exists.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def check_not_exist(element_name, parent_element_name = None):

	end_time = datetime.now() + timedelta(seconds=default_timeout)
	status = False

	while datetime.now() < end_time:
		element = get_element(element_name, parent_element_name)
		if element is None:
			return True
		time.sleep(1)

	return False

# Function: check_equals
# Verifies that the value from an element is the same as the value passed as
# parameter. The both values are stripped and converter to uppercase before the
# comparison, so it is not case sensitive.
#
# Parameters:
# - element_name: Name of the element in which the value will be tested.
# - value: Value to be used in the comparison.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Returns:
# True in case the value is the same. False if values are different.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def check_equals(element_name, value, parent_element_name = None):

	element = get_element(element_name, parent_element_name)

	if element is not None:
		return tu(element.text) == tu(value)

# Function: check_visible
# Verifies that an element is visible in the current page. It waits for the
# current timeout, that can be set by <set_timeout> before considering that the
# element is not visible in the screen.
#
# It uses the selenium built-in function "is_visible()" to make this validation.
#
# - element_name: Name of the element in which the value will be tested.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Returns:
# True in case the element is visible in the page. False in case it isn't.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def check_visible(element_name, parent_element_name = None):
	global default_timeout

	element = get_element(element_name, parent_element_name)
	end_time = datetime.now() + timedelta(seconds=default_timeout)

	if element is not None:
		while ct() < end_time:
			try:
				if element.is_displayed():
					return True

			except StaleElementReferenceException as e:
				element = get_element(element_name, parent_element_name)

				if element is None:
					break

			time.sleep(1)

		return False
	else:
		return False

def check_not_visible(element_name, parent_element_name = None):
	global default_timeout

	element = get_element(element_name, parent_element_name)
	end_time = datetime.now() + timedelta(seconds=default_timeout)

	if element is not None:
		while ct() < end_time:
			try:
				if not element.is_displayed():
					return True

			except StaleElementReferenceException as e:
				element = get_element(element_name, parent_element_name)

				if element is None:
					break

			time.sleep(1)

		return False
	else:
		return False

def check_contains(element_name, value, parent_element_name = None):

	element = get_element(element_name, parent_element_name)

	if element is not None:
		return tu(get_value(element_name)).find(tu(value)) != -1

def check_enabled(element_name, parent_element_name = None):
	end_time = datetime.now() + timedelta(seconds=default_timeout)
	status = False

	while datetime.now() < end_time:
		element = get_element(element_name, parent_element_name)
		if element.is_enabled():
			return True
		time.sleep(1)

	return False

def check_disabled(element_name, parent_element_name = None):
	end_time = datetime.now() + timedelta(seconds=default_timeout)
	status = False

	while datetime.now() < end_time:
		element = get_element(element_name, parent_element_name)
		if not element.is_enabled():
			return True
		time.sleep(1)

	return False

################################################################################
# Section: "Verify" functions
# The "verify" functions are going to verify something and will either return
# True or False. They will also log the result of this verification in the
# report.
################################################################################

# Function: verify_exist
# Verifies that an element exist in the current page. It waits for the current
# timeout, that can be set by <set_timeout> before considering that the element
# doesn't exist in the screen. It logs an entry in the report accordingly to the
# results.
#
# - element_name: Name of the element in which the value will be tested.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Returns:
# True in case the element exists in the page. False in case it doesn't.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def verify_exist(element_name, parent_element_name = None):

	report.add_sub_step(msg_report['verify_exist'] % (build_element_name(element_name)))
	if check_exist(element_name, parent_element_name):
		return True
	else:
		fail_step(msg_report['verify_exist_fail'] % (build_element_name(element_name)))
		return False

# Function: verify_not_exist
# Verifies that an element does not exist in the current page. It waits for the
# current timeout, that can be set by <set_timeout> before considering that the
# element does exist in the screen. It logs an entry in the report accordingly
# to the results.
#
# Parameters:
# - element_name: Name of the element in which the value will be tested.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Returns:
# True in case the element doesn't exists in the page. False in case it exists.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def verify_not_exist(element_name, parent_element_name = None):

	report.add_sub_step(msg_report['verify_not_exist'] % (build_element_name(element_name)))
	if check_not_exist(element_name, parent_element_name):
		return True
	else:
		fail_step(msg_report['verify_not_exist_fail'] % (build_element_name(element_name)))
		return False

# Function: verify_equals
# Verifies that the value from an element is the same as the value passed as
# parameter. The both values are stripped and converter to uppercase before the
# comparison, so it is not case sensitive. It logs an entry in the report
# accordingly to the results.
#
# Parameters:
# - element_name: Name of the element in which the value will be tested.
# - value: Value to be used in the comparison.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Returns:
# True in case the value is the same. False if values are different.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def verify_equals(element_name, value, parent_element_name = None):
	global report

	report.add_sub_step(msg_report['verify_equals'] % (build_element_name(element_name), value))
	if check_equals(element_name, value, parent_element_name):
		return True
	else:
		set_timeout(5)
		if get_element(element_name) is not None:
			highlight(element_name)
			report.fail_sub_step(msg_report['verify_equals_fail'] % (value, get_value(element_name)))
		else:
			report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )

		set_timeout()
		return False

# Function: verify_visible
# Verifies that an element is visible in the current page. It waits for the
# current timeout, that can be set by <set_timeout> before considering that the
# element is not visible in the screen. It logs an entry in the report
# accordingly to the results.
#
# It uses the selenium built-in function "is_visible()" to make this validation.
#
# - element_name: Name of the element in which the value will be tested.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Returns:
# True in case the element is visible in the page. False in case it isn't.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def verify_visible(element_name, parent_element_name = None):

	report.add_sub_step(msg_report['verify_visible'] % (build_element_name(element_name)))
	if check_visible(element_name, parent_element_name):
		return True
	else:
		set_timeout(5)
		if get_element(element_name) is not None:
			highlight(element_name)
			report.fail_sub_step(msg_report['verify_visible_fail'])
		else:
			report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )

		set_timeout()
		return False

def verify_not_visible(element_name, parent_element_name = None):

	report.add_sub_step(msg_report['verify_not_visible'] % (build_element_name(element_name)))
	if check_not_visible(element_name, parent_element_name):
		return True
	else:
		set_timeout(5)
		if get_element(element_name) is not None:
			highlight(element_name)
			report.fail_sub_step(msg_report['verify_visible_fail'])
		else:
			report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )

		set_timeout()
		return False

def verify_contains(element_name, value, parent_element_name = None):
	global report

	report.add_sub_step(msg_report['verify_contains'] % (build_element_name(element_name), value))
	if check_contains(element_name, value, parent_element_name):
		return True
	else:
		set_timeout(5)
		if get_element(element_name) is not None:
			highlight(element_name)
			report.fail_sub_step(msg_report['verify_contains_fail'] % (value, get_value(element_name)))
		else:
			report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )

		set_timeout()
		return False

def verify_enabled(element_name, parent_element_name = None):

	report.add_sub_step(msg_report['verify_enabled'] % (build_element_name(element_name)))
	if check_enabled(element_name, parent_element_name):
		return True
	else:
		fail_step(msg_report['verify_enabled_fail'] % (build_element_name(element_name)))
		return False

def verify_disabled(element_name, parent_element_name = None):

	report.add_sub_step(msg_report['verify_disabled'] % (build_element_name(element_name)))
	if check_disabled(element_name, parent_element_name):
		return True
	else:
		fail_step(msg_report['verify_disabled_fail'] % (build_element_name(element_name)))
		return False

################################################################################
# Section: "Assert" functions
# The "assert" functions are going to verify something and will either return
# True or False. They will also log the result of this verification in the
# report. If abort_on_assertion is True at config.py, the test will abort on
# error.
################################################################################

# Function: assert_exist
# Verifies that an element exist in the current page. It waits for the current
# timeout, that can be set by <set_timeout> before considering that the element
# doesn't exist in the screen. It logs an entry in the report accordingly to the
# results. In case it fails, it may abort the test, acorrdingly to the flag
# "abort_on_assertion" that can be found in the config.py.
#
# - element_name: Name of the element in which the value will be tested.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Returns:
# True in case the element exists in the page. False in case it doesn't.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def assert_exist(element_name, parent_element_name = None):

	report.add_sub_step(msg_report['assert_exist'] % (build_element_name(element_name)))
	if check_exist(element_name, parent_element_name):
		return True
	else:
		if abort_on_assertion:
			fail_step(msg_report['assert_exist_abort'] % (build_element_name(element_name)))
			sys.exit(1)
		else:
			fail_step(msg_report['assert_exist_fail'] % (build_element_name(element_name)))
			return False

# Function: assert_not_exist
# Verifies that an element does not exist in the current page. It waits for the
# current timeout, that can be set by <set_timeout> before considering that the
# element does exist in the screen. It logs an entry in the report accordingly
# to the results. In case it fails, it may abort the test, acorrdingly to the
# flag "abort_on_assertion" that can be found in the config.py.
#
# Parameters:
# - element_name: Name of the element in which the value will be tested.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Returns:
# True in case the element doesn't exists in the page. False in case it exists.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def assert_not_exist(element_name, parent_element_name = None):

	report.add_sub_step(msg_report['assert_not_exist'] % (build_element_name(element_name)))
	if check_not_exist(element_name, parent_element_name):
		return True
	else:
		if abort_on_assertion:
			fail_step(msg_report['assert_not_exist_abort'] % (build_element_name(element_name)))
			sys.exit(1)
		else:
			fail_step(msg_report['assert_not_exist_fail'] % (build_element_name(element_name)))
			return False

# Function: assert_equals
# Verifies that the value from an element is the same as the value passed as
# parameter. The both values are stripped and converter to uppercase before the
# comparison, so it is not case sensitive. It logs an entry in the report
# accordingly to the results. In case it fails, it may abort the test,
# acorrdingly to the flag "abort_on_assertion" that can be found in the
# config.py.
#
# Parameters:
# - element_name: Name of the element in which the value will be tested.
# - value: Value to be used in the comparison.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Returns:
# True in case the value is the same. False if values are different.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def assert_equals(element_name, value, parent_element_name = None):
	global report

	report.add_sub_step(msg_report['assert_equals'] % (build_element_name(element_name), value))
	if check_equals(element_name, value, parent_element_name):
		return True
	else:
		if abort_on_assertion:
			report.fail_sub_step(msg_report['assert_equals_fail'] % (build_element_name(element_name), value, get_value(element_name)))
			sys.exit(1)
		else:
			set_timeout(5)
			if get_element(element_name) is not None:
				highlight(element_name)
				report.fail_sub_step(msg_report['assert_equals_abort'] % (element_name, value, get_value(element_name)))
			else:
				report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )

			set_timeout()

		return False

# Function: assert_visible
# Verifies that an element is visible in the current page. It waits for the
# current timeout, that can be set by <set_timeout> before considering that the
# element is not visible in the screen. It logs an entry in the report
# accordingly to the results. In case it fails, it may abort the test,
# acorrdingly to the flag "abort_on_assertion" that can be found in the
# config.py.
#
# It uses the selenium built-in function "is_visible()" to make this validation.
#
# - element_name: Name of the element in which the value will be tested.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Returns:
# True in case the element is visible in the page. False in case it isn't.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def assert_visible(element_name, parent_element_name = None):

	report.add_sub_step(msg_report['assert_visible'] % (build_element_name(element_name)))
	if check_visible(element_name, parent_element_name):
		return True
	else:
		if abort_on_assertion:
			report.fail_sub_step(msg_report['assert_visible_fail'])
			sys.exit(1)
		else:
			set_timeout(5)
			if get_element(element_name) is not None:
				highlight(element_name)
				report.fail_sub_step(msg_report['assert_visible_abort'])
			else:
				report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )

			set_timeout()

		return False

def assert_visible(element_name, parent_element_name = None):

	report.add_sub_step(msg_report['assert_not_visible'] % (build_element_name(element_name)))
	if check_not_visible(element_name, parent_element_name):
		return True
	else:
		if abort_on_assertion:
			report.fail_sub_step(msg_report['assert_not_visible_fail'])
			sys.exit(1)
		else:
			set_timeout(5)
			if get_element(element_name) is not None:
				highlight(element_name)
				report.fail_sub_step(msg_report['assert_not_visible_abort'])
			else:
				report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )

			set_timeout()

		return False

def assert_contains(element_name, value, parent_element_name = None):
	global report

	report.add_sub_step(msg_report['assert_contains'] % (build_element_name(element_name), value))
	if check_contains(element_name, value, parent_element_name):
		return True
	else:
		if abort_on_assertion:
			report.fail_sub_step(msg_report['assert_contains_fail'] % (build_element_name(element_name), value, get_value(element_name)))
			sys.exit(1)
		else:
			set_timeout(5)
			if get_element(element_name) is not None:
				highlight(element_name)
				report.fail_sub_step(msg_report['assert_contains_abort'] % (element_name, value, get_value(element_name)))
			else:
				report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )

			set_timeout()

		return False

def assert_enabled(element_name, parent_element_name = None):

	report.add_sub_step(msg_report['assert_enabled'] % (build_element_name(element_name)))
	if check_enabled(element_name, parent_element_name):
		return True
	else:
		if abort_on_assertion:
			fail_step(msg_report['assert_enabled_abort'] % (build_element_name(element_name)))
			sys.exit(1)
		else:
			fail_step(msg_report['assert_enabled_fail'] % (build_element_name(element_name)))
			return False

def assert_disbled(element_name, parent_element_name = None):

	report.add_sub_step(msg_report['assert_disabled'] % (build_element_name(element_name)))
	if check_disabled(element_name, parent_element_name):
		return True
	else:
		if abort_on_assertion:
			fail_step(msg_report['assert_disabled_abort'] % (build_element_name(element_name)))
			sys.exit(1)
		else:
			fail_step(msg_report['assert_disabled_fail'] % (build_element_name(element_name)))
			return False

################################################################################
# Section: Wait functions
# The wait functions are used to wait implicitly for events to happen.
################################################################################

# Function: wait_for_loading
# Waits for an object that is currently visible to not be displayed
# anymore. It accepts an interval as an optional argument so that the function
# waits a certain amount of time before checking if the loading screen is being
# displayed on screen. That is necessary as sometimes there might be a lag
# between the last action and the trigger of the javascript that will display
# the loading message on screen. Usually, short amounts of time are ideal (1
# second or so.)
#
# Parameters:
# - element_name: Name of the element in which the value will be tested.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def wait_for_loading(element_name, interval = None, parent_element_name = None):
	global driver
	global repository
	global default_timeout

	if interval is not None:
		time.sleep(interval)

	element = repository.get_element(element_name)

	if element is not None:

		wait = WebDriverWait(driver, default_timeout)
		try:
			wait.until(ExpectedConditions.invisibility_of_element_located((element.get_by(), element.get_description())))
		except TimeoutException as e:
			report.add_sub_step(msg_report['wait_for_loading_timeout_fail'] % (default_timeout))
			report.fail_sub_step(msg_report['wait_for_loading_timeout_fail'] % (default_timeout))
			sys.exit(1)
			return
		except NoSuchElementException as e:
			return

# Function: wait_for_any_value
# Waits for an object like a label or a text field to have a value. (any value)
# It waits for the current timeout, that can be set by <set_timeout> before
# considering that the element is doesn't have any value.
#
# Parameters:
# - element_name: Name of the element in which the value will be tested.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def wait_for_any_value(element_name, parent_element_name = None):
	global driver
	global default_timeout

	report.add_sub_step(msg_report['wait_for_any_value'] % (build_element_name(element_name)))

	element = get_element(element_name, parent_element_name)
	end_time = datetime.now() + timedelta(seconds=default_timeout)

	if element is not None:

		while datetime.now() <= end_time:
			if tu(get_value(element_name, parent_element_name)) != '':
				return True
			time.sleep(1)

	else:
		report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )
		return False


	report.fail_sub_step(msg_report['wait_for_any_value_timeout_fail'] % (default_timeout, build_element_name(element_name)))
	return False

# Function: wait_for_value
# Waits for an object like a label or a text field to have an specific value.
# It waits for the current timeout, that can be set by <set_timeout> before
# considering that the element is doesn't have any value.
#
# Parameters:
# - element_name: Name of the element in which the value will be tested.
# - value: Value to be compared.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def wait_for_value(element_name, value, parent_element_name = None):
	global driver
	global repository
	global default_timeout

	start_step(msg_report['wait_for_value'] % (build_element_name(element_name), value))

	element = repository.get_element(element_name)

	if element is not None:
		wait = WebDriverWait(driver, default_timeout)
		try:
			wait.until(ExpectedConditions.text_to_be_present_in_element((element.get_by(), element.get_description()), value))
		except NoSuchElementException as e:
			start_step(msg_report['wait_for_value_no_element_fail'] % (build_element_name(element_name)))
			return False
		except TimeoutException as e:
			highlight(element_name)
			start_step(msg_report['wait_for_value_timeout_fail'] % (default_timeout, build_element_name(element_name), value))
			return False

	else:
		report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )
		return False

	return True

# Function: wait_for_clickable
# Waits for an object to be clickable. It waits for the current timeout, that
# can be set by <set_timeout> before considering that the element is doesn't
# have any value. It has an optional initial interval parameter to wait for
# before checking if the element is clickable or not.
#
# Parameters:
# - element_name: Name of the element in which the value will be tested.
# - value: Value to be compared.
# - interval: Interval to wait before waiting for the element to be clockable.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def wait_for_clickable(element_name, interval = None, parent_element_name = None):
	global driver
	global repository
	global default_timeout

	if interval is not None:
		time.sleep(interval)

	element = repository.get_element(element_name)

	if element is not None:

		wait = WebDriverWait(driver, default_timeout)
		try:
			wait.until(ExpectedConditions.element_to_be_clickable((element.get_by(), element.get_description())))
		except TimeoutException as e:
			highlight(element_name)
			report.add_sub_step(msg_report['wait_for_clickable'] % (build_element_name(element_name)))
			report.fail_sub_step(msg_report['wait_for_clickable_timeout_fail'] % (build_element_name(element_name), default_timeout))
			sys.exit(1)
			return
		except NoSuchElementException as e:
			return

	else:
		report.fail_sub_step(msg_report['element_not_found'] % (build_element_name(element_name)) )
		return False

################################################################################
# Section: Support functions
# Support functions are used to execute actions that are not directly related to
# the UI but might interact or change elements.
################################################################################

# Function: set_timeout
# Sets the timeout for the entire framework. Any function that waits some time
# for something to happen (and that interval is not passed as parameter) has to
# use the framework's default framework.
#
# Parameter:
# - timeout: (option) Timeout in seconds.
# If no parameter is passed, the timeout will be reset to the default value.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def set_timeout(timeout=None):
	global default_timeout

	if timeout is None:
		default_timeout = framework_default_timeout
	else:
		default_timeout = timeout

# Function: set_index
# Set the index of an element. Calls the set_index from the Element in order
# to change it. This is changed directly in the XPath expression.
#
# Parameters:
# element_name - Name of the element in which you want to change the index.
# index - Index to be used. First acceptable index is 1.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def set_index(element_name, index):
	global repository

	repository.get_element(element_name).set_index(index)

# Function: set_frame
# Sets the current frame context from Selenium. When working with frames, you
# need to explicitly pass to Selenium which frame has to be used so that the
# xpath expressions work properly. The element passed as parameter needs to
# describe a frame or iframe element that can be found in the html.
#
# Parameters:
# - element_name: (optional) If element_name has any value, the function will
#  try to find the element in the repository and the in the current page. After
#  that, it will pass this element to selenium.
#  If no parameter is passed or element_name is None, the function will restore
#  the original Selenium frame context.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def set_frame(element_name=None):
	global driver

	if element_name is None:
		driver.switch_to_default_content()
	else:
		element = get_element(element_name, None)
		driver.switch_to_frame(element)

################################################################################
# Section: Debug functions
# Debug functions are used to help during the development process.
################################################################################

# Function: highlight
# Will highlight an element in the current page, by injecting a javascript that
# color of the element and adds a red border around it.
# Not recomended to be used in actual test cases as it change sthe page. The
# main purpose of this function is to debug tests when having dificulty to
# find elements.
#
# Parameters:
# - element_name: Name of the element to be highlightes in the current page.
# - parent_element_name: (optional) Name of the element to be retrieved from
#   within the parent_element.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def highlight(element_name, parent_element_name = None):

	element = get_element(element_name, parent_element_name)

	if element is not None:
		driver.execute_script("arguments[0].scrollIntoView(true);", element)
		driver.execute_script("arguments[0].setAttribute('style', arguments[1]);", element, "color: red; border: 3px solid red;")

################################################################################
# Section: Reporting functions
# Functions used to interact with the reporting system. They are important to
# keep the report organized when it is generated after the test execution ends.
################################################################################

def start_iteration(description):
	global report
	report.add_iteration(description)

# Function: start_action
# Starts a new action in the report. An action can be considered a major group
# of steps or, in case of shorter test cases, it can be considered the test case
# itself.
#
# Parameters:
# - description: Description of the action that is going to be opened.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def start_action(description):
	global report
	report.add_action(description)

# Function: start_action
# Starts a new step in the report. The step is comprised of a set of smaller
# steps called "sub-steps". The framework manages the addition of sub-steps to
# the report accodingly to the functions that are called during the test case
# execution. The step is used to group those steps within a logical group.
#
# Parameters:
# - description: Description of the step that is going to be opened.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def start_step(description):
	global report
	report.add_step(description)

# Function: pass_step
# Passes the current step adding a comment to it. This function is useful when
# it is necessary to pass a step and add some information about that step, like
# the ID of something that has been created. This way, this information is
# visible in the report.
#
# Parameters:
# - comment: Comment to be added to the step.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def pass_step(comment):
	global report
	report.pass_step(comment)

# Function: fail_step
# Fails an step explicitly. This function is used to fail an step manually, when
# there is some custom validation going on. The framework functions take care
# of failing the steps when needed, but if a custom validation routine is being
# created the usage of fail_step might be neecessary.
#
# Parameters:
# - comment: Comment to be added to the step.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def fail_step(comment):
	global report
	report.fail_sub_step(comment)

# Function: end_action
# Closes the current action. Usually, this function will be called when a major
# group of steps has been completed or even when the test case is about to
# finish. Always when opening an action, there might be a matching end_action
# call, else the report might be generated with wrong step nesting.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def end_action():
	report.finish_report()

def end_iteration():
	report.finish_report()

# Function: relevant_data
# Send a pair of name:value to the report to be displayed in a separate Section
# when the report is generated. Useful to highlight import data that has been
# created/gathered during the script execution.
#
# Parameters:
# - name: Name of the data that will be displayed in the report. Using the same
#         name twice will overwrite previous values assigned to that name.
# - value: Value to be displayed in the report.
#
# Author:
# Danilo Guimaraes danilo.guimares@catalinamarketing.com
def relevant_data(name, value):
	report.add_relevant_data(name, value)

################################################################################

# Initializes the abstraction layer after the import
initialize_abs_layer()
