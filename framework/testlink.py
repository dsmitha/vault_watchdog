#! /usr/bin/python
"""
Testlink API Sample Python 3.x Client implementation
"""
import xmlrpc.client
#import xmlrpc.server
import pdb
from framework.utils import *
from framework.reporting import *
from config import *
from framework.awards.settings import machine_settings as ms
class TestlinkAPIClient:
    global report
    # substitute your server URL Here
    SERVER_URL = "http://testlink.catalina.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php"

    def __init__(self, devKey):
        self.server = xmlrpc.client.ServerProxy(self.SERVER_URL)
        self.devKey = devKey

    def reportTCResult(self, tcid, tpid, status):
        data = {"devKey":self.devKey, "tcid":tcid, "tpid":tpid, "status":status}
        return self.server.tl.reportTCResult(data)

    def createBuild(self, tpid, buildname, buildnotes):
        data = {"devKey":self.devKey, "testplanid":tpid, "buildname":buildname, "buildnotes":buildnotes}
        return self.server.tl.createBuild(data)

    def getInfo(self):
        return self.server.tl.about()

    def getTestCaseIDByName(self, testcasename):
        data = {"devKey":self.devKey, "testcasename":testcasename}
        return self.server.tl.getTestCaseIDByName(data)

    def reportTCResult(self, testcaseid, testplanid, status, buildname, platformid):
        data = {"devKey":self.devKey, "testcaseid": testcaseid, "testplanid":testplanid, "status":status, "buildname":buildname, "platformid":platformid}
        return self.server.tl.reportTCResult(data)
    def uploadTestCaseAttachment(self, testcaseid, filename, filetype, content):
        data = {"devKey":self.devKey, "testcaseid": testcaseid, "filename": filename, "filetype": filetype, "content": content}
        return self.server.tl.uploadTestCaseAttachment(data)
    def assignTestCaseExecutionTask(self,testcaseid,testplanid,projectid,buildname,user,platformid):
        data={"devKey":self.devKey,"testprojectid":projectid,"testplanid":testplanid,"testcaseid":testcaseid,"buildname":buildname,"user":user,"platformid":platformid}
        return self.server.tl.assignTestCaseExecutionTask(data)



    '''def getbuildidfrombuildname(self, testplanid, builddesc):
        data = {"devKey":"c34800724dd6c6bc1e1061b4b1e7786c", "testplanid":testplanid}
        lsbuilds = self.server.tl.getBuildsforTestPlan
        builds_splt = str(lsbuilds[0]).split(",")
        for j in builds_split:
            print (j)
            if j.find(buildsdesc) == 0'''
    def addtestcasetotestplan(self, testprojectid, testplanid, tcexternalid, tcversion, platformid):
        data = {"devKey":self.devKey, "testprojectid":testprojectid, "testplanid":testplanid, "testcaseexternalid": tcexternalid, "version": tcversion,"platformid":platformid}
        #
        return self.server.tl.addTestCaseToTestPlan(data)
    def getTestCase(self, testcaseid):
        data = {"devKey":self.devKey, "testcaseid":testcaseid}
        return self.server.tl.getTestCase(data)

# substitute your Dev Key Here
# get info about the server
#print client.getInfo()
# Substitute for tcid and tpid that apply to your project
#result = client.createBuild(5, "API_0.0.0.5", "Build API_0.0.0.5 Notes")
#print (result)
def createbuild(params):
    start_step("Create Build %s in Testlink" % daily_build_ver)
    #daily_build_version = "A - %s" %awards_version
    result = client.createBuild(params["testplanid"], daily_build_ver, params["build notes"])
    #result = client.createBuild(5, "API_0.0.0.6", "Build API_0.0.0.6 Notes")
    #result = "[{'status': True, 'operation': 'createBuild', 'id': '12', 'message': 'Success!'}]"
    #result_split = str(result[0]).split(",")
    #
    #for i in result_split:
        #print (i)
    if str(result[0]).find("Success")!= -1:
        print("success")
        pass_step("[createbuild] The testlink build %s was created"% daily_build_ver)
    elif str(result[0]).find("already exists") != -1:
        print("success")
        pass_step("[createbuild] The testlink build %s already created"% daily_build_ver)
    else:
        print("failure")
        fail_step("[createbuild] The testlink build was not created")


def assigntestcaseuser(testcaseid,testplanid,projectid,user):
    start_action("Assign %s to testcases "%user)
    client.assignTestCaseExecutionTask(testcaseid,testplanid,projectid,daily_build_ver,user,ms.platformid)



def updateresult(testcaseid, testplanid, status):

    start_step("Update result in Testlink")
#    if len(testcaseid) == 0:
#        sys.exit(0)
    #
    #tcstatus = report.get_overall_status()
    if status == "Passed":
        status = "p"
    elif status == "Failed":
        validate_step="No"
        config_file_path = "%s\%s" % (base_path, "config.py")
        fh=open(config_file_path,"r+")
        lines=fh.readlines()
        for i in range(len(lines)):
            varname="validate_step_%s"%ms.machine_name
            print (varname )
            #
            if len(re.findall(varname,lines[i]))>0:

                validate_step="yes"
                break
        print(validate_step)
        #

        if tu(validate_step)=="NO":
            status="b"
            print(status)

        else:
            status = "f"


    elif status == "Blocked":
        status = "b"
    #
    print(status)
    print(ms.platformid)

    #pdb.set_trace()
    if len(testcaseid) != 0:
        tcstatus = client.reportTCResult(testcaseid, testplanid, status, daily_build_ver, ms.platformid)
        update_stepinfo_in_config("No","YES")
        if str(tcstatus[0]).find("Success")!= -1:
            print("Passed")
        #pass_step("[updateresult] The testcase was updated successfully")
        #client.uploadTestCaseAttachment(params["testcaseid"], "test_result.txt", "text", "Pass")
        #fail_step("[updateresult] The testcase was not updated successfully")
        #lient.uploadTestCaseAttachment(params["testcaseid"], "test_result.txt", "text", "Fail")
            pass_step("Result was updated in Testlink")
#client = TestlinkAPIClient("dbf4613faf25823935389e627708cbac")
client = TestlinkAPIClient("1373cdfcdd9afcbbd141dfb238d850e0")
'''def update_testlinkresult(params):
    status = client.reportTCResult(params["testcaseid"], params["testplanid"], params["teststatus"], params["buildid"], params["platformid"])
'''

def addtestcasetotestplan(params):
    start_step("Associate %s to a test plan"%params["tcexternalid"])
    tcversion = gettestcaseversion(params["tcexternalid"])
    #tcversion = 1
    client.addtestcasetotestplan(params["testprojectid"], params["testplanid"], params["tcexternalid"], tcversion, ms.platformid)
    #
    #print (tcplanstatus)
    pass_step("Testcase was added to Testplan in Testlink")
#result = client.reportTCResult(3, 5, "pass_bala")
#result = client.reportTCResult(1132, 56646, "p")
# Typically you'd want to validate the result here and probably do something more useful with it
#print "reportTCResult result was: %s" %(result)
#client.updateresult("ppg-2",platformid,"A-1.0.0.13","Passed","passed")
#inttestid = client.getTestCaseIDByName("case 1-1-2")
#print (inttestid)
#
#status = client.updateresult( 5, 1,"A-1.0.0.0.16", 29, "Passed","Passed" )
#print (client.listmethods)
#print(client.system.methodSignature("reportTCResult"))
def gettestcaseversion(params):
	#start_step("Associate %s to a test plan"%params["tcexternalid"])
	print(params)
	testcaseid = params
	rtcreturn = client.getTestCase(sys.argv[3])
	return rtcreturn[0]["version"]     #print (tcplanstatus)
    #pass_step("Testcase was added to Testplan in Testlink")
'''uploadTestCaseAttachment( struct $args, string $args["devKey"],
                          int $args["testcaseid"],
                          string $args["filename"],
                          string $args["filetype"], string $args["content"]  ) '''
#updateResult( struct $args, string $args["devKey"], int $args["testplanid"], int $args["platformid"], int $args["buildid"], int $args["testcaseid"], string $args["status"], string $args["notes"]  )
#updateResult( 5, platformid,"API_0.0.0.4", testcaseid, "Passed","Passed" )
#getTestPlanByName( struct $args, string $args["devKey"], string $args["testprojectname"], string $args["testplanname"]  )
#{"devkey":"c34800724dd6c6bc1e1061b4b1e7786c", "testprojectname": "Playground project", "testplanname": "Testplan for 1.0.0 Release"}
#{"devkey":"c34800724dd6c6bc1e1061b4b1e7786c", "testprojectname": "Playground project", "testplanname": "Testplan for 1.0.0 Release"}
#status = client.reportTCResult(34, 5, "p", buildid, 1)
#print (status)
#client.uploadTestCaseAttachment(34, "Auto_Exec_Res.txt", "text", "pass")
