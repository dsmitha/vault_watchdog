from win32com.client import Dispatch
import os
import re
import pdb

# Provides a list of files to be uploaded to ALM when the test finishes
upload_stack = []
link_stack = []

class ALMConnection:

    def __init__(self, server, domain, project, username, password):
        # Snippet of code from:
        # https://techblog.polteq.com/en/retrieve-all-testcase-data-with-hp-qc-ota-and-python/
        self.alm_connection = Dispatch("TDApiOle80.TDConnection.1")
        self.alm_connection.InitConnectionEx(server)
        self.alm_connection.Login(username, password)
        self.alm_connection.Connect(domain, project)

    def update_test_instance(self, test_set_id, test_name, status, run_id, instance, report_path):
        global upload_stack

        # Get the test set
        ts_fac = self.alm_connection.TestSetFactory
        set_list = ts_fac.NewList(test_set_id)
        test_set = set_list[0]

        # Get the test instance
        test_filter = test_set.TSTestFactory.Filter
        test_filter["TSC_NAME"] = '"' + test_name + '"'
        test_filter["TC_TEST_INSTANCE"] = '"' + instance + '"'
        test_instance = test_set.TSTestFactory.NewList(test_filter.text)[0]

        # Get the run instance
        # test_run = test_instance.RunFactory.AddItem(None)
        test_run = test_instance.RunFactory.NewList(run_id)[0]

        # Setup the test run
        #
        test_run.status = status
        test_run.name = "Python: " + test_instance.RunFactory.UniqueRunName

        # Submit the test run
        test_run.Post()

        # Attach the HTML file to the run
        report_path = os.path.abspath(report_path)
        att = test_run.Attachments.AddItem(None)
        att.Description = "Test Execution Report"
        att.FileName = report_path
        att.Type = 1
        att.Post()

        # Attach the other files
        for file_description in upload_stack:
            att = test_run.Attachments.AddItem(None)
            att.Type = 1
            att.Description = file_description[1]
            att.FileName = file_description[0]
            att.Post()

        # Attach Links
        for link_description in link_stack:
            att = test_run.Attachments.AddItem(None)
            att.Type = 2
            att.Description = link_description[0]
            att.FileName = link_description[0]
            att.Post()

    def close_connection(self):
        self.alm_connection.Logout()

class ALMTestCase:
    def __init__(self, server, domain, project, username, password):
        # Snippet of code from:
        # https://techblog.polteq.com/en/retrieve-all-testcase-data-with-hp-qc-ota-and-python/
        self.alm_connection = Dispatch("TDApiOle80.TDConnection.1")
        self.alm_connection.InitConnectionEx(server)
        self.alm_connection.Login(username, password)
        self.alm_connection.Connect(domain, project)

    def load_test_case(self, test_case_id = None, test_case_name = None):

        # Initialize some fields
        self.description = ""
        self.id = ""
        self.name = ""
        self.steps = []

        # Get the test case by ID
        if test_case_id is not None:
            test = self.alm_connection.TestFactory.NewList(test_case_id)[0]

        # Get the test case by Name
        else:
            test_fac = self.alm_connection.TestFactory
            test_fac_filter = test_fac.Filter
            test_fac_filter["TS_NAME"] = test_case_name
            test = test_fac.NewList(test_fac_filter.text)[0]

        # Store the test information
        self.description = test.Field("TS_DESCRIPTION")
        self.id = test.ID
        self.name = test.Name

        # Remove html tags from description
        self.description = self.description.replace("<html>", "").replace("<body>", "")
        self.description = self.description.replace("</html>", "").replace("</body>", "")

        # Get the steps from the test Case
        steps = test.DesignStepFactory.NewList("")

        # Append the steps information
        for step in steps:
            self.steps.append([step.Order, step.StepDescription, step.StepExpectedResult])

    def close_connection(self):
        self.alm_connection.Logout()

class ALMTestSet:

    def __init__(self, server, domain, project, username, password):
        # Snippet of code from:
        # https://techblog.polteq.com/en/retrieve-all-testcase-data-with-hp-qc-ota-and-python/
        self.alm_connection = Dispatch("TDApiOle80.TDConnection.1")
        self.alm_connection.InitConnectionEx(server)
        self.alm_connection.Login(username, password)
        self.alm_connection.Connect(domain, project)

        self.machine_name = ""

    def get_machine_name_from_lab(self, test_set_id):

        # get the lab name
        test_set_name = self.alm_connection.TestSetFactory.NewList(test_set_id).Item(1).Name

        # get the machine name within the lab name
        #split is added to get the VM name if printer type is added
        #Added by Smitha
        machine_name = re.findall(r"\[.+\]", test_set_name)[0][1:-1].split("_")[0]
        #
        return machine_name
    #Added new function to get the printer name
    # Added by
    def get_printer_name_from_lab(self,test_set_id):
        # get the lab name
        test_set_name = self.alm_connection.TestSetFactory.NewList(test_set_id).Item(1).Name

        # get the machine name within the lab name
        testset_name = re.findall(r"\[.+\]", test_set_name)[0][1:-1]
        printer_name=testset_name.split("_")[1:]
        print (printer_name)
        #
        return printer_name

    def close_connection(self):
        self.alm_connection.Logout()

def add_file_for_alm_upload(filename, description):
    global upload_stack
    upload_stack.append([filename, description])

def add_file_for_alm_linking(filename, description):
    global link_stack
    link_stack.append([filename, description])
