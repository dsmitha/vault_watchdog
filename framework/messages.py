# Initialization
msg_report = {}

################################################################################
# Reporting Messages
################################################################################
# Errors:
msg_report['element_not_found'] = 'Could not find element \'%s\' on screen.'

# def get_element_from_page():
msg_report['msg_element_repository'] = 'Could not find the element \'%s\' in the element repository.'
msg_report['msg_element_not_found'] = 'Element \'%s\' has not been found in the current page.<br/><br/><strong>System Message:</strong><br/>%s'
msg_report['msg_element_not_found_within_element'] = 'Element \'%s\' has not been found within element \'%s\' children.<br/><br/><strong>System Message:</strong><br/>%s'

# def navigate_to():
msg_report['navigate_to'] = 'Navigate to \'%s\'.'
msg_report['couldnt_navigate_to'] = 'Could not navigate to \'%s\'.<br/><br/><strong>System Message:</strong><br/>%s'

# def refresh_page():
msg_report['refresh_page'] = 'Refresh the current page.'
msg_report['couldnt_refresh'] = 'Could not refresh the page.<br/><br/><strong>System Message:</strong><br/>%s'

# def click():
msg_report['click'] = 'Click on the element \'%s\'.'
msg_report['click_fail'] = 'Couldn not click on the element \'%s\'.<br/><br/><strong>System Message:</strong><br/>%s'

# def hover():
msg_report['hover'] = 'Hover the element \'%s\'.'
msg_report['hover_fail'] = 'Couldn not hover the element \'%s\'.<br/><br/><strong>System Message:</strong><br/>%s'

# def set_value():
msg_report['set_value'] = 'Set value \'%s\' on the element \'%s\'.'
msg_report['set_value_fail'] = 'Could not set value \'%s\' on the element \'%s\' in the current page.<br/><br/><strong>System Message:</strong><br/>%s'

# def get_value():
msg_report['get_value'] = 'Get the value from the element \'%s\'.'

# def type_value():
msg_report['type_value'] = 'Type the value \'%s\' in the element \'%s\'.'
msg_report['type_value_fail'] = 'Could not type value \'%s\' on the element \'%s\' in the current page.<br/><br/><strong>System Message:</strong><br/>%s'

# def send_keystrokes():
msg_report['send_keystrokes'] = 'Send the following keys to the element \'%s\': \'%s\'.'
msg_report['send_keystrokes_fail'] = 'Could not send the following keys to the element \'%s\': \'%s\'.<br/><br/><strong>System Message:</strong><br/>%s'

# def cleat_field():
msg_report['clear_field'] = 'Clear the field \'%s\'.'
msg_report['clear_field_fail'] = 'Could not clear the field \'%s\'.<br/><br/><strong>System Message:</strong><br/>%s'

# def select_value():
msg_report['select_value'] = 'Select value \'%s\' on the element \'%s\'.'
msg_report['select_value_fail'] = 'Could not select value \'%s\' on the element \'%s\' in the current page.<br/><br/><strong>System Message:</strong><br/>%s'

# def verify_exist():
msg_report['verify_exist'] = 'Validate that the element \'%s\' can be found in the screen.'
msg_report['verify_exist_fail'] = 'Validation failed. The element \'%s\' could NOT be found in the screen.'

# def assert_exist():
msg_report['assert_exist'] = 'Assert that the element \'%s\' can be found in the screen.'
msg_report['assert_exist_fail'] = 'Assertion failed. The element \'%s\' could NOT be found in the screen.'
msg_report['assert_exist_abort'] = 'Assertion failed. The element \'%s\' could NOT be found in the screen. Aborting the execution.'

# def verify_not_exist():
msg_report['verify_not_exist'] = 'Validate that the element \'%s\' cannot be found in the screen.'
msg_report['verify_not_exist_fail'] = 'Validation failed. The element \'%s\' could be found in the screen.'

# def assert_not_exist():
msg_report['assert_not_exist'] = 'Assert that the element \'%s\' cannot be found in the screen.'
msg_report['assert_not_exist_fail'] = 'Assertion failed. The element \'%s\' could be found in the screen.'
msg_report['assert_not_exist_abort'] = 'Assertion failed. The element \'%s\' could be found in the screen. Aborting the execution.'

# def verify_equals():
msg_report['verify_equals'] = 'Verify that the element \'%s\' value equals to \'%s\'.'
msg_report['verify_equals_fail'] = 'Validation failed. Element value is not \'%s\', found \'%s\'.'

# def assert_equals():
msg_report['assert_equals'] = 'Assert that the element \'%s\' value equals to \'%s\'.'
msg_report['assert_equals_fail'] = 'Assertion failed. The element \'%s\' value is NOT equals to \'%s\', found \'%s\'.'
msg_report['assert_equals_abort'] = 'Assertion failed. The element \'%s\' value is NOT equals to \'%s\', found \'%s\'. Aborting the execution.'

# def verify_visible():
msg_report['verify_visible'] = 'Verify that the element \'%s\' is visible.'
msg_report['verify_visible_fail'] = 'Validation failed. Element is not visible.'

# def assert_visible():
msg_report['assert_visible'] = 'Assert that the element \'%s\' is visible.'
msg_report['assert_visible_fail'] = 'Assertion failed. Element is not visible.'
msg_report['assert_visible_abort'] = 'Assertion failed. Element is not visible. Aborting the execution.'

# def verify_not_visible():
msg_report['verify_not_visible'] = 'Verify that the element \'%s\' is not visible.'
msg_report['verify_not_visible_fail'] = 'Validation failed. Element is visible.'

# def assert_not_visible():
msg_report['assert_not_visible'] = 'Assert that the element \'%s\' is not visible.'
msg_report['assert_not_visible_fail'] = 'Assertion failed. Element is visible.'
msg_report['assert_not_visible_abort'] = 'Assertion failed. Element is visible. Aborting the Execution.'

# def verify_enabled():
msg_report['verify_enabled'] = 'Validate that the element \'%s\' is enabled.'
msg_report['verify_enabled_fail'] = 'Validation failed. The element \'%s\' is NOT enabled.'

# def assert_enabled():
msg_report['assert_enabled'] = 'Assert that the element \'%s\' is enabled.'
msg_report['assert_enabled_fail'] = 'Assertion failed. The element \'%s\' is NOT enabled.'
msg_report['assert_enabled_abort'] = 'Assertion failed. The element \'%s\' is NOT enabled. Aborting the execution.'

# def verify_disabled():
msg_report['verify_disabled'] = 'Validate that the element \'%s\' is disabled.'
msg_report['verify_disabled_fail'] = 'Validation failed. The element \'%s\' is NOT disabled.'

# def assert_disabled():
msg_report['assert_disabled'] = 'Assert that the element \'%s\' is disabled.'
msg_report['assert_disabled_fail'] = 'Assertion failed. The element \'%s\' is NOT disabled.'
msg_report['assert_disabled_abort'] = 'Assertion failed. The element \'%s\' is NOT disabled. Aborting the execution.'

# def wait_for_loading
msg_report['wait_for_loading_timeout_fail'] = 'Waiting on loading more than %s seconds. Aborting.'

# def wait_for_any_value
msg_report['wait_for_any_value'] = 'Waiting for the element %s to have any value.'
msg_report['wait_for_any_value_timeout_fail'] = 'Waiting on more than %s seconds for the element %s to have a value.'

# def wait_for_value
msg_report['wait_for_value'] = 'Waiting for the element %s to have the value \'%s\'.'
msg_report['wait_for_value_no_element_fail'] = 'Could not find the element %s on screen.'
msg_report['wait_for_value_timeout_fail'] = 'Waiting on more than %s seconds for the element %s to have the value \'%s\'.'

msg_report['wait_for_clickable'] = 'Waiting for the element \'%s\' to be clickable.'
msg_report['wait_for_clickable_timeout_fail'] = 'Timeout exceeded while waiting for element \'%s\' to be clickable. (%s seconds)'

# def verify_contains
msg_report['verify_contains'] = 'Verify that the element \'%s\' contains the value \'%s\'.'
msg_report['verify_contains_fail'] = 'Validation failed. Element does not contains the value \'%s\'. Element value is \'%s\'.'

# def assert_contains
msg_report['assert_contains'] = 'Assert that the element \'%s\' contains the value \'%s\'.'
msg_report['assert_contains_fail'] = 'Assertion failed. The element \'%s\' does not contains the value \'%s\'. Element value is \'%s\'.'
msg_report['assert_contains_abort'] = 'Assertion failed. The element \'%s\' does not contains the value \'%s\'. Element value is \'%s\'. Aborting the execution.'
