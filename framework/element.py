import re
from selenium.webdriver.common.by import By
from framework.utils import tu

# Class: Element
# Stores the element properties that are needed by the framework.
#
# Author:
# Danilo Guimaraes <danilo.guimares@catalinamarketing.com>
class Element:

	# Set the default values for an element
	element_name = 'not defined'
	element_description_type = 'not_defined'
	element_description = 'not_defined'

	# Constructor: Element
	# Initialize the element with all properties.
	#
	# Parameters:
	# - element_name: Name of the element
	# - element_description_type: Type of element description. (xpath, css, etc.)
	# - element_description: Description of the element. (ex.: the xpath)
	def __init__(self, element_name, element_description_type, element_description):

		self.element_name = element_name
		self.element_description_type = tu(element_description_type)
		self.element_description = element_description

	# Method: get_by
	# Return the Selenium By object accordingly to the type of description it has.
	# Currently, only xpath supported
	def get_by(self):
		if self.element_description_type == "XPATH":
			return By.XPATH

	# Method: get_description
	# Gets the element description of the object. (xpath, css selector, etc.)
	def get_description(self):
		return self.element_description

	# Method: set_index
	# Sets the index of an element using xpath. If there is no index markup in the
	# element description, add it.
	def set_index(self, index):
		if self.element_description_type == "XPATH":

			# If there is no index in the xpath, encapsulate the xpath in the index markup
			if re.search("\(.*\)\[[0-9]*]", self.element_description.strip()) is None:
				self.element_description = "(%s)[%i]" % (self.element_description.strip(), int(index))
			else:
				to_remove = re.search("\[[0-9]*]$", self.element_description.strip()).group(0)
				self.element_description = self.element_description.replace(to_remove, "")
				self.element_description = "%s[%i]" % (self.element_description.strip(), int(index))
