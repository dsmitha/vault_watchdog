from datetime import datetime
from datetime import timedelta
import re

# Title: Utils
# This is the default utility library for the Abstraction Layer Framework.
# Any function that will make your life easier and that is not in the list below should be here:
#
# - Directly related to your applicaton (ex.: create_program());
# - Directly related to framework stuff like reporting or test settings;
#
# If you have a bunch of functions that are part of something bigger, consider creating your own
# library for that. (webservice or database functions, for example)

# Function: tu
# Returns an string in uppercase and without trailing spaces.
# "tu" stands for TrimUcase
#
# Author:
# Danilo Guimaraes <danilo.guimares@catalinamarketing.com>
def tu(string):
	return string.strip().upper()

def tl(string):
	return string.strip().lower()

def et(offset):
	return ct() + timedelta(seconds=offset)

def ct():
	return datetime.now()

def ie(value):
	return tu(value) == ""

def clear_xpath(string):
	if re.search("\(.*\)\[[0-9]*]$", string) is not None:
		to_remove = re.search("\[[0-9]*]$", string.strip()).group(0)
		string = string.replace(to_remove, "")

	return string

def take_screenshot(filename):
	import framework.abstraction_layer
	framework.abstraction_layer.get_driver().save_screenshot(filename)
