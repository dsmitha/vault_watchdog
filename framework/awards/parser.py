# File: awards.awards_parser.py
# This file contains the classes that can be used for parsing Awards test cases.
# Those classes are able to load test cases from ALM or parsing test cases from
# structured keyword text files. It is important to mention that as the
# ALMTestParser uses the OTA library from ALM, this is not a cross-platform
# solution and works only in Windows.
#
# Author:
# Danilo Guimaraes

import pdb

# Framework imports
from framework.awards.keywords import keywords
from framework.awards.keywords1 import keywords1
from framework.utils import tl
from framework.alm import ALMTestCase
from config import *

# Python imports
import re
import time
from html.parser import HTMLParser

# Class: AwardsFileParser
# This class parses an awards keyword text file and store all function calls
# related to it. It has a method that runs all those keywords, so that it can
# run the actual test case by reading the keywords from the file.
#
# This is not being used as of now because the tests for the current solution
# are using the ALM Test parser. Still, that might be needed to add backwards
# compatibility in the future.
#
# Author:
# Danilo Guimaraes
class AwardsFileParser:

    # Method: __init__
    # Initializes the parser with a test case file.
    #
    # Author:
    # Danilo Guimaraes (danilo.guimaraes@catalinamarketing.com)
    def __init__(self, filename):
        self.file = open(filename).read()
        self.calls = []

    # Method: parse_file
    # Reads the file that has been loaded and splits each keyword and parameters
    # into a array ready to be used by the method run_main()
    #
    # Author:
    # Danilo Guimaraes
    def parse_file(self):

        # Begin by creating an array with each row
        self.file = self.file.split("\n")

        # Then, split each row by the tabs
        self.file = [row.split("\t") for row in self.file]

        # Now, store the function calls
        for i, row in enumerate(self.file):

            # Test if the first entry of the row is empty
            if row[0].strip() != "":

                # Create a new list for the statement
                current_statement = []

                # Store the statements as a list. The first entry is the call name
                # and the subsequent entries are the parameters
                for j, s in enumerate(row):
                    current_statement.append(s)

                # Add the call to the list of calls
                self.calls.append(current_statement)

    # Method: run_main
    # Calls all funcions for each keyword found in the file.
    # Functions should be registered at the file awards_keywords.py
    def run_main(self):
        # Call all functions
        for c in self.calls:
            # Call the function for the keyword and pass the parameters array as
            # an argument.
            pdb.set_trcae()
            if tl(c[0]) in keywords:
                keywords[tl(c[0])](*c[1:])
                print(keywords[tl(c[0])](*c[1:]))

            else:
                print("Ignoring " + c[0] + ". Not implemented yet.")

# Class: ALMTestParser
# This class connects to ALM and get a test case from there. This test case is
# then parsed and divided into an array with the list of functions and their
# respective parameters, so that it can run the test case straight from ALM.
#
# Author:
# Danilo Guimaraes
class ALMTestParser:

    # Method: __init__
    # Connects to ALM and loads the connection specified in the config file.
    # (config.py) in the project root.
    def __init__(self):
        self.test_case = ALMTestCase(alm_server_path, alm_domain, alm_project, alm_username, alm_password)
        print (self.test_case)

        self.calls = []

    # Method: load_test_case
    # Loads a test case from ALM and parses it. The test case is broken into
    # a array that carries a group of dictionaries. Each dictionary is a
    # function call that carries the function name and a list of pairs of
    # parameters and values.
    #
    # Author:
    # Danilo Guimaraes
    def load_test_case(self, test_case_id = None, test_case_name = None):

        # Load the test case from ALM
        self.test_case.load_test_case(test_case_id, test_case_name)


        # Store each function call with the parameters
        for step in self.test_case.steps:

            # Initialize an empty statement
            current_statement = []
            parameters = {}

            # Get the step description, split by new Line, clean it up and
            # convert to lowercase
            step_description = step[1].replace("\t", "").split("\n")

            # Cleanup any HTML tag
            for i, step_row in enumerate(step_description):
                step_description[i] = re.sub(r'<[^>]+>', "", step_row)

            # Get the function name and other parameters:
            for row in step_description:
                # Look for "[Action Word]" and replace it by empty
                if re.match("(?i)\[ *action +word *\]", row) is not None:
                    to_replace = re.match("(?i)\[ *action +word *\]", row).group(0)
                    function_name = row.replace(to_replace, "").strip()
                    current_statement.insert(0, function_name)

                # Append the parameters
                elif re.match("\[.*?\]", row) is not None:

                    # Get the parameter name
                    param_name = re.match("\[.*?\]", row).group(0)


                    # Prepare the value and the parameter name to be stored
                    #print(row)
                    #print(row.replace(param_name, ""))
                    value = str(row.replace(param_name, "").strip())
                    #print(value)

                    param_name = param_name.replace("[", "").replace("]", "").strip().lower()


                    # Store it
                    parameters[param_name] = value


            # Append the parameter to the call
            current_statement.append(parameters)


            # Add the statement to the calls Array
            self.calls.append(current_statement)

    # Method: run_main
    # Calls all funcions for each keyword found in the file.
    # Functions should be registered at the file awards_keywords.py
    def run_main(self): #,test_case_file):
        # Call all functions
        #print(test_case_name)
        #
        for c in self.calls:
            # Call the function for the keyword and pass the parameters array as
            # an argument.
            #print(str(c[1]))
            #
            if tl(c[0]) in keywords:
                print("dfjghdgjhdf")
                test_case_file.write(keywords1[tl(c[0])]+"({")
                for (key,value) in c[1].items():
                    test_case_file.write('"'+key+'":"'+value+'",')
                #test_case_file.write(c[1])
                test_case_file.write("})\n")
                print (keywords1[tl(c[0])])
                print((c[1]))
                #
                #keywords[tl(c[0])](c[1])

                #

            else:

                print("Ignoring " + c[0] + ". Not implemented yet.")
