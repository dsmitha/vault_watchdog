# File: awards.utils.py

import pdb

# Framework imports
from framework.reporting import *
from framework.awards.trapi import trapi
from framework.awards.trapi import TrapiStatus
from framework.awards.defaults import *
from framework.utils import *
from framework.awards.settings import machine_settings as ms
from config import keep_cmd_on_error

# Python imports
import re
import sys
import os
import shutil
import filecmp

# Function: stablish_connection
# Tries to connect to the awards box during a certain amount of time.
#
# Parameters:
# - ip: IP address of the awards box
# - port: port of the awards box
# - timeout: max time to wait for connection. (optional)
#
# Author:
# Danilo Guimaraes
def stablish_connection(ip, port, timeout = DEFAULT_TIMEOUT):

    # Try to connect to the remote machine
    end_time = et(timeout)
    while trapi.status != TrapiStatus.connected and ct() < end_time:
        trapi.close()
        trapi.connect(ip, port)
        time.sleep(1)

    # If not connected after the timeout, fail and return None
    if trapi.status != TrapiStatus.connected:
        return False

    return True

def close_connection():
    trapi.close()

# Function: send_keys
# Send keys to the awards test box accordingly to what has been converted by the
# function generate_ascii_keystrokes.
#
# It now supports an optional parameter called interval, that allows to type
# values in a different pace than usual. The default is zero, so it goes as fast
# as possible.
#
# Author:
# Danilo Guimaraes
def send_keys(value, interval = 0):

    # Initialize some stuff
    error_stack = []

    add_sub_step("[send_keys] Send the following keys to the awards box: %s." % value)

    if not stablish_connection(ms.awards_machine_ip, ms.awards_machine_port_number):
        fail_sub_step("[send_keys]: Could not connect to remote machine via IP %s:%s." % (ms.awards_machine_ip, ms.awards_machine_port_number))
        return False

    # Get the ascii codes
    keystrokes = generate_ascii_keystrokes(value)


    # As it has been connected now, send the keys
    for key in keystrokes:

        if not trapi.send_key_events(key[0], key[1]):
            error_stack.append(key)

    # Check if we had errors
    if len(error_stack) > 0:
        fail_sub_step("[send_keys]: Some keys could not be sent to the remote machine: %s." % (str(error_stack)) )

    return True

# Function: generate_ascii_keystrokes
# This is the function that generates keystrokes for the awards box. Trying to be
# really simple here, the way it works is different than the original version.
#
# Basically, any special key needs to be enclosed by [ and ]. Every keystroke
# is going to be a sequence of key_press and key_release. If you want to send
# just the keypress (so the key will be "held"), you can add a ! before or after
# the key name. To send just the key_release event, you can add a . instead.
# If you want to use Shift, just go ahead and use [shift], using the same ! and
# . codes for key_press and key_release.
#
# The function itself will convert the characters and special keytrokes into
# ascii code. The ASCII code the TRAPI uses is COMPLETELY different than the
# regular ascii table. So please, make sure you take a look at the reference.
#
# Pretty much, the differences are:
# - Only lowercase letters (and they are where the uppercase letters shold be)
# - Symbols are in completely different places
# - This is an extended codepage, so it has 256 bytes
# - Numbers are in the expected place, but we also have keypad numbers in another
#   place.
#
# The idea of the "conversion" to TRAPI is:
# - Any lowecase key is converted to uppercase code, so they remain lowercase in
#   TRAPI. (nice, huh?)
# - Any uppercase key will trigger the shift flag, so we can keep the case as
#   it has been typed in the code.
# - Most used symbols have been converted to the equivalend in the trapi ascii
#   table.
#
# The only exception we have for escaping characters is to use \ to escape the
# brackets when using brackets in a string. Not escaping brackets will make the
# [code think that this is a very, very long keyword] that will be ignored. Do
# it like this: \[ and \].
#
# By the way, this is *not* case sensitive. :)
#
# Only one very important thing: it *literally* emulates a keyboard. So, some
# characters that are shifted when we type them (: for example) actually
# use the ascii code from the non-shifted character (;) and need to have shift
# appended to it, so that is press *shift + ;* that is actually *:* .
#
# Examples:
# - Sending ctrl+alt+del: [ctrl!][alt!][del!]
# - Sending shift+esc: [shift!][esc][shift.]
#
# List of  all accepted values:
# - ENTER
# - CTRL
# - ALT
# - SHIFT
# - ESC
# - F1
# - F2
# - F3
# - F4
# - F5
# - F6
# - F7
# - F8
# - F9
# - F10
# - F11
# - F12
# - BACKSPACE
# - TAB
# - CAPSLOCK
# - SCROLLLOCK
# - PAUSE
# - INS
# - HOME
# - PGUP
# - DEL
# - END
# - PGDN
# - UP
# - LEFT
# - DOWN
# - RIGHT
# - NUMLOCK
# - KP_DIV
# - KP_MULT
# - KP_SUBTRACT
# - KP7
# - KP8
# - KP9
# - KP_ADD
# - KP4
# - KP5
# - KP6
# - KP1
# - KP2
# - KP3
# - KP0
# - KP_DOT
#
# Author:
# Danilo Guimaraes
def generate_ascii_keystrokes(value):

    # Initialize some values
    keys = []
    ascii_list = {}
    shift_flags = []

    # This list shouldn't be changed, really :)
    # This is the list of ascii codes used to convert the keystrokes.
    # Special keywords
    ascii_list["ENTER"] = 13
    ascii_list["CTRL"] = 17
    ascii_list["ALT"] = 18
    ascii_list["SHIFT"] = 16
    ascii_list["ESC"] = 27
    ascii_list["F1"] = 112
    ascii_list["F2"] = 113
    ascii_list["F3"] = 114
    ascii_list["F4"] = 115
    ascii_list["F5"] = 116
    ascii_list["F6"] = 117
    ascii_list["F7"] = 118
    ascii_list["F8"] = 119
    ascii_list["F9"] = 120
    ascii_list["F10"] = 121
    ascii_list["F11"] = 122
    ascii_list["F12"] = 123
    ascii_list["BACKSPACE"] = 8
    ascii_list["TAB"] = 9
    ascii_list["CAPSLOCK"] = 20
    ascii_list["SCROLLLOCK"] = 145
    ascii_list["PAUSE"] = 19
    ascii_list["INS"] =  45
    ascii_list["HOME"] = 36
    ascii_list["PGUP"] = 33
    ascii_list["DEL"] = 46
    ascii_list["END"] = 35
    ascii_list["PGDN"] = 34
    ascii_list["UP"] = 38
    ascii_list["LEFT"] = 37
    ascii_list["DOWN"] = 40
    ascii_list["RIGHT"] = 39
    ascii_list["NUMLOCK"] = 144
    ascii_list["KP_DIV"] = 111
    ascii_list["KP_MULT"] = 106
    ascii_list["KP_SUBTRACT"] = 109
    ascii_list["KP7"] = 103
    ascii_list["KP8"] = 104
    ascii_list["KP9"] = 105
    ascii_list["KP_ADD"] = 107
    ascii_list["KP4"] = 100
    ascii_list["KP5"] = 101
    ascii_list["KP6"] = 102
    ascii_list["KP1"] = 97
    ascii_list["KP2"] = 98
    ascii_list["KP3"] = 99
    ascii_list["KP0"] = 96
    ascii_list["KP_DOT"] = 110

    # Those keys need to be converted to keyboard code
    ascii_list[59] = 186 # ;
    ascii_list[58] = 186 # : and append SHIFT to it
    ascii_list[61] = 187 # =
    ascii_list[44] = 188 # ,
    ascii_list[45] = 189 # -
    ascii_list[95] = 189 # _ and append SHIFT to it
    ascii_list[46] = 190 # .
    ascii_list[47] = 191 # /
    ascii_list[92] = 220 # \
    ascii_list[91] = 235 # [
    ascii_list[93] = 237 # ]
    ascii_list[39] = 222 # '
    ascii_list[34] = 222 # " and append SHIFT to it
    ascii_list[43] = 107 # +
    ascii_list[45] = 109 # -
    ascii_list[42] = 106 # *
    ascii_list[46] = 190 # .
    ascii_list[62] = 190 # > and append SHIFT to it
    ascii_list[63] = 191 # / and append SHIFT to it
    ascii_list[37] = 53 # % and append shift to it
    ascii_list[60] = 188 # < and append shift to it
    ascii_list[64] = 50 # @ and append shift to it
    ascii_list[40] = 57 # ( and append shift to it
    ascii_list[41] = 48 # ) and append shift to it
    ascii_list[33] = 49 # ! and append shift to it


    # Those keys use another ascii code and need to have shift in order
    # to send the proper thing
    shift_flags = [58, 95, 62, 63, 37, 60, 64, 34, 40, 41, 33]

    # First of all, split the value in keywords and characters
    # \\\[ - Will pick the escaped bracket
    # \\\] - Will pick the other escaped bracket
    # \[.+?\] - Will pick special keys
    # . - Will pick any other character
    keystrokes = re.findall(r"\\\[|\\\]|\[.+?\]|.",value)

    # Go through each keystroke and start changing them
    for i, key in enumerate(keystrokes):

        # Some flags
        shift_flag = False
        hold_flag = None
        conversion_flag = False

        # For brackets, just add them without \
        if re.match(r"\\\[", key) is not None:
            ascii_key = ascii_list[ord("[")]

        elif re.match(r"\\\]", key) is not None:
            ascii_key = ascii_list[ord("]")]

        # For special keys, make some handling
        elif re.match("\\[.+?\\]", key) is not None:

            # Cleanup the keyword and remove the brackets
            key = key.strip().upper()[1:-1]

            # Check for ! and . and mark the flag
            if key.find("!") != -1:
                hold_flag = True
                key = key.replace("!", "")

            elif key.find(".") != -1:
                hold_flag = False
                key = key.replace(".", "")

            # Get the ascii. If not found, ignore the keyword.
            if key in ascii_list:
                ascii_key = ascii_list[key]
            else:
                continue

        # Normal characters, just add them
        else:

            ascii_key = ord(key)

            if ascii_key in shift_flags:
                shift_flag = True

            # Convert lowerkey to actual lowerkey
            if ascii_key in range(97, 123):
                ascii_key -= 32 # Convert to uppercase,
                                # that is actually lowercase in TRAPI

            # If this is uppercase, need to add shift
            elif ascii_key in range(65,91):
                shift_flag = True

            # If this is in the list of conversion, do the conversion
            elif ascii_key in ascii_list:
                ascii_key = ascii_list[ascii_key]


        # Shift key check
        if shift_flag:
            keys.append([ascii_list["SHIFT"], KEY_PRESS])

        # Append the keys
        if hold_flag is None:
            keys.append([ascii_key, KEY_PRESS])
            keys.append([ascii_key, KEY_RELEASE])

        elif hold_flag == True:
            keys.append([ascii_key, KEY_PRESS])

        elif hold_flag == False:
            keys.append([ascii_key, KEY_RELEASE])

        # Release shift key
        if shift_flag:
            keys.append([ascii_list["SHIFT"], KEY_RELEASE])

    return keys

# Function: get_console_text
# Retrieves the text from the awards console, returning it in a string.
#
# Author:
# Danilo Guimaraes
def get_console_text():

    # Initialize some Values
    screen = None

    # Try to connect to the remote machine
    if not stablish_connection(ms.awards_machine_ip, ms.awards_machine_port_number):
        fail_sub_step("[get_console_text]: Could not connect to remote machine via IP %s:%s." % (ms.awards_machine_ip, ms.awards_machine_port_number))
        return None

    # Get the screen
    end_time = et(DEFAULT_TIMEOUT)
    screen = trapi.get_console_screen()
    while (screen is None or screen.find("Error Getting Console Information!") != -1) and ct() < end_time:
        time.sleep(1)
        screen = trapi.get_console_screen()

    # Return the screen (even if it is none)
    return screen

# Function: find_text_in_console
# Gets the console text and tries to find a text in it. The lookup is
# case-sensitive.
#
# Parameters:
# - value: the value you want to search for in the console. A regular expression
# in case regex_flag is True.
# - regex_flag: Flags if a regular expression is being used. Optional.
#
# Returns:
# - True or false, accordingly to the results of the lookup.
#
# Author:
# Danilo Guimaraes
def find_text_in_console(value, regex_flag = False):

    # Get the console text
    console_text = get_console_text()

    # Test if the console text could be found
    if console_text is None:
        return False

    # If the regex flag has been provided, match by regular expression
    if regex_flag:

        # Try to match the expression in the screen
        match = re.findall(value, console_text)
        if len(match) > 0:
            return True

    # Else, do a regular find.
    else:

        if console_text.find(value) != -1:
            return True

    return False

# Function: match_text_in_console
# Gets the console text and tries to match a regular expression. In case it
# it works, it returns the first occurence of that regular expression.
#
# Parameters:
# - value: Regular expression to be matched.
#
# Returns:
# None if nothing is found or the first match for the regular expression
# provided.
#
# Author:
# Danilo Guimaraes
def match_text_in_console(value,match_all="No"):

    # Get the console text
    console_text = get_console_text()

    if console_text is None:
        return None

    # Try to match the expression in the screen
    match = re.findall(value, console_text)

    if len(match) > 0:
        if tu(match_all)=="YES":
            return match
        else:
            return match[0]
    else:
        return None

# Function: get_console_screen_area
# Gets a specified area from the console screen. That area is defined by
# character-coordinates (the screen has 80x25 characters). X1 and X2 should not
# be the same. Also, Y1 and Y2 should not be the same.
#
# Parameters:
# - x1: Leftmost character coordinate.
# - x2: Rightmost character coordinate.
# - y1: Upmost character coordinate.
# - y2: Downmost character coordinate.
#
# Returns:
# A string with the area of the screen that has been defined in the parameters.
#
# Author:
# Danilo Guimaraes
def get_console_screen_area(x1, y1, x2, y2):

    # Initialize some variables
    screen_area = []

    # Get the console text
    console_text = get_console_text()

    # Split the screen
    console_text = console_text.split("\n")

    # Get the screen area
    for row in console_text[y1:y2]:
        screen_area.append(row[x1:x2])

    # Join it in one string
    screen_area = "\n".join(screen_area)

    # Return it
    return screen_area

# Function: wait_for_connection
# Waits for the remote machine to allow connections in the TRAPI port.
#
# Parameters:
# - timeout: A custom timeout, that is going to be the maximum amount of time
# that the function is going to wait for the remote machine to be accessible.
# Optional parameter, will wait for whatever is defined in the DEFAULT_TIMEOUT
# by default.
#
# Author:
# Danilo Guimaraes
def wait_for_connection(timeout=DEFAULT_TIMEOUT):

    if not stablish_connection(ms.awards_machine_ip, ms.awards_machine_port_number, timeout):
        fail_sub_step("[wait_for_connection]: Could not connect to remote machine via IP %s:%s after %d seconds." % (ms.awards_machine_ip, ms.awards_machine_port_number, timeout))
        sys.exit(0)

# Function: wait_for_connection
# Waits for the remote machine to NOT allow connections in the TRAPI port.
#
# Parameters:
# - timeout: A custom timeout, that is going to be the maximum amount of time
# that the function is going to wait for the remote machine to be unaccessible.
# Optional parameter, will wait for whatever is defined in the DEFAULT_TIMEOUT
# by default.
#
# Author:
# Danilo Guimaraes
def wait_for_no_connection(timeout=DEFAULT_TIMEOUT):

    end_time = et(timeout)
    while trapi.status == TrapiStatus.connected and ct() < end_time:
        trapi.close()
        time.sleep(1)
        trapi.connect(ms.awards_machine_ip, ms.awards_machine_port_number)

    # If not connected after the timeout, fail and return None
    if trapi.status == TrapiStatus.connected:
        fail_sub_step("[wait_for_no_connection]: Waited for the machine at %s:%s to be unavailable for more than %d seconds." % (ms.awards_machine_ip, ms.awards_machine_port_number, timeout))
        sys.exit(0)

# Function: wait_for_text_in_console
# Wait for a certain text to be displayed in the console screen.
#
# Parameters:
# - value: Text to be found on screen or regular expression to be matched.
# - regex_flag: Indicates if value is a regular expression or not.
# - timeout: Custom timeout that is going to be used as the maximum wait time
# for the function. Optional parameter, will wait for DEFAULT_TIMEOUT by
# default.
#
# Returns:
# True in case the text has been found (or matched) in the screen. Else, it
# returns false.
#
# Author:
# Danilo Guimaraes
def wait_for_text_in_console(value, regex_flag = False, timeout=DEFAULT_TIMEOUT):

    # Initialize some variables
    end_time = et(timeout)

    # Wait for the value on screen
    while ct() < end_time:
        if find_text_in_console(value, regex_flag):
            return True
        time.sleep(1)

    return False

# Function: get_ini_file_value
# Gets the value from a specific section in an ini file in the awards machine.
#
# Parameters:
# - filename: Name (and path) of the .ini file to be used.
# - section: Section of the .ini file contains the value that is being looked
# for.
# - value: Value that has to be retrieved from the .ini file.
#
# Returns:
# Return the value that has been retirved from the ini file. If the value
# doesn't exists, just return None.
#
# Author:
# Danilo Guimaraes
def get_ini_file_value(filename, section, value):

    # Initialize some values
    entry_value = None

    add_sub_step("Get the following information from INI file: section %s, value %s at file %s." % (section, value, filename) )

    # If not connected after the timeout, fail and return None
    if not stablish_connection(ms.awards_machine_ip, ms.awards_machine_port_number):
        fail_sub_step("[get_ini_file_value]: Could not connect to remote machine via IP %s:%s." % (ms.awards_machine_ip, ms.awards_machine_port_number))
        return False

    # Get the ini file Value
    end_time = et(DEFAULT_TIMEOUT)
    entry_value = trapi.get_ini_entry(section, value, filename)
    while entry_value is None and ct() < end_time:
        time.sleep(1)
        entry_value = trapi.get_ini_entry(section, value, filename)

    return entry_value

# Function: get_file_contents
# Gets the contents of a file from the Awards box.
#
# Parameters:
# - filename: name of the file in which the contents have to be retrieved.
#
# Returns:
# The contents of the file that has been passed as parameter.
#
# Author:
# Danilo Guimaraes
def get_file_contents(filename):

    # Initialize some values
    text_contents = None

    # Try to connect to the remote machine
    if not stablish_connection(ms.awards_machine_ip, ms.awards_machine_port_number):
        add_sub_step("Get the text contents from '%s'." % filename )
        fail_sub_step("[get_file_contents]: Could not connect to remote machine via IP %s:%s." % (ms.awards_machine_ip, ms.awards_machine_port_number))
        return False

    # Get the ini file Value
    end_time = et(DEFAULT_TIMEOUT)
    text_contents = trapi.file_contents_txt(filename)
    while text_contents is None and  ct() < end_time:
        time.sleep(1)
        text_contents = trapi.file_contents_txt(filename)

    # Return the file without |E@N@D|\x00 in the end
    return text_contents.rsplit("|", 2)[0]

def get_file_contents_bin(filename):

    # Initialize some values
    file_contents = None

    # Try to connect to the remote machine
    if not stablish_connection(ms.awards_machine_ip, ms.awards_machine_port_number):
        add_sub_step("Get the text contents from '%s'." % filename )
        fail_sub_step("[get_file_contents_bin]: Could not connect to remote machine via IP %s:%s." % (ms.awards_machine_ip, ms.awards_machine_port_number))
        return False

    # Get the ini file Value
    end_time = et(DEFAULT_TIMEOUT)
    file_contents = trapi.file_contents_bin(filename)
    while file_contents is None and ct() < end_time:
        time.sleep(1)
        file_contents = trapi.file_contents_bin(filename)

    # Return the file contents
    return file_contents

# Function: finish_execution
# Do some handling before finishing the test execution. Right now, it only
# adds a report message stating that the thing is going to abort.
#
# Author:
# Danilo Guimaraes
def finish_execution():
    add_sub_step("Aborting execution.")
    fail_sub_step("Test has been aborted due to critical errors during test execution.")
    #
    # (report will be generated automatically by the atexit() handler)
    if keep_cmd_on_error:
        sys.exit(1)
    else:
        sys.exit(0)

# Function: tail
# Works like the comand "tail -f". It will look for a value in the last rows of
# a file during a certain amount of time. Depending of the size of the files
# being tailed, that can be a resource hog. (there is no way to just get the
# last rows of a file thru the API, so it needs to load all the thing, break
# into rows, reverse and then find the value... quite an effort :P)
#
# Parameters:
# - filename: name of the file to be tailed.
# - value: value to be looked for. Case sensitive.
# - regex_false: Use true if using regular expressions. Optional, by default is
# false.
#
# Returns: False in case it has not found the value else it returns True.
#
# Author:
# Danilo Guimaraes
def tail(filename, value, regex_flag=False, timeout = DEFAULT_TIMEOUT):

    add_sub_step("Tail the file '%s' in order to find the value '%s'." % (filename, value))

    file_contents = get_file_contents(filename)
    if file_contents is None:
        # something wrong happened
        fail_sub_step("[tail] Could not get the file '%s'." % filename)

    # Set the timeout
    end_time = et(timeout)
    while ct() < end_time:

        # Get the file contents
        file_contents = get_file_contents(filename)

        # Get the last 20 lines from the file
        rows = file_contents.split("\n")
        if len(rows) >= 50:
            rows = rows[-50:]
            rows = "\n".join(rows)
        else:
            rows = "\n".join(rows)

        # Check if the contents are there
        if regex_flag:
            match = re.findall(value, rows)
            if len(match) > 0:
                return True

        else:
            if rows.find(value) != -1:
                return True

        time.sleep(3)

    fail_sub_step("[tail] Could not find '%s' in '%s' after '%d' seconds." % (value, filename, timeout))

    return False

# Function: bottom_row
# Captures the bottomost row of the screen with any contents in it.
#
# Parameters:
# - screen: The screen to be used.
#
# Returns:
# The bottom row with contents.
#
# Author:
# Danilo Guimaraes
def bottom_row(screen):

    # Split it
    screen = screen.split("\n")[:-1]

    for row in reversed(screen):
    	if row.strip() != "":
    		return row

    return None

# Function: wait_for_bottom_row
# Waits for the bottom row to have an expected value.
#
# Parameters:
# - value: The value to be expected or a regular expression in case regex_flag
# is True.
# - regex_flag: True or False, indicates if value is a regular expression.
# - timeout: total amount of time to wait for the value to show up in the bottom
# row of the screen. Option, defaults to DEFAULT_TIMEOUT.
#
# Returns:
# True in case the value has been found, False in case the timeout has been
# reached.
#
# Author:
# Danilo Guimaraes
def wait_for_bottom_row(value, regex_flag = False, timeout=DEFAULT_TIMEOUT):

    # Initialize some variables
    end_time = et(timeout)

    # Wait for the value on screen
    while ct() < end_time:
        console_screen = get_console_text()

        if console_screen is None:
            continue

        if regex_flag:
            if len(re.findall(value, bottom_row(console_screen))) > 0:
                return True

        else:
            if bottom_row(console_screen).find(value) != -1:
                return True

        time.sleep(1)

    return False

# Function: take_screenshot
# Captures the console screen text and then:
# - Converts any special character to the equivalent unicode in html format.
# - Encloses it with HTML tags and CSS for formatting purposes
# - Attach the screenshot to the report.
#
# The characters from screen comes encoded in cp437. They need to be converted
# to unicode. Everything < 128 is the same, after that the codes start to change.
# As reference, the following table has been used:
# ftp://ftp.unicode.org/Public/MAPPINGS/VENDORS/MICSFT/PC/CP437.TXT
#
# Author:
# Danilo Guimaraes
def take_screenshot():

    # This is a cp437 -> ASCII conversion table, taken from
    # HERE: ftp://ftp.unicode.org/Public/MAPPINGS/VENDORS/MICSFT/PC/CP437.TXT
    conversion = {
        '0x80': "&#x00c7;",
        '0x81': "&#x00fc;",
        '0x82': "&#x00e9;",
        '0x83': "&#x00e2;",
        '0x84': "&#x00e4;",
        '0x85': "&#x00e0;",
        '0x86': "&#x00e5;",
        '0x87': "&#x00e7;",
        '0x88': "&#x00ea;",
        '0x89': "&#x00eb;",
        '0x8a': "&#x00e8;",
        '0x8b': "&#x00ef;",
        '0x8c': "&#x00ee;",
        '0x8d': "&#x00ec;",
        '0x8e': "&#x00c4;",
        '0x8f': "&#x00c5;",
        '0x90': "&#x00c9;",
        '0x91': "&#x00e6;",
        '0x92': "&#x00c6;",
        '0x93': "&#x00f4;",
        '0x94': "&#x00f6;",
        '0x95': "&#x00f2;",
        '0x96': "&#x00fb;",
        '0x97': "&#x00f9;",
        '0x98': "&#x00ff;",
        '0x99': "&#x00d6;",
        '0x9a': "&#x00dc;",
        '0x9b': "&#x00a2;",
        '0x9c': "&#x00a3;",
        '0x9d': "&#x00a5;",
        '0x9e': "&#x20a7;",
        '0x9f': "&#x0192;",
        '0xa0': "&#x00e1;",
        '0xa1': "&#x00ed;",
        '0xa2': "&#x00f3;",
        '0xa3': "&#x00fa;",
        '0xa4': "&#x00f1;",
        '0xa5': "&#x00d1;",
        '0xa6': "&#x00aa;",
        '0xa7': "&#x00ba;",
        '0xa8': "&#x00bf;",
        '0xa9': "&#x2310;",
        '0xaa': "&#x00ac;",
        '0xab': "&#x00bd;",
        '0xac': "&#x00bc;",
        '0xad': "&#x00a1;",
        '0xae': "&#x00ab;",
        '0xaf': "&#x00bb;",
        '0xb0': "&#x2591;",
        '0xb1': "&#x2592;",
        '0xb2': "&#x2593;",
        '0xb3': "&#x2502;",
        '0xb4': "&#x2524;",
        '0xb5': "&#x2561;",
        '0xb6': "&#x2562;",
        '0xb7': "&#x2556;",
        '0xb8': "&#x2555;",
        '0xb9': "&#x2563;",
        '0xba': "&#x2551;",
        '0xbb': "&#x2557;",
        '0xbc': "&#x255d;",
        '0xbd': "&#x255c;",
        '0xbe': "&#x255b;",
        '0xbf': "&#x2510;",
        '0xc0': "&#x2514;",
        '0xc1': "&#x2534;",
        '0xc2': "&#x252c;",
        '0xc3': "&#x251c;",
        '0xc4': "&#x2500;",
        '0xc5': "&#x253c;",
        '0xc6': "&#x255e;",
        '0xc7': "&#x255f;",
        '0xc8': "&#x255a;",
        '0xc9': "&#x2554;",
        '0xca': "&#x2569;",
        '0xcb': "&#x2566;",
        '0xcc': "&#x2560;",
        '0xcd': "&#x2550;",
        '0xce': "&#x256c;",
        '0xcf': "&#x2567;",
        '0xd0': "&#x2568;",
        '0xd1': "&#x2564;",
        '0xd2': "&#x2565;",
        '0xd3': "&#x2559;",
        '0xd4': "&#x2558;",
        '0xd5': "&#x2552;",
        '0xd6': "&#x2553;",
        '0xd7': "&#x256b;",
        '0xd8': "&#x256a;",
        '0xd9': "&#x2518;",
        '0xda': "&#x250c;",
        '0xdb': "&#x2588;",
        '0xdc': "&#x2584;",
        '0xdd': "&#x258c;",
        '0xde': "&#x2590;",
        '0xdf': "&#x2580;",
        '0xe0': "&#x03b1;",
        '0xe1': "&#x00df;",
        '0xe2': "&#x0393;",
        '0xe3': "&#x03c0;",
        '0xe4': "&#x03a3;",
        '0xe5': "&#x03c3;",
        '0xe6': "&#x00b5;",
        '0xe7': "&#x03c4;",
        '0xe8': "&#x03a6;",
        '0xe9': "&#x0398;",
        '0xea': "&#x03a9;",
        '0xeb': "&#x03b4;",
        '0xec': "&#x221e;",
        '0xed': "&#x03c6;",
        '0xee': "&#x03b5;",
        '0xef': "&#x2229;",
        '0xf0': "&#x2261;",
        '0xf1': "&#x00b1;",
        '0xf2': "&#x2265;",
        '0xf3': "&#x2264;",
        '0xf4': "&#x2320;",
        '0xf5': "&#x2321;",
        '0xf6': "&#x00f7;",
        '0xf7': "&#x2248;",
        '0xf8': "&#x00b0;",
        '0xf9': "&#x2219;",
        '0xfa': "&#x00b7;",
        '0xfb': "&#x221a;",
        '0xfc': "&#x207f;",
        '0xfd': "&#x00b2;",
        '0xfe': "&#x25a0;",
        '0xff': "&#x00a0;",
        '0xa': "<br/>",
        '0x20': "&nbsp;"
    }

    # Get the screenshot
    screen = get_console_text()

    # Handle an error that is very likely to happen
    if screen is None:
        return "Not able to capture screen."

    # Convert the characters and build a new screen from it
    html_screen = ""
    for char in screen:
        if hex(ord(char)) in conversion:
            html_screen += conversion[hex(ord(char))]
        else:
            html_screen += char

    # After the screen has been captured, encapsulate it within a div with style
    before_tags = "<div  style='font-family: monospace; background-color:#000000; color:#FFFFFF; height: 375px; width: 616px; line-height: 15px;'>"
    after_tags = "</div>"
    html_screen = "%s%s%s" % (before_tags,html_screen,after_tags)

    # Add the screenshot to the report
    report.add_screenshot(html_screen, "console")

# Function: download_file
# Downloads a file from the awards box in binary format. Saves the file with the
# name passed as argument.
#
# Parameters:
# - file_to_download: Path of the file to be downloaded from the awards box.
# - new_file_name: Name of the file to be saved locally.
#
# Returns:
# - False in case the file is None. Else, returns True.
#
# Author:
# Danilo Guimaraes
def download_file(file_to_download, new_file_name):

    # Get the file contents
    awards_file = get_file_contents_bin(file_to_download)

    # Check if the file is good
    if awards_file is None:
        return False

    # Get the directory and create it if needed:
    directory = new_file_name.rsplit("\\",1)[0]
    if not os.path.exists(directory):
        os.makedirs(directory)

    # Create the file
    with open(new_file_name, "bw+") as f:
        f.write(awards_file)
        f.close()

    return True

# Function: zip_folder
# Zips a folder in the local machine.
#
# Parameters:
# - folder_path: folder that has to be zipped.
# - destination: Name of the file to be saved locally.
#
# Author:
# Danilo Guimaraes
def zip_folder(folder_path, destination):

    # zip it :)
    shutil.make_archive(destination, "zip", folder_path)


#Function Unzip_folder

def unzip_folder(folder_path,destination_path):
    #
    print(folder_path)
    #
    shutil.unpack_archive(folder_path,destination_path)
# Function: wait_for_prompt
# Wait until the prompt is returned to user control.
#
# Parameters:
# - current_folder: folder in which the terminal currently is.
#
# Author:
# Danilo Guimaraes
def wait_for_prompt(current_folder):
    wait_for_bottom_row(r"^%s> *$" % current_folder, True)

# Function: copy_local_file
# Copy a local file to somewhere else.
#
# Parameters:
# - filename: file that has to be copied.
# - destination: Name of the destination file.
#
# Author:
# Danilo Guimaraes
def copy_local_file(filename, destination):

    shutil.copyfile(filename, destination)

def check_process_is_running(process_name):

    # Try to connect to the remote machine
    if not stablish_connection(ms.awards_machine_ip, ms.awards_machine_port_number):
        fail_sub_step("[check_process_is_running]: Could not connect to remote machine via IP %s:%s." % (ms.awards_machine_ip, ms.awards_machine_port_number))
        return False

    # Get the list of processes
    processes = trapi.get_proc_list()

    # Try to find it
    if tu(processes).find(tu(process_name)) == -1:
        return False

    # Return true
    return True

def check_file_exists(filename):

    # Try to connect to the remote machine
    if not stablish_connection(ms.awards_machine_ip, ms.awards_machine_port_number):
        fail_sub_step("[check_file_exists]: Could not connect to remote machine via IP %s:%s." % (ms.awards_machine_ip, ms.awards_machine_port_number))
        return False

    return trapi.file_exists(filename)

def get_latest_file():

    console_text = get_console_text()
    #
    for file in console_text.split("\n"):
        if file=="" or re.findall("_",file):
            pass
        elif re.findall("xml$",file.strip()):
            file_name=file
            #
            print(file_name)
    #
            return file_name

def compare_file(file1,file2):

    if filecmp.cmp(file1,file2):
        return True
    else:
        return False

def get_file_size(filename):
    size=os.path.getsize(filename)
    return size
