# File: Defaults
#
# Filename:
# > framework/awards/defaults.py
#
# Description:
# This file carries the contant values for the awards automation. Pretty much
# anything you see in the code THAT_IS_WRITTEN_LIKE_THIS is a constant. That
# means this value is not meant to be changed during the execution and it is
# being retrieved from here.
#
# Notes:
# - The contants are import to place static values (file paths, default values,
#   etc) in one single place. If they need to be changed in the future, they can
#   be changed here.

# Default settings
from pathlib import Path
import pdb
base_path=str(Path(__file__).parent.parent.parent)
print (base_path)


DEFAULT_AWARDS_MACHINE_PORT_NUMBER = 3000
#DEFAULT_AWARDS_MACHINE_PORT_NUMBER = 3000
#DEFAULT_AWARDS_MACHINE_IP_ADDRESS = "192.168.1.3"
#DEFAULT_AWARDS_MACHINE_IP_ADDRESS = "192.168.56.102"
#DEFAULT_AWARDS_MACHINE_IP_ADDRESS = "10.10.112.57"
DEFAULT_AWARDS_MACHINE_IP_ADDRESS = "10.10.112.65"
DEFAULT_AWARDS_MACHINE_NAME = "Awards 2"

# Default paths
DEFAULT_AWARDS_STARTUP_FOLDER = r"C:\strsystm"
DEFAULT_AWARDS_CDS_DATABASE = r"CDSDB.zip"
DEFAULT_AWARDS_CDS_DATABASE_PATH = r"C:\strsystm\comm\a"
DEFAULT_AWARDS_SO_DATABASE_PATH = r"C:\strsystm\comm\a"
DEFAULT_AWARDS_SO_DEFAULT_DATABASE = r"SO0198ZP.198"
DEFAULT_CDS_JOB_FILE_PATH = r"C:\CDS\mirror\related_file"
DEFAULT_CDS_DATABASE_REPORT_PATH = r"C:\strsystm\REPORTS\CDSDATABASE.RPT"
DEFAULT_DATABASE_REPORT_PATH = r"C:\strsystm\REPORTS\DATABASE.RPT"
DEFAULT_STORE_INI_DEST_PATH = r"C:\strsystm\data\STORE.ini"
DEFAULT_SHUTTLE_INI_DEST_PATH = r"C:\strsystm\data\SHUTTLE.ini"
DEFAULT_CDS_STORE_INI_NAME = r"store.aaa"
DEFAULT_SO_STORE_INI_NAME = r"STR27X.INI"
DEFAULT_STORE_INI_ORIG_PATH = r"C:\strsystm\comm\a"
DEFAULT_SHUTTLE_INI_ORIG_PATH = r"C:\strsystm\comm\a\shuttle.shu"
REPORT_DATABASE_PATH = r"C:\strsystm\reports\DATABASE.RPT"
DEFAULT_TS_FILE_PATH = r"C:\strsystm\comm\a"
DEFAULT_LOCAL_TEMP_PATH = r"C:\temp\awards_auto\syslogs"
DEFAULT_ZIP_LOG_PATH = r"C:\temp\awards_auto\syslogs" # Do not add .zip to this one.
DEFAULT_SUT_TEMP_PATH = r"C:\temp\awards_auto"
DEFAULT_EVIDENCE_STORAGE_PATH = base_path+r"\evidences" #I:\SQA\shared\Automation\Projects\Awards
DEFAULT_DBLIST_GEN_PATH = r"C:\QuickCash.txt"
DEfAULT_IMAGES_PATH = r"C:\strsystm\IMAGES"
DEFAULT_DW_LOG_PATH = r"C:\strsystm\LOG\DW.log"
DEFAULT_XMLS_PATH  = r"C:\strsystm\XMLDB"
DEFAULT_BATCH_INI_PATH = r"C:\strsystm\data\BATCH.INI"
DEFAULT_TS_FILE_REPO = base_path+r"\data"#I:\SQA\shared\Automation\Projects\Awards
DEFAULT_IC_FILE_PATH = r"C:\strsystm\comm\a"
DEFAULT_IC_FILE_DESTINATION = r"C:\strsystm\comm"
DEFAULT_SOFTWARE_UPDATE_REPORT_PATH = r"C:\strsystm\reports\SOFTUPG.RPT"
DEFAULT_UPDATE_FILEPATH = r"C:\strsystm\comm\firstupd.zip"
DEFAULT_UPDATE_LOCAL_FILEPATH = r"C:\temp\awards_auto"
DEFAULT_CI_NIGHTLY_BUILD_PATH = r"\\stpshared\shared\Store_Systems\public\ci_builds\nightly"
DEFAULT_AWARDS_BARCODE_FILE_REPO= base_path+r"\barcode_file"#I:\SQA\shared\Automation\Projects\Awards
DEFAULT_STRSYSTM_DATA_PATH = r"C:\strsystm\data"
DEFAULT_VAULT_BUILDPATH=r"I:\SQA\SHARED\AUTOMATION\VAULT"
DEFAULT_ERROR_LOG_PATH=r"C:\strsystm\Log\ERRORLOG2.txt"
DEFAULT_WEBPOSLOG_PATH=r"c:\WEBPOS\web_services\nodejs"
DEFAULT_CMD_FILEPATH=r"I:\SQA\shared\Automation\Projects\BAT Testing\Scripts"
DEFAULT_CMD_FILE_DESTINATION=r"C:\strsystm\comm"
DEFAULT_NETOP_PATH=r"C:\Program Files (x86)\DanWare Data\NetOp Remote Control\Guest\ngstw32.exe"
DEFAULT_CAPT_LOG_PATH=r"C:\\strsystm\CAPTURE\ALL.CAP"
DEFAULT_CAPT_DEBUG_LOG_PATH=r"C:\\strsystm\CAPTURE\ON2.CAP"
DEFAULT_CONFIG_JSON_PATH=r"C:\\STRsysym\comm\web_services\nodejs\pos\src\config.json"
DEFAULT_COPY_ERROR_LOG=r"I:\SQA\shared\Automation\Projects\Awards_TL\reports\Memory_utilization"
DEFAULT_VAULT_CI_NIGHTLY_BUILD_PATH=r"\\stpshared\shared\Store_Systems\public\release_builds\Vault"
DEFAULT_VAULT_IP="107.22.109.94"
DEFAULT_VAULT_URL="http://107.22.109.94/dmp/rest/"
# Default Values
DEFAULT_UPDATE_FILENAME = "firstlab.xpe"
DEFAULT_UPDATE_FILE_SUT="firstupd.zip"
DEFAULT_UPD_FILENAME = "firstupd.xpe"
DEFAULT_STORE_FILENAME="storelab.zip"
DEFAULT_STR_FILENAME="WebPosAnalyzer.zip"
DEFAULT_CDS_JOB_NAME = "force_load.job"
DEFAULT_DISPLAY_PASSWORD = "support"
DEFAULT_FTP_SERVER_ADDRESS = "10.10.108.56"
DEFAULT_FTP_SERVER_USERNAME = "csauto"
DEFAULT_FTP_SERVER_PASSWORD = "CatalinaAuto1"

# Constants
KEY_PRESS = 0
KEY_RELEASE = 1
SHIFT_KEY = 16
MAX_TIMES_TO_TRY_CONNECTION = 10
MAX_TIMES_TO_TRY = 10
DEFAULT_TIMEOUT = 600 # Timeout in seconds
REBOOT_INIT_TIME = 660
