# File: Settings
#
# Filename:
# > framework/awards/settings.py
#
# Description:
# The settings for all machines are managed by .cfg files located at a folder
# called settings that can be found at the root of the report.
# This file contains the parser for those settings file, that is going to be
# loaded once the framework is loaded. (through an import on other files)
#
# Those settings are loaded by the first parameter passed at the python call (
# that can be found by sys.argv[1]). In case no parameter is passed the Settings
# class is going to load the default values from the awards_defaults.py .

# Framework imports
from framework.awards.defaults import *
from config import *
#Removed code to import ALM file
#Mindtree QA
#Date May-3-2017

# Python modules imports
import sys
import re
import pdb

# Class: Settings
# Loads and stores the settings for a awards box. In case no parameter is passed
# during the python call, it will use the default settings.
#
# Author: Danilo Guimaraes
class Settings:

    # Constructor: Settings
    # Loads a file named as something.cfg that is passed as parameter to the
    # python call. It will load the settings present in the file and in case
    # one or more of them are not found, defaults are going to be used.
    # In case no parameter can be found, parameters are going to be used.
    # In case no parameter is passed to the python call, teh defaults are going
    # to be used.
    def __init__(self):
        # Set the defaults
        self.awards_machine_name = DEFAULT_AWARDS_MACHINE_NAME
        self.awards_machine_ip = DEFAULT_AWARDS_MACHINE_IP_ADDRESS
        self.awards_machine_port_number = DEFAULT_AWARDS_MACHINE_PORT_NUMBER
        #self.awards_printer_type=DEFAULT_PRINTER_TYPE
        # Get the name of the machine from ALM
        if len(sys.argv) > 1:

            #Removed code to get machine name from ALM
            #Date-3-May-2017
            #Mindtree QA


            print(sys.argv[0])
            #pdb.set_trace()
            self.machine_name = sys.argv[1]
            #lab.close_connection
            self.awards_printer_type=sys.argv[2]

            print(self.awards_printer_type)
            #
            self.platformid=137
            #pdb.set_trace()
            #if len(self.awards_printer_type)>0:

            if self.awards_printer_type == "CMC6":
                self.platformid =137

            if self.awards_printer_type=="FR_CMC6":
                self.platformid = 140

            if self.awards_printer_type == "CMC7":
                self.platformid = 138
            if self.awards_printer_type == "JP_CMC7":
                self.platformid = 141
            if self.awards_printer_type == "CMC8":

                self.platformid = 9
            if self.awards_printer_type == "CMC9":

                self.platformid = 194
            if self.awards_printer_type == "CMC9_test":

                self.platformid = 62
            if self.awards_printer_type == "US-CDS-Vault-CMC6":
                self.platformid = 226
            if self.awards_printer_type == "US-CDS-Vault-CMC7":
                self.platformid = 227
            if self.awards_printer_type == "US-CDS-Vault-CMC8":
                self.platformid = 228
            # Set the filename
            filename = base_path + "/settings/" + self.machine_name + ".cfg"
        else:
            print("No settings file name provided. Using defaults.")
            return

        # Parse the file and store the data
        for param in open(filename):

            # Split the file and prepare the string for comparison
            values = param.split("=")
            values[0] = values[0].upper().strip()
            values[1] = values[1].strip()

            # Associate the proper values
            if values[0] == "AWARDS_MACHINE_NAME":
                self.awards_machine_name = values[1]

            elif values[0] == "AWARDS_MACHINE_IP":
                if self.is_valid_ip(self.awards_machine_ip):
                    self.awards_machine_ip = values[1]

            elif values[0] == "AWARDS_MACHINE_PORT_NUMBER":
                self.awards_machine_port_number = int(values[1])

    # Method: is_valid_ip
    # (Very) Quick and simple validation on the IP address passed as parameter,
    # just to make sure something that makes sense has been used as IP address.
    def is_valid_ip(self, ip):

        # Check if this is a valid ip format
        match = re.findall("(\d{1,3}\.){3}\d{1,3}", ip)
        if len(match) == 0:
            return False

        # Now check if everything is < 255
        sections = ip.split(".")
        print (sections)
        for section in sections:
            if not (-1 < int(section) < 256):
                return False

        return True

################################################################################
machine_settings = Settings()
