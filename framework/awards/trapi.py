# File: Trapi
#
# Filename:
# > framework/awards/trapi.py
#
# Description:
# This is a complete refactor from the original port of the TRAPI. The original
# code was a literal port from the original C++ code that was not really well
# organized.
#
# The main objective of this refactor is to fix some things:
# - Add a status flow to the API. Currently, there is some things being done
#   this way but the code has not been done with this in mind.
# - Remove the redundancy of having a MSock.py that basically replicates most
#   of the methods from the socket library.
# - Remove lots of unused code from the MSock, as only two or three methods
#   are being used.
# - Have better behavior and error handling overall.
# - Unify the two libraries in one file.
#
# A brief history on all this TRAPI stuff:
# Awards is an embbed application, it can't be changed too much else it is not
# going to relfect what is going to be deployed to production. The main
# challenge regarding the Awards automation is the fact that we can't install
# anything in the system computer in order to automate it. We need to do that
# remotely.
#
# The first solution for that was using an application called TestRunner,
# developed by Qronus Inc. That was basically a "clone" of the WinRunner from
# Mercury Interactive (they were syster companies, after all). The scripts would
# be written in TSL (a proprietary language, that looks like a shitty CScript
# implementation) as they would be written in WinRunner. The main difference
# between these two applications is the fact that TestRunner came with a set of
# cables to connect the two boxes (the one that is executing TestRunner and
# the one that us running Awards) so that it could control the Awards box
# remotely. Well, that was like... 2001, 2002? So yeah, that might have been
# the solution by that time.
#
# Apperently all this cable-management hell started annoying someone that
# created this thing called TRAPI. It is basically a pair or applications
# comprised of a listener and a DLL. Those were created originally in C++ and
# have been created this way so that the trapi.exe can be dropped into the
# awards box in order to control it with minimum system changes thru TCP
# packets. THIS is a good solution.
#
# The DLL was needed because WinRunner could not create a TCP socket so it had
# to import the TRAPI DLL in order to do it so. Not a surprise, even today
# UFT is not capable of doing it by itself. (the reason why we opted in for
# Python)
#
# It ends up that all this stuff is antiquated today. It has been more than a
# decade that TestRunner is not supported anymore and there is no reason to
# keep using that DLL stuff today. So we migrated everything to Python. :)
#
# It works and has easier maintenance. That's what matters after all.
#
# Important: we have NOT touched the trapi.exe source code in any way, it is
# still the same old trapi.exe that has been used for all those years. It works,
# we don't need to touch that.
#
# Finally, you might be asking where did I found all this information: I haven't
# . I have figured it out, what means it might be wrong. :D

import pdb

import socket
import logging
import select
import time

from enum import Enum

# Enum: TrapiStatus
# This class represents the possible statuses for the trapi connection.
class TrapiStatus(Enum):
    disconnected = 0
    connected = 1
    error = -1

# Exception: TrapiEXEMemoryFullException
# Used to raise some errors.
class TrapiEXEMemoryFullException(Exception):
    pass

# Class: Trapi
# Manages and handle the connection with the SUT box. This object basically
# abstracts all the interaction with the Awards box. The methos to connect,
# send keys, get console text, etc.
#
# How the protocol works:
# - It is a TCP connection, usually connecting to the port 3000 in the SUT box.
# - The listener in the SUT box is managed by the TRAPI.exe that can be found
#   at C:\xptools\trapi.exe in the awards box.
# - Pretty much what needs to be done is to establish a TCP connection betwen
#   the two boxes.
# - After that is done, TCP packets need to be sent with the right formatting
#   in order to get response from the awards box. Those messages usually follows
#   this format:
#   > %d||%d
#   > %d||%d|%d
#   The first value is related to the message type that is being sent. What
#   comes after the || are the parameters for the message. Those are specific
#   to the message type and are separated with a | in case there is more than
#   one.
# - Usually, the trapi.exe will send an ack message back, so after every send()
#   we need to run a receive().
# - When transferring files, the max buffer size is 8192 (this is set in the
#   trapi.exe code). If you are transfering any file bigger than (almost) 8k,
#   this file will be split in multiple packages. Look for the |E@N@D| tag in
#   the packets received to know when to stop reading the buffer. If you don't
#   do that, those packets will flood the buffer and weird stuff will start to
#   happen. :P
#
# Some tips:
# - To see if the trapi is listening in the port 3000 in the awards box,
#   run a netstat -o -n -a in order to see all processes listening.
# - If you are trying to connect and is sure that the ip and port are correct and
#   the machine still refuses the connection, that means you have a stuck socket
#   in the awards box.
#   To solve that, just kill the trapi.exe in the awards box and launch it again
#   , like so:
#   > c:\xptools\trapi.exe 3000
#
# Debugging Verbose:
# This code has a lot of verbose that can be displayed in the console while the
# test is running. This is managed by the python package /logging/.
# The level of verbose is configured directly in the logging package, the
# standard, pythonic way. By default, only info and errors should show up. If
# you increase the level of debugging you should see screens and messages also.
#
# Author:
# Danilo Guimaraes (danilo.guimares@catalinamarketing.com)
class Trapi:

    # Method: __init__
    # Initialzies the Trapi object instance.
    def __init__(self):

        # Setthe verbose log
        logging.basicConfig(level=logging.DEBUG)
        self.log = logging.getLogger("TRapi")

        # Set the initial status of the connection
        self.status = TrapiStatus.disconnected

        # Create an empty placeholder for the socket
        self.socket = None

        # Set the default encoding
        self.encoding = "mbcs"

        self.log.info("TRapi Initialized.")

    # Method: connect
    # Connects to the Awards box using the ip and the port specified.
    #
    # Parameters:
    # - ip: the ip of the awards box
    # - port: port to connect to in the awards box
    def connect(self, ip, port, count=0):

        # Close any existing socket before connecting
        self.close()

        self.log.info("Connecting to %s:%s." % (ip, port) )
        
        # Create a new socket for the new connection
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
            self.socket.settimeout(5)
            self.socket.connect((ip, port))
            self.status = TrapiStatus.connected

            self.log.info("Connected.")

            self.ip = ip
            self.port = port

            return True

        # We don't really want to do much for exceptions, just return false so
        # an error message can be logged in the report and the test can be
        # aborted.
        except ConnectionAbortedError:
            if count < 5:
                status = connect(ip, port, count + 1)
                if status:
                    return True
            else:
                self.log.error("Couldn't connect to %s:%s. Connection Aborted by the Peer." % (ip, port) )
                return False

        except:
            self.log.error("Couldn't connect to %s:%s." % (ip, port) )
            return False

    # Method: close
    # Closes the existing connection.
    def close(self):

        self.log.info("Closing any socket open and/or connected.")

        # Try to close the socket if it is connected.
        if self.socket is not None and self.status == TrapiStatus.connected:
            try:

                # DO NOT SHUTDOWN THE SOCKET, THAT DOESN'T WORKS!
                # self.socket.shutdown(2)

                # Just close it and wait a bit of time so that the other side
                # can also close the socket (it might take a few seconds) when
                # having memory leak issues.
                self.socket.close()
                time.sleep(5)

            # Nothing to be done for the exception, just need to move on and
            # clean out the socket variable. If an exception is raised, that
            # means we have a bad socket or we might not even be connected at
            # all, what doesn't really matters as we are going to assign None
            # to self.socket
            except:
                pass

        # Set socket as None and restore the status of the connection
        self.socket = None
        self.status = TrapiStatus.disconnected

        self.log.info("TRApi is now disconnected.")

    # Method: send
    # Sends a TCP message to the awards box.
    #
    # Parameters:
    # - message: message to be sent to the awards box
    def send(self, message):

        # Send the data using mbcs as encoding
        try:
            self.log.debug("Sending data: %s." % (str(message)))
            self.socket.send(bytes(message, self.encoding))
            return True

        # Raise in case of exceptions, so that the function that has called it
        # can do the proper handling.
        except:
            self.status = TrapiStatus.error
            self.log.error("Error sending data: %s." % (str(message)), exc_info = True)
            raise

    # Method: receive
    # Listens for a TCP packet from the awards box. Returns the received buffer.
    # This function wil download the entire packet until |E@N@D| is found in the
    # buffer. This is needed as TCP will not let you know how many packets you
    # should expect for a given request. That will also avoid the system to try
    # to listen for a packet with empty buffer. (see below)
    #
    # Warning:
    # Do not listen for packets if you are not sure there is packet in the buffer
    # waiting to be read. If it happens to listen without a packet in the
    # buffer, the socket (and your entire code) will freeze waiting for the
    # packet to be read while the TCP timeout is not expired. (a looong timeout)
    def receive(self):

        # Receive the data
        # Use 8192 as max buffer size as the TRAPI.exe is not going to send
        # packets bigger than that. (it may crash if tried)
        try:
            self.log.debug("Receiving data.")

            full_data = b''
            data = b''
            count = 0

            # Will wait until E@N@D is found in the stream
            while full_data.find(bytes('E@N@D', "UTF-8")) == -1:

                # Set the socket timeout to 5 seconds
                self.socket.settimeout(5)

                # Use this select statement to check if the buffer is ready to
                # be read.
                # We need to check this first due to the issue mentioned below.
                ready = select.select([self.socket], [], [], 5)

                # If it is ready, read the buffer.
                if ready[0]:

                    # Using 8192 bytes as the buffer size. By default, that buffer
                    # size will only be used if both sides (and the entire network)
                    # infrastructure support gigabit speeds. Else, a lower frame size
                    # will be negotiated during the TCP handshake process
                    self.socket.settimeout(5)
                    data = self.socket.recv(8192)
                    full_data += data

                # If it is not ready, try again
                else:

                    # Wait a bit and count one try
                    time.sleep(1)
                    count += 1

                    # Here, 2 is a "reasonable number". No specific reason for
                    # for it, just sound the better way to try 5 times before
                    # trying to reconnect.
                    if count == 2:

                        # Ok, this is a BUG in the TRAPI.exe that runs in the
                        # Awards box.
                        # What happens is that, for some reason, the code from
                        # the TRAPI.exe won't clean the buffer/cache/memory/whatever
                        # properly, whar generates a memory leak. Seems that
                        # everytime it sends a message back to the script,
                        # it is stacking this message in the memory, along with
                        # everything else that has been sent since the socket
                        # has been connected.
                        # With the memory full it starts paging and every time
                        # a new message is sent, it will take more and more time,
                        # up to the point that the script gets stuck trying to
                        # read an empty buffer and the awards box gets stuck
                        # with no memory.
                        # When you close the socket, it cleans the memory and
                        # everybody is happy again.
                        # The solution is to change the C++ code for TRAPI.exe,
                        # but we'll not do that right now. Instead, we'll handle
                        # the issue by checking that, if the awards box has not
                        # replied back after 2 trials, we'll disconnect and
                        # connect again.
                        self.socket.settimeout(None)
                        data = self.socket.recv(8192)

                        print("Received data.")
                        time.sleep(5)

                        self.log.debug("Something wrong happened. Most likely the memoty is full in the Awards box. Closing the socket and reconnecting....")
                        print("Connecting.")
                        self.connect(self.ip, self.port)
                        return False

            return full_data

        # Raise in case of exceptions, so that the function that has called it
        # can do the proper handling.
        except:
            self.status = TrapiStatus.error
            self.log.error("Error receiving data.", exc_info = True)
            raise

    # Method: send_key_events
    # Sends key events to the awards box.
    #
    # Parameters:
    # - ascii_code: ascii code to be sent to the Awards box. This ascii code
    #   is not the same as the usual ascii code from the standard table. To
    #   get more information about that, see <send_keys>.
    # - action_type: 1 for key press and 0 for key release
    def send_key_events(self, ascii_code, action_type):

        # Send key events to the awards box.
        # For each key sent, we need to listen for the ack.
        try:
            message = "%s||%d|%d" % ("01", int(ascii_code), int(action_type))
            self.send(message)
            self.receive()
            return True

        # Return false so that the caller can handle the error and add it to
        # the report.
        except:
            self.log.error("Error sending data: %s." % (str(message)), exc_info = True)
            return False

    # Method: get_console_screen
    # Retrieves the screen from the Awards box. Returns it as a plain string.
    def get_console_screen(self, count=0):

        # Try to get the console screen from the SUT box.
        try:
            message = "%s||" % ("02")
            self.send(message)
            screen = self.receive().decode(self.encoding)

            self.log.debug("Captured screen: \n %s." % (screen) )

            if screen == False:
                screen = None
                if count < 5:
                    screen = self.get_console_screen(count + 1)

            return screen

        # In case of errors, just log the error
        except:
            self.log.error("Error retrieving the screen text.", exc_info = True)
            return None

    # Method: file_contents_txt
    # Get the contents of a file from the awards box and encode it as plain
    # text. Use this to retrieve the contents and read as plain text.
    #
    # Parameters:
    # - filename: path of the file (in the awards box) to have the contents
    #   retrieved.
    def file_contents_txt(self, filename, count=0):

        # Try to retrieve the file contents from the awards box.
        try:
            message = "%s||%s" % ("07", filename)
            self.send(message)

            # Read the file contents until end has been reached
            data = self.receive().decode(self.encoding)

            if data == False:
                if count < 5:
                    data = self.file_contents_txt(filename, count + 1)

            return data


        # Log the error message and return None.
        except:
            self.log.error("Error retrieving file: %s." % (filename), exc_info = True)
            return None

    # Method: file_contents_bin
    # Get the contents of a file from the awards box and return the bytes. Use
    # this to retrieve the contents and build a file using the bytes.
    #
    # Parameters:
    # - filename: path of the file (in the awards box) to have the contents
    #   retrieved.
    def file_contents_bin(self, filename):

        # Try to get the file. For this one, do not decode it, just return
        # the bytes.
        try:
            message = "%s||%s" % ("07", filename)
            self.send(message)

            # Transfer the file until |E@N@D| has been found
            data = self.receive()

            # Remove the |E@N@D| from the end of the file
            if data == False:
                if count < 5:
                    data = self.file_contents_bin(filename, count + 1)

            return data.rsplit(bytes("|", "UTF-8"), 2)[0]

        except:
            self.log.error("Error retrieving file: %s." % (filename), exc_info = True)
            return None

    # Method: get_ini_entry
    # Gets the value of a given entry in a INI file.
    #
    # Parameters:
    # - filename: path of the file (in the awards box) to have the contents
    #   retrieved
    # - item: value to be retrieved from the INI file
    # - section: name of the section that holds the value to be retrieved in the
    #   INI file.
    def get_ini_entry(self, section, item, filename):

        # Get the file and discard the |E@N@D|
        try:
            message = "%s||%s|%s|%s" % ("08", section, item, filename)
            self.send(message)
            response = self.receive().decode(self.encoding).split("|")[0]

            self.log.debug("Process list:\n%s" % (response) )

            return response

        # Just log the error and return none
        except:
            self.log.error("Error retrieving INI entry: %s." % (filename), exc_info = True)
            return None

    # Method: get_proc_list
    # Gets the list of processes running in the system. Returns a string with
    # the list of processes running.
    def get_proc_list(self):

        # Try to retrieve the process list
        try:
            message = "%s||" % ("12")

            self.send(message)
            response = self.receive().decode(self.encoding)

            self.log.debug("Process list:\n%s" % (response) )

            return response

        # Just log the error and return none
        except:
            self.log.error("Error getting the process list.", exc_info = True)
            return ""

    # Method: file_exists
    # Verify if a file exists in a given path at the SUT box. Returns True or
    # False, accordingly.
    #
    # Parameters:
    # - filename: path of the file (in the awards box) to be tested
    def file_exists(self, filename):

        try:
            message = "%s||%s" % ("06", filename)
            self.send(message)
            response = self.receive().decode(self.encoding).split("|")[0]

            if response.strip().upper() == "TRUE":
                return True

            else:
                return False

        except:
            self.log.error("Error verifying if file exists in SUT box.", exc_info = True)
            return None

# Creates the TRAPI code and exposes the variable to the user
trapi = None
trapi = Trapi()
