import cv2
import numpy as np
from matplotlib import pyplot as plt
def compare_barcode(img_rgb,template):

	#img_rgb = cv2.imread('big.png')
	img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
	#template = cv2.imread('small.png',0)
	w, h = template.shape[::-1]

	res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
	threshold = 0.8
	loc = np.where( res >= threshold)
	if len(loc[::-1][0]) == 0:
		print ("No match")
	for pt in zip(*loc[::-1]):
		
		cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)
	#print (img_rgb)
	cv2.imwrite('res.png',img_rgb)
	
	
if __name__=='__main__':
	img_rgb = cv2.imread('big.png')
	template = cv2.imread('small.png',0)
	compare_barcode(img_rgb,template)
