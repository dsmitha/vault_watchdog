�
�[FX[  �               @   s�  d  Z  d d l m Z m Z m Z m Z d d l m Z d d l m	 Z	 d d l
 Z d d l m Z d d l m Z d d l j Z d d l j Z d d	 �  Z Gd
 d �  d e � Z Gd d �  d e � Z Gd d �  d e � Z Gd d �  d e � Z Gd d �  d e � Z Gd d �  d e � Z Gd d �  d e � Z Gd d �  d e � Z Gd d �  d e � Z  Gd d �  d e � Z! Gd d �  d e � Z" Gd  d! �  d! e � Z# Gd" d# �  d# e � Z$ d S)$a�  
This module defines default legend handlers.

It is strongly encouraged to have read the :ref:`legend guide
<plotting-guide-legend>` before this documentation.

Legend handlers are expected to be a callable object with a following
signature. ::

    legend_handler(legend, orig_handle, fontsize, handlebox)

Where *legend* is the legend itself, *orig_handle* is the original
plot, *fontsize* is the fontsize in pixles, and *handlebox* is a
OffsetBox instance. Within the call, you should create relevant
artists (using relevant properties from the *legend* and/or
*orig_handle*) and add them into the handlebox. The artists needs to
be scaled according to the fontsize (note that the size is in pixel,
i.e., this is dpi-scaled value).

This module includes definition of several legend handler classes
derived from the base class (HandlerBase) with the following method.

    def legend_artist(self, legend, orig_handle, fontsize, handlebox):


�    )�absolute_import�division�print_function�unicode_literals)�six)�zipN)�Line2D)�	Rectanglec             C   s   |  j  | j �  d � d  S)Nr   )�update_from�get_children)Ztgt�src� r   �]C:\Users\csauto4\Documents\test-automation\framework\awards\Libs\matplotlib\legend_handler.py�update_from_first_child)   s    r   c               @   ss   e  Z d  Z d Z d d d d d � Z d d �  Z d d	 �  Z d
 d �  Z d d �  Z d d �  Z	 d d �  Z
 d S)�HandlerBasea   
    A Base class for default legend handlers.

    The derived classes are meant to override *create_artists* method, which
    has a following signature.::

      def create_artists(self, legend, orig_handle,
                         xdescent, ydescent, width, height, fontsize,
                         trans):

    The overridden method needs to create artists of the given
    transform that fits in the given dimension (xdescent, ydescent,
    width, height) that are scaled by fontsize if necessary.

    g        Nc             C   s    | | |  _  |  _ | |  _ d  S)N)�_xpad�_ypad�_update_prop_func)�selfZxpadZypad�update_funcr   r   r   �__init__=   s    zHandlerBase.__init__c             C   s6   |  j  d  k r" |  j | | � n |  j  | | � d  S)N)r   �_default_update_prop)r   �legend_handle�orig_handler   r   r   �_update_propA   s    zHandlerBase._update_propc             C   s   | j  | � d  S)N)r
   )r   r   r   r   r   r   r   G   s    z HandlerBase._default_update_propc             C   s;   |  j  | | � | j | � | j d  � | j d  � d  S)N)r   �_set_artist_props�set_clip_box�set_clip_path)r   r   r   �legendr   r   r   �update_propJ   s    zHandlerBase.update_propc             C   sT   | |  j  | } | |  j | } | |  j  | } | |  j | } | | | | f S)N)r   r   )r   r   r   �xdescent�ydescent�width�height�fontsizer   r   r   �adjust_drawing_areaR   s
    zHandlerBase.adjust_drawing_areac          	   C   s�   |  j  | | | j | j | j | j | � \ } } } } |  j | | | | | | | | j �  � }	 x |	 D] }
 | j |
 � qj W|	 d S)aU  
        Return the artist that this HandlerBase generates for the given
        original artist/handle.

        Parameters
        ----------
        legend : :class:`matplotlib.legend.Legend` instance
            The legend for which these legend artists are being created.
        orig_handle : :class:`matplotlib.artist.Artist` or similar
            The object for which these legend artists are being created.
        fontsize : float or int
            The fontsize in pixels. The artists being created should
            be scaled according to the given fontsize.
        handlebox : :class:`matplotlib.offsetbox.OffsetBox` instance
            The box which has been created to hold this legend entry's
            artists. Artists created in the `legend_artist` method must
            be added to this handlebox inside this method.

        r   )r%   r    r!   r"   r#   �create_artists�get_transform�
add_artist)r   r   r   r$   �	handleboxr    r!   r"   r#   �artists�ar   r   r   �legend_artist[   s    zHandlerBase.legend_artistc	       	      C   s   t  d � � d  S)NzDerived must override)�NotImplementedError)	r   r   r   r    r!   r"   r#   r$   �transr   r   r   r&   �   s    zHandlerBase.create_artists)�__name__�
__module__�__qualname__�__doc__r   r   r   r   r%   r,   r&   r   r   r   r   r   -   s   	%r   c               @   s:   e  Z d  Z d d d d � Z d d �  Z d d �  Z d S)	�HandlerNpointsg333333�?Nc             K   s&   t  j |  | � | |  _ | |  _ d  S)N)r   r   �
_numpoints�_marker_pad)r   �
marker_pad�	numpoints�kwr   r   r   r   �   s    	zHandlerNpoints.__init__c             C   s!   |  j  d  k r | j S|  j  Sd  S)N)r4   r7   )r   r   r   r   r   �get_numpoints�   s    zHandlerNpoints.get_numpointsc       
      C   s�   |  j  | � } | d k rP t j | |  j | | |  j | | � } | }	 n: | d k r� t j | | d � } d | d | g }	 n  | |	 f S)N�   �   g      �?)r9   �np�linspacer5   )
r   r   r    r!   r"   r#   r$   r7   �xdata�xdata_markerr   r   r   �	get_xdata�   s    		zHandlerNpoints.get_xdata)r/   r0   r1   r   r9   r@   r   r   r   r   r3   �   s   r3   c               @   s.   e  Z d  Z d d d d � Z d d �  Z d S)�HandlerNpointsYoffsetsNc             K   s#   t  j |  d | | �| |  _ d  S)Nr7   )r3   r   �	_yoffsets)r   r7   �yoffsetsr8   r   r   r   r   �   s    zHandlerNpointsYoffsets.__init__c             C   s9   |  j  d  k r | | j } n | t j |  j  � } | S)N)rB   �_scatteryoffsetsr<   �asarray)r   r   r    r!   r"   r#   r$   �ydatar   r   r   �	get_ydata�   s    z HandlerNpointsYoffsets.get_ydata)r/   r0   r1   r   rG   r   r   r   r   rA   �   s   rA   c               @   s4   e  Z d  Z d Z d d d d � Z d d �  Z d S)�HandlerLine2Dz'
    Handler for Line2D instances.
    g333333�?Nc             K   s    t  j |  d | d | | �d  S)Nr6   r7   )r3   r   )r   r6   r7   r8   r   r   r   r   �   s    zHandlerLine2D.__init__c	             C   s  |  j  | | | | | | � \ }	 }
 | | d t j |	 j t � } t |	 | � } |  j | | | � | j d � | j d � t |
 | d  t	 |
 � � � } |  j | | | � | j
 d � | j d k r� | j �  | j } | j | � n  | | _ | j | � | j | � | | g S)Ng       @�default� �Noner:   )r@   r<   �ones�shape�floatr   r   �set_drawstyle�
set_marker�len�set_linestyle�markerscale�get_markersize�set_markersizeZ
_legmarker�set_transform)r   r   r   r    r!   r"   r#   r$   r.   r>   r?   rF   �legline�legline_marker�newszr   r   r   r&   �   s"    !	zHandlerLine2D.create_artists)r/   r0   r1   r2   r   r&   r   r   r   r   rH   �   s   rH   c               @   s=   e  Z d  Z d Z d d d � Z d d �  Z d d �  Z d S)	�HandlerPatchz&
    Handler for Patch instances.
    Nc             K   s   t  j |  | � | |  _ d S)a'  
        The HandlerPatch class optionally takes a function ``patch_func``
        who's responsibility is to create the legend key artist. The
        ``patch_func`` should have the signature::

            def patch_func(legend=legend, orig_handle=orig_handle,
                           xdescent=xdescent, ydescent=ydescent,
                           width=width, height=height, fontsize=fontsize)

        Subsequently the created artist will have its ``update_prop`` method
        called and the appropriate transform will be applied.

        N)r   r   �_patch_func)r   Z
patch_funcr8   r   r   r   r   �   s    zHandlerPatch.__init__c       	      C   so   |  j  d  k r5 t d | | f d | d | � } n6 |  j  d | d | d | d | d | d | d | � } | S)	N�xyr"   r#   r   r   r    r!   r$   )r[   r	   )	r   r   r   r    r!   r"   r#   r$   �pr   r   r   �_create_patch�   s    zHandlerPatch._create_patchc	       
      C   sH   |  j  | | | | | | | � }	 |  j |	 | | � |	 j | � |	 g S)N)r^   r   rV   )
r   r   r   r    r!   r"   r#   r$   r.   r]   r   r   r   r&   �   s
    zHandlerPatch.create_artists)r/   r0   r1   r2   r   r^   r&   r   r   r   r   rZ   �   s   rZ   c               @   s:   e  Z d  Z d Z d d �  Z d d �  Z d d �  Z d S)	�HandlerLineCollectionz/
    Handler for LineCollection instances.
    c             C   s!   |  j  d  k r | j S|  j  Sd  S)N)r4   �scatterpoints)r   r   r   r   r   r9     s    z#HandlerLineCollection.get_numpointsc             C   sr   | j  �  d } | j �  d } | j �  d } | j | � | j | � | d d  k	 rn | j | d � n  d  S)Nr   r:   )�get_linewidth�
get_dashes�
get_colors�	set_color�set_linewidth�
set_dashes)r   r   r   �lw�dashes�colorr   r   r   r     s    z*HandlerLineCollection._default_update_propc	             C   s{   |  j  | | | | | | � \ }	 }
 | | d t j |	 j t � } t |	 | � } |  j | | | � | j | � | g S)Ng       @)r@   r<   rL   rM   rN   r   r   rV   )r   r   r   r    r!   r"   r#   r$   r.   r>   r?   rF   rW   r   r   r   r&     s    !z$HandlerLineCollection.create_artistsN)r/   r0   r1   r2   r9   r   r&   r   r   r   r   r_     s   	r_   c               @   sd   e  Z d  Z d Z d d d d � Z d d �  Z d d �  Z d	 d
 �  Z d d �  Z d d �  Z	 d S)�HandlerRegularPolyCollectionz-
    Handler for RegularPolyCollections.
    Nc             K   s#   t  j |  d | | �| |  _ d  S)NrC   )rA   r   �_sizes)r   rC   �sizesr8   r   r   r   r   &  s    z%HandlerRegularPolyCollection.__init__c             C   s!   |  j  d  k r | j S|  j  Sd  S)N)r4   r`   )r   r   r   r   r   r9   +  s    z*HandlerRegularPolyCollection.get_numpointsc             C   s�   |  j  d  k r� | j �  } t | � s3 d g } n  t | � | j d }	 t | � | j d }
 |  j | � } | d k  r� d |	 |
 |	 |
 g } q� |	 |
 } | t j d d | � |
 } n	 |  j  } | S)Nr:   r;   �   g      �?r   )	rk   �	get_sizesrQ   �maxrS   �minr9   r<   r=   )r   r   r   r    r!   r"   r#   r$   Zhandle_sizes�size_maxZsize_minr7   rl   �rngr   r   r   rn   1  s    
 	z&HandlerRegularPolyCollection.get_sizesc             C   s>   |  j  | | � | j | j � | j d  � | j d  � d  S)N)r   �
set_figure�figurer   r   )r   r   r   r   r   r   r   r   F  s    z(HandlerRegularPolyCollection.update_propc          
   C   s:   t  | � | j �  d | j �  d | d | d | �} | S)N�rotationrl   �offsets�transOffset)�type�get_numsides�get_rotation)r   r   rl   rv   rw   r]   r   r   r   �create_collectionO  s    	z.HandlerRegularPolyCollection.create_collectionc	             C   s�   |  j  | | | | | | � \ }	 }
 |  j | | | | | | � } |  j | | | | | | | � } |  j | | d t t |
 | � � d | �} |  j | | | � | | _ | g S)Nrv   rw   )r@   rG   rn   r{   �listr   r   �_transOffset)r   r   r   r    r!   r"   r#   r$   r.   r>   r?   rF   rl   r]   r   r   r   r&   X  s    		z+HandlerRegularPolyCollection.create_artists)
r/   r0   r1   r2   r   r9   rn   r   r{   r&   r   r   r   r   rj   "  s   		rj   c               @   s"   e  Z d  Z d Z d d �  Z d S)�HandlerPathCollectionz@
    Handler for PathCollections, which are used by scatter
    c             C   s5   t  | � | j �  d g d | d | d | �} | S)Nr   rl   rv   rw   )rx   �	get_paths)r   r   rl   rv   rw   r]   r   r   r   r{   q  s
    	z'HandlerPathCollection.create_collectionN)r/   r0   r1   r2   r{   r   r   r   r   r~   m  s   r~   c               @   s"   e  Z d  Z d Z d d �  Z d S)�HandlerCircleCollectionz'
    Handler for CircleCollections
    c             C   s"   t  | � | d | d | �} | S)Nrv   rw   )rx   )r   r   rl   rv   rw   r]   r   r   r   r{   ~  s    	z)HandlerCircleCollection.create_collectionN)r/   r0   r1   r2   r{   r   r   r   r   r�   z  s   r�   c               @   sF   e  Z d  Z d Z d d d d d d � Z d d �  Z d	 d
 �  Z d S)�HandlerErrorbarz
    Handler for Errorbars
    g      �?Ng333333�?c             K   s2   | |  _  | |  _ t j |  d | d | | �d  S)Nr6   r7   )�
_xerr_size�
_yerr_sizerH   r   )r   �	xerr_size�	yerr_sizer6   r7   r8   r   r   r   r   �  s    		zHandlerErrorbar.__init__c       	      C   s<   |  j  | } |  j d  k r% | } n |  j | } | | f S)N)r�   r�   )	r   r   r    r!   r"   r#   r$   r�   r�   r   r   r   �get_err_size�  s
    	zHandlerErrorbar.get_err_sizec	                s�  | \ }	 }
 } |  j  | | | | | | � \ } } | | d t j | j t � } t | | � } t j | � } t j | d  t | � � � } |  j | | | | | | � \ �  � t | | � } |	 d  k r� | j	 d � | j	 d � n |  j
 | |	 | � | j d � | j d � |  j
 | |	 | � | j d � | j d k rl| j �  | j } | j | � n  g  } g  } | j rj�  f d d �  t | | � D� } t j | � } |  j
 | | d | � | j | � |
 rjt | �  | � } t | �  | � } |  j
 | |
 d | � |  j
 | |
 d | � | j d	 � | j d	 � | j | � | j | � qjn  | j r\� f d
 d �  t | | � D� } t j | � } |  j
 | | d | � | j | � |
 r\t | | � � } t | | � � } |  j
 | |
 d | � |  j
 | |
 d | � | j d � | j d � | j | � | j | � q\n  g  } | j | � | j | � | j | � | j | � x | D] } | j | � q�W| S)Ng       @FrI   rK   r:   c                s6   g  |  ], \ } } | �  | f | �  | f f � q Sr   r   )�.0�x�y)r�   r   r   �
<listcomp>�  s   	z2HandlerErrorbar.create_artists.<locals>.<listcomp>r   �|c                s6   g  |  ], \ } } | | �  f | | �  f f � q Sr   r   )r�   r�   r�   )r�   r   r   r�   �  s   	�_)r@   r<   rL   rM   rN   r   rE   rQ   r�   �set_visibler   rO   rP   rR   rS   rT   rU   �has_xerrr   �mcoll�LineCollection�append�has_yerr�extendrV   )r   r   r   r    r!   r"   r#   r$   r.   Z	plotlines�caplinesZbarlinecolsr>   r?   rF   rW   Zydata_markerrX   rY   Zhandle_barlinecolsZhandle_caplines�verts�collZcapline_leftZcapline_rightr*   �artistr   )r�   r�   r   r&   �  sz    !		zHandlerErrorbar.create_artists)r/   r0   r1   r2   r   r�   r&   r   r   r   r   r�   �  s
   
r�   c               @   sF   e  Z d  Z d Z d d d d d d � Z d d �  Z d d	 �  Z d S)
�HandlerStemz
    Handler for Errorbars
    g333333�?Nc          	   K   s/   t  j |  d | d | d | | �| |  _ d  S)Nr6   r7   rC   )rA   r   �_bottom)r   r6   r7   �bottomrC   r8   r   r   r   r   �  s
    zHandlerStem.__init__c             C   sA   |  j  d  k r' | d | j d } n | t j |  j  � } | S)Ng      �?)rB   rD   r<   rE   )r   r   r    r!   r"   r#   r$   rF   r   r   r   rG     s    zHandlerStem.get_ydatac	             C   s�  | \ }	 }
 } |  j  | | | | | | � \ } } |  j | | | | | | � } |  j d  k ri d } n	 |  j } t | | d  t | � � � } |  j | |	 | � g  } xE t | | � D]4 \ } } t | | g | | g � } | j | � q� Wx0 t | |
 � D] \ } } |  j | | | � qWt t j	 | � t j
 | � g | | g � } |  j | | | � | g } | j | � | j | � x | D] } | j | � q�W| S)Ng        )r@   rG   r�   r   rQ   r   r   r�   r<   �amin�amaxr�   rV   )r   r   r   r    r!   r"   r#   r$   r.   �
markerline�	stemlines�baseliner>   r?   rF   r�   Zleg_markerlineZleg_stemlines�thisx�thisy�lZlm�mZleg_baseliner*   r�   r   r   r   r&     s2    			zHandlerStem.create_artists)r/   r0   r1   r2   r   rG   r&   r   r   r   r   r�   �  s
   r�   c               @   s.   e  Z d  Z d Z d d �  Z d d �  Z d S)�HandlerTuplez
    Handler for Tuple
    c             K   s   t  j |  | � d  S)N)r   r   )r   �kwargsr   r   r   r   :  s    zHandlerTuple.__init__c	          
   C   sj   | j  �  }	 g  }
 xQ | D]I } | j |	 | � } | j | | | | | | | | � } |
 j | � q W|
 S)N)�get_legend_handler_map�get_legend_handlerr&   r�   )r   r   r   r    r!   r"   r#   r$   r.   �handler_mapZa_listZhandle1�handlerZ_a_listr   r   r   r&   =  s    	zHandlerTuple.create_artistsN)r/   r0   r1   r2   r   r&   r   r   r   r   r�   6  s   r�   c               @   s.   e  Z d  Z d Z d d �  Z d d �  Z d S)�HandlerPolyCollectionzH
    Handler for PolyCollection used in fill_between and stackplot.
    c             C   s�   d d �  } d d �  } | j  | | j �  � � | j | | j �  � � | j | j �  � | j | j �  � | j | | j	 �  � � | j
 | | j �  � � | j | | j �  � � | j | j �  � | j | j �  � d  S)Nc             S   s.   t  j j |  � }  t |  � r& |  d Sd Sd  S)Nr   �none)�mcolors�colorConverter�to_rgba_arrayrQ   )�colorsr   r   r   �first_colorS  s    z7HandlerPolyCollection._update_prop.<locals>.first_colorc             S   s   t  |  � r |  d Sd  Sd  S)Nr   )rQ   )Z
prop_arrayr   r   r   �	get_firstY  s    z5HandlerPolyCollection._update_prop.<locals>.get_first)�set_edgecolor�get_edgecolor�set_facecolor�get_facecolor�set_fill�get_fill�	set_hatch�	get_hatchre   �get_linewidthsrR   �get_linestylesrV   �get_transformsrs   �
get_figure�	set_alpha�	get_alpha)r   r   r   r�   r�   r   r   r   r   R  s    z"HandlerPolyCollection._update_propc	       
      C   sJ   t  d | | f d | d | � }	 |  j |	 | | � |	 j | � |	 g S)Nr\   r"   r#   )r	   r   rV   )
r   r   r   r    r!   r"   r#   r$   r.   r]   r   r   r   r&   h  s
    z$HandlerPolyCollection.create_artistsN)r/   r0   r1   r2   r   r&   r   r   r   r   r�   N  s   r�   )%r2   �
__future__r   r   r   r   �matplotlib.externalsr   �matplotlib.externals.six.movesr   �numpyr<   �matplotlib.linesr   �matplotlib.patchesr	   �matplotlib.collections�collectionsr�   �matplotlib.colorsr�   r�   r   �objectr   r3   rA   rH   rZ   r_   rj   r~   r�   r�   r�   r�   r�   r   r   r   r   �<module>   s,   "Y&)!Kp@