# File: Keywords
#
# Filename:
# > framework/awards/keywords.py
#
# Description:
# Register all your keywords and functions in this file. Just add a new entry in
# the "keywords" dictionary followed by the function name.
# Register all keywords in lowercase, so that there are no case-sensitivity
# issues.
#
# Notes:
# - Everytime a new keyword is created, it must have a function somewhere. It
# will usually be at the file <Awards Keywords>. After the function is done,
# it needs to be registered in this file so that the parser can find it later.
# If you create a new keyword and do not register in the keywords dictionary in
# this file, the keyword will not work at all.
#
# Example:
# (start code)
# keywords = {
#    # High Level Keywords
#    "a a a initialize store system": awards_keywords.a_a_a_initialize_store_system,
#    "print coupon": awards_keywords.print_coupon
# }
# (end code)

import framework.awards.core as core
import awards_keywords
import framework.testlink as testlink

# The dictionary below will carry all the keyword names and their respective
# function reference. You might notice there is no () after the function name.
# That is because we are not assgining the return value from the execution of
# the function, we are actually just giving the reference address for this
# function. The parser will later use this to trigger all those functions.
keywords1 = {
    # High Level Keywords
    "a a a initialize store system":" awards_keywords.a_a_a_initialize_store_system",
    "print coupon":" awards_keywords.print_coupon",
    "print coupon - negative":" awards_keywords.print_coupon_negative",
    "print blind coupon":" awards_keywords.print_blind_coupon",
    "extract and report system logs":" awards_keywords.extract_and_report_system_logs",
    "validate coupon not printed":" awards_keywords.validate_coupon_not_printed",
    "validate coupon printed":" awards_keywords.validate_coupon_printed",
    "force load":" awards_keywords.force_load",
    "force load - negative":" awards_keywords.force_load_negative",
    "run batch: dailytask":" awards_keywords.run_batch_dailytask",
    "run batch: database":" awards_keywords.run_batch_database",
    "replace store ini":" awards_keywords.replace_store_ini",
    "validate cds section in ini file":" awards_keywords.validate_cds_section_in_ini_file",
    "validate cds section not in ini file":" awards_keywords.validate_cds_section_not_in_ini_file",
    "validate quickcash dw log":" awards_keywords.validate_quickcash_dw_log",
    "validate images folder":" awards_keywords.validate_images_folder",
    "validate xmldb folder":" awards_keywords.validate_xmldb_folder",
    "validate coupon data - dmc":" awards_keywords.validate_coupon_data_dmc",
    "validate coupon data - da":" awards_keywords.validate_coupon_data_da",
    "validate realtime section in ini file":" awards_keywords.validate_realtime_section_in_ini_file",
    "validate nbic condition":" awards_keywords.validate_nbic_condition",
    "new store":" awards_keywords.new_store",
    "delete file":" awards_keywords.delete_file",
    "copy ic file":" awards_keywords.copy_ic_file",
    "validate amount of clus":" awards_keywords.validate_amount_of_clus",
    "clean logs":" awards_keywords.clean_logs",
    "set store and chain number":" awards_keywords.set_store_and_chain_number",
    "validate shuttle.ini":" awards_keywords.validate_shuttle_ini",
    "wait for connection":" awards_keywords.wait_for_connection",
    "validate system is operational":" awards_keywords.validate_system_is_operational",
    "update awards":" awards_keywords.update_awards",
    "cmcset update":" awards_keywords.cmcset_update",
    "validate file contains":" awards_keywords.validate_file_contains",
    "validate xmllog":"awards_keywords.validate_xmllog",
    "validate cds section on screen":"awards_keywords.validate_cds_section_on_screen",
    "validate bar code":"awards_keywords.validate_bar_code",
     "verify storefix ini":"awards_keywords.verify_storefix_ini",
     "verify storefix single section":"awards_keywords.verify_storefix_single_section",
     "start vm":" awards_keywords.start_vm",
     "create build":" testlink.createbuild",
     "update testlink":" testlink.updateresult",
     "add testcase to testplan":" testlink.addtestcasetotestplan",
     "validate address2":" awards_keywords.validate_address2",
     "install vault":"awards_keywords.install_vault",
    # Low Level Keywords
    "send keys":" awards_keywords.send_keys",
    "validate text is on screen":" awards_keywords.validate_text_is_on_screen",
    "wait for text on screen":" awards_keywords.wait_for_text_on_screen",
    "go to multitasking store system":" awards_keywords.go_to_multitasking_store_system",
    "open a new dos session":" awards_keywords.open_a_new_dos_session",
    "leave dos session":" awards_keywords.leave_dos_session",
    "go to running groups screen":" awards_keywords.go_to_running_groups_screen",
    "close all running groups":" awards_keywords.close_all_running_groups",
    "validate clus":" awards_keywords.validate_clus",
    "shutdown awards":" awards_keywords.shutdown_awards",
    "start awards":" awards_keywords.start_awards",
    "validate INI file value":" awards_keywords.validate_ini_file_value",
    "tail file":" awards_keywords.tail_file",
    "run ts using include file":" awards_keywords.run_ts_using_include_file",
    "set date":" awards_keywords.set_date",
    "set time":" awards_keywords.set_time",
    "validate file exists":" awards_keywords.validate_file_exists",
    "validate file does not exists":" awards_keywords.validate_file_not_exists",
    "copy store ini":" awards_keywords.copy_store_ini",
    "delete store ini":" awards_keywords.delete_store_ini",
    "load so file":" awards_keywords.load_so_file",
    "validate mclu":" awards_keywords.validate_mclu",
    "validate process is running":" awards_keywords.validate_process_is_running",
    "validate process is not running":" awards_keywords.validate_process_is_not_running",
    "validate process is running in awards":" awards_keywords.validate_process_is_running_in_awards",
    "extended wait":" awards_keywords.extended_wait",
    "update ini file before print coupon":"awards_keywords.update_ini_file_before_print_coupon",
    "add printer":"awards_keywords.add_printer",
    "start vault":"awards_keywords.start_vault",
    "update vault config file":"awards_keywords.update_vault_config_file",
    "validate logrec":"awards_keywords.validate_logrec",
    # Legacy Keywords
    "stop awards":" awards_keywords.stop_awards",
    "start awards":" awards_keywords.start_awards",
    "os2_session":" awards_keywords.os2_session",
    "execute ts":" awards_keywords.execute_ts",
    "awards session":" awards_keywords.awards_session",
    "report results":" awards_keywords.report_results",
    #"document": core.document",
    #"date": core.date",
    #"version": core.version",
    #"author": core.author",
    #"report results": core.report_results",
    #"step": core.step",
    #"testcase": core.testcase",
}
