# File: Core
#
# Filename:
# > framework/awards/core.py
#
# Description:
# This is the file that carries the set of *core* keywords and functions for the
# awards automation. Not much happens here in fact, we have the functions needed
# to manage the initialization and termination processes of the framework.
# If there is something that the framework needs to do and it is related to the
# *behavior* of the automation, that code goes here.
#
# Note:
# - This is a self-starting module. By the time you import it, it is going to
# automatically trigger the function <initialize_awards_core>

import pdb

# Framework Imports
from framework.reporting import *
# Removed ALM import - Mindtree- Date May-3-2017
from framework.awards.trapi import trapi
from framework.awards.trapi import TrapiStatus
from config import extract_logs_at_end
#from framework.testlink import *
import awards_keywords
import awards_navigation
import awards_actions
import framework.testlink as testlink
import framework.awards.utils as utils
# Python imports
import atexit
import signal
import sys


# Just manages if the report has been generated already.
report_generated_flag = False

# Function: initialize_awards_core
# This function is going to take care of the initialization of the system. Also,
# it registers any listeners that need to be registered. (like the atexit and
# the sigint)
#
# Author:
# Danilo Guimaraes
def initialize_awards_core():
    global report
    global report_generated_flag

    report_generated_flag = False
    #Assigning user to Testlink
    if len(sys.argv) >5:
        testlink.assigntestcaseuser(sys.argv[3],sys.argv[4],sys.argv[5],"Automation")
    else:
        testlink.assigntestcaseuser(sys.argv[3],sys.argv[4],"3","Automation")
    #pdb.set_trace()
    #awards_navigation.to_running_groups_screen()
    # Handles finish of execution and keyboard interruption
    atexit.register(finalize_awards_core)
    signal.signal(signal.SIGINT,handle_sigint)

# Function: finalize_awards_core
# This function is triggered when the exection of the automation finishes,
# whatever the condition is. This is called even when exceptions are found and
# the script crashes.
# So far, it is only triggering the generation of the report file.
#
# Author:
# Danilo Guimaraes
def finalize_awards_core():
    #import ALMConnection
    global report
    global report_generated_flag

    # Extract the system reports
    if extract_logs_at_end:
        if trapi.status == TrapiStatus.connected:
            awards_keywords.extract_and_report_system_logs({"cds enabled?": "yes"})
        else:
            report.add_step("Extract and report system logs")
            report.fail_step("TRAPI is not connected, so the system reports won't be extracted.")

    # IF the report has not bene generated already, do it

    if not report_generated_flag:
        #
        report.generate_report_file()
        report_generated_flag = True

    # Post the results in ALM/Testlink
    # Will only do that if argv > 1, so that means arguments have been passed to
    # the main script
    print (len(sys.argv))

    # Removed part of code updating result in ALM
    #Mindtree QA
    #Date- May-3-2017

    if len(sys.argv) >2:
        tl_testcaseid = sys.argv[3]



        tl_planid = sys.argv[4]
        #tl_platformid = sys.argv[8]
        print(report.get_overall_status())

        #pdb.set_trace()
        if len(tl_testcaseid.split(","))>1:
            testids=tl_testcaseid.split(",")
            status=report.get_overall_status()
            print (status)
            #pdb.set_trace()
            for testid in testids:
                print(testid)
                #pdb.set_trace()
                testlink.updateresult(testid, tl_planid,status)
        else:


            testlink.updateresult(tl_testcaseid, tl_planid, report.get_overall_status())
    if tu(memory_leak_test)== "YES":
        awards_keywords.validate_memory_utils({"Error_msg":"ERROR - SysMon::Swap file","validate_step":"Yes"})
    if len(sys.argv) >2:
        tl_testcaseid = sys.argv[3]

    #pdb.set_trace()
    awards_navigation.to_multitastking_store_system()
    # Check if the sucess message is displayed on screen
    utils.send_keys("p[enter]")
    if not utils.find_text_in_console("(1 *Ready)",True):
        awards_actions.start_awards()
    #awards_actions.shutdown_awards()
    #if "vault" not in params:
    awards_navigation.to_running_groups_screen()
    awards_actions.close_all_running_groups()
    #
    # Close the connection before finishing the execution
    trapi.close()

# Function: handle_sigint
# Handles the SIGINT signal. This is usually triggered when pressing CTRL+C in
# the terminal while running the test. By default, python would trigger a
# KeyboardInterrupt exception but we don't really want that. When the test is
# interrupted the best thing to be done is to add some message to the report,
# close the reporting and exit the test without screaming errors everywhere. :)
# The trigger for this function is registered at <initialize_awards_core>.
#
# Author:
# Danilo Guimaraes
def handle_sigint(signum, frame):

    # Add a message to the report
    report.add_sub_step("The test has been forced to abort. (CTRL+C has been pressed)")
    report.fail_sub_step("SIGINT has been triggered.")

    # Finalize the report
    finalize_awards_core()

    # Print something in the terminal and leave the execution
    print("CTRL+C has been pressed! Aborting.")
    sys.exit(0)

##################################################################
initialize_awards_core()
