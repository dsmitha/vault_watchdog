# File: Awards Actions
#
# Filename:
# > awards_actions.py
#
# Description:
# This file holds the actions for the Awards automation. Those are the functions
# meant to execute actions in the system, like setting values, copying files,
# running commands, etc. Also, they run basic validation, just to make sure
# their action has been properly executed. Those functions are not meant to
# validate data and/or processes, this is the responsibility of functions from
# the module Awards Validations.
#
# Notes:
# - Actions will always register *steps* in the report, unless there is some
#   very specific need for the action to register a Action in the report.
#
# Naming Covention:
# All function names should start with a *verb* that expains what is going to be
# done:
# - focus_pane
# - reboot_system
# - set_date



#Comments for the newly added functions are updated
#Date 3-May-2017/4-May-2017
#Mindtree QA
import pdb

# Framework imports
from framework.reporting import *
from framework.utils import *
from framework.awards.defaults import *
from framework.awards.trapi import *
from framework.awards.settings import machine_settings as ms
import framework.awards.utils as utils


# NAV imports
import awards_utils
import awards_validations
import awards_navigation
# Python Imports
import datetime
import time
import ftplib
import sys
import shutil
import os,operator
import re
import codecs
# Function: select_multitasking_store_systems
# Selects the multitasking store system in the Running Groups screen.
#
# Process:
# Focus the right pane, press [home] and [enter]
#
# Screen:
# Should be at the running groups screen.
# Author:
# Danilo Guimaraes
def select_multitasking_store_systems():

    start_step("Load the multitasking store systems screen.")

    # Focus the right pane
    focus_pane("right")
    #

    # Send Home and Enter to load the multitasking store systems
    utils.send_keys("[home][enter]")
    #

    #

# Function: focus_pane
# Focus the right or left pane in the Running Groups screen, accordingly to the
# parameter passed.
#
# Parameters:
# - side: Either "RIGHT" or "LEFT" (not case sensitive) as string.
#
# Process:
# Keep pressing tab until the pane is highlighted. It checks for the ASCII code
# 205 (the character for highlighted border) in certain areas of the screen to
# know if the right pane has been highlighted.
#
# Screen:
# Should be at the running groups screen.
#
# Author:
# Danilo Guimaraes
def focus_pane(side):

    # Initialize some Values
    end_time = et(DEFAULT_TIMEOUT)

    add_sub_step("[focus_pane] Focus the %s panel." % side)

    # Check if we are in the right screen
    if not utils.find_text_in_console("Start Group"):
        return False

    # Check wich panel is currently highlighted
    # ASCII 205 will be retrieved from screen when the panel is highlighted
    # ASCII 196 will be retrieved from screen when the panel is not highlighted
    while ct() < end_time:

        right_area = utils.get_console_screen_area(45,2,46,3)
        left_area = utils.get_console_screen_area(5,2,6,3)

        if tu(side) == "RIGHT" and ord(right_area) == 205:
            return True

        elif tu(side) == "LEFT" and ord(left_area) == 205:
            return True

        utils.send_keys("[tab]")

        time.sleep(0.3)
    #block_sub_step("could not find panel")
    fail_sub_step("[focus_pane] Could not focus the %s panel." % side)
    utils.take_screenshot()
    return False

# Function: reboot_system
# Reboots the system by sending alt+ctrl+del.
#
# Process:
# Sends [alt!][ctrl!][del!] to the awards box.
#
# Screen:
# Any
#
# Author:
# Danilo Guimaraes
def reboot_system():

    add_sub_step("[reboot_system] Rebooting the system.")

    # Send alt+ctrl+del to the machine
    utils.send_keys("[ctrl!][alt!][del!]")

    # Wait for the awards machine to be unavailable (what means that it has started
    # the rebooting process)
    utils.wait_for_no_connection()

    # Wait until a new connection can be done (what means the machine has
    # completed most of the reboot process)
    utils.wait_for_connection(REBOOT_INIT_TIME)

    # Wait until "CMC is now operational" is displayed on screen and we are
    # ready to go
    if not utils.wait_for_text_in_console("CMC Is Now Operational|Start Group", True, REBOOT_INIT_TIME):

        # Last try with the Workaround
        # Sometimes we might be at the multitasking store systems screen since
        # the beginning but it has not displayed the "CMC Is Now Operational"
        # on screen. Typing "p" and pressing enter in there might make it show
        # up.
        utils.send_keys("p[enter]")
        if not utils.find_text_in_console("CMC Is Now Operational", True):
            fail_sub_step("[reboot_system] Could not find 'CMC Is Now Operational' on screen after %d seconds." % (REBOOT_INIT_TIME))
            utils.take_screenshot()
            return False

    # All good :)
    add_sub_step("[reboot_system] System reboot is completed.")

# Function: close_all_running_groups
# Will close all DOS sessions loaded leaving only the multitask store System
# running. To exit the sessions, it simply types exit and press enter. In case
# the session is locked with something else, it will not be able to close the
# session.
#
# Process:
# Focus the right pane in the running groups screen. After that, it checks if
# the bottom row in the right pane is "multitasking store system". If not, it
# sends [end][enter] and types exit[enter] in the DOS session. Repeats this
# verification until "multitasking store system" is the bottom row.
#
# Screen:
# Should be at the running groups screen
#
# Author:
# Ajay Keerthi G
def close_all_running_groups():

    # Initialize some stuff
    iterations = 0

    start_step("Close all running groups.")

    # Close all running groups
    bot_row = utils.bottom_row(utils.get_console_screen_area(43,3,75,23))

    if bot_row == None:
        return
    #
    while bot_row.find("Multitasking Store System") == -1:
        focus_pane("right")
        utils.send_keys("[end][enter]")
        if utils.find_text_in_console("console - \x03 ",True) or utils.find_text_in_console("[INFO] console",True):
            utils.send_keys("[ctrl!]c[ctrl.]")
            time.sleep(2)
            utils.send_keys("[ctrl!]c[ctrl.]")
            time.sleep(2)
        utils.send_keys("exit[enter]")

        iterations += 1

        # Check if we are stuck in this process
        # If so, reboot the system
        if iterations > MAX_TIMES_TO_TRY:
            reboot_system()

        time.sleep(1)
        bot_row = utils.bottom_row(utils.get_console_screen_area(43,3,75,23))

# Function: shutdown_awards
# Will shutdown the awards system.
#
# Process:
# Sends "e[enter]" to the awards box. Waits until the system shutdown message is
# displayed on screen.
#
# Screen:
# Should be at the Awards screen.
#
# Author:
# Danilo Guimaraes
def shutdown_awards():

    start_step("Shutdown the Awards system.")
    awards_navigation.to_multitastking_store_system()
    #
    if not utils.wait_for_text_in_console("C:\\strsystm\\", False,timeout=1):
        # Send E and press Enter
        #
        utils.send_keys("E[enter]")

        # Wait for "FYI - AWARDS: ONLINE SESSION IS SHUTDOWN" to be displayed on
        # screen.
        if not utils.wait_for_text_in_console("FYI - AWARDS: ONLINE SYSTEM IS SHUTDOWN", False, DEFAULT_TIMEOUT):
            fail_step("[shutdown_awards] Could not find the message 'FYI - AWARDS: ONLINE SESSION IS SHUTDOWN' on screen.")
            utils.take_screenshot()

            utils.finish_execution()
        pass_step("AWARDS: ONLINE SYSTEM IS SHUTDOWN")

    utils.take_screenshot()

# Function: log_cleanup
# Deletes a list of files needed for any of the processes (SO or CDS). The list
# of files to be deleted can be found in a list within the function code.
#
# Process:
# For each file in the list, runs a "rm -rf %s [enter]" in a dos session.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def log_cleanup():

    # Files to be deleted listed below. If more are needed, just add in the
    # list below.
    files_to_delete = [
        r"C:\strsystm\log\*.*",
        r"C:\strsystm\backup\*.*",
        r"C:\strsystm\capture\*.*",
        r"C:\strsystm\reports\*.*",
        r"C:\strsystm\comm\*.*"
    ]

    # Those files are not going to be deleted, and that's ok.
    # For that reason, we send a list of exception to the function.
    exceptions = [
        r"perfmon\.csv",
        r".*\.xml",
        r"DUMPALOC\.OUT",
        r".*\.rpt"
    ]

    start_step("Delete all files needed for the usual process.")

    # Delete all file
    awards_utils.delete_list_of_files(files_to_delete)

    # Validate they have been deleted sucessfully
    awards_utils.validate_list_of_deleted_files(files_to_delete, exceptions)

# Function: new_database_file_cleanup
# Deletes a list of files needed when a new database has to be loaded (SO or
# CDS). The list of files to be deleted can be found in a list within the
# function code.
#
# Process:
# For each file in the list, runs a "rm -rf %s [enter]" in a dos session.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def new_database_file_cleanup():

    # Files to be deleted listed below. If more are needed, just add in the
    # list below.
    files_to_delete = [
        r"C:\strsystm\db\*.*",
        r"C:\strsystm\shuttle\*.*",
        r"C:\strsystm\images\*.*",
        r"C:\strsystm\xmldb\*.*",
        r"C:\strsystm\IFS\*.*",
        r"C:\strsystm\LOG\*.*",
        r"C:\CDS\mirror\IFS\*.*",
        r"C:\CDS\mirror\images\*.*",
        r"C:\CDS\mirror\Award_file\*.*",
        r"C:\CDS\mirror\CC_file\*.*",
        r"C:\CDS\mirror\related_file\*.*",
        r"C:\CDS\mirror\To_catalina\*.*"
    ]

    start_step("Delete all files needed to use a new database.")

    # Delete all files
    awards_utils.delete_list_of_files(files_to_delete)
    exceptions = [
    r"perfmon\.csv",
    r".*\.xml",
    r"*.log"]
    # Validate they have been deleted sucessfully
    awards_utils.validate_list_of_deleted_files(files_to_delete, exceptions)

# Function: set_date
# Sets the date of the system in the SUT.
#
# Parameters:
# - date: a date (datetime.date) object containing the desired date.
#
# Process:
# Sends "date %s [enter]", then cleans the screen ("cls[enter]") and verify
# that the date is the expected.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def set_date(date):

    # Initialize some variables
    date = "%02d/%02d/%04d" % (date.month, date.day, date.year)

    start_step("Set the system date to %s." % date)

    # Send the date command and check if it has worked well
    utils.send_keys("date %s[enter]" % date)
    utils.send_keys("cls[enter]")
    utils.send_keys("date /t[enter]")

    # Check if the corret date has been displayed
    if not utils.wait_for_text_in_console(date, False, 3):
        date_from_screen = utils.match_text_in_console(r"\d{1,2}/\d{1,2}/\d{4}")

        if date_from_screen is not None:
            utils.take_screenshot()
            fail_step("[set_date] Date has not been set properly. Date found is: %s." % date_from_screen)

        else:
            utils.take_screenshot()
            fail_step("[set_date] Date has not been set properly. Could not find any dates in the screen.")

    utils.take_screenshot()

# Function: set_time
# Sets the time of the system in the SUT.
#
# Parameters:
# - time: a time (datetime.time) object containing the desired date.
#
# Process:
# Sends "time [enter]". Types the date and then cleans the screen ("cls[enter]").
# Type "echo %time%" and verify that the time is the expected.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def set_time(time):

    # Initialize some variables
    time = "%d:%d" % (time.hour, time.minute)

    start_step("Set the system time to %s." % time)

    # Send the date command and check if it has worked well
    utils.send_keys("time [enter]")
    utils.send_keys("%s [enter]" % time)
    utils.send_keys("cls[enter]")
    utils.send_keys(r"echo %time% [enter]")

    # Check if the corret date has been displayed
    if not utils.wait_for_text_in_console(time, False, 3):
        time_from_screen = utils.match_text_in_console(r"\d{1,2}:\d{1,2}")

        if time_from_screen is not None:
            utils.take_screenshot()
            fail_step("[set_date] Time has not been set properly. Time found is: %s." % time_from_screen)

        else:
            utils.take_screenshot()
            fail_step("[set_date] Time has not been set properly. Could not find any time in the screen.")

    utils.take_screenshot()

# Function: start_awards
# Starts the awards application.
#
# Process:
# Sends "awards[enter]". Waits for "<--------CMC Is Now Operational" to be
# displayed on screen.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def start_awards(generate_log=True):

    if generate_log:
        start_step("Start Awards")
    else:
        start_step("Try to start awards (and move on in case it doesn't works)")
    #pdb.set_trace()

    # Go to the proper directory
    utils.send_keys("cd %s [enter]" % (DEFAULT_AWARDS_STARTUP_FOLDER))
    #pdb.set_trace()

    # Type something in the console
    utils.send_keys("awards[enter]")

    # Set the end time
    end_time = et(REBOOT_INIT_TIME)

    # Check if the sucess message is displayed on screen
    while not utils.find_text_in_console("(<--------CMC Is Now Operational)|(1 *Ready)|(1 *No Response No ping)",True) and ct() < end_time:
        time.sleep(3)

    # Find the CMC is now operational on screen
    if ct() > end_time:
        utils.send_keys("p[enter]")
        if not utils.find_text_in_console("(<--------CMC Is Now Operational)|(1 *Ready)|(1 *No Response No ping)",True):
            if generate_log:
                fail_sub_step("[start_awards] Could not find 'CMC Is Now Operational' on screen after %d seconds." % (REBOOT_INIT_TIME))
                utils.take_screenshot()
                utils.finish_execution()
            else:
                add_sub_step("[start_awards] Could not start awards. Not aborting the test.")

            return False

    utils.take_screenshot()
    pass_step("AWARDS Started Successully")
    return True


# Function: cds_force_batch_load
# Force the batch load for the CDS process.
#
# Process:
# Sends "Batchname CDSforceload force[enter]" to the awards box.
#
# Screen:
# Should be at the awards session screen.
#
# Author:
# Danilo Guimaraes
def cds_force_batch_load():

    start_step("Force a CDS batch load.")

    # Send the keys to for the cds batch load
    utils.send_keys("Batchname CDSforceload force[enter]")
    #
# Function: so_force_batch_load
# Force the batch load for the SO process.
#
# Process:
# Sends "Batchname database force[enter]" to the awards box.
#
# Screen:
# Should be at the awards session screen.
#
# Author:
# Danilo Guimaraes
def so_force_batch_load():

    start_step("Force a SO batch load.")

    # Send the keys to for the cds batch load
    utils.send_keys("Batchname database force[enter]")

# Function: load_cds_database
# Loads the CDS database.
#
# Parameters:
# - database_name: Name of the CDS database to be loaded. Optional, if not
# provided the system will load the default database.
#
# Process:
# Delete the folder C:\CDS that might exist in the system and extract the new
# CDS database using pkunzip.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def load_cds_database(database_name = DEFAULT_AWARDS_CDS_DATABASE):

    start_step("Load the CDS database '%s'." % database_name)

    # Delete any existing CDS folder and unzip the new one
    #if "need databse cleanup?" not in params or tu(params["need databse cleanup?"])=="":
    utils.send_keys(r"rm -rf C:\CDS[enter]")

    # unzip the new database
    print (database_name)

    print(DEFAULT_AWARDS_CDS_DATABASE_PATH)
    utils.send_keys(r"pkunzip %s\%s C:\ [enter]" % (DEFAULT_AWARDS_CDS_DATABASE_PATH, database_name) )

    utils.wait_for_text_in_console(r"pkunzip Done", False, DEFAULT_TIMEOUT)

    # Verify the zip has been extracted properly
    utils.send_keys(r"dir C:\CDS[enter]")

    if not (utils.wait_for_text_in_console(r"<DIR> +Mirror", True, 5) or utils.wait_for_text_in_console(r"<DIR> +mirror", True, 5)) :
        #
        fail_step("[load_cds_database] Could not load the CDS database.")
        #
        utils.take_screenshot()
        utils.finish_execution()

# Function: create_force_load_job
# Creates an empty file at C:\CDS\mirror\related_file with the name passed as
# parameter.
#
# Parameters:
# - job_name: job file name. If not provided, the default will be used.
#
# Process:
# Sends "type NUL > %s\%s[enter]" in order to create an empty file.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def create_force_load_job(job_name = DEFAULT_CDS_JOB_NAME):

    start_step("Create the file '%s%s'." % (DEFAULT_CDS_JOB_FILE_PATH, job_name) )

    # Create the empty file
    utils.send_keys(r"type NUL> %s\%s[enter]" % (DEFAULT_CDS_JOB_FILE_PATH, job_name) )
    #
    # Validate it has been created properly
    utils.send_keys("cls[enter]")
    utils.send_keys(r"dir %s\%s[enter]" % (DEFAULT_CDS_JOB_FILE_PATH, job_name) )
    #
    time.sleep(5)
    if not utils.wait_for_text_in_console(r"1 File(s)", False, 5):
        fail_step("[create_force_load_job] Could not create the file '%s%s'." % (DEFAULT_CDS_JOB_FILE_PATH, job_name) )
        utils.take_screenshot()
        utils.finish_execution()

# Function: copy_store_ini
# Copy the store.ini passed as parameter.
#
# Parameters:
# - ini_file_name: ini file name. Optional, if not provided the default SO
# process file is going to be used.
#
# Process:
# Copy the ini file to the default path.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def copy_store_ini(ini_file_name=DEFAULT_SO_STORE_INI_NAME):

    start_step("Copy the Store.ini from '%s\%s' to '%s'." % (DEFAULT_STORE_INI_ORIG_PATH, ini_file_name, DEFAULT_STORE_INI_DEST_PATH) )
    #
    # Send the copy command
    utils.send_keys("rm %s[enter]" % DEFAULT_STORE_INI_DEST_PATH)
    utils.send_keys("copy %s\%s %s[enter]" % (DEFAULT_STORE_INI_ORIG_PATH, ini_file_name, DEFAULT_STORE_INI_DEST_PATH) )

    # verify the copy has worked well
    utils.send_keys("cls[enter]")
    utils.send_keys("dir %s[enter]" % DEFAULT_STORE_INI_DEST_PATH)
    if not utils.wait_for_text_in_console("1 File(s)", False, 3):
        fail_step("[copy_store_ini] Could not copy the Store.ini from '%s\%s' to %s." %(DEFAULT_STORE_INI_ORIG_PATH, ini_file_name, DEFAULT_STORE_INI_DEST_PATH) )
        utils.take_screenshot()
        utils.finish_execution()

# Function: delete_store_ini
# Delete the store.ini currently at C:\strsystm\data
#
# Process:
# Run rm -rf at the DEFAULT_STORE_INI_DEST_PATH
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def delete_store_ini():

    start_step("Delete the store.ini at '%s'." % (DEFAULT_STORE_INI_DEST_PATH) )

    # Send the copy command
    utils.send_keys("rm %s[enter]" % DEFAULT_STORE_INI_DEST_PATH)

    # verify it has been deleted
    utils.send_keys("cls[enter]")
    utils.send_keys("dir %s[enter]" % DEFAULT_STORE_INI_DEST_PATH)


    if not utils.wait_for_text_in_console("File Not Found", False, 3):
        fail_step("[delete_store_ini] Could not delete the store.ini at %s." % (DEFAULT_STORE_INI_DEST_PATH) )
        utils.take_screenshot()
        utils.finish_execution()

# Function: copy_shuttle_ini
# Copy the default shuttle.ini.
#
# Process:
# Copy the ini file to the default path.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def copy_shuttle_ini():

    start_step("Copy the Shuttle.ini from '%s' to '%s'." % (DEFAULT_SHUTTLE_INI_ORIG_PATH, DEFAULT_SHUTTLE_INI_DEST_PATH) )

    # Send the copy command
    utils.send_keys("rm %s[enter]" % DEFAULT_SHUTTLE_INI_DEST_PATH)
    utils.send_keys("copy %s %s[enter]" % (DEFAULT_SHUTTLE_INI_ORIG_PATH, DEFAULT_SHUTTLE_INI_DEST_PATH) )

    # verify the copy has worked well
    utils.send_keys("cls[enter]")
    utils.send_keys("dir %s[enter]" % DEFAULT_SHUTTLE_INI_DEST_PATH)
    if not utils.wait_for_text_in_console("1 File(s)", False, 5):
        fail_step("[copy_store_ini] Could not copy the Shuttle.ini from %s to %s." (DEFAULT_SHUTTLE_INI_ORIG_PATH, DEFAULT_SHUTTLE_INI_DEST_PATH) )
        utils.take_screenshot()
        utils.finish_execution()

# Function: exit_dos_session
# Exits the current dos session.
#
# Process:
# Send "exit[enter]" to the awards box.
#
# Screen:
# Should be at a DOS session. (doh!)
#
# Author:
# Danilo Guimaraes
def exit_dos_session():

    start_step("Exit the DOS session.")
    utils.send_keys("exit[enter]")

# Function: load_cds_database
# Loads a SO database.
#
# Parameters:
# - database_name: Name of the OS database to be loaded. Optional, if not
# provided the system will load the default database.
#
# Process:
# Copy the SO file to "c:\strsystm\comm\"
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def load_so_database(database_name = DEFAULT_AWARDS_SO_DEFAULT_DATABASE):
    #
    start_step("Load the SO database '%s'." % database_name)

    # Copy the new database
    #
    utils.send_keys(r"copy %s\%s c:\strsystm\comm\ [enter]"  % (DEFAULT_AWARDS_SO_DATABASE_PATH, database_name) )
    #
    # Verify the zip has been extracted properly
    utils.send_keys(r"cls[enter]")
    utils.send_keys(r"dir %s\%s [enter]" % (r"c:\strsystm\comm", database_name) )
    if not utils.wait_for_text_in_console(r"1 File(s)", False, 5):
        fail_step("[load_cds_database] Could not load the SO database.")
        utils.take_screenshot()
        utils.finish_execution()

# Function: get_triggered_coupon_amount
# Returns the amount of coupos that have been triggered by the system so far.
#
# Process:
# Press [f4] until the amount of coupons is displayed on screen.
#
# Screen:
# Should be at the awards session screen.
#
# Author:
# Danilo Guimaraes
def get_triggered_coupon_amount():

    start_step("Get the amount of triggered coupons so far.")

    # Get the amount of coupons triggered
    coupon_amount = awards_utils.get_triggered_coupon_amount()
    utils.take_screenshot()

    # Add the information to the step
    pass_step("%d coupons have been triggered so far." % int(coupon_amount))

    return coupon_amount

# Function: run_ts
# Runs the TS process using the include file passed as parameter.
#
# Process:
# Copies the TS file to C:\, run the TS process. After that, dele the TS file
# from C:\.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def run_ts_with_inc_file(file_name = None,vault="No"):

    if file_name is None:
        fail_step("[run_ts] File name is None. Please provide a file name.")
        utils.finish_execution()

    start_step("Run TS for '%s'." % file_name)



    # Copy the TS file
    #utils.send_keys(r"copy %s\%s C:\ [enter]" % (DEFAULT_TS_FILE_PATH, file_name) )
    #utils.send_keys(r"cd C:\ [enter]")

    # Run the ts and trigger the process
    if tu(vault)=="YES":
        utils.send_keys(r"cd c:\vault [enter]")
        utils.send_keys(r"ts [enter]")
        ip=ms.awards_machine_ip.replace(".","")
        utils.send_keys("1,id,"+datetime.datetime.now().strftime("%Y%m%d%H%M")+str(ip)+"[enter]")
    else:
        utils.send_keys(r"ts [enter]")
    #utils.send_keys(r"inc,%s [enter]" % file_name)

    # Get the TS file from the repository
    #
    with open("%s\%s" % (DEFAULT_TS_FILE_REPO, file_name)) as ts_file:
        for r in ts_file:
            utils.send_keys("%s" % r)
            utils.send_keys("[enter]")
            time.sleep(2)

    utils.wait_for_bottom_row(r"^=1=> *$", True)

    # Leave TS
    time.sleep(2)
    utils.send_keys(r"exit[enter]")
    time.sleep(2)
    #
    if tu(vault)=="YES":
        utils.wait_for_bottom_row(r"^C:\\VAULT> *$", True)
    else:
        utils.wait_for_bottom_row(r"^C:\\> *$", True)

    # Delete the TS file
    #utils.send_keys(r"rm -rf C:\%s [enter]" % (file_name) )

# Function: run_rollover
# Triggers a report rollover in the Awards application.
#
# Process:
# Sends "rollover[enter]" to the awards box.
#
# Screen:
# Should be at a awards session screen.
#
# Author:
# Danilo Guimaraes
def run_rollover(logname=None):

    start_step("Run a report rollover.")
    if logname==None:
        utils.send_keys("rollover[enter]")
    else:
        utils.send_keys("rollover %s[enter]"%logname)
    if not utils.wait_for_text_in_console("Closing the current active application log and opening new log file"):
        fail_step("[run_rollover] Could not find the expected rollover feedback message on screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

# Function: generate_app_log_file
# Generates the app log file based on the last rollover that has been run.
#
# Process:
# Delete any existing log file with the same name in the logs folder. After
# that, runs the readlog command to generate the file. Waits until the prompt is
# available again.
#
# Screen:
# Should be at a awards session screen.
#
# Author:
# Danilo Guimaraes
def generate_app_log_file(log_file_name):

    start_step("Generate the app log file.")

    # Generate the log file
    utils.send_keys(r"cd C:\strsystm\log [enter]")
    utils.send_keys(r"rm -rf %s [enter]" % log_file_name)
    utils.send_keys(r"readlog ?????_??.* > %s [enter]" % log_file_name)

    utils.wait_for_bottom_row(r"^C:\\strsystm\\LOG>  +$", True)

    # Validate log file has been created
    utils.send_keys("cls[enter]")
    utils.send_keys(r"dir %s [enter]" % log_file_name)

    if not utils.wait_for_text_in_console("1 File(s)"):
        fail_step(r"[generate_app_log_file] Could not find the expected log file in the C:\log directory.")
        utils.finish_execution()
    exit_dos_session()
# Function: run_ts_with_parameters
# Run the TS process by typing the values on screen instead of using include
# files.
#
# Process:
# In a dos session, it will type TS and start typing whatever has been passed
# as parameter in the params list.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def run_ts_with_parameters(params):

    if len(params) == 0:
        fail_step("[run_ts_with_parameters] No parameter has been provided.")
        utils.finish_execution()

    start_step("Run TS using parameters.")

    # Run the ts and trigger the process
    utils.send_keys(r"ts [enter]")

    # Run each param
    leave_flag = True
    for param in params:
        utils.send_keys(r"%s [enter]" % params[param].strip())

        # Check if we are leaving TS already
        if tu(params[param]).find("EXIT") != -1:
            leave_flag = False

        # Else, wait for the bottom row
        else:
            utils.wait_for_bottom_row(r"^=1=> *$", True)

    # Leave TS
    if leave_flag:
        utils.send_keys(r"exit[enter]")
        utils.wait_for_bottom_row(r"^C:\\> *$", True)
        utils.wait_for_prompt("C:\\")

# Function: force_batch_run
# Force a batch run in the awards system.
#
# Process:
# Will type batchname %s force [enter] in an online prompt.
#
# Screen:
# Should be at a awards session.
#
# Author:
# Danilo Guimaraes
def force_batch_run(batchname):

    start_step("Force batch run: %s." % batchname)
    utils.send_keys("batchname %s force [enter]" % batchname)

# Function: generate_dblist
# Will dump a new dblist in a file.
#
# Process:
# Goes to the C:\ and type dblist > filename. After that, checks that the file
# has been created sucessfully.
#
# Screen:
# Should be at a DOS session, with Awards shut down.
#
# Author:
# Danilo Guimaraes
def generate_dblist(filename):

    start_step("Generate the dblist and export it to '%s'." % filename)

    # Go to C:\ to generate the file
    utils.send_keys(r"cd C:\ [enter")
    utils.send_keys(r"dblist > %s [enter]")
    utils.wait_for_command_prompt(r"C:\\")

    # Validate the file has been created
    utils.send_keys("cls [enter]")
    utils.send_keys("dir %s [enter]")
    if not utils.wait_for_text_in_console("1 File(s)", False, 5):
        fail_step("[generate_dblist] Could not copy the generated the file '%s'." (filename) )
        utils.take_screenshot()
        utils.finish_execution()

# Function: delete_file
# Will delete a file in the Awards box.
#
# Process:
# Runs rm -rf and then checks if the file exist.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def delete_file(filename):

    start_step("Delete the file '%s'." % filename)

    utils.send_keys("rm -rf %s [enter]" % filename)
    utils.send_keys("cls [enter]")
    utils.send_keys("dir %s [enter]" % filename)

    if not utils.wait_for_text_in_console("0 File\(s\)|File Not Found", True, 5):
        fail_step("[delete_file] Could delete the file '%s'." % (filename) )
        utils.take_screenshot()
        utils.finish_execution()

# Function: copy_file
# Will copy a file in the awards box
#
# Process:
# Runs copy in DOS session using the parameters passed.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def copy_file(origin, destination):

    start_step("Copy the file '%s' to '%s'." % (origin, destination))

    utils.send_keys("cd C:\ [enter]")
    utils.send_keys("copy %s %s [enter]" % (origin, destination))
    utils.send_keys("cls [enter]")
    utils.send_keys("dir %s [enter]" % destination)
    utils.wait_for_prompt(r"C:\\")

    if not utils.wait_for_text_in_console("1 File(s)", False, 5):
        fail_step("[copy_file] Could not copy the file '%s' to '%s'." % (origin, destination))
        utils.take_screenshot()
        utils.finish_execution()

# Function: generate_dw_log_file
# Will generate a DW log file in the C:\strsystm\LOG folder.
#
# Process:
# Go to the C:\strsystm\log and erase any existing file with filename passed as
# parameter. Then, runs readlog in order to generate the file. Finally, checks
# if the file has been created.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def generate_dw_log_file(log_file_name):

    start_step("Generate the DW log file.")

    # Generate the log file
    utils.send_keys(r"cd C:\strsystm\log [enter]")
    utils.send_keys(r"rm -rf %s [enter]" % log_file_name)
    utils.send_keys(r"readlog DW????02.??? > %s [enter]" % log_file_name)

    utils.wait_for_bottom_row(r"^C:\\strsystm\\LOG>  +$", True)

    # Validate log file has been created
    utils.send_keys("cls[enter]")
    utils.send_keys(r"dir %s [enter]" % log_file_name)

    if not utils.wait_for_text_in_console("1 File(s)"):
        fail_step(r"[generate_dw_log_file] Could not find the expected log file in the C:\strsystm\log directory.")
        utils.finish_execution()

# Function: run_newstore
# Go thru the newstore process.
#
# Process:
# Type newstore and go thru the new store process.
#
# Screen:
# Should be at a DOS session. Awards needs to be shut down.
#
# Author:
# Danilo Guimaraes
def run_newstore(country, printer, locale,store_num="250",chain_num="376",display="NO",printer_type="CMC6"):

    # List of values for selection
    countries = {
        "united states": "1",
        "france": "2",
        "italy": "3",
        "japan": "4",
        "united kingdom": "5",
        "belgium": "6",
        "spain": "7",
        "germany": "8",
        "mexico": "9"
    }

    printers = {
        "cmc6": "1",
        "cmc7": "2",
        "cmc5e02c": "3",
        "cmc5": "3",
        "cmc8": "4",
        "other": "5",
        "CMC9" :"6"
    }

    locales = {
        "us": "1",
        "fr": "2",
        "it": "3",
        "jp": "4",
        "uk": "5",
        "de": "6"
    }

    # Initiate the newstore process
    utils.send_keys("cd C:\\ [enter]")
    utils.send_keys("newstore [enter]")
    utils.wait_for_text_in_console("Please select the following from list", False, 10)
    utils.send_keys("1[enter]")

    # Select the country
    utils.wait_for_text_in_console("Which country does the current store will reside in?", False, 10)
    utils.send_keys("%s[enter]" % countries[tl(country)])
    time.sleep(2)

    # Select the printer type
    #pdb.set_trace()
    utils.wait_for_text_in_console("Which printer type is being installed?", False, 10)
    utils.send_keys("%s[enter]" % printers[tl(printer)])
    time.sleep(3)

    # Select the locale
    utils.wait_for_text_in_console("Which locale does the current store reside in?", False, 10)
    utils.send_keys("%s[enter]" % locales[tl(locale)])
    time.sleep(3)

    # Exit to prompt
    utils.wait_for_text_in_console("Please select from the following list", False, 30)


    # Wait for the prompt window
    if tu(display) == "NO":
        #pdb.set_trace()
        utils.send_keys("2[enter]")
        utils.wait_for_prompt("C:\\")
    #Configure, Store, Board, controller and Lane
    else:
        utils.send_keys("1[enter]")
        time.sleep(60)

        #pdb.set_trace()
        set_store_and_chain_number({"exit display":"NO","enter display":"no","store number":store_num,"chain number":chain_num})
        #pdb.set_trace()
        config_ethernet_board("10.8.70.138","10.8.70.1",enter_display="No",exit_display="NO")
        config_sdrstm_board(enter_display="No",exit_display="NO")
        #pdb.set_trace()
        configure_controller(enter_display="No",exit_display="NO")
        #pdb.set_trace()
        add_printer_type(printer_type,delete_lane="NO",enter_display="No")

# Function: set_printer_type
# Set a different printer type in Lane 1.
#
# Process:
# - Load Display
# - Go to the lane configuration screen
# - Select changes for lane 1 accordingly to the parameters
# - Apply the changes
# - Leave to DOS
#
# Note:
# When setting a CMC6 printer type or any other printer that does not have the
# proper driver already loaded (usually anything > than CMC4) the system will
# display a message stating that the system could not load the proper drivers
# the selected printer. This is expected, and there is some code to handle that
# error message when it shows up.
#
# Screen:
# Should be at a DOS session. Awards needs to be shut down.
#
# Author:
# Danilo Guimaraes
def set_printer_type(printer_type):

    start_step("Set the printer type to '%s' in the display application." % printer_type)

    # Amount of times the key down needs to be pressed in the printer type drop
    # down.
    printer_options = {
        "unknown": 0,
        "cmc6": 1,
        "cmc7": 2,
        "cmc5": 3,
        "cmc4": 4,
        "sesenta1": 5,
        "stdprinter": 6,
        "cmc5e02c": 7,
        "oni": 8
    }

    awards_utils.load_display_application()

    # Press C to get into the configuration menu
    utils.send_keys("C")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_printer_type] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press L to get into the lane configuration summary screen
    utils.send_keys("L")
    if not utils.wait_for_text_in_console("LANE CONFIGURATION SUMMARY", False, 15):
        fail_step("[set_printer_type] Could not load the lane configuration menu.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press C to open the change lane configuration screen
    utils.send_keys("C")
    if not utils.wait_for_text_in_console("CHANGE LANE CONFIGURATION", False, 15):
        fail_step("[set_printer_type] Could not load the lane configuration change screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press down a bunch of times in order to reach the printer type field and
    # then press enter to open the drop down.
    utils.send_keys("[down][down][down][down][down][down][enter]")

    # Press [down] a certain amount of times in order to reach the right option
    # in the dropdown
    for i in range(0, printer_options[tl(printer_type)]):
        utils.send_keys("[down]")


    # Press enter to confirm the selection
    utils.send_keys("[enter]")

    # Press F1 to confirm the changes
    utils.send_keys("[f1]")
    if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 15):
        fail_step("[set_printer_type] Could not find the changes confirmation message on screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    utils.send_keys("[f1]")
    if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 15):
        fail_step("[set_printer_type] Could not find the changes confirmation message on screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press Y to confirm the changes
    # Added by Smitha
    utils.send_keys("Y")
    if not utils.wait_for_text_in_console("MAC ADDRESS-LANE MAPPING", False, 60):
        fail_step("[set_printer_type] Could not find the changes confirmation message on screen.")
        utils.take_screenshot()

    #
    utils.send_keys("[esc]")

    if not utils.wait_for_text_in_console("LANE CONFIGURATION SUMMARY", False, 15):
        if utils.wait_for_text_in_console("PRINTER ENUMERATION ERROR", False, 60):
            utils.send_keys("[esc]")
            time.sleep(15)



        else:
            fail_step("[set_printer_type] Could not load the lane configuration change screen after confirming changes to the lane setup.")
            utils.take_screenshot()
            utils.finish_execution()

    utils.take_screenshot()
    #
    # Press E to leave to the previous screen
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_printer_type] Could not leave to the configuration menu.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press E to go to the main menu
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("MAIN MENU", False, 15):
        fail_step("[set_printer_type] Could not leave to the main menu.")
        utils.take_screenshot()
        utils.finish_execution()

    # Leave to the exit confirmation screen
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("Exit program?", False, 15):
        fail_step("[set_printer_type] Could not reach the exit confirmation screen.")
        utils.take_screenshot()
        utils.finish_execution()

    # Leave to the prompt
    utils.send_keys("Y")

# Function: set_print_yes
# Set the print to be yes for a particular CLU
#
# Process:
# - Load Display
# - Go to the Database screen
# - Search for the particular CLU
# - Update the print? to Yes
# - Apply the change
# - Leave to DOS
#
# Note:
# We are doing this in order to make sure only the CLU which we are intended to print
# are being print and nothing other
#
# Screen:
# Should be at a DOS session. Awards needs to be shut down.
#
# Author:
# Ajay Keerthi G
def set_print_yes(params):
    #
    coupon_number = params["clu"]
    print_flag = params["print flag"]
    if "validation flag" not in params or params["validation flag"]=="" :
        params["validation flag"]="No"

    validation_flag = params["validation flag"]


    start_step("Set the print to '%s' in the display application." % print_flag)

    # Amount of times the key down needs to be pressed in the printer type drop
    # down.
    awards_utils.load_display_application()

    # Press D to get into the database menu
    utils.send_keys("D")
    if not utils.wait_for_text_in_console("DATABASE MENU", False, 15):
        fail_step("[set_print_yes] Could not load the database menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press C to get into the Coupon information menu screen
    utils.send_keys("C")
    if not utils.wait_for_text_in_console("COUPON DATABASE MENU", False, 15):
        fail_step("[set_print_yes] Could not load the coupon database menu.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press C to open the coupon definition maintenance screen
    utils.send_keys("C")
    if not utils.wait_for_text_in_console("COUPON DEFINITION MAINTENANCE", False, 15):
        fail_step("[set_print_yes] Could not load the coupon definition maintenance screen.")
        utils.take_screenshot()
        utils.finish_execution()
    #
    strcoupon = str(coupon_number)
    match = re.findall(",", strcoupon)
    if len(match) > 0:
        new_coupon_num = strcoupon.split(",")
        for icpnm in new_coupon_num:
            coupon_number = icpnm
            change_coupon_definition(coupon_number, print_flag)
    else:
        change_coupon_definition(coupon_number, print_flag)
    #
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("COUPON DATABASE MENU", False, 15):
        fail_step("[set_print_yes] Could not leave to the coupon database menu.")
        utils.take_screenshot()
        utils.finish_execution()

        utils.take_screenshot()
    if "validation flag" in params or tu(params["validation flag"]) == "YES":
        # Press V to go to the COUPON VALIDATION MAINTENANCE screen
        utils.send_keys("V")
        if not utils.wait_for_text_in_console("COUPON VALIDATION MAINTENANCE", False, 15):
            fail_step("[change_coupon_validation] Could not load the coupon validation maintenance screen.")
            utils.take_screenshot()
            utils.finish_execution()

        utils.take_screenshot()
        strcoupon = str(coupon_number)
        match = re.findall(",", strcoupon)
        if len(match) > 0:
            new_coupon_num = strcoupon.split(",")
            for icpnm in new_coupon_num:
                coupon_number = icpnm
                change_coupon_validation(coupon_number, validation_flag)
        else:
            change_coupon_validation(coupon_number, validation_flag)
    # Press E to leave to the previous screen
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("COUPON DATABASE MENU", False, 15):
        fail_step("[set_print_yes] Could not leave to the coupon database menu.")
        utils.take_screenshot()
        utils.finish_execution()

    # Press E to go to the database menu
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("DATABASE MENU", False, 15):
        fail_step("[set_print_yes] Could not leave to the database menu.")
        utils.take_screenshot()
        utils.finish_execution()

    # Press E to go to the main menu
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("MAIN MENU", False, 15):
        fail_step("[set_print_yes] Could not leave to the main menu.")
        utils.take_screenshot()
        utils.finish_execution()

    # Leave to the exit confirmation screen
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("Exit program?", False, 15):
        fail_step("[set_printer_type] Could not reach the exit confirmation screen.")
        utils.take_screenshot()
        utils.finish_execution()

    # Leave to the prompt
    utils.send_keys("Y")
    #change_coupon_validation(params)

#Function: change_coupon_validation
def change_coupon_validation(cpn_num, validate_flag):
    # Press C to open the change coupon validation screen
    utils.send_keys("C")
    if not utils.wait_for_text_in_console("CHANGE A COUPON VALIDATION", False, 15):
        fail_step("[change_coupon_validation] Could not load the change a coupon validation screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    #
    # enter the coupon number we are going to change and click enter
    utils.send_keys("%s[enter]" % cpn_num)
    #
    # Press down a bunch of times in order to reach the printer type field and
    # then press enter to open the drop down.
    utils.send_keys("[down]")

    # Press enter to confirm the selection

    utils.send_keys("V")
    utils.send_keys("[enter]")
    #pdb.set_trace()

    #pdb.set_trace()
    # Press F1 to confirm the changes
    utils.send_keys("[f1]")
    #
    if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 20):
        fail_step("[set_print_yes] Could not find the changes confirmation message on screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press Y to confirm the changes
    utils.send_keys("Y")
    if not utils.wait_for_text_in_console("COUPON VALIDATION MAINTENANCE", False, 15):

        fail_step("[set_print_yes] Could not load the coupon definition maintenance screen after confirming changes to the coupon change screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
# Function: replace_batch_ini
# Replace the batch.ini in the system.
#
# Process:
# - Delete the current batch.ini
# - Copy the batch.ini to the proper path.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def replace_batch_ini(filename):

    # Do the copy
    utils.copy("rm -rf %s[enter]" % (DEFAULT_BATCH_INI_PATH))
    utils.copy("copy %s %s[enter]" % (filename, DEFAULT_BATCH_INI_PATH))

    # verify the copy has worked well
    utils.send_keys("cls[enter]")
    utils.send_keys("dir %s[enter]" % DEFAULT_BATCH_INI_PATH)
    if not utils.wait_for_text_in_console("1 File(s)", False, 5):
        fail_step("[replace_batch_ini] Could not copy the new batch.ini from %s to %s." (filename, DEFAULT_BATCH_INI_PATH) )
        utils.take_screenshot()
        utils.finish_execution()

# Function: reconnect
# Will disconnect the TRAPI and then will connect it again.
#
# Process:
# - Will disconnect and reconnect the trapi socket.
#
# Note:
# Currently disabled, everything is commented out.
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def reconnect():
    pass
    #start_step("Reconnect to the awards Box in order to cleanup the memory.")
    # Close the connection and wait 2 seconds for the Awards box to close the
    # existing socket
    # trapi.close()
    # time.sleep(2)
    # trapi.connect(ms.awards_machine_ip, ms.awards_machine_port_number)
    #
    # # Test if the connection happened properly
    # if trapi.status != TrapiStatus.connected:
    #     fail_step("Could not connect to the Awards box.")
    #     utils.finish_execution()

# Function: run_cmcset_for_all_coupons
# Will turn on all coupons for the current date. This process avoids the
# automation to fail when a long timespan has passed.
#
# Process:
# - Will terminate cmc and wait 20 seconds for it
# - Will run cmcset -a -d and wait 5 seconds
# - Will run cmcset -a -n and wait 5 seconds
# - Will run start cmc
#
# Note 1:
# There is no proper visual feedback for terminate cmc, so for that reason we'll
# have a 20-second wait after running this command. In case the coupons are not
# being turned on, check if this timeout is being enought before trying to run
# the other commands. You can tweak it (increase the timeout, for example) if
# 20 seconds is not being enought.
#
# Note 2:
# After running start cmc, you might want to invoke <validate_system_is_running>
# to make sure the system is running already. It will show
# <--------CMC IS NOW OPERATIONAL--------> on screen after the system boots up.
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def run_cmcset_for_all_coupons():

    start_step("Run cmcset -a -d and cmcset -a -n.")

    utils.send_keys("terminate cmc[enter]")
    time.sleep(20)
    utils.take_screenshot()


    utils.send_keys("call cmcset -a -d[enter]")
    time.sleep(5)
    utils.take_screenshot()


    utils.send_keys("call cmcset -a -n[enter]")
    time.sleep(5)
    utils.take_screenshot()

    utils.send_keys("start cmc[enter]")
    utils.take_screenshot()

# Function: run_cmcset_off_for_all_coupons
# Will turn off all coupons for the current date. This process avoids the
# automation to fail when a long timespan has passed.
#
# Process:
# - Will terminate cmc and wait 20 seconds for it
# - Will run cmcset -a -d and wait 5 seconds
# - Will run cmcset -a -f and wait 5 seconds
# - Will run start cmc
#
# Note 1:
# There is no proper visual feedback for terminate cmc, so for that reason we'll
# have a 20-second wait after running this command. In case the coupons are not
# being turned on, check if this timeout is being enought before trying to run
# the other commands. You can tweak it (increase the timeout, for example) if
# 20 seconds is not being enought.
#
# Note 2:
# After running start cmc, you might want to invoke <validate_system_is_running>
# to make sure the system is running already. It will show
# <--------CMC IS NOW OPERATIONAL--------> on screen after the system boots up.
#
# Screen:
# N/A
#
# Author:
# Ajay Keerthi G
def run_cmcset_off_for_all_coupons():

    start_step("Run cmcset -a -d and cmcset -a -f and turn on the cmcset on for a CLU")

    utils.send_keys("terminate cmc[enter]")
    time.sleep(30)

    utils.take_screenshot()

    utils.send_keys("call cmcset -a -d[enter]")
    time.sleep(5)
    utils.take_screenshot()

    utils.send_keys("call cmcset -a -f[enter]")
    time.sleep(5)
    utils.take_screenshot()

    utils.send_keys("start cmc[enter]")
    time.sleep(20)
    utils.take_screenshot()


def upload_from_sut_to_ftp_server(files, ip=DEFAULT_FTP_SERVER_ADDRESS, username=DEFAULT_FTP_SERVER_USERNAME, password=DEFAULT_FTP_SERVER_PASSWORD):

    start_action("Transfer files via FTP.")

    # Connect to the FTP server
    start_step("Open the FTP application.")
    utils.send_keys("cd C:\\ [enter]")
    utils.send_keys("xptools\\ftp.exe[enter]")
    utils.wait_for_text_in_console("ftp>")
    utils.take_screenshot()

    start_step("Connect to the FTP Server.")
    utils.send_keys("open %s[enter]" % ip)
    utils.take_screenshot()

    utils.wait_for_text_in_console("User \(.*\):", regex_flag = True)
    utils.send_keys("%s[enter]" % username)
    utils.take_screenshot()

    utils.wait_for_text_in_console("Password: ")
    utils.send_keys("%s[enter]" % password)
    utils.take_screenshot()

    # Make sure we are connected to the server
    utils.wait_for_text_in_console("230 Logged on")
    utils.take_screenshot()

    start_step("Switch to passive mode.")
    utils.send_keys("quote pasv[enter]")
    utils.wait_for_text_in_console("227 Entering Passive Mode")
    utils.take_screenshot()

    start_step("Switch the transfer mode to binary.")
    utils.send_keys("binary[enter]")
    utils.wait_for_text_in_console("200 Type set to I")
    utils.take_screenshot()

    start_step("Delete any existing file with the same filename.")
    for filename in files:
        utils.send_keys("delete %s[enter]" % (filename))
        utils.take_screenshot()

    # Put each file into the folder
    start_step("Transfer each of the files.")
    for filename in files:
        utils.send_keys("put %s[enter]" % (files[filename]))
        #utils.wait_for_text_in_console("226 Successfully transferred \"/%s\"" % filename)
        utils.wait_for_prompt("ftp")
        utils.take_screenshot()

    # Close the FTP session
    start_step("Close the FTP session.")
    utils.send_keys("bye[enter]")
    utils.wait_for_prompt(r"C:\\")
    utils.take_screenshot()

    end_action()

def download_from_ftp_to_local(files, local_folder, ip=DEFAULT_FTP_SERVER_ADDRESS, username=DEFAULT_FTP_SERVER_USERNAME, password=DEFAULT_FTP_SERVER_PASSWORD):

    start_action("Retrieve files from the FTP server at %s." % ip)

    # Just a flag
    connected = False

    try:

        # Connect to the FTP server
        start_step("Connect to the FTP server.")
        ftp = ftplib.FTP(DEFAULT_FTP_SERVER_ADDRESS)
        connected = True
        pass_step("Connected.")

        # Log into the server
        start_step("Log into the FTP server with the username '%s'." % (username))
        ftp.login(DEFAULT_FTP_SERVER_USERNAME, DEFAULT_FTP_SERVER_PASSWORD)
        pass_step("Logged In.")

        # Switch the connection to PASV
        start_step("Switch the connection to passive mode.")
        ftp.set_pasv(True)
        pass_step("Connection switched to passive mode.")

        # Download each of the files into the local directory


        for filename in files:

            # Download the file
            start_step("Download the file '%s' and save it at '%s'." % (filename, local_folder))


            if filename in ftp.nlst():
                #
                ftp.retrbinary("RETR %s" % filename,
                    open(r"%s\%s" % (local_folder, filename), 'wb').write)

    except:
        fail_step("Something bad happened while dealing with the FTP server.<br>Information:<br>%s" % str(sys.exc_info()))
        utils.finish_execution()

    # Try to close the connection in case we expect to be connected
    if connected:
        start_step("Closing the connection.")
        ftp.quit()

    end_action()

def upload_from_local_to_ftp(files, ip=DEFAULT_FTP_SERVER_ADDRESS, username=DEFAULT_FTP_SERVER_USERNAME, password=DEFAULT_FTP_SERVER_PASSWORD):

    start_action("Upload files to the FTP server at %s." % ip)

    # Just a flag
    connected = False

    try:

        # Connect to the FTP server
        start_step("Connect to the FTP server.")
        ftp = ftplib.FTP(DEFAULT_FTP_SERVER_ADDRESS)
        connected = True
        pass_step("Connected.")

        # Log into the server
        start_step("Log into the FTP server with the username '%s'." % (username))
        ftp.login(DEFAULT_FTP_SERVER_USERNAME, DEFAULT_FTP_SERVER_PASSWORD)
        pass_step("Logged In.")

        # Switch the connection to PASV
        start_step("Switch the connection to passive mode.")
        ftp.set_pasv(True)
        pass_step("Connection switched to passive mode.")
        print(files)

        # Upload each of the files in the list
        for filename in files:

            start_step("Upload the file '%s'." % (filename))
            print(files[filename]+"\\"+filename)
            actual_file = open(files[filename]+"\\"+filename, 'rb')

            ftp.storbinary("STOR %s" % filename, actual_file)

            #ftp.transfercmd("STOR %s" % actual_file)
            actual_file.close()


    except:
        fail_step("Something bad happened while dealing with the FTP server.<br>Information:<br>%s" % str(sys.exc_info()))
        utils.finish_execution()

    # Try to close the connection in case we expect to be connected
    if connected:
        start_step("Closing the connection.")
        ftp.quit()

    end_action()

def download_from_ftp_to_sut(files, ip=DEFAULT_FTP_SERVER_ADDRESS, username=DEFAULT_FTP_SERVER_USERNAME, password=DEFAULT_FTP_SERVER_PASSWORD):

    start_action("Download files via FTP.")
    #
    # Connect to the FTP server
    start_step("Open the FTP application.")
    utils.send_keys("cd C:\\ [enter]")
    utils.send_keys("xptools\\ftp.exe[enter]")
    utils.wait_for_text_in_console("ftp>")
    utils.take_screenshot()

    start_step("Connect to the FTP Server.")
    utils.send_keys("open %s[enter]" % ip)
    utils.take_screenshot()

    utils.wait_for_text_in_console("User \(.*\):", regex_flag = True)
    utils.send_keys("%s[enter]" % username)
    utils.take_screenshot()

    utils.wait_for_text_in_console("Password: ")
    utils.send_keys("%s[enter]" % password)
    utils.take_screenshot()

    # Make sure we are connected to the server
    utils.wait_for_text_in_console("230 Logged on")
    utils.take_screenshot()

    start_step("Switch to passive mode.")
    utils.send_keys("quote pasv[enter]")
    utils.wait_for_text_in_console("227 Entering Passive Mode")
    utils.take_screenshot()

    start_step("Switch the transfer mode to binary.")
    utils.send_keys("binary[enter]")
    utils.wait_for_text_in_console("200 Type set to I")
    utils.take_screenshot()
    #
    # Download each of the files
    start_step("Transfer each of the files.")
    for filename in files:
        utils.send_keys("get %s [enter]" % ( filename))
        utils.wait_for_prompt("ftp")
        utils.take_screenshot()

    # Close the FTP session
    start_step("Close the FTP session.")
    utils.send_keys("bye[enter]")
    utils.wait_for_prompt(r"C:\\")
    utils.take_screenshot()

    end_action()

def download_daily_build_from_server(params,builddate=None, build_folder=None):
    start_step("Download the daily build file from the CI folder.")

    # Initializing some values
    filename=""
    print(builddate)
    now = datetime.datetime.now()
    if tu(params["version"])=="DAILY" or  tu(params["version"])=="NIGHTLY":
        filename = DEFAULT_UPDATE_FILENAME
    if tu(params["version"])=="FIRSTUPD":
        filename=DEFAULT_UPD_FILENAME
    if tu(params["version"])=="STORELAB":
        if tu(params["patch"])=="YES":
            filename = DEFAULT_UPDATE_FILENAME
        else:
            filename=DEFAULT_STORE_FILENAME
    if tu(params["version"])=="STRSYSTM":

        filename= DEFAULT_STR_FILENAME
        print(filename)

    local_path = DEFAULT_UPDATE_LOCAL_FILEPATH + "\%s" %ms.machine_name
    if build_folder==None:
        remote_path = DEFAULT_CI_NIGHTLY_BUILD_PATH

        if not os.path.exists(local_path):
            #shutil.rmtree(local_path, ignore_errors=True)
            os.makedirs(local_path)
        print(builddate)

        if builddate==None:
            expected_remote_path_name = "%04d%02d%02d" % (now.year, now.month, now.day)
        else:
            expected_remote_path_name=builddate
        #expected_remote_path_name="20161205"
        print(expected_remote_path_name)
        #
        # Check if we have a directory with the current day (that means, if we have
        # the build in place already)
        #
        alist={}
        directory=[]
        directories = os.listdir(remote_path)
        latest=""
        print (directories)

        # Get the latest folder in the mightly build directory
        #Added by Smitha D
        build_files=[]
        for file in directories:
            if expected_remote_path_name in file:
                build_files.append(file)
        for file in build_files:
            if os.path.isdir(os.path.join(remote_path,file)):
                timestamp = os.path.getmtime( os.path.join(remote_path,file ))
                # get timestamp and directory name and store to dictionary
                alist[os.path.join(remote_path,file)]=timestamp

            # sort the timestamp
        for i in sorted(alist.items(), key=operator.itemgetter(1)):
            latest="%s" % ( i[0])
        # latest=sorted(alist.iteritems(), key=operator.itemgetter(1))[-1]
        print ("newest directory is ", latest)
        if expected_remote_path_name in latest:
            directory = [latest.split("\\")[-1]]
            print(directory)


        #
        # Check if we have the folder
        if len(directory) == 0:
            pass_step("[download_daily_build_from_server] No new build triggered for the date '%s' in the nightly staging folder." % expected_remote_path_name)
            sys.exit(0)

        # If we found it, assign the directory name to the variable
        directory = directory[0]

        # Now, we need to retrieve the subfolder that will contain the build name
        remote_path = "%s\%s" % (remote_path, directory)
        directories = os.listdir(remote_path)

        if len(directories) == 0:
            pass_step("[download_daily_build_from_server] Could not find a subbolder under '%s'." % remote_path)
            sys.exit(0)

        directory = directories[0]

        # Store the build name that we`ll return later
        build_version = directory[4:-4]

        # Finally, let's check if we have the update file in that folder
        remote_path = "%s\%s" % (remote_path, directory)
    else:
        if not os.path.exists(local_path):
            #shutil.rmtree(local_path, ignore_errors=True)
            os.makedirs(local_path)
        remote_path = build_folder
        build_version = builddate
    files = os.listdir(remote_path)
    print (files)

    #
    print (filename)

    if not filename in files:
        fail_step("[download_daily_build_from_server] Could not find the file '%s' at '%s'." % (filename, remote_path))
        sys.exit(0)

    # Download the file

    file_path = "%s\%s" % (remote_path, filename)
    shutil.copy(file_path, local_path)
    print(file_path)
    pdb.set_trace()
    #os.rename("%s\\%s"%(local_path, filename),"%s\\%s"%(local_path, DEFAULT_UPD_FILENAME))
    #
    return build_version

def capture_clus_from_screen():

    start_step("Capture CLUs from the screen.")

    utils.send_keys("[f6]")

    # Get the value from screen
    if not utils.wait_for_text_in_console("CLUS", False, 5):
        fail_step("[capture_clus_from_screen] Could not capture the amount of CLUS from the screen.")
        utils.finish_execution()

    screen = utils.get_console_text()
    clus = re.findall("\d+ *CLUS", screen)

    # Check if we found the CLUs on screen
    if len(clus) == 0:
        fail_step("Could not capture CLUs from screen.")
        utils.take_screenshot()
        utils.finish_execution()

    # Isolate the clus number in the string
    clus = re.findall("\d+", clus[0])[0]
    utils.take_screenshot()

    pass_step("CLUs '%s'." % clus)

    return int(clus)

def capture_messages_from_screen():

    start_step("Capture MESSAGES from the screen.")

    utils.send_keys("[f6]")

    # Get the value from screen
    if not utils.wait_for_text_in_console("MESSAGES", False, 5):
        fail_step("[capture_clus_from_screen] Could not capture the amount of MESSAGES from the screen.")
        utils.finish_execution()

    screen = utils.get_console_text()
    messages = re.findall("\d+ *MESSAGES", screen)

    # Check if we found the CLUs on screen
    if len(messages) == 0:
        fail_step("Could not capture MESSAGES from screen.")
        utils.take_screenshot()
        utils.finish_execution()

    # Isolate the clus number in the string
    messages = re.findall("\d+", messages[0])[0]
    utils.take_screenshot()

    pass_step("MESSAGES '%s'." % messages)

    return messages

def capture_pictures_from_screen():

    start_step("Capture PICTURES from the screen.")

    utils.send_keys("[f6]")

    # Get the value from screen
    if not utils.wait_for_text_in_console("PICTURES", False, 5):
        fail_step("[capture_clus_from_screen] Could not capture the amount of PICTURES from the screen.")
        utils.finish_execution()

    screen = utils.get_console_text()
    pictures = re.findall("\d+ *PICTURES", screen)

    # Check if we found the CLUs on screen
    if len(pictures) == 0:
        fail_step("Could not capture PICTURES from screen.")
        utils.take_screenshot()
        utils.finish_execution()

    # Isolate the clus number in the string
    pictures = re.findall("\d+", pictures[0])[0]
    utils.take_screenshot()

    pass_step("PICTURES '%s'." % pictures)

    return pictures

def capture_triggers_from_screen():

    start_step("Capture TRIGGERS from the screen.")

    utils.send_keys("[f6]")

    # Get the value from screen
    if not utils.wait_for_text_in_console("TRIGGERS", False, 5):
        fail_step("[capture_clus_from_screen] Could not capture the amount of TRIGGERS from the screen.")
        utils.finish_execution()

    screen = utils.get_console_text()
    triggers = re.findall("\d+ *TRIGGERS", screen)

    # Check if we found the CLUs on screen
    if len(triggers) == 0:
        fail_step("Could not capture TRIGGERS from screen.")
        utils.take_screenshot()
        utils.finish_execution()

    # Isolate the clus number in the string
    triggers = re.findall("\d+", triggers[0])[0]
    utils.take_screenshot()

    pass_step("TRIGGERS '%s'." % triggers)

    return triggers

def set_store_and_chain_number(params):

    start_step("Set store and chain number to %s/%s." % (params["store number"], params['chain number']) )
    if "enter display" not in params or params["enter display"]=="":
        params["enter display"]="YES"
    if "exit display" not in params or params["exit display"]=="":
        params["exit display"]="YES"
    if "market" not in params or params["market"]=="":
        params["market"]="01"
    if "pix num" not in params or params["pix num"]=="":
        params["pix num"]="01"
    awards_utils.load_display_application(params["enter display"])

    # Press C to get into the configuration menu
    utils.send_keys("C")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_store_and_chain_number] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press S to get into the store configuration menu
    utils.send_keys("S")
    if not utils.wait_for_text_in_console("STORE CONFIGURATION SUMMARY", False, 15):
        fail_step("[set_store_and_chain_number] Could not load the store configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press C to get into the change store configuration menu
    utils.send_keys("C")
    if not utils.wait_for_text_in_console("CHANGE STORE CONFIGURATION SUMMARY", False, 15):
        fail_step("[set_store_and_chain_number] Could not load the change store configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Type the data that needs to be changed
    utils.send_keys(params["store number"])
    utils.send_keys("[tab] store[tab]")
    utils.send_keys(params["chain number"])
    utils.send_keys("[tab]")
    utils.send_keys(params["chain number"])
    utils.send_keys("[tab][tab][tab][tab]")
    utils.send_keys(params["market"])
    utils.send_keys("[tab][tab][tab]")
    utils.send_keys(params["pix num"])

    # Save the changes
    utils.send_keys("[f1]")
    if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 15):
        fail_step("[set_store_and_chain_number] Could not load the change confirmation message in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Leave the display application
    utils.send_keys("Y")
    if not utils.wait_for_text_in_console("STORE CONFIGURATION SUMMARY", False, 15):
        fail_step("[set_store_and_chain_number] Could not load the store configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
    #pdb.set_trace()

    utils.send_keys("[esc]")
    #pdb.set_trace()
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_store_and_chain_number] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    #pdb.set_trace()
    if tu(params["exit display"]) == "YES":
        utils.take_screenshot()

        utils.send_keys("E")
        if not utils.wait_for_text_in_console("MAIN MENU", False, 15):
            fail_step("[set_store_and_chain_number] Could not load the main menu in the display application.")
            utils.take_screenshot()
            utils.finish_execution()

        utils.take_screenshot()

        utils.send_keys("E")
        if not utils.wait_for_text_in_console("Exit program?", False, 15):
            fail_step("[set_store_and_chain_number] Could not load the exit confirmation message in the display application.")
            utils.take_screenshot()
            utils.finish_execution()

        utils.take_screenshot()

        utils.send_keys("Y")

def wait_for_no_connection(timeout=DEFAULT_TIMEOUT):
    add_sub_step("Wait for the awards machine to be reachable through the network.")
    utils.wait_for_no_connection(timeout)

def wait_for_connection(timeout=DEFAULT_TIMEOUT):
    add_sub_step("Wait for the awards machine to be reachable through the network.")
    utils.wait_for_connection(timeout)

def capture_awards_version():
    start_step("Capture the version of the Awards system that is currently running.")

    # Show up ther version on screen and capture it
    utils.send_keys("[f1]")
    time.sleep(2)
    version = utils.match_text_in_console("(AWARDS: +?((\\d+?\\.){3})\\d+?)")

    # Check if we got to find the version on screen
    if version is None or len(version)==0:
        fail_step("[capture_awards_version] Could not capture the awards version from screen.")
        sys.exit(0)

    # Remove the AWARDS : from the string
    version = version[0]
    version = version.replace("AWARDS: ", "")

    # Return it without surrounding spaces
    pass_step("Current awards version is '%s'." % tu(version))
    return tu(version)

def capture_os_version():
    start_step("Capture the version of the OS that is currently running.")

    # Show up ther version on screen and capture it
    utils.send_keys("[f1]")
    version = utils.match_text_in_console("(OS: +((\\d+\\.){3})\\d+)")

    # Check if we got to find the version on screen
    if version is None or len(version)==0:
        fail_step("[capture_os_version] Could not capture the os version from screen.")
        sys.exit(0)

    # Remove the AWARDS : from the string
    version = version[0].replace("OS: ", "")

    # Return it without surrounding spaces
    pass_step("Current OS version is '%s'." % tu(version))
    return tu(version)

def capture_build_date():
    start_step("Capture the build date of the OS that is currently running.")

    # Get the entry from the INI file
    version = utils.get_ini_file_value(DEFAULT_STORE_INI_DEST_PATH, "SoftwareVersion", "OSBuildDate")

    # Check if we got to find the version on screen
    if version is None:
        fail_step("[capture_build_date] Could not capture the build date from the store.ini.")
        sys.exit(0)

    # Remove the quotes from the string
    version = version.replace('"', '')

    # Return it without surrounding spaces
    pass_step("Current Build Date version is '%s'." % tu(version))
    return tu(version)

def update_config_file(awards_version, os_version, build_date,new_build_version):

    # Build the config file path. It will always be at the framework root
    config_file_path = "%s\%s" % (base_path, "config.py")

    # Get the config file contents
    fh= open(config_file_path,"r+")
    #commented and open file in different format
    '''with open(config_file_path, "w+") as cfg_file:
        file_contents = cfg_file.readlines()'''
    file_content=fh.readlines()
    # Now that we have the contents, find any row with the values and comment
    # You might want to cleanup the file from time to time
    for  row in range(0,len(file_content)):#fh.readlines():
        if "awards_version" in file_content[row]:
            print (awards_version)

            file_content[row]=file_content[row].split("=")[0]+'="' + awards_version +'"\n'

        if "os_version" in file_content[row]:
            file_content[row]=file_content[row].split("=")[0]+'="' +  os_version +'"\n'
        if   "daily_build_ver" in file_content[row]:
            file_content[row]=file_content[row].split("=")[0]+'="' +  new_build_version+'"\n'
        if "build_version" in file_content[row]:
            file_content[row]=file_content[row].split("=")[0]+'="' +  build_date+'"\n'

    # Add the new statements to the file
    #Not needed
    '''file_contents.insert(0, 'build_date = "%s"\n' % build_date)
    file_contents.insert(0, 'os_version = "%s"\n' % os_version)
    file_contents.insert(0, 'awards_version = "%s"\n' % awards_version)
    file_contents.insert(0, 'build_version = "%s"\n' % build_version)'''
    # Write the new config file
    with open(config_file_path, "w+") as cfg_file:
        for row in file_content:
            cfg_file.write(row)


def change_coupon_definition(coupon_number, print_flag):
    # Press C to open the change coupon definition screen
    utils.send_keys("C")
    if not utils.wait_for_text_in_console("CHANGE A COUPON DEFINITION", False, 15):
        fail_step("[set_print_yes] Could not load the change a coupon definition screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
    #
    # enter the coupon number we are going to change and click enter
    utils.send_keys("%s[enter]" % coupon_number)
    time.sleep(3)
    #
    # Press down a bunch of times in order to reach the printer type field and
    # then press enter to open the drop down.
    utils.send_keys("[down][down][down][down][down][down][down][down][down]")
    time.sleep(1)
    utils.send_keys("[left]")
    print(tu(print_flag))

    #
    # Press enter to confirm the selection
    if tu(print_flag) == "YES":

        utils.send_keys("Y")
        utils.send_keys("[enter]")
    else:
        utils.send_keys("N")
        utils.send_keys("[enter]")
    utils.send_keys("[down][down][down][down][down][down][down][down][up]")
    utils.send_keys("0")
    #pdb.set_trace()
    # Press F1 to confirm the changes
    #

    utils.send_keys("[f1]")
    if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 20):
        fail_step("[set_print_yes] Could not find the changes confirmation message on screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press Y to confirm the changes
    utils.send_keys("Y")
    if not utils.wait_for_text_in_console("COUPON DEFINITION MAINTENANCE", False, 15):

        fail_step("[set_print_yes] Could not load the coupon definition maintenance screen after confirming changes to the coupon change screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

def clear_print_statistics():

    #start_step("Clear Print statisstics")
    #Go to the multitasking store system to check the amount of triggered coupons
    #awards_navigation.to_multitastking_store_system()
    #coupon_amount = get_triggered_coupon_amount()
    awards_navigation.to_multitastking_store_system()
    time.sleep(2)
    #
    utils.send_keys("s z")

    utils.send_keys("[enter]")
    time.sleep(2)
    #
    #coupon_amount = get_triggered_coupon_amount()
    #awards_validations.validate_triggered_coupon_amount(0)




# Function: copy_store_ini_from_SUT_local
# copy the coupon im age  file from  SUT to local temp folder
#
# Parameters :Params
#
# Process:
# upload coupon file from SUT to FTP
# From FTP download it on the local  machine
#
# Screen:
# N/A
#
# Author:
# Smitha D


def copy_coupon_from_SUT_local(local_temp_path="%s_%s" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))):
    #local_temp_path = "%s_%s" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    awards_utils.prepare_sut_temp_directory(DEFAULT_SUT_TEMP_PATH)
    awards_utils.prepare_local_temp_directory(local_temp_path)

    directories = {r'C:\Strsystm\PrnSimImageDump\*.*':'PrnSimImageDump.Zip'}
    for directory in directories:
        orig_file = "%s\%s" % (DEFAULT_SUT_TEMP_PATH, directories[directory])
        utils.send_keys("rm %s[enter]" % orig_file)
        print (directory)

        awards_utils.zip_folder_in_sut(directory, orig_file)
    files_for_transfer = {}
    for filename in directories:
        files_for_transfer[directories[filename]] = "%s\%s" % (DEFAULT_SUT_TEMP_PATH, directories[filename])

    upload_from_sut_to_ftp_server(files_for_transfer)
    utils.send_keys("exit [enter]")
    #
    # Download the files from the FTP server to the local machine
    download_from_ftp_to_local(files_for_transfer, local_temp_path)

    time.sleep(2)

    #
    if os.path.exists(local_temp_path+r"\PrnSimImageDump.zip"):
        utils.unzip_folder(local_temp_path+r"\PrnSimImageDump.zip",local_temp_path)
    else:
        fail_sub_step("Coupon images are not generated on SUT")

# Function: copy_ini_from_SUT_local
#  copy the ini file from  SUT to local temp folder
#
# Parameters :Params
#
# Process:
# upload ini file from SUT to FTP
# From FTP download it on the local  machine
#
# Screen:
# N/A
#
# Author:
# Smitha D

def copy_ini_from_SUT_local(ini_filepath):
    awards_navigation.to_new_dos_session()

    flag=0
    local_temp_path = "%s_%s" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    awards_utils.prepare_sut_temp_directory(DEFAULT_SUT_TEMP_PATH)
    #awards_utils.prepare_local_temp_directory(local_temp_path)

    directories = {ini_filepath:'data.zip'}
    for directory in directories:
        orig_file = "%s\%s" % (DEFAULT_SUT_TEMP_PATH, directories[directory])
        utils.send_keys("rm %s[enter]" % orig_file)
        print (directory)

        awards_utils.zip_folder_in_sut(directory, orig_file)
    files_for_transfer = {}
    for filename in directories:
        files_for_transfer[directories[filename]] = "%s\%s" % (DEFAULT_SUT_TEMP_PATH, directories[filename])

    upload_from_sut_to_ftp_server(files_for_transfer)
    awards_navigation.leave_dos_session()

    # Download the files from the FTP server to the local machine
    download_from_ftp_to_local(files_for_transfer, local_temp_path)
    time.sleep(2)
    #
    utils.unzip_folder(local_temp_path+r"\data.zip",local_temp_path)
# Function: copy_store_ini_from_SUT_local
#  copy the ini file from  SUT to local temp folder
#
# Parameters :Params
#
# Process:
# upload ini file from SUT to FTP
# From FTP download it on the local  machine
#
# Screen:
# N/A
#
# Author:
# Smitha D

def copy_store_ini_from_SUT_local():
    awards_navigation.to_new_dos_session()

    flag=0
    local_temp_path = "%s_%s" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    awards_utils.prepare_sut_temp_directory(DEFAULT_SUT_TEMP_PATH)
    #awards_utils.prepare_local_temp_directory(local_temp_path)

    directories = {r'C:\strsystm\data\store.ini':'data.zip'}
    for directory in directories:
        orig_file = "%s\%s" % (DEFAULT_SUT_TEMP_PATH, directories[directory])
        utils.send_keys("rm %s[enter]" % orig_file)
        print (directory)

        awards_utils.zip_folder_in_sut(directory, orig_file)
    files_for_transfer = {}
    for filename in directories:
        files_for_transfer[directories[filename]] = "%s\%s" % (DEFAULT_SUT_TEMP_PATH, directories[filename])

    upload_from_sut_to_ftp_server(files_for_transfer)

    # Download the files from the FTP server to the local machine
    download_from_ftp_to_local(files_for_transfer, local_temp_path)
    time.sleep(2)
    #
    utils.unzip_folder(local_temp_path+r"\data.zip",local_temp_path)
    #awards_actions.copy_coupon_from_SUT_local()


# Function: upload_ini_to_sut
#  Upload the ini file from local temp folder to SUT
#
# Parameters :Params
#
# Process:
# upload ini file from local temp folder FTP
# From FTP download it on the SUT machine
#
# Screen:
# N/A
#
# Author:
# Smitha D

def upload_ini_to_sut():
    local_temp_path = "%s_%s" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    upload_from_local_to_ftp({"storefix.ini":local_temp_path, "next.ini":local_temp_path})
    #"next.ini":local_temp_path
    awards_navigation.to_new_dos_session()
    download_from_ftp_to_sut({"next.ini":DEFAULT_STRSYSTM_DATA_PATH,"storefix.ini":local_temp_path})
    #"next.ini":DEFAULT_STRSYSTM_DATA_PATH,
    utils.send_keys("exit [enter]")
    awards_navigation.to_new_dos_session()
    utils.send_keys(r"copy next.ini c:\strsystm\data\store.ini [enter]")
    time.sleep(2)
    utils.send_keys("Y [enter]")
    utils.send_keys("exit [enter]")
    #
    awards_navigation.to_multitastking_store_system()
    start_awards()
    #
    awards_navigation.to_new_dos_session()
    #
    utils.send_keys(r"copy storefix.ini c:\strsystm\comm\storefix.ini [enter]")
    time.sleep(2)

    utils.send_keys("exit [enter]")


def upload_ini_to_SUT1(ini_filepath):
    ini_filename=os.path.split(ini_filepath)[-1]
    local_temp_path = "%s_%s" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    upload_from_local_to_ftp({ini_filename:local_temp_path})


    awards_navigation.to_new_dos_session()
    download_from_ftp_to_sut({ini_filename:DEFAULT_STRSYSTM_DATA_PATH})

    utils.send_keys("exit [enter]")
    awards_navigation.to_new_dos_session()
    utils.send_keys(r"copy %s %s [enter]"%(ini_filename,ini_filepath))
    time.sleep(2)
    utils.send_keys("Y [enter]")
    time.sleep(2)
    utils.send_keys("exit [enter]")



# Function: run_cmcset_for_specific_oclu
# Will turn on specific coupon for the current date. This process avoids the
# automation to fail when a long timespan has passed.
#
# Process:
# - Will terminate cmc and wait 20 seconds for it
# - Will run cmcset -a -d and wait 5 seconds
# - Will run cmcset -<oclu> -n and wait 5 seconds
# - Will run start cmc
#
# Note 1:
# There is no proper visual feedback for terminate cmc, so for that reason we'll
# have a 20-second wait after running this command. In case the coupons are not
# being turned on, check if this timeout is being enought before trying to run
# the other commands. You can tweak it (increase the timeout, for example) if
# 20 seconds is not being enought.
#
# Note 2:
# After running start cmc, you might want to invoke <validate_system_is_running>
# to make sure the system is running already. It will show
# <--------CMC IS NOW OPERATIONAL--------> on screen after the system boots up.
#
# Screen:
# N/A
#
# Author:
# Ajay Keerthi
def run_cmcset_for_specific_oclu(params):

    start_step("Run cmcset for a specific OCLU %s" %params["oclu"])

    utils.send_keys("terminate cmc[enter]")
    time.sleep(20)
    utils.take_screenshot()
    #
    utils.send_keys("call cmcset %s -n[enter]" %params["oclu"])
    #
    time.sleep(8)
    utils.take_screenshot()

    utils.send_keys("start cmc[enter]")
    utils.take_screenshot()
#Function Name: update_ini_file
#Descrption: Update the ini file
#Author Smitha D
def update_ini_file(ini_filepath,sectionname,value,copy_file="Yes",upload_file="Yes"):
    awards_validations.update_section_ini_file(ini_filepath,sectionname,value,copy_file)
    #
    if tu(upload_file)=="YES":

        upload_ini_to_SUT1(ini_filepath)


def set_pix_number(params):

    start_step("Set pix number to %s/%s." % ( params['pix number']) )

    awards_utils.load_display_application()

    # Press C to get into the configuration menu
    utils.send_keys("C")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_store_and_chain_number] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press S to get into the store configuration menu
    utils.send_keys("S")
    if not utils.wait_for_text_in_console("STORE CONFIGURATION SUMMARY", False, 15):
        fail_step("[set_store_and_chain_number] Could not load the store configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press C to get into the change store configuration menu
    utils.send_keys("C")
    if not utils.wait_for_text_in_console("CHANGE STORE CONFIGURATION SUMMARY", False, 15):
        fail_step("[set_store_and_chain_number] Could not load the change store configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
    time.sleep(1)
    utils.send_keys("[down][down][down][down][down][down][down]")
    # Type the data that needs to be changed
    utils.send_keys(params["pix number"])


    # Save the changes
    utils.send_keys("[f1]")
    if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 15):
        fail_step("[set_store_and_chain_number] Could not load the change confirmation message in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Leave the display application
    utils.send_keys("Y")
    if not utils.wait_for_text_in_console("STORE CONFIGURATION SUMMARY", False, 15):
        fail_step("[set_store_and_chain_number] Could not load the store configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    utils.send_keys("E")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_store_and_chain_number] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    utils.send_keys("E")
    if not utils.wait_for_text_in_console("MAIN MENU", False, 15):
        fail_step("[set_store_and_chain_number] Could not load the main menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    utils.send_keys("E")
    if not utils.wait_for_text_in_console("Exit program?", False, 15):
        fail_step("[set_store_and_chain_number] Could not load the exit confirmation message in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    utils.send_keys("Y")
#Function NAme: add_printer_type
# Set the printer type
#Author Smitha D
def add_printer_type(printer_type,delete_lane="YES",enter_display="YES"):

    start_step("Set the printer type to '%s' in the display application." % printer_type)

    # Amount of times the key down needs to be pressed in the printer type drop
    # down.
    printer_options = {
        "unknown": 0,
        "cmc6": 1,
        "cmc7": 2,
        "cmc5": 3,
        "cmc4": 4,
        "sesenta1": 5,
        "stdprinter": 6,
        "cmc5e02c": 7,
        "oni": 8,
        "cmc8": 9
    }

    if tu(enter_display)=="YES":
        awards_utils.load_display_application()

    # Press C to get into the configuration menu
        utils.send_keys("C")
        if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
            fail_step("[set_printer_type] Could not load the configuration menu in the display application.")
            utils.take_screenshot()
            utils.finish_execution()

        utils.take_screenshot()

    # Press L to get into the lane configuration summary screen
    utils.send_keys("L")
    if not utils.wait_for_text_in_console("LANE CONFIGURATION SUMMARY", False, 15):
        fail_step("[set_printer_type] Could not load the lane configuration menu.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
    if tu(delete_lane)=="YES":
    # Press C to open the change lane configuration screen
        utils.send_keys("d")
        time.sleep(20)
        if not utils.wait_for_text_in_console(" DELETE LANE CONFIGURATION", False, 15):
            fail_step("[set_printer_type] Could not load the lane configuration delete screen.")
            utils.take_screenshot()
            utils.finish_execution()

        utils.take_screenshot()
        #
        utils.send_keys("001")
        utils.send_keys("001")
        utils.send_keys("[f1]")
        if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 15):
            fail_step("[set_printer_type] Could not find the changes confirmation message on screen.")
            utils.take_screenshot()
            utils.finish_execution()
        utils.send_keys("Y")
        time.sleep(40)
        if not utils.wait_for_text_in_console("LANE CONFIGURATION SUMMARY ", False, 15):
            fail_step("[set_printer_type] Could not find the changes confirmation message on screen.")
            utils.take_screenshot()
            utils.finish_execution()
    utils.send_keys("A")
    if not utils.wait_for_text_in_console("ADD LANE CONFIGURATION", False, 15):
        fail_step("[set_printer_type] Could not find the ADD LANE CONFIGURATION.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys("[down][down][down][down][down][enter][down][enter]")
    time.sleep(2)
    # Press down a bunch of times in order to reach the printer type field and
    # then press enter to open the drop down.
    utils.send_keys("[down][down][down][down][down][down][enter]")

    # Press [down] a certain amount of times in order to reach the right option
    # in the dropdown
    time.sleep(3)
    for i in range(0, printer_options[tl(printer_type)]):
        utils.send_keys("[down]")


    # Press enter to confirm the selection
    utils.send_keys("[enter]")

    # Press F1 to confirm the changes
    utils.send_keys("[f1]")
    if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 15):
        fail_step("[set_printer_type] Could not find the changes confirmation message on screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
    utils.send_keys("Y")

    time.sleep(60)

    # Press Y to confirm the changes
    # Added by Smitha
    utils.send_keys("Y")
    if not utils.wait_for_text_in_console("MAC ADDRESS-LANE MAPPING", False, 60):
        fail_step("[set_printer_type] Could not find the changes confirmation message on screen.")
        utils.take_screenshot()

    #
    utils.send_keys("1 [tab] 1")
    utils.send_keys("[f1]")
    if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 15):
        fail_step("[set_printer_type] Could not find the changes confirmation message on screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.send_keys("Y")
    time.sleep(30)
    if not utils.wait_for_text_in_console("LANE CONFIGURATION SUMMARY", False, 15):

            fail_step("[set_printer_type] Could not load the lane configuration screen after adding new lane.")
            utils.take_screenshot()
            utils.finish_execution()

    utils.take_screenshot()
    #
    # Press E to leave to the previous screen
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_printer_type] Could not leave to the configuration menu.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press E to go to the main menu
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("MAIN MENU", False, 15):
        fail_step("[set_printer_type] Could not leave to the main menu.")
        utils.take_screenshot()
        utils.finish_execution()

    # Leave to the exit confirmation screen
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("Exit program?", False, 15):
        fail_step("[set_printer_type] Could not reach the exit confirmation screen.")
        utils.take_screenshot()
        utils.finish_execution()

    # Leave to the prompt
    utils.send_keys("Y")
    #block_test("Printer is not added ")
#Function Name: Generate_db_log_file()
#Descrition:
# run dblist -m >log.txt
#run dblist -s >log.txt
#
#Author:
#Smitha D

def generate_db_log_file(option,log_file_name):
    start_step("Generate DB log file")
    utils.send_keys(r"cd C:\strsystm\log [enter]")
    utils.send_keys(r"rm -rf %s [enter]" % log_file_name)
    utils.send_keys(r"dblist -%s > %s [enter]" % (option,log_file_name))
    utils.wait_for_bottom_row(r"^C:\\strsystm\\LOG>  +$", True)
    # Validate log file has been created
    utils.send_keys("cls[enter]")
    utils.send_keys(r"dir %s [enter]" % log_file_name)

    if not utils.wait_for_text_in_console("1 File(s)"):
        fail_step(r"[generate_dw_log_file] Could not find the expected log file in the C:\strsystm\log directory.")
        utils.finish_execution()


#Function Name: kill_node()
#description:
#Kill vault process(node)
#
#Author:
#Smitha D
def kill_vault_node():
    start_step("Killing vault Node process")
    vault_pid=[]
    num_services=0
    awards_navigation.to_new_dos_session()
    utils.send_keys("ps > proc.txt [enter]")
    utils.send_keys("grep node proc.txt [enter]")
    time.sleep(2)
    text_console=utils.get_console_text()
    vault_pid= utils.match_text_in_console(r"\d+.*node","Yes")
    print(type(vault_pid))

    if vault_pid!=None:
        num_services=len(vault_pid)
        print (num_services)
    for i in range(0,num_services):
        print (vault_pid[i])
        pid=vault_pid[i].split(" ")[0]
        print (pid)

        utils.send_keys("kill %s[enter]"%(pid))
    #utils.send_keys("exit [enter]")
    pass_step("Node process killed succesfully")




#Function Name: start_vault()
#description:
#Satrt run.cmd
#
#Author:
#Smitha D
def start_vault():
    start_step("Start vault process")
    kill_vault_node()
    utils.send_keys(r"cd c:\vault [enter]")

    utils.send_keys("run.cmd [enter]")
    pass_step("Vault  started successfully")


#Function Name: Compare files
#Description: compares 2 files
#Author: Smitha D

def comparefile(file1,file2):
    start_step("compare files  %s and %s"%(file1,file2))
    #
    result=utils.compare_file(file1,file2)
    #
    if result == False:
        #
        fail_step("%s file is not same"%file1)
    else:
        #
        add_sub_step("%s files are same"%file1)


#Function name: Verify file_content
#Description: Validates file contents
#Author: Smitha D

def verify_file_content(file1):
    start_step("Verify the file content")
    ts_flag=0
    ts_readonly=0
    fh=open(file1,"r")
    for line in fh.readlines():
        if re.findall("ts.exe",line) or re.findall("*.trp"):
            ts_flag=1
        if re.findall(".*R\t.*",line):
            ts_readonly=1
    if ts_flag==1 or ts_read_only!=1:
        fail_step("gsee.txt contents are not updated")
    else:
        pass_step("gsee.txt file is properly updated")


#Function Name: validate_print_coupon
###
#Description: print coupon by running command coup all an award machine
# Verify number of coupons printed
###
#Author: Smitha D

def validate_print_coupon():
    clear_print_statistics()
    start_step("Validate Print coupon")
    awards_navigation.to_multitastking_store_system()
    utils.send_keys("coup all[enter]")
    time.sleep(1)
    utils.send_keys("coup all[enter]")
    time.sleep(1)
    utils.send_keys("coup all[enter]")
    time.sleep(10)
    print_count=3
    awards_validations.validate_triggered_coupon_amount( print_count)
    pass_step("3 coupons are generated")


#Function Name: vault_files
###
#Description: Verify all the files required for vault are presentin c:\vault folder
###
#Author: Smitha D

def vault_files(params):
    start_step("Veriy vault files for all countries are present ")
    files=["VAULT.DEU","VAULT.FRA","VAULT.GBR","VAULT.GER","VAULT.ITA","VAULT.JPN","VAULT.USA"]
    for file in files:
        utils.send_keys("C:\Vault")
        utils.send_keys("cls[enter]")
        utils.send_keys(r"dir %s[enter]" %file)

        #
        time.sleep(5)
        if not utils.wait_for_text_in_console(r"1 File(s)", False, 5):
            fail_step("File %s not found in Vault folder"%file)
            break
    pass_step("All files are present in the Vault folder")

#Function Name:display_board_menu
#Descrption: Common function to enter baord Configuration menu
#Authoe: Smitha D

def display_board_menu(enter_display="YES"):
    if tu(enter_display) == "YES":
        awards_utils.load_display_application()
        utils.send_keys("C")
        if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
            fail_step("[DISPALY_BOARD] Could not load the configuration menu in the display application.")
            utils.take_screenshot()
            utils.finish_execution()

    utils.take_screenshot()
    #Press B to go to Board menu
    utils.send_keys("B")
    if not utils.wait_for_text_in_console("BOARD CONFIGURATION SUMMARY", False, 15):
        fail_step("[DISPLAY_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    #Press A to Add Board
    utils.send_keys("A")
    if not utils.wait_for_text_in_console("ADD BOARD CONFIGURATION", False, 15):
        fail_step("[DISPLAY_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()


#Function name: config_ethernet_board
#Description: Configure Ethernet board from display
#Parameters :
#enter_display: Load display if not in display mode
#IPadres- IP address to be entered for board configuration
#Author: Smitha D

def config_ethernet_board(ip_address,gateway_ip,enter_display="YES",exit_display="NO"):
    start_step("Configuring ethernet board")
    #Load board menu if not in board menu page

    if enter_display=="YES":
        display_board_menu(enter_display)
    #Press E to select Ethernet
    utils.send_keys("E[enter]")
    time.sleep(2)
    # PRess I to select board type and press F1 to save changes
    utils.send_keys("I[enter]")
    utils.send_keys("[F1]")
    if not utils.wait_for_text_in_console("Ethernet", False, 15):
        fail_step("[ADD_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    #Press T to select TCP/Ip and prss F1 to save
    utils.send_keys("T[enter]")
    utils.send_keys("[F1]")
    #Seclt Yes for POS and COM
    if not utils.wait_for_text_in_console("Does this board support the POS loop?", False, 15):
        fail_step("[ADD_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys("Y[enter]")
    time.sleep(2)
    utils.send_keys("Y[enter]")
    time.sleep(2)
    #Enter IP address  and gateway IP address of the machine and Save by pressing F1
    utils.send_keys(ip_address)
    utils.sendkeys("[down][down][down]")
    utils.sendkeys(gateway_ip)
    utils.send_keys("[F1]")
    #Press Y to save changes
    if not utils.wait_for_text_in_console("Are you sure you want to save the changes?", False, 15):
        fail_step("[ADD_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys("Y")
    if not utils.wait_for_text_in_console("ADD BOARD CONFIGURATION", False, 15):
        fail_step("[ADD_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    #Press E to go to ADD Boards Menu
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("ADD BOARD CONFIGURATION", False, 15):
        fail_step("[ADD_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys("Y")
    #PRess E to go to Configuration Menu
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[ADD_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys("Y")

#Exit the display
    if tu(exit_display) == "YES":
        utils.send_keys("E")
        if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
            fail_step("[set_printer_type] Could not leave to the main menu.")
            utils.take_screenshot()
            utils.finish_execution()
        utils.send_keys("E")
        if not utils.wait_for_text_in_console("MAIN MENU", False, 15):
            fail_step("[set_printer_type] Could not leave to the main menu.")
            utils.take_screenshot()
            utils.finish_execution()

#Function Name: config_sdrstm_board
#Description: Configure sdrstm board type
#Author: Smitha D
def config_sdrstm_board(enter_display="YES",exit_display="YES"):
    start_step("Configuring ethernet board")
    #If not in display board menu then load display
    if enter_display == "YES":
        display_board_menu(enter_display)
    # PRess A to add new board
    utils.send_keys("A")
    # PRess T T to select Testing
    utils.send_keys("T T[enter]")
    time.sleep(2)
    # Press S to select sdrstm and Press F1 to save changes
    utils.send_keys("S[enter]")
    utils.send_keys("[F1]")
    if not utils.wait_for_text_in_console("PRINTER SIMULATION CONFIGURATION", False, 15):
        fail_step("[ADD_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    #Press S to select static IP  and press F1 save changes
    utils.send_keys("S[enter]")
    #pdb.set_trace()
    #utils.send_keys("[down]")
    #pdb.set_trace()
    utils.send_keys("Y[enter]")
    utils.send_keys("[F1]")
    if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 15):
        fail_step("[ADD_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys("Y")
    time.sleep(2)
    # Go to Board configuration Menu
    if not utils.wait_for_text_in_console("BOARD CONFIGURATION SUMMARY", False, 15):
        fail_step("[ADD_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    #Press esc to go to configuration Menu
    utils.send_keys("E")

    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_printer_type] Could not leave to the main menu.")
        utils.take_screenshot()
        utils.finish_execution()
    #Exit display
    if tu(exit_display)=="YES":
        utils.send_keys("E")
        if not utils.wait_for_text_in_console("MAIN MENU", False, 15):
            fail_step("[set_printer_type] Could not leave to the main menu.")
            utils.take_screenshot()
            utils.finish_execution()

        # Leave to the exit confirmation screen
        utils.send_keys("E")
        if not utils.wait_for_text_in_console("Exit program?", False, 15):
            fail_step("[set_printer_type] Could not reach the exit confirmation screen.")
            utils.take_screenshot()
            utils.finish_execution()

    # Leave to the prompt
        utils.send_keys("Y")

#Function Name:configure_controller
#Description : Configure contoller board from Display
# Paramters -
#Enter_display- To enter display mode if already in display mode then set enter_display=No
#Start_display: This is added because if newstore is called then display will be loaded automatically
def configure_controller(enter_display="YES",exit_display="YES",start_display="YES"):
    if enter_display=="YES":
        awards_utils.load_display_application(start_display=start_display)
    #Press C to enter configuration Menu
    utils.send_keys("C")
    if not utils.wait_for_text_in_console("CONTROLLER CONFIGURATION SUMMARY", False, 15):
        fail_step("[configure_controller] Could not loaController menu.")
        utils.take_screenshot()
        utils.finish_execution()
    #Press A to Add Controller
    utils.send_keys("A")
    if not utils.wait_for_text_in_console("ADD CONTROLLER CONFIGURATION", False, 15):
        fail_step("[configure_controller] Could not loaController menu.")
        utils.take_screenshot()
        utils.finish_execution()
    #Select Controller type and press F! to sav changes
    utils.send_keys("UUUUUUU[enter]")
    utils.send_keys("[F1]")
    if not utils.wait_for_text_in_console("Default will assign controller to first legal board ", False, 15):
        fail_step("[configure_controller] Could not loaController menu.")
        utils.take_screenshot()
        utils.finish_execution()
    # Press F1 to save changes
    utils.send_keys("[F1]")
    if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 15):
        fail_step("[configure_controller] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys("Y")
    time.sleep(2)
    #pdb.set_trace()
    if not utils.wait_for_text_in_console("Cash tender type", False, 15):
        fail_step("[configure_controller] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    # PRess esc to go to Controller coniguration Menu
    utils.send_keys("[esc]")

    if not utils.wait_for_text_in_console("CONTROLLER CONFIGURATION SUMMARY", False, 15):
        fail_step("[configure_controller] Could not load Controller menu.")
        utils.take_screenshot()
        utils.finish_execution()
    # Press esc to go to CONFIGURATION MENU
    utils.send_keys("[esc]")

    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_printer_type] Could not leave to the main menu.")
        utils.take_screenshot()
        utils.finish_execution()

    #checck if control should be there in display or not
    if tu(exit_display)=="YES":
        # Press E to go to Main menu
        utils.send_keys("E")
        if not utils.wait_for_text_in_console("MAIN MENU", False, 15):
            fail_step("[set_printer_type] Could not leave to the main menu.")
            utils.take_screenshot()
            utils.finish_execution()

        # Leave to the exit confirmation screen
        utils.send_keys("E")
        if not utils.wait_for_text_in_console("Exit program?", False, 15):
            fail_step("[set_printer_type] Could not reach the exit confirmation screen.")
            utils.take_screenshot()
            utils.finish_execution()
        utils.send_keys("Y")





#Function Name: capture_WEBPOS
###
#Description: Capture WEBPOS from the screen
###
#Author: Smitha D

def capture_WEBPOS():

        start_step("Capture the version of the WEBPOS  from screen.")

        awards_navigation.to_multitastking_store_system()
        # Show up ther version on screen and capture it
        utils.send_keys("[f1]")
        utils.send_keys("[f1]")
        version = utils.match_text_in_console("WebPOS")
        print (version)
        #pdb.set_trace()

        # Check if we got to find the version on screen
        if version is None or len(version)==0:
            fail_step("[capture_WPOS] WEBPOS is not displayed on the console")
            sys.exit(0)



        # Return it without surrounding spaces
        pass_step("WEBPOS is displayed on the console")
        return tu(version)


#Function Name: get_process_from_ps_command
###
#Description:Get the process name by running PS command
###
#Author: Smitha D
def get_process_from_ps_command(process_name):
    awards_navigation.to_new_dos_session()
    utils.send_keys("ps > proc.txt [enter]")
    utils.send_keys("grep %s proc.txt [enter]"%process_name)
    proc_name=utils.match_text_in_console(r"\d+.*%s"%process_name,"Yes")
    if len(proc_name)>0:
        pass_step("Process %s is running"%process_name)
    else:
        fail_step("Process %s is not running"%process_name)

#Function Name: capture_WEBPOS_messages
###
#Description: Capture the mmessages displayed on the award screen
###
#Author: Smitha D

def capture_WEBPOS_messages(messages):
    start_step("Capture messages from screen")

    for message in messages:
        print (message)

        version = utils.match_text_in_console(message)

    # Check if we got to find the version on screen
        if version is None or len(version)==0:
            time.sleep(2)
            version = utils.match_text_in_console(message)
            if version is None or len(version)==0:
                fail_step("[capture_WPOS] %s is not displayed on the console"%message)
                sys.exit(0)
    pass_step("All the messages are displayed")

#Function Name: copy_file_from_SUT_local
###
#Description: Copy files from SUT to local machine
###
#Author: Smitha D
def copy_file_from_SUT_local():
    awards_navigation.to_new_dos_session()

    flag=0
    local_temp_path = "%s_%s" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    awards_utils.prepare_local_temp_directory(local_temp_path)
    #prepare dictionary object with filename and filepath for uploading to FTP rom SUT
    directories = {DEFAULT_CAPT_LOG_PATH:'data.zip'}
    for directory in directories:
        orig_file = "%s\%s" % (DEFAULT_SUT_TEMP_PATH, directories[directory])
        utils.send_keys("rm %s[enter]" % orig_file)
        print (directory)

        awards_utils.zip_folder_in_sut(directory, orig_file)
    files_for_transfer = {}
    for filename in directories:
        files_for_transfer[directories[filename]] = "%s\%s" % (DEFAULT_SUT_TEMP_PATH, directories[filename])
    #Uload files to ftp sever
    upload_from_sut_to_ftp_server(files_for_transfer)
    awards_navigation.leave_dos_session()
    print(files_for_transfer)


    # Download the files from the FTP server to the local machine
    download_from_ftp_to_local(files_for_transfer, local_temp_path)
    time.sleep(2)
    if os.path.exists(local_temp_path+r"\data.zip"):
        utils.unzip_folder(local_temp_path+r"\data.zip",local_temp_path)
    else:
        fail_sub_step("CAPT log file is not generated in SUT")

#Function Name: kill_node()
#description:
#Kill vault process(node)
#
#Author:
#Smitha D
def kill_process(process_name):
    start_step("Killing %s process" %process_name)
    vault_pid=[]
    num_services=0
    awards_navigation.to_new_dos_session()
    #command to get the process running on the machine
    utils.send_keys("ps > proc.txt [enter]")
    #Command to get PID of the process
    utils.send_keys("grep %s proc.txt [enter]"%process_name)
    time.sleep(2)
    text_console=utils.get_console_text()
    #Regular expression to get the PI from console text
    vault_pid= utils.match_text_in_console(r"\d+.*%s"%process_name,"Yes")
    print(type(vault_pid))

    if vault_pid!=None:
        num_services=len(vault_pid)
        print (num_services)
    #kill all the PIDs
    for i in range(0,num_services):
        print (vault_pid[i])
        pid=vault_pid[i].split(" ")[0]
        print (pid)

        utils.send_keys("kill %s[enter]"%(pid))

    pass_step("Node process killed succesfully")

# Function_name: rest_firewall_setup
#Description: Reset firewall setup
#Author : Smitha D

def rest_firewall_setup():
    start_step("Reset firewall setup")
    #Command to reset the firewall ports
    utils.send_keys("netsh firewall reset[enter]")
    time.sleep(2)
    pass_step("Firewall reset successfull")

#Function_name:set_firewall_setup
#Description: Set the firewall for port 8199
#Author: Smitha D

def set_firewall_setup(protocol):
    start_step("Set the firewall setup")
    #command to info about firewall opened for the protocol
    utils.send_keys("netsh firewall add portopening %s WebPOSPort[ente]"%protocol)
    time.sleep(2)
    pass_step("Firewall set for TCP for port 8199")

#Function name: show_firewal_port_opened
##Descrption enter command netsh firewall portopening to get the info about the ports opened
# check ports for the rocess listed in <firewall_process> are opened
#Autor: Smitha D
def show_firewal_port_opened(firewall_process):
    start_step("Validate firewall ports opened")
    utils.send_keys("netsh firewall show  portopening[enter]")
    time.sleep(5)
    for process in firewall_process:
        proc_name=utils.find_text_in_console(process,True)
        print (proc_name)
        #pdb.set_trace()
        if proc_name == None or  len(proc_name)<0:
            fail_step("Firewall port is not openrd for %s"%process)
            utils.finish_execution()
    pass_step("Firewall is opened for all process")



#Function Name: get_tranid_from_Simulatorlog
####
#Description: get all the Tranid from log file and store it in array
##
#Author: Smitha D

def get_tranid_from_Simulatorlog(filename):
    start_step("Get all the TRANID from POS Simulator Log")
    tranid=[]
    fh=open(filename,'r')
    lines=fh.readlines()
    for line in lines:
        if len(line.split("TranId"))>0:
            print(line.split("TranId")[1])
            tranid.append(line.split("TranId")[1].split(" ")[1])

    return tranid
#Function Name: delete_WEBPOSboard
###
##Description: Delete the WEBPOS board configured

def delete_WEBPOSboard():
    start_step("Configure board for WPOS setup")

    awards_utils.load_display_application()
    utils.send_kays("C")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_printer_type] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.send_kays("B")
    if not utils.wait_for_text_in_console("ADD BOARD CONFIGURATION", False, 15):
        fail_step("[set_printer_type] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys("d")
    time.sleep(2)
    utils.send_keys("[pg down]")
    utils.send_keys("[pg down]")
    utils.send_keys("[F1]")
    if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 15):
        fail_step("[set_printer_type] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys("Y")
    if not utils.wait_for_text_in_console("BOARD CONFIGURATION", False, 15):
        fail_step("[set_printer_type] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys("[Esc]")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_printer_type] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys("[Esc]")
    if not utils.wait_for_text_in_console("MAIN MENU", False, 15):
        fail_step("[set_printer_type] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys("[Esc]")
    if not utils.wait_for_text_in_console("Exit program?", False, 15):
        fail_step("[set_printer_type] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys("Y")
# Function: create_lock_files
# Creates an empty file at C:\CDS\mirror\related_file with the name passed as
# parameter.
#
# Parameters:
# - job_name: job file name. If not provided, the default will be used.
#
# Process:
# Sends "type NUL > %s\%s[enter]" in order to create an empty file.
#
# Screen:
# Should be at a DOS session.
#
# Author:
# Danilo Guimaraes
def create_lock_file(filename,file_path):

    start_step("Create the file '%s%s'." % (file_path, filename) )

    # Create the empty file
    utils.send_keys(r"type NUL> %s\%s[enter]" % (file_path, filename) )
    #
    # Validate it has been created properly
    utils.send_keys("cls[enter]")
    utils.send_keys(r"dir %s\%s[enter]" % (file_path, filename) )
    #
    time.sleep(5)
    if not utils.wait_for_text_in_console(r"1 File(s)", False, 5):
        fail_step("[create_force_load_job] Could not create the file '%s%s'." % (file_path, filename) )
        utils.take_screenshot()
        utils.finish_execution()



#Function name: add_lane_display
#Description: Add lane from display
#Author: Smitha D


def add_lane_display(printer_type,lane_num="001",term_id="0001",lane_max=False):
    start_step("Add CMC Printer")
    printer_options = {
        "unknown": 0,
        "cmc6": 1,
        "cmc7": 2,
        "cmc5": 3,
        "cmc4": 4,
        "sesenta1": 5,
        "stdprinter": 6,
        "cmc5e02c": 7,
        "cmc8": 8,
        "cmc9":9
    }
    #awards_utils.load_display_application()
    delete_lane_display(display_exit="NO")
    #pdb.set_trace()

    # Press C to get into the configuration menu


    # Press L to get into the lane configuration summary screen
    utils.send_keys("L")
    if not utils.wait_for_text_in_console("LANE CONFIGURATION SUMMARY", False, 15):
        fail_step("[set_printer_type] Could not load the lane configuration menu.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
    # Press A to Add Lane
    utils.send_keys("A")
    if not utils.wait_for_text_in_console("ADD LANE CONFIGURATION", False, 15):
        fail_step("[set_printer_type] Could not find the ADD LANE CONFIGURATION.")
        utils.take_screenshot()
        utils.finish_execution()
    #Enter Lane number
    utils.send_keys(lane_num)
    utils.send_keys("[left][left][left][down]")
    #pdb.set_trace()
    utils.send_keys(term_id)
    #pdb.set_trace()
    utils.send_keys("[left][left][down][down][down][down][enter]")
    #pdb.set_trace()
    time.sleep(2)
    utils.send_keys("y[enter]")
    # Press down a bunch of times in order to reach the printer type field and
    # then press enter to open the drop down.
    utils.send_keys("[down][down][down][down][down][down][enter]")
    #pdb.set_trace()

    #time.sleep(1)
    #pdb.set_trace()
    # Press [down] a certain amount of times in order to reach the right option
    # in the dropdown
    for i in range(0, printer_options[tl(printer_type)]):
        utils.send_keys("[down]")


    # Press enter to confirm the selection
    utils.send_keys("[enter]")

    # Press F1 to confirm the changes
    utils.send_keys("[f1]")
    if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 15):
        fail_step("[set_printer_type] Could not find the changes confirmation message on screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
    utils.send_keys("Y")

    time.sleep(60)

    # Press Y to confirm the changes
    # Added by Smitha
    utils.send_keys("Y")


    utils.take_screenshot()
    #This is added to verify the functionality of Lane Max
    #If the lane bumber is greater han 128 Eror messge should be displayed
    if lane_max==True:
            if not utils.wait_for_text_in_console("Attempted to install Lane with number exceeding maximum lane" ,False, 60):
                fail_step("[set_printer_type] Could not leave to the configuration menu.")
                utils.take_screenshot()
                #utils.finish_execution()
            utils.send_keys("E")
    else:
        #Verify MAC Address Page is displayed
        if not utils.wait_for_text_in_console("MAC ADDRESS-LANE MAPPING", False, 60):
            #pdb.set_trace()
            if not utils.wait_for_text_in_console("PRINTER ENUMERATION ERROR", False, 15):

                fail_step("[set_printer_type] Could not leave to the configuration menu.")
                utils.take_screenshot()
                utils.finish_execution()

            utils.send_keys("[Esc]")
    #
        else:
            #Enter the Mac Addess and prss F1 to save the changes
            utils.send_keys("1 [tab] 1")
            utils.send_keys("[f1]")
            if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 15):
                fail_step("[set_printer_type] Could not find the changes confirmation message on screen.")
                utils.take_screenshot()
                utils.finish_execution()

            utils.send_keys("Y")
            time.sleep(30)
        #pdb.set_trace()

    # Lane configuration Summary Page will be displayed after saving changes
    if not utils.wait_for_text_in_console("LANE CONFIGURATION SUMMARY", False, 15):

            fail_step("[set_printer_type] Could not load the lane configuration screen after adding new lane.")
            utils.take_screenshot()
            utils.finish_execution()

    utils.take_screenshot()
    #
    # Press E to leave to the previous screen
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_printer_type] Could not leave to the configuration menu.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press E to go to the main menu
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("MAIN MENU", False, 15):
        fail_step("[set_printer_type] Could not leave to the main menu.")
        utils.take_screenshot()
        utils.finish_execution()

    # Leave to the exit confirmation screen
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("Exit program?", False, 15):
        fail_step("[set_printer_type] Could not reach the exit confirmation screen.")
        utils.take_screenshot()
        utils.finish_execution()

    # Leave to the prompt
    utils.send_keys("Y")
    pass_step("Printer added successully")

#Function Name: delete_lane_display
#Description: Delete Lane from display
#Author: Smitha D
def delete_lane_display(display_exit="YES"):
    awards_utils.load_display_application()

    # Press C to get into the configuration menu
    utils.send_keys("C")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_printer_type] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press L to get into the lane configuration summary screen
    utils.send_keys("L")
    if not utils.wait_for_text_in_console("LANE CONFIGURATION SUMMARY", False, 15):
        fail_step("[set_printer_type] Could not load the lane configuration menu.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press d to delete Lane
    utils.send_keys("d")
    time.sleep(20)
    if not utils.wait_for_text_in_console(" DELETE LANE CONFIGURATION", False, 15):
        fail_step("[set_printer_type] Could not load the lane configuration delete screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
    #
    #Eneter the Lane number to delete  and Press F1 to save changes
    utils.send_keys("001")
    utils.send_keys("001")
    utils.send_keys("[f1]")
    if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 15):
        fail_step("[set_printer_type] Could not find the changes confirmation message on screen.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys("Y")
    #time.sleep(40)
    #After delete LAne configuration sumary page wll be displayed
    if not utils.wait_for_text_in_console("LANE CONFIGURATION SUMMARY ", False, 65):
        fail_step("[set_printer_type] Could not find the changes confirmation message on screen.")
        utils.take_screenshot()
        utils.finish_execution()
    #Press E to go to Configuration menu
    utils.send_keys("[esc]")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_printer_type] Could not leave to the configuration menu.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
    if tu(display_exit)=="YES":
        # Press E to go to the main menu
        utils.send_keys("E")
        if not utils.wait_for_text_in_console("MAIN MENU", False, 15):
            fail_step("[set_printer_type] Could not leave to the main menu.")
            utils.take_screenshot()
            utils.finish_execution()

        # Leave to the exit confirmation screen
        utils.send_keys("E")
        if not utils.wait_for_text_in_console("Exit program?", False, 15):
            fail_step("[set_printer_type] Could not reach the exit confirmation screen.")
            utils.take_screenshot()
            utils.finish_execution()

        # Leave to the prompt
        utils.send_keys("Y")


# Function name :exit_Laneconfig_display
#Description: Common function to Exit display from Lane configuration menu
#Author: Smitha D
def exit_Laneconfig_display():
    #pdb.set_trace()
    if not utils.wait_for_text_in_console("LANE CONFIGURATION SUMMARY ", False, 15):
        fail_step("[set_printer_type] Could not find the changes confirmation message on screen.")
        utils.take_screenshot()
        utils.finish_execution()
    #Press  E to enter Configuration Menu
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_printer_type] Could not leave to the configuration menu.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press E to go to the main menu
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("MAIN MENU", False, 15):
        fail_step("[set_printer_type] Could not leave to the main menu.")
        utils.take_screenshot()
        utils.finish_execution()

    # Leave to the exit confirmation screen
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("Exit program?", False, 15):
        fail_step("[set_printer_type] Could not reach the exit confirmation screen.")
        utils.take_screenshot()
        utils.finish_execution()

    # Leave to the prompt
    utils.send_keys("Y")
#Function Name: config_board
#Description: Add new  configuration board
# Author : Smitha D
def config_board():
    start_step("Configure board ")
    # Function to enter display
    awards_utils.load_display_application()
    # Press c to go  to Configuration Menu
    utils.send_kays("C")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[ADD_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
   #Press B to go to Board Configuration Menu
    utils.take_screenshot()
    utils.send_keys("B")
    if not utils.wait_for_text_in_console("BOARD CONFIGURATION SUMMARY", False, 15):
        fail_step("[ADD_WEBPOS_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    #Press A to ADD new board
    utils.send_keys("A")
    if not utils.wait_for_text_in_console("ADD BOARD CONFIGURATION", False, 15):
        fail_step("[ADD_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    #Press E to selet Ethernet
    utils.send_keys("E[enter]")
    time.sleep(2)
    #Press V to select
    utils.send_keys("V[enter]")
    #Press F1 to save changes
    utils.send_keys("[F1]")
    if not utils.wait_for_text_in_console("VMWARE ETHERNET CONFIGURATION", False, 15):
        fail_step("[ADD_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    #Prss T to select TCP/IP and press F1 to save changes
    utils.send_keys("T[enter]")
    utils.send_keys("[F1]")
    if not utils.wait_for_text_in_console("VMWARE ETHERNET CONFIGURATION", False, 15):
        fail_step("[ADD_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    #Select Yes for POS and COM
    utils.send_keys("Y[enter][down]Y[enter][down]")
    #Enter IP address and save changes
    utis.send_keys(ip_address)
    utils.send_keys("[F1]")
    utils.send_keys("Y")

    if not utils.wait_for_text_in_console("ADD BOARD to the store system", False, 15):
        fail_step("[ADD_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    #Press E to go to Board configuration Menu
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("ADD BOARD CONFIGURATION", False, 15):
        fail_step("[ADD_WEBPOS_BOARD] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys("Y")


#Function Name:get_printertype_display
# Get all the printer types from display
#Author: Smitha D
def get_printertype_display(pinters,lane_num="001",term_id="0001"):
    awards_utils.load_display_application()
    #pdb.set_trace()

    # Press C to get into the configuration menu
    utils.send_keys("C")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_printer_type] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
    time.sleep(2)

    # Press L to get into the lane configuration summary screen
    utils.send_keys("L")
    if not utils.wait_for_text_in_console("LANE CONFIGURATION SUMMARY", False, 15):
        fail_step("[set_printer_type] Could not load the lane configuration menu.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
    #Press A to
    utils.send_keys("A")
    if not utils.wait_for_text_in_console("ADD LANE CONFIGURATION", False, 15):
        fail_step("[set_printer_type] Could not find the ADD LANE CONFIGURATION.")
        utils.take_screenshot()
        utils.finish_execution()
    utils.send_keys(lane_num)
    utils.send_keys("[left][left][left][down]")
    #pdb.set_trace()
    utils.send_keys(term_id)
    #pdb.set_trace()
    utils.send_keys("[left][left][down][down][down][down][enter]")
    #pdb.set_trace()
    time.sleep(2)
    utils.send_keys("y[enter]")
    # Press down a bunch of times in order to reach the printer type field and
    # then press enter to open the drop down.
    utils.send_keys("[down][down][down][down][down][down][enter]")
#Verify all the printer types are displaye
    capture_WEBPOS_messages(pinters)
    utils.send_keys("[enter]")
# Press esc to laeve the page
    utils.send_keys("[esc]")
#Exit display
    exit_Laneconfig_display()



#Function NAme:capture_driver_version
# Description : Get the info Driver from console
#Author: Smitha D
def capture_driver_version():
    start_step("Capture the version of the Printer driver that is currently running.")

    # Show up ther version on screen and capture it
    utils.send_keys("[f1]")
    version = utils.match_text_in_console("(DRIVER:3.12.1202)")

    # Check if we got to find the version on screen
    if version is None or len(version)==0:
        fail_step("[capture_os_version] Could not capture the os version from screen.")
        sys.exit(0)

    # Remove the DRIVER : from the string
    version = version[0].replace("DRIVER: ", "")

    # Return it without surrounding spaces
    pass_step("Current OS version is '%s'." % tu(version))
    return tu(version)

#Function NAme:get_lane_mapping_screen_info
# Description : Get the info about lanes added
#Author: Smitha D

def get_lane_mapping_screen_info(lane_info):
    #Function to go to Display
    awards_utils.load_display_application()
    #Press C to go to Coniguration Menu
    utils.send_keys("C")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_printer_type] Could not load the configuration menu in the display application.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
    time.sleep(2)

    # Press L to get into the lane configuration summary screen
    utils.send_keys("L")
    if not utils.wait_for_text_in_console("LANE CONFIGURATION SUMMARY", False, 15):
        fail_step("[set_printer_type] Could not load the lane configuration menu.")
        utils.take_screenshot()
        utils.finish_execution()
    #Press F5 to get the Lane mapping Status
    utils.send_keys("[F5]")
    time.sleep(2)
    if not utils.wait_for_text_in_console("LANEMAPPING STATUS", False, 15):
        fail_step("[set_printer_type] Could not load the lane mapping status page")
        utils.take_screenshot()
        utils.finish_execution()
    #Verify all the info displayed properly
    capture_WEBPOS_messages(lane_info)
    #Press esc to leave the page
    utils.send_keys("[esc]")
    # Exit display
    exit_Laneconfig_display()

#Function name get_printertype_preload
#Descriptino: Get the printer type from preload
#Author: Smitha D
def get_printertype_preload(printers):
    start_action("Get the printer types from preload")
    utils.send_keys("cd C:\\ [enter]")
    utils.send_keys("newstore [enter]")
    utils.wait_for_text_in_console("Please select the following from list", False, 10)
    utils.send_keys("1[enter]")

    # Select the country
    utils.wait_for_text_in_console("Which country does the current store will reside in?", False, 10)
    utils.send_keys("%s[enter]" % countries[tl(country)])
    time.sleep(2)

    # Select the printer type
    #pdb.set_trace()
    utils.wait_for_text_in_console("Which printer type is being installed?", False, 10)
    utils.send_keys("%s[enter]" % printers[tl(printer)])
    time.sleep(3)
    #Get the printers listed on Console and verify all the printers are listed
    capture_WEBPOS_messages(printers)

    # Select the locale
    utils.wait_for_text_in_console("Which locale does the current store reside in?", False, 10)
    utils.send_keys("%s[enter]" % locales[tl(locale)])
    time.sleep(3)

    #Exit newstore
    utils.send_keys("2[enter]")
    utils.wait_for_prompt("C:\\")

#function Name: Change Lane mapping
#Description change the lanes
#Author Smitha D
def change_lane_mapping():
    start_step("Change lane mapping")
    awards_utils.load_display_application()


    # Press C to get into the configuration menu
    utils.send_keys("c")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_printer_type] Could not load the lane configuration menu.")
        utils.take_screenshot()
        utils.finish_execution()

    # Press L to get into the lane configuration summary screen
    utils.send_keys("L")
    if not utils.wait_for_text_in_console("LANE CONFIGURATION SUMMARY", False, 15):
        fail_step("[set_printer_type] Could not load the lane configuration menu.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
    # Press A to Add Lane
    utils.send_keys("C")
    if not utils.wait_for_text_in_console("CHANGE LANE CONFIGURATION", False, 15):
        fail_step("[set_printer_type] Could not find the ADD LANE CONFIGURATION.")
        utils.take_screenshot()
        utils.finish_execution()


    # Press F1 to confirm the changes
    utils.send_keys("[f1]")
    if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 15):
        fail_step("[set_printer_type] Could not find the changes confirmation message on screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
    utils.send_keys("Y")

    time.sleep(60)

    # Press Y to confirm the changes
    # Added by Smitha
    utils.send_keys("Y")


    utils.take_screenshot()
    #This is added to verify the functionality of Lane Max
    #If the lane bumber is greater than 128 Eror messge should be displayed
    if lane_max==True:
            if not utils.wait_for_text_in_console("Attempted to install Lane with number exceeding maximum lane" ,False, 60):
                fail_step("[set_printer_type] Could not leave to the configuration menu.")
                utils.take_screenshot()
                #utils.finish_execution()
            utils.send_keys("E")
    else:
        #Verify MAC Address Page is displayed
        if not utils.wait_for_text_in_console("MAC ADDRESS-LANE MAPPING", False, 60):
            #pdb.set_trace()
            if not utils.wait_for_text_in_console("PRINTER ENUMERATION ERROR", False, 15):

                fail_step("[set_printer_type] Could not leave to the configuration menu.")
                utils.take_screenshot()
                utils.finish_execution()

            utils.send_keys("[Esc]")
    #
        else:
            #Enter the Mac Addess and prss F1 to save the changes

            utils.send_keys("[f1]")
            if not utils.wait_for_text_in_console("Would you like to save these changes?", False, 15):
                fail_step("[set_printer_type] Could not find the changes confirmation message on screen.")
                utils.take_screenshot()
                utils.finish_execution()

            utils.send_keys("Y")
            time.sleep(30)
        #pdb.set_trace()

    # Lane configuration Summary Page will be displayed after saving changes
    if not utils.wait_for_text_in_console("LANE CONFIGURATION SUMMARY", False, 15):

            fail_step("[set_printer_type] Could not load the lane configuration screen after adding new lane.")
            utils.take_screenshot()
            utils.finish_execution()

    utils.take_screenshot()
    #
    # Press E to leave to the previous screen
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("CONFIGURATION MENU", False, 15):
        fail_step("[set_printer_type] Could not leave to the configuration menu.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

    # Press E to go to the main menu
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("MAIN MENU", False, 15):
        fail_step("[set_printer_type] Could not leave to the main menu.")
        utils.take_screenshot()
        utils.finish_execution()

    # Leave to the exit confirmation screen
    utils.send_keys("E")
    if not utils.wait_for_text_in_console("Exit program?", False, 15):
        fail_step("[set_printer_type] Could not reach the exit confirmation screen.")
        utils.take_screenshot()
        utils.finish_execution()

    # Leave to the prompt
    utils.send_keys("Y")
    pass_step("Printer changed successully")
#Function Name: get_system_details
#Description: Run "STATUS SYSTEM" command and copy details to text file
#Author: Smitha D
def get_system_details():
    start_step("Getting meomry details of AWARDS machine")
    awards_navigation.to_multitastking_store_system()
    utils.send_keys("STATUS SYSTEM[enter]")
    time.sleep(2)
    screen = utils.get_console_text()
    memory_usage=re.findall("SysInfo \[CPU = \d+%, VirtualMemory = \d+%\]", screen)
    vir_memory=""
    if len(memory_usage)>0:
        vir_memory=memory_usage[-1].split("=")[-1].split("%")[0]
        filename="%s\%s_memoryusage_%s.txt"%(DEFAULT_COPY_ERROR_LOG,ms.awards_machine_name,datetime.datetime.now().strftime("%Y_%m_%d"))
        if not os.path.isfile(filename):
            fh=open(filename,"w")
            fh.close()
        fh=open(filename,"a")
        print(memory_usage[-1])


        fh.write("\n"+datetime.datetime.now().strftime("%Y/%m/%d %H:%M%S")+"\t"+sys.argv[0].split("\\")[-1]+ "\t"+memory_usage[-1])
        fh.close()
    return vir_memory
#Function name: download_vault_build
#Decription: Get the latest vault build
#Author: mitha D
def download_latest_vault(params):
    remote_path=DEFAULT_VAULT_CI_NIGHTLY_BUILD_PATH
    directories = os.listdir(remote_path)
    latest=""
    alist={}
    local_path = DEFAULT_UPDATE_LOCAL_FILEPATH + "\%s" %ms.machine_name
    print (sorted(directories))
    latest=sorted(directories)[-1]

    if not os.path.exists(local_path):
        os.makedirs(local_path)
    if latest != vault_build_ver:
        if (sorted([tu(latest),tu(vault_build_ver)]))[-1] == latest:
                files=os.listdir(os.path.join(remote_path,latest))
                for file_name in files:
                    if tu(file_name.split(".")[-1])=="ZIP":
                        filename=file_name
                        break
                file_path = "%s\%s\%s" % (remote_path,latest,filename)
                shutil.copy(file_path, local_path)
                update_vault_version(latest)
                download_from_ftp_to_sut({ms.machine_name +"\\"+ filename:filename})

                utils.send_keys(r"copy %s %s\%s [enter]"%(filename,DEFAULT_IC_FILE_DESTINATION,"vault.zip"))
                utils.send_keys("exit[enter]")
                awards_navigation.to_multitastking_store_system()
                shutdown_awards()
                start_awards()
                pass_step("Vault upated successfully")
    else:
        pass_step("Vault build is upto date")




#Function name : update_vault_version(
#Description update vault verssion in config file
#Author Smith D
def update_vault_version(vault_version):

    # Build the config file path. It will always be at the framework root
    config_file_path = "%s\%s" % (base_path, "config.py")

    # Get the config file contents
    fh= open(config_file_path,"r+")

    file_content=fh.readlines()
    # Now that we have the contents, find any row with the values and comment
    # You might want to cleanup the file from time to time
    for  row in range(0,len(file_content)):#fh.readlines():

        if "vault_build_ver" in file_content[row]:
            file_content[row]=file_content[row].split("=")[0]+'="' +  vault_version +'"\n'

    # Write the new config file
    with open(config_file_path, "w+") as cfg_file:
        for row in file_content:
            cfg_file.write(row)
