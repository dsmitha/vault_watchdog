# File: Awards Navigation
#
# Filename:
# > awards_navigation.py
#
# Description:
# This file holds the navigation functions for the Awards automation. You can
# consider a navigation function anything that takes you from one screen to
# another. The main intent of navigation functions is to execute the actions
# needed to navigate through the application and validate that this navigation
# has been done properly.
#
# Notes:
# - Navigation will register *steps* in the report, but sometimes you might
#   want to encapsulate them into actions, mainly when they call other
#   navigation functions.
#
# Naming convention:
# Navigation functions should use the following name convention:
# - *to*_somewhere
# - *from*_somewhere_*to*_somewhere_else
# - *leave*_somewhere

# Framework imports
from framework.reporting import *
from framework.awards.defaults import *
import framework.awards.utils as utils

# NAV imports
import awards_actions

# Python imports
import time

# Function: to_multitastking_store_system
# Navigates to the Awards session.
#
# Process:
# Go to the running groups screen and then select the multitasking store system.
#
# Screen:
# Any
#
# Author:
# Danilo Guimaraes
def to_multitastking_store_system():

    start_action("Navigate to the Multitasking Store System console.")

    # First we need to make sure we are in the running groups screen
    to_running_groups_screen()

    start_step("Go to the Multitasking Store System console.")

    # In the running groups screen, select the right pane and load the
    # multitasking store systems
    awards_actions.select_multitasking_store_systems()

    utils.take_screenshot()

    end_action()

# Function: to_running_groups_screen
# Navigates to the Running Groups screen.
#
# Process:
# Press [ctrl!][esc!] until the running groups screen can be found.
#
# Screen:
# Any
#
# Author:
# Danilo Guimaraes
def to_running_groups_screen():

    # Initialize some variables
    tries = MAX_TIMES_TO_TRY

    start_step("Go to the Running Groups screen.")

    # Try to get to the running groups screen a few times
    while tries >= 0:

        # Check if we are in the right screen
        if not utils.find_text_in_console("Start Group"):

            # If not, Press and release CTRL + Esc
            utils.send_keys("[ctrl!][esc!]")
            utils.send_keys("[ctrl.][esc.]")

            # One less chance
            tries -=1

            # Wait a bit
            time.sleep(3)

        # If we are in the right screen, jump out of the loop
        else:
            break

    # Check if it has failed
    #fail_step("not in multi tasking")
    if tries < 0:
        fail_step("[awards_navigation.to_running_groups_screen] Could not load the running groups screen.")
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()

# Function: to_new_dos_session
# Navigates to the Running Groups screen and open a new dos session from there.
#
# Process:
# Go to the running groups screen. Then, open a new dos session.
#
# Screen:
# Any
#
# Author:
# Danilo Guimaraes
def to_new_dos_session():

    # Initialize some variables
    tries = MAX_TIMES_TO_TRY

    add_sub_step("Open a new DOS session.")

    # Try to get to the running groups screen a few times
    while tries > 0:

        # Check if we are in the right screen
        if not utils.find_text_in_console("Start Group"):

            # If not, Press and release CTRL + Esc
            utils.send_keys("[ctrl!][esc!]")
            utils.send_keys("[ctrl.][esc.]")

            # One less chance
            tries -=1

            # Wait a bit
            time.sleep(1)

        # If we are in the right screen, jump out of the loop
        else:
            break

        # Check if it has failed
        if tries == 0:
            fail_step("[to_new_dos_session] Could not load the running groups screen.")
            utils.take_screenshot()
            utils.finish_execution()

    # Focus the left pane and initiate a new dos session
    awards_actions.focus_pane("left")
    utils.send_keys("[home][enter]")

    # Make sure are in the right place
    if not utils.wait_for_text_in_console("Microsoft Windows XP"):
        fail_step("[to_new_dos_session] Could open a new MSDOS session.")
        utils.finish_execution()

    utils.take_screenshot()
# Function: to_vault_session
# Move o the console where vault is running.
#
# Process:
# Send "[down]" to the awards box and select the vault running console.
#
# Screen:
# Should be at a running DOS session.
#
# Author:
# Smitha D
def to_vault_session():
    tries = MAX_TIMES_TO_TRY
    while tries > 0:

        # Check if we are in the right screen
        if not utils.find_text_in_console("Start Group"):

            # If not, Press and release CTRL + Esc
            utils.send_keys("[ctrl!][esc!]")
            utils.send_keys("[ctrl.][esc.]")

            # One less chance
            tries -=1

            # Wait a bit
            time.sleep(1)

        # If we are in the right screen, jump out of the loop
        else:
            break

        # Check if it has failed
        if tries == 0:
            fail_step("[to_new_dos_session] Could not load the running groups screen.")
            utils.take_screenshot()
            utils.finish_execution()
    bot_row = utils.bottom_row(utils.get_console_screen_area(43,3,75,23))
    awards_actions.focus_pane("right")
    utils.send_keys("[home][down]")


    while bot_row.find("DOS Session - run.cmd") != 0:

        utils.send_keys("[down]")
        print (bot_row)
        print(bot_row.find("DOS Session - run.cmd"))

        bot_row = utils.bottom_row(utils.get_console_screen_area(43,3,75,23))
    utils.send_keys("[enter]")

# Function: leave_dos_session
# Finishes the current DOS session.
#
# Process:
# Send "exit[enter]" to the awards box.
#
# Screen:
# Should be at a running DOS session.
#
# Author:
# Danilo Guimaraes
def leave_dos_session():
    #start_step("Exit the DOS session.")
    utils.send_keys("exit[enter]")

    utils.take_screenshot()
