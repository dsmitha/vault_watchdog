from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"need databse cleanup?":"Yes","data source name":"XSD_DB.zip","cds enabled?":"Yes","need database?":"no","printer type":"CMC6","expected clu":"16",})
awards_keywords.validate_xmllog({"text_to_validate":r"Info Processing File: \CDS\MIRROR\AWARD_FILE\1136366.XML created from schema Version:","file_name":"log.txt","validate_step":"Yes"})
