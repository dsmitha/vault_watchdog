from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"cds enabled?":"Yes","data source name":"XSD_DB.zip","printer type":"CMC6","expected clu":"16","need databse cleanup?":"yes","need database?":"No",})
awards_keywords.validate_xmllog({"text_to_validate":r"Info Validating (parser ver 1.0.0) file  \CDS\MIRROR\AWARD_~1\854168.XML","file_name":"log.txt","validate_step":"Yes"})
awards_keywords.validate_xmllog({"text_to_validate":r"Info Processing File: \CDS\MIRROR\AWARD_FILE\854168.XML created from schema Version: 1.0.0","file_name":"log.txt","validate_step":"Yes"})
