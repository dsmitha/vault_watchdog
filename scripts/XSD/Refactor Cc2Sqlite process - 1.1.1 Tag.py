from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"cds enabled?":"Yes","data source name":"XSD_DB.zip","need databse cleanup?":"yes","printer type":"CMC6","need database?":"No","expected clu":"16",})
awards_keywords.validate_xmllog({"text_to_validate":r"Error Unable to load parser for  file :\CDS\MIRROR\AWARD_~1\111111.XML","file_name":"log,txt","validate_step":"Yes"})
