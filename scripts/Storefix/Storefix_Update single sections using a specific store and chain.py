from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    Ssys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.a_a_a_initialize_store_system({"store number":"0250","data source name":"CDS0250ZP.376.zip","expected clu":"499","cds enabled?":"Yes","chain number":"376","need database?":"No","printer type":"CMC6",})
awards_keywords.verify_storefix_single_section({"store number":"0250","section name":"omni","big database?":"No","expected clu":"773","cds enabled?":"Yes","storefix file path":"I:\SQA\shared\Automation\Projects\Awards\Storefix","chain number":"376&nbsp;","printer type":"CMC6","validate_step":"Yes"})
