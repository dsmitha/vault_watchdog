from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.a_a_a_initialize_store_system({"store number":"0250","printer type":"CMC6","chain number":"376","cds enabled?":"Yes","need database?":"No","data source name":"CDS0250ZP.376.zip","expected clu":"773",})
awards_keywords.verify_storefix_single_section({"store number":"0250","printer type":"CMC6","section name":"Coupons","chain number":"376&nbsp;","cds enabled?":"Yes","expected clu":"773","big database?":"No","storefix file path":"I:\SQA\shared\Automation\Projects\Awards\Storefix","validate_step":"Yes"})
