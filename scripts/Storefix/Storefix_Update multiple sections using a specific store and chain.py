from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.a_a_a_initialize_store_system({"cds enabled?":"Yes","store number":"0250","printer type":"CMC6","data source name":"CDS9251ZP.402.zip","chain number":"376","need database?":"No","expected clu":"773",})
awards_keywords.verify_storefix_ini({"storefix file path":"I:\SQA\shared\Automation\Projects\Awards\Storefix","printer type":"CMC6","cds enabled?":"Yes","chain number":"376","store number":"0250","expected clu":"773","validate_step":"Yes"})
