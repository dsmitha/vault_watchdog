from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.cmcset_update({"print flag":"Yes","clu":"1264758","validation flag":"Yes",})
awards_keywords.print_coupon({"validate barcode":"yes","coupon number":"1264758","print count":"1","validate coupon":"No","include file name":"GS1E_so.txt",})
awards_keywords.validate_bar_code({"barcode color":"black","include barcode file name":"GS1E.png","barcode type":"EAN1","validate_step":"Yes"})
