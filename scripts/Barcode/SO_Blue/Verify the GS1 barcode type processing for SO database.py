from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.cmcset_update({"clu":"1263798","print flag":"Yes",})
awards_keywords.print_coupon({"validate barcode":"yes","coupon number":"1263798","include file name":"GS1_so.txt","validate coupon":"No","print count":"1",})
awards_keywords.validate_bar_code({"include barcode file name":"GS1_so.png","barcode type":"GS1","validate_step":"Yes"})
