from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.cmcset_update({"print flag":"Yes","clu":"1264779",})
awards_keywords.print_coupon({"coupon number":"1264779","include file name":"EAN_so.txt","validate barcode":"yes","validate coupon":"No","print count":"1",})
awards_keywords.validate_bar_code({"include barcode file name":"EAN_so.png","barcode color":"blue","barcode type":"EAN","validate_step":"Yes"})
