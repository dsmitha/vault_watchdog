from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.cmcset_update({"clu":"1235127","validation flag":"Yes","print flag":"Yes",})
awards_keywords.print_coupon({"coupon number":"1235127","validate barcode":"yes","validate coupon":"No","print count":"1","include file name":"GS1E.txt",})
awards_keywords.validate_bar_code({"barcode type":"GS1E","barcode color":"black","include barcode file name":"GS1E.png","validate_step":"Yes"})
