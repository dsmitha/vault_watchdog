from pathlib import Path
import os,sys


if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.cmcset_update({"clu":"1237970","print flag":"Yes","validation flag":"Yes",})

awards_keywords.print_coupon({"validate barcode":"yes","validate coupon":"No","include file name":"QRCode.txt","print count":"1","coupon number":"1237970",})
awards_keywords.validate_bar_code({"barcode color":"black","barcode type":"QRCode","include barcode file name":"QRCode.png","validate_step":"Yes"})
