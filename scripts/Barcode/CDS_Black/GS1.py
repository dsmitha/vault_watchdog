from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.cmcset_update({"print flag":"Yes","clu":"1310571",})
awards_keywords.print_coupon({"validate coupon":"No","coupon number":"1310571","include file name":"mfd3.txt","validate barcode":"yes","print count":"1",})
awards_keywords.validate_bar_code({"include barcode file name":"GS1.png","barcode color":"black","barcode type":"GS1","validate_step":"Yes"})
