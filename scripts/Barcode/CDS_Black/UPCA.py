from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.cmcset_update({"clu":"1310569","print flag":"Yes",})
awards_keywords.print_coupon({"coupon number":"1310569","print count":"1","validate barcode":"yes","include file name":"mfd1.txt",})
awards_keywords.validate_bar_code({"barcode color":"black","include barcode file name":"UPCA.png","barcode type":"UPCA","validate_step":"Yes"})
