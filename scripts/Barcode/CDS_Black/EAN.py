from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.cmcset_update({"clu":"1310570","print flag":"Yes",})
awards_keywords.print_coupon({"validate coupon":"No","validate barcode":"Yes","include file name":"mfd2.txt","coupon number":"1310570","print count":"1",})
awards_keywords.validate_bar_code({"include barcode file name":"EAN.png","barcode color":"black","barcode type":"EAN","validate_step":"Yes"})
