from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core
from framework.reporting import *
import awards_keywords
platformid=sys.argv[2]

awards_keywords.cmcset_update({"validation flag":"Yes","clu":"1228044","print flag":"Yes",})
awards_keywords.print_coupon({"include file name":"c128.txt","validate barcode":"yes","validate coupon":"No","coupon number":"1228044","print count":"1",})
awards_keywords.validate_bar_code({"barcode type":"c128","include barcode file name":"C128.png","barcode color":"black","validate_step":"Yes"})
