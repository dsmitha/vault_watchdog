from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.cmcset_update({"validation flag":"Yes","print flag":"Yes","clu":"1237752",})
awards_keywords.print_coupon({"coupon number":"1237752","validate coupon":"No","print count":"1","validate barcode":"yes","include file name":"PDF417.txt",})
awards_keywords.validate_bar_code({"include barcode file name":"PDF417.png","barcode color":"blue","barcode type":"PDF417","validate_step":"Yes"})
