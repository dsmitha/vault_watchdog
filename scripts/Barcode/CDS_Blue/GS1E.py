from pathlib import Path
import os,sys


if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.cmcset_update({"print flag":"Yes","validation flag":"No","clu":"1235127",})
awards_keywords.print_coupon({"coupon number":"1235127","validate coupon":"No","include file name":"GS1E.txt","print count":"1","validate barcode":"yes",})
awards_keywords.validate_bar_code({"barcode color":"blue","include barcode file name":"GS1E.png","barcode type":"GS1E","validate_step":"Yes"})
