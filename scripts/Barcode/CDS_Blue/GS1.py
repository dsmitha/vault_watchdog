from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

VM = sys.argv[1]
platformid = sys.argv[2]
testcaseid = sys.argv[3]
testplanid = sys.argv[4]

testlink.addtestcasetotestplan({"testprojectid":"3","testplanid":testplanid,"tcexternalid":"AW-11", "platformid":platformid})
awards_keywords.cmcset_update({"oclu":"104","print flag":"Yes",})
awards_keywords.print_coupon({"include file name":"mfd3.txt","validate coupon":"No","print count":"1","coupon number":"1310571","validate barcode":"yes",})
awards_keywords.validate_bar_code({"barcode type":"GS1","include barcode file name":"GS1.png","barcode color":"blue","validate_step":"Yes"})
