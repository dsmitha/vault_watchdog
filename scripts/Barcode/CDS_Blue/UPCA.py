from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

VM = sys.argv[1]
platformid = sys.argv[2]
testcaseid = sys.argv[3]
testplanid = sys.argv[4]

testlink.addtestcasetotestplan({"testprojectid":"3","testplanid":testplanid,"tcexternalid":"AW-17", "platformid":platformid})
awards_keywords.cmcset_update({"print flag":"Yes","oclu":"71",})
awards_keywords.print_coupon({"coupon number":"1310569","print count":"1","validate barcode":"yes","include file name":"mfd1.txt","validate coupon":"No",})
awards_keywords.validate_bar_code({"barcode color":"blue","include barcode file name":"UPCA.png","barcode type":"UPCA","validate_step":"Yes"})
