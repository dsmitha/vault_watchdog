from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.cmcset_update({"validation flag":"Yes","print flag":"Yes;","clu":"1264758",})
awards_keywords.print_coupon({"print count":"1","validate barcode":"yes","validate coupon":"No","include file name":"GS1E_so.txt","coupon number":"1264758",})
awards_keywords.validate_bar_code({"barcode type":"EAN1","include barcode file name":"GS1E.png","barcode color":"black","validate_step":"Yes"})
