from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
palformid=sys.argv[2]

awards_keywords.cmcset_update({"print flag":"Yes","clu":"1264779",})
awards_keywords.print_coupon({"print count":"1","validate barcode":"yes","coupon number":"1264779","include file name":"EAN_so.txt","validate coupon":"No",})
awards_keywords.validate_bar_code({"barcode type":"EAN","include barcode file name":"EAN_so.png","barcode color":"black","validate_step":"Yes"})
