from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.cmcset_update({"clu":"1263640","print flag":"Yes",})
awards_keywords.print_coupon({"validate barcode":"yes","include file name":"UPCA_so.txt","validate coupon":"No","coupon number":"1263640","print count":"1",})
awards_keywords.validate_bar_code({"include barcode file name":"UPCA_So.png","barcode type":"UPCA","barcode color":"black","validate_step":"Yes"})
