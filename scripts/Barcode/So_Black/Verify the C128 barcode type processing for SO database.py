from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
palformid=sys.argv[2]

awards_keywords.cmcset_update({"print flag":"Yes","clu":"1264620","validation flag":"Yes",})
awards_keywords.print_coupon({"validate coupon":"No","include file name":"c128_so.txt","coupon number":"1264620","print count":"1","validate barcode":"yes",})
awards_keywords.validate_bar_code({"include barcode file name":"C128_so.png","barcode color":"black","barcode type":"c128","validate_step":"Yes"})
