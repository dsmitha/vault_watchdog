from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.cmcset_update({"print flag":"Yes","clu":"1264779","validation flag":"Yes",})
awards_keywords.print_coupon({"validate coupon":"No","print count":"1","include file name":"PDF417_so.txt","validate barcode":"yes","coupon number":"1264779",})
awards_keywords.validate_bar_code({"include barcode file name":"PDF417_so.png","barcode type":"PDF417","barcode color":"black","validate_step":"Yes"})
