from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.cmcset_update({"clu":"1264742","print flag":"Yes","validation flag":"Yes",})
awards_keywords.print_coupon({"print count":"1","validate barcode":"yes","coupon number":"1264742","validate coupon":"No","include file name":"QRCODE_so.txt",})
awards_keywords.validate_bar_code({"barcode color":"black","include barcode file name":"QRCode_so.png","barcode type":"QRCODE","validate_step":"Yes"})
