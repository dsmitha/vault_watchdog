from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
VM = sys.argv[1]
platformid = sys.argv[2]
testcaseid = sys.argv[3]
testplanid = sys.argv[4]

testlink.addtestcasetotestplan({"testprojectid":"3","testplanid":testplanid,"tcexternalid":"AW-27", "platformid":platformid})
#awards_keywords.a_a_a_initialize_store_system({"need database?":"No","data source name":"CDS0250ZP.376.zip","big database?":"No","store number":"0250","printer type":"CMC6","chain number":"376","expected clu":"7","cds enabled?":"Yes",})
awards_keywords.shutdown_awards({})
awards_keywords.update_ini_file_before_print_coupon({"value":"Enabled:No","sectionname":"CDS","ini filename":"c:\strsystm\data\store.ini",})
awards_keywords.start_awards({})
awards_keywords.force_load_negative({"regex? (true/false)":"False","cds enabled?":"Yes","expected message":"CDS IS DISABLED IN STORE.INI","validate_step":"Yes"})
