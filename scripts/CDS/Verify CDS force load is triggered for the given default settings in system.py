from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core
from framework.awards.settings import machine_settings as ms

from framework.reporting import *
import awards_keywords

ip_address=""
awards_keywords.validate_section_in_ini_file({"ini file path":"C:\strsystm\data\system.ini","schedule":"at +00:06:00, repeat 5 min, cdsForceLoad","section name":"process","validate_step":"Yes"})
awards_keywords.validate_file_exists({"filename":"c:\strsystm\log\CDSforceloadstatus-??_??_????.log"})
awards_keywords.validate_cds_dblog({"messages":["CdsForceLoadDBCommand processing STARTED",r"\cds\mirror\related_file\Force_load.job file NOT found"]})
