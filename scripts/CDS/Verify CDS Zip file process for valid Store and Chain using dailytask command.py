from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
awards_keywords.load_cds_database({"database name":"CDS0250ZP.376.zip","expected clus":"17"})
awards_keywords.run_batch_dailytask({"cds enabled?":"Yes","expected clus":"17"})
