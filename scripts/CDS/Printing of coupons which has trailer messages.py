from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core
from framework.awards.settings import machine_settings as ms

from framework.reporting import *
import awards_keywords
print (sys.argv[3])
#pdb.set_trace()

VM = sys.argv[1]
platformid = sys.argv[2]
testcaseid = sys.argv[3]
testplanid = sys.argv[4]


awards_keywords.a_a_a_initialize_store_system({"cds enabled?":"Yes","big database?":"No","need database?":"No","store number":"0250","expected clu":"773","printer type":"CMC6","data source name":"CDS0250ZP.376.zip","chain number":"376",})
awards_keywords.cmcset_update({"print flag":"Yes","oclu":"73",})
awards_keywords.print_coupon({"validate barcode":"yes","include file name":"mfd2.txt","validate coupon":"No","print count":"1","coupon number":"1310570",})
awards_keywords.validate_bar_code({"include barcode file name":"EAN.png","barcode color":"blck","barcode type":"EAN","validate_step":"Yes"})
