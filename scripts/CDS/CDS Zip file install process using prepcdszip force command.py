from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
VM = sys.argv[1]
platformid = sys.argv[2]
testcaseid = sys.argv[3]
testplanid = sys.argv[4]

awards_keywords.run_batch_prepcds({"database name":"CDS0250ZP.376.zip"})
awards_keywords.cmcset_update({"clu":"1303278","print flag":"Yes",})
awards_keywords.print_coupon({"include file name":"cds_AFT.txt","print count":"2","coupon number":"1303278","validate coupon":"No","validate_step":"Yes"})
