from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"printer type":"CMC4","store number":"0198","data source name":"addressdb.zip","chain number":"198","cds enabled?":"No","big database?":"No","need database?":"YES","expected clu":"499",})
awards_keywords.run_batch_dailytask({"cds enabled?":"Yes","validate_step":"Yes"})
