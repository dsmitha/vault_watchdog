from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
#testlink.addtestcasetotestplan({"testprojectid":"3","tcexternalid":"SS-18","testplanid":sys.argv[4],})
awards_keywords.a_a_a_initialize_store_system({"chain number":"376","expected clu":"33","printer type":"CMC6","data source name":"SO0198ZP.198","store number":"0198","need database?":"YES","big database?":"No","cds enabled?":"No","validate_step":"Yes"})
