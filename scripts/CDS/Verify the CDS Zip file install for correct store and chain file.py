from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
VM = sys.argv[1]
platformid = sys.argv[2]
testcaseid = sys.argv[3]
testplanid = sys.argv[4]
awards_keywords.load_cds_database({"database name":"CDS0250ZP.376.zip"})
awards_keywords.force_load({"cds enabled?":"Yes","big database?":"No","expected clu":"17","validate_step":"Yes"})
