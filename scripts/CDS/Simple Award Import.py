from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"expected clu":"773","big database?":"Yes","store number":"0250","data source name":"CDS0250ZP.376.zip","cds enabled?":"Yes","chain number":"376","printer type":"CMC6","need database?":"No",})
awards_keywords.cmcset_update({"clu":"1325649","print flag":"Yes",})
awards_keywords.print_coupon({"coupon number":"1329376","include file name":"CDS_Basic.txt","print count":"1","validate_step":"Yes"})
