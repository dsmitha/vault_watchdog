from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"data source name":"CDS0250ZP.376.zip","big database?":"No","need database?":"No","printer type":"CMC6","expected clu":"773","chain number":"376","store number":"0250","cds enabled?":"Yes",})
awards_keywords.validate_images_folder({"expected amount of files":"30","validate_step":"Yes"})
