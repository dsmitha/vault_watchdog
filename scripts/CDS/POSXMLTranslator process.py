from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
#pdb.set_trace()
awards_keywords.validate_cds_section_in_ini_file({"afarialockfilename":"TransferLock.FLG","ccdir":"cc_file","jobfilebasepattern":"*","awarddir":"award_file","enabled":"Yes","awardfilebasepattern":"*","catalinadir":"to_catalina","mirror":"\cds\mirror","ccfilebasepattern":"*","ini file path":"C:\strsystm\install\store.USA","jobdir":"related_file","awardfileextension":"xml","jobfileextension":"job","ccfileextension":"xml","validate_step":"Yes"})
