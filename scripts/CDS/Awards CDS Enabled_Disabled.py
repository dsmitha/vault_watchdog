from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"cds enabled?":"Yes","big database?":"No","need database?":"No","store number":"0250","expected clu":"773","printer type":"CMC6","data source name":"CDS0250ZP.376.zip","chain number":"376",})
awards_keywords.shutdown_awards({})
#awards_keywords.replace_store_ini({"store ini name":"Store.CDSNO",})
awards_keywords.update_ini_file_before_print_coupon({"value":"Enabled=No","sectionname":"CDS","ini filename":"c:\strsystm\data\store.ini",})
awards_keywords.start_awards({})
awards_keywords.validate_cds_section_on_screen({"enabled":"No","validate_step":"Yes"})
awards_keywords.shutdown_awards({})
#awards_keywords.replace_store_ini({"store ini name":"Store.CDSYES",})
awards_keywords.update_ini_file_before_print_coupon({"value":"Enabled=Yes","sectionname":"CDS","ini filename":"c:\strsystm\data\store.ini",})
awards_keywords.start_awards({})
awards_keywords.validate_cds_section_on_screen({"enabled":"Yes","validate_step":"Yes"})
