from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
awards_keywords.update_firstupd({})
awards_keywords.validate_cds_section_not_in_ini_file({"Enabled":"Yes","Mirror":"\cds\mirror","awardDir":"awards_file","JobDir":"relatedfile","CatalinaDir":"to_catalina",\
"CCDir":"cc_file","CCFileExtension":"xml","CCFileBasepattern":"*","AwardsfileExtension":"xml","AwardFileBasePattern":"*","JobfileExtension":"job",\
 "JobFileBasePattern":"*"," AfariaLockFileName":"TransferLock.FLG","ini file path":r"C:\strsystm\data\store.ini","validate_step":"Yes"})
