from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.a_a_a_initialize_store_system({"need database?":"No","need databse cleanup?":"yes","data source name":"CDS0250ZP.376.zip","printer type":"CMC6","expected clu":"16","cds enabled?":"Yes",})
awards_keywords.validate_xmllog({"text_to_validate":r"Error Failed to find msg number  5 in the messages section in file :\CDS\MIRROR\AWARD_~1\854656.XML","validate_step":"Yes"})
awards_keywords.validate_xmllog({"text_to_validate":r"Error Failed to find msg number  7 in the messages section in file :\CDS\MIRROR\AWARD_~1\854656.XML","file_name":"log.txt","validate_step":"Yes"})
