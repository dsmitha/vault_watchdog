from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.a_a_a_initialize_store_system({"data source name":"CDS0250ZP.376.zip","printer type":"CMC6","expected clu":"773","need database?":"No","store number":"0250","cds enabled?":"Yes","chain number":"376",})
awards_keywords.cmcset_update({"clu":"1326084","print flag":"Yes",})
awards_keywords.print_coupon({"coupon number":"1326084","print count":"1","include file name":"cds_id.txt","validate coupon":"No","validate_step":"Yes"})
