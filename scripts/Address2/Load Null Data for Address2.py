from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.a_a_a_initialize_store_system({"expected clu":"22","need database?":"No","printer type":"CMC6","data source name":"addressdb.zip","need databse cleanup?":"Yes","cds enabled?":"Yes",})
awards_keywords.shutdown_awards({})
awards_keywords.validate_address2({"case sensitive?":"False","search option":"file contains","option":"m","regex?":"False","text to validate":"Address 2 line NULL","validate_step":"Yes"})
awards_keywords.validate_address2({"case sensitive?":"False","search option":"file contains","option":"s","regex?":"False","text to validate":"10275","validate_step":"Yes"})
awards_keywords.start_awards({})
