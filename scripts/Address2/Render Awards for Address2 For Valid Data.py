from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.a_a_a_initialize_store_system({"expected clu":"22","need database?":"No","printer type":"CMC6","data source name":"addressdb.zip","need databse cleanup?":"Yes","cds enabled?":"Yes",})
awards_keywords.cmcset_update({"print flag":"Yes","clu":"1234728",})
awards_keywords.print_coupon({"include file name":"add2_null.txt","print count":"2","coupon number":"1349258","validate coupon":"No","validate_step":"Yes"})
awards_keywords.WEBPOS_start_capture_log({})
awards_keywords.print_coupon({"include file name":"add2_null.txt","print count":"2","coupon number":"1349258","validate coupon":"No","validate_step":"Yes"})
awards_keywords.validate_bar_code({"barcode type":"add2","include barcode file name":"add2_valid.png","barcode color":"black","validate_step":"Yes"})
