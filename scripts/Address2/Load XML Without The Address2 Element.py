from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core

from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"need database?":"YES","need databse cleanup?":"No","cds enabled?":"Yes","expected clu":"8","data source name":"addressdb.zip","printer type":"CMC6",})
awards_keywords.shutdown_awards({})
awards_keywords.validate_address2({"text to validate":"10277","regex?":"False","option":"m","search option":"file not contains","case sensitive?":"False",})
awards_keywords.validate_address2({"text to validate":"10277","regex?":"False","option":"s","search option":"file not contains","case sensitive?":"False",})
awards_keywords.start_awards({})
awards_keywords.validate_mclu({"mclu":"1237970","validate_step":"Yes"})
