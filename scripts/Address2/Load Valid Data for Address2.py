from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.a_a_a_initialize_store_system({"printer type":"CMC6","need databse cleanup?":"&nbsp;yes","cds enabled?":"Yes","data source name":"addressdb.zip","need database?":"YES","expected clu":"8",})
awards_keywords.shutdown_awards({})
awards_keywords.validate_address2({"case sensitive?":"False","regex?":"False","text to validate":"880156, EL PASO, TX 88588-0156","search option":"file contains","option":"m","validate_step":"Yes"})
awards_keywords.validate_address2({"case sensitive?":"False","regex?":"False","text to validate":"20043","search option":"file contains","option":"s","validate_step":"Yes"})
awards_keywords.start_awards({})
awards_keywords.validate_mclu({"mclu":"1323580","validate_step":"Yes"})
