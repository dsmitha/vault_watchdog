from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.setup_cds_vault({"data source name":"CDS0250.376.zip"})
awards_keywords.update_ini_file_before_print_coupon({"value":"RTPOS_Enabled=Yes","sectionname":"Realtime","ini filename":"c:\strsystm\data\store.ini",})
awards_keywords.update_ini_file_before_print_coupon({"value":"RTTRIG_Enabled=No","sectionname":"Realtime","ini filename":"c:\strsystm\data\store.ini",})
awards_keywords.force_load({"cds enabled?":"Yes","big database?":"No","expected clu":"33"})
awards_keywords.validate_file_not_exists("filename":r"C:\strsystm\DBREPORTFILE")
