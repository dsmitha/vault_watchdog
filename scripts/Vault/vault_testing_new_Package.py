from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.install_vault({})
awards_keywords.update_vault_config_file({"value":"CLOUD_POS=tcp://"+DEFAULT_VAULT_IP+":18620","upload file":"No","sectionname":"default","copy file":"Yes","ini filename":"c:\VAULT\VAULT.usa","validate_step":"Yes"})
awards_keywords.update_vault_config_file({"value":"CLOUS_ADS=tcp://"+DEFAULT_VAULT_IP+":18621","upload file":"No","sectionname":"default","copy file":"No","ini filename":"c:\VAULT\VAULT.usa","validate_step":"Yes"})
awards_keywords.update_vault_config_file({"value":"TEMPLATE_API="+DEFAULT_VAULT_URL+"templates","upload file":"No","sectionname":"default","copy file":"No","ini filename":"c:\VAULT\VAULT.usa","validate_step":"Yes"})
awards_keywords.update_vault_config_file({"value":"FONT_API="+DEFAULT_VAULT_URL+"fonts","upload file":"No","sectionname":"default","copy file":"No","ini filename":"c:\VAULT\VAULT.usa","validate_step":"Yes"})
awards_keywords.update_vault_config_file({"value":"HTTP_PROXY=tcp://"+DEFAULT_VAULT_IP+":18622","upload file":"Yes","sectionname":"default","copy file":"No","ini filename":"c:\VAULT\VAULT.usa","validate_step":"Yes"})
awards_keywords.shutdown_awards({})
awards_keywords.start_awards({})
awards_keywords.print_coupon({"validate coupon":"No","vault":"Yes","print count":"1","include file name":"vault1.txt","validate_step":"Yes"})
