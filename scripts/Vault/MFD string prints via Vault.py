from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.shutdown_awards({})
awards_keywords.update_ini_file_before_print_coupon({"value":"RTPOS_Enabled=Yes","sectionname":"Realtime","ini filename":"c:\strsystm\data\store.ini",})
awards_keywords.update_ini_file_before_print_coupon({"value":"RTTRIG_Enabled=No","sectionname":"Realtime","ini filename":"c:\strsystm\data\store.ini",})
awards_keywords.start_vault({})
awards_keywords.setup_cds_vault({"data source name":"CDS0250.376.zip"})
awards_keywords.force_load({"cds enabled?":"Yes","big database?":"No","expected clu":"33"})
awards_keywords.shutdown_awards({})
awards_keywords.update_vault_config_file({"value":"CLOUD_POS=tcp://"+DEFAULT_VAULT_IP+":18620","upload file":"No","sectionname":"default","copy file":"Yes","ini filename":"c:\VAULT\VAULT.usa","validate_step":"Yes"})
awards_keywords.update_vault_config_file({"value":"CLOUS_ADS=tcp://"+DEFAULT_VAULT_IP+":18621","upload file":"No","sectionname":"default","copy file":"No","ini filename":"c:\VAULT\VAULT.usa","validate_step":"Yes"})
awards_keywords.update_vault_config_file({"value":"TEMPLATE_API="+DEFAULT_VAULT_URL+"templates","upload file":"No","sectionname":"default","copy file":"No","ini filename":"c:\VAULT\VAULT.usa","validate_step":"Yes"})
awards_keywords.update_vault_config_file({"value":"FONT_API="+DEFAULT_VAULT_URL+"fonts","upload file":"No","sectionname":"default","copy file":"No","ini filename":"c:\VAULT\VAULT.usa","validate_step":"Yes"})
awards_keywords.update_vault_config_file({"value":"HTTP_PROXY=tcp://"+DEFAULT_VAULT_IP+":18622","upload file":"Yes","sectionname":"default","copy file":"No","ini filename":"c:\VAULT\VAULT.usa","validate_step":"Yes"})

awards_keywords.start_vault({})
awards_keywords.print_coupon({"include file name":"vault_TC0008.txt","vault":"Yes","validate coupon":"No","print count":"1","validate_step":"Yes"})
