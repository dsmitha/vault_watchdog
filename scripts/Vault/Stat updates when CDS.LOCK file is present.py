from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.force_load({"cds enabled?":"Yes","big database?":"No","expected clu":"33"})
awards_keywaords.create_new_lock_files({"CDS Lock":"CDS.LOCK","TRANSFERLOCK":"TRANSFERLOCK.flg","CDS LOCK FILEPATH":r"c:\cds\mirror","TRANSFERLOCK FILEPATH":r"C:\cds\mirror\related_files")
awards_keywords.force_load({"cds enabled?":"Yes","big database?":"No","expected clu":"33"})
