from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
VM = sys.argv[1]
platformid = sys.argv[2]
testcaseid = sys.argv[3]
testplanid = sys.argv[4]

testlink.addtestcasetotestplan({"testprojectid":"3","testplanid":testplanid,"tcexternalid":"AW-759","platformid":platformid})

awards_keywords.start_vault({})
awards_keywords.kill_process({"process name":"RTTRIG"})
awards_keywords.print_coupon({"validate coupon":"No","vault":"Yes","print count":"1","include file name":"vault1.txt","validate_step":"Yes"})
