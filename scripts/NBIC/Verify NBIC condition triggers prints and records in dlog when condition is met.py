from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"store number":"0004","cds enabled?":"No","expected clu":"131","data source name":"SO0004ZP.091","printer type":"CMC6","need database?":"No","chain number":"091","need databse cleanup?":"No",})
awards_keywords.cmcset_update({"clu":"1393848","print flag":"Yes",})
awards_keywords.print_coupon({"include file name":"NBIC_CP.txt","coupon number":"1393848","print count":"1","validate_step":"Yes"})
