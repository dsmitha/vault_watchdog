from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"expected clu":"131","chain number":"376","store file":"STR20X.INI","data source name":"SO0004ZP.091","cds enabled?":"No","store number":"0004","need databse cleanup?":"Yes","need database?":"No",})
awards_keywords.cmcset_update({"print flag":"Yes","clu":"OFF",})
awards_keywords.print_coupon({"include file name":"NBIC_NC.txt","print count":"0","validate_step":"Yes"})
