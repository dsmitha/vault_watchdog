from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.a_a_a_initialize_store_system({"data source name":"SO0004ZP.091","chain number":"091","cds enabled?":"No","need databse cleanup?":"No","need database?":"No","expected clu":"131","store number":"0004","validate_step":"Yes"})
