from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"expected clu":"773","cds enabled?":"Yes","printer type":"CMC6","data source name":"CDS0250ZP.376.zip","store number":"0250","chain number":"376","need database?":"No",})
awards_keywords.cmcset_update({"print flag":"Yes","clu":"1309235",})
awards_keywords.print_coupon({"include file name":"cds_levent.txt","print count":"1","validate coupon":"No","coupon number":"1309235","validate_step":"Yes"})
