from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
testlink.addtestcasetotestplan({"testprojectid":"3","tcexternalid":"SS-18","testplanid":sys.argv[4],})
##awards_keywords.a_a_a_initialize_store_system({"chain number":"376","store number":"0250","cds enabled?":"Yes","data source name":"CDS0250ZP.376.zip","need database?":"No","printer type":"CMC6","expected clu":"7",})
awards_keywords.cmcset_update({"clu":"1301629","print flag":"Yes",})
awards_keywords.print_coupon({"coupon number":"1301629","print count":"1","include file name":"cds_chkdir.txt","validate coupon":"No","validate_step":"Yes"})
