from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"data source name":"CDS0250ZP.376.zip","cds enabled?":"Yes","printer type":"CMC6","need database?":"No","chain number":"376","expected clu":"773","store number":"0250",})
awards_keywords.cmcset_update({"print flag":"Yes","clu":"1329376",})
awards_keywords.print_coupon({"coupon number":"1329376","print count":"1","validate coupon":"No","include file name":"cds_basic.txt","validate_step":"Yes"})
