from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"data source name":"CDS0250ZP.376.zip","chain number":"376","store number":"0250","printer type":"CMC6","expected clu":"773","cds enabled?":"Yes","need database?":"No",})
awards_keywords.cmcset_update({"clu":"1300832","print flag":"Yes",})
awards_keywords.print_coupon({"include file name":"cds_levntcon.txt","print count":"1","validate coupon":"No","coupon number":"1300832","validate_step":"Yes"})
