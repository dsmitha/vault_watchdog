from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
	sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"expected clu":"34","printer type":"CMC6","cds enabled?":"Yes","chain number":"376","data source name":"CDS0250.376.zip","store number":"0250","need database?":"No",})
awards_keywords.cmcset_update({"clu":"1303278","print flag":"Yes",})
awards_keywords.print_coupon({"include file name":"cds_AFT.txt","print count":"2","coupon number":"1303278","validate coupon":"No","validate_step":"Yes"})
