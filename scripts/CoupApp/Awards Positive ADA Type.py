from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"chain number":"376","need database?":"No","printer type":"CMC6","expected clu":"773","cds enabled?":"Yes","data source name":"CDS0250ZP.376.zip","store number":"0250",})
awards_keywords.cmcset_update({"clu":"1306985","print flag":"Yes",})
awards_keywords.print_coupon({"coupon number":"1306985","print count":"1","validate coupon":"No","include file name":"CDS_act.txt","validate_step":"Yes"})
