from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core
from framework.reporting import *
import awards_keywords

awards_keywords.cmcset_update({"print flag":"Yes","clu":"1349257, 1349258",})
awards_keywords.print_coupon({"include file name":"CDS_alt.txt","print count":"2","coupon number":"1349258","validate coupon":"No","validate_step":"Yes"})
awards_keywords.print_coupon({"include file name":"CDS_alt1.txt","print count":"3","coupon number":"1349257","validate_step":"Yes"})
