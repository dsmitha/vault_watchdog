from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"store number":"0250","data source name":"CDS0250ZP.376.zip","cds enabled?":"Yes","printer type":"CMC6","chain number":"376","need database?":"No","expected clu":"773",})
awards_keywords.cmcset_update({"clu":"1309239","print flag":"Yes",})
awards_keywords.print_coupon({"print count":"1","include file name":"cds_ordrsiz.txt","validate coupon":"No","coupon number":"1309239","validate_step":"Yes"})
