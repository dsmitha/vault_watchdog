from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"need database?":"No","data source name":"CDS0250ZP.376.zip","expected clu":"773","store number":"0250","chain number":"376","printer type":"CMC6","cds enabled?":"Yes",})
awards_keywords.cmcset_update({"clu":"1348911","print flag":"Yes",})
awards_keywords.print_coupon({"include file name":"CDS_pgh.txt","validate coupon":"No","print count":"1","coupon number":"1348911","validate_step":"Yes"})
