from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"expected clu":"37","printer type":"CMC6","cds enabled?":"Yes","need database?":"No","chain number":"376","store number":"0250","data source name":"CDS0250.376 - Copy1.zip",})
awards_keywords.delete_file({"file name":"C:\strsystm\log\cds_*.*",})
awards_keywords.cmcset_update({"print flag":"Yes","clu":"1309218,1309216,1309217",})
awards_keywords.print_coupon({"print count":"1","coupon number":"1309218","include file name":"cds_fran1.txt","validate coupon":"No",})
awards_keywords.print_coupon({"print count":"2","coupon number":"1309216","include file name":"cds_fran2.txt","validate coupon":"No",})
awards_keywords.print_coupon({"print count":"5","coupon number":"1309218","include file name":"cds_fran.txt","validate coupon":"No",})
awards_keywords.validate_coupon_printed({"log file name":"CDS_FRAN.001","coupon number":"1309216","validate_step":"Yes"})
awards_keywords.validate_coupon_printed({"log file name":"CDS_FRAN.001","coupon number":"1309217","validate_step":"Yes"})
awards_keywords.validate_coupon_printed({"log file name":"CDS_FRAN.001","coupon number":"1309218","validate_step":"Yes"})
