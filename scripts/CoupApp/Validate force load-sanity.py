from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
#testlink.addtestcasetotestplan({"testprojectid":"3","tcexternalid":"SS-18","testplanid":sys.argv[4],})
##awards_keywords.a_a_a_initialize_store_system({"chain number":"376","expected clu":"773","printer type":"CMC6","need database?":"No","store number":"0250","cds enabled?":"Yes","data source name":"CDS0250ZP.376.zip",})
awards_keywords.go_to_multitasking_store_system({})
awards_keywords.force_load({"big database?":"No","cds enabled?":"Yes","expected clu":"33","validate_step":"Yes"})