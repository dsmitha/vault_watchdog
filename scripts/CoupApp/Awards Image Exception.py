from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"need database?":"No","store number":"0250","chain number":"376","cds enabled?":"Yes","printer type":"CMC6","expected clu":"773","data source name":"CDS0250ZP.376.zip",})
awards_keywords.cmcset_update({"clu":"1303291","print flag":"Yes",})
awards_keywords.print_coupon({"include file name":"cds_imagex.txt","coupon number":"1303291","validate coupon":"No","print count":"1","validate_step":"Yes"})
