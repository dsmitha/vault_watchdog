from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"printer type":"CMC6","need database?":"No","data source name":"CDS0250ZP.376.zip","chain number":"376","store number":"0250","cds enabled?":"Yes","expected clu":"773",})
awards_keywords.cmcset_update({"print flag":"Yes","clu":"1309233",})
awards_keywords.print_coupon({"include file name":"cds_dat.txt","validate coupon":"No","print count":"1","coupon number":"1309233","validate_step":"Yes"})
