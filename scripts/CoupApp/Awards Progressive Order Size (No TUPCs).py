from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"expected clu":"34","printer type":"CMC6","cds enabled?":"Yes","chain number":"376","data source name":"CDS0250.376.zip","store number":"0250","need database?":"No",})
#awards_keywords.a_a_a_initialize_store_system({"expected clu":"773","need database?":"No","data source name":"CDS0250ZP.376.zip","cds enabled?":"Yes","store number":"0250","printer type":"CMC6","chain number":"376",})
awards_keywords.cmcset_update({"print flag":"Yes","clu":"1301765,1301766",})
awards_keywords.print_coupon({"validate coupon":"No","print count":"1","coupon number":"1301765","include file name":"cds_pos2.txt","validate_step":"Yes"})
awards_keywords.print_coupon({"validate coupon":"No","print count":"2","coupon number":"1301766","include file name":"cds_pos3.txt","validate_step":"Yes"})
