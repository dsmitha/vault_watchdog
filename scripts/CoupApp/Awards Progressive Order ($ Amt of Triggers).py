from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"printer type":"CMC6","data source name":"CDS0250ZP.376.zip","chain number":"376","expected clu":"773","store number":"0250","cds enabled?":"Yes","need database?":"No",})
awards_keywords.cmcset_update({"print flag":"Yes","clu":"1301759,1301760",})
awards_keywords.print_coupon({"validate coupon":"No","print count":"1","include file name":"cds_posaot1.txt","coupon number":"1301759","validate_step":"Yes"})
awards_keywords.print_coupon({"validate coupon":"No","print count":"2","include file name":"cds_posaot2.txt","coupon number":"1301760","validate_step":"Yes"})
