from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"chain number":"376","store number":"0250","data source name":"CDS0250ZP.376.zip","printer type":"CMC6","expected clu":"773","need database?":"No","cds enabled?":"Yes",})
awards_keywords.leave_dos_session({})
awards_keywords.go_to_multitasking_store_system({})
awards_keywords.force_load({"expected clu":"33","cds enabled?":"Yes","big database?":"No",})
awards_keywords.validate_images_folder({"expected amount of files":"30","validate_step":"Yes"})
awards_keywords.open_a_new_dos_session({})
awards_keywords.validate_xmldb_folder({"expected amount of files":"443","validate_step":"Yes"})
