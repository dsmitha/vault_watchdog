from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"chain number":"376","expected clu":"773","store number":"0250","printer type":"CMC6","cds enabled?":"Yes","data source name":"CDS0250ZP.376.zip","need database?":"No",})
awards_keywords.cmcset_update({"print flag":"Yes","clu":"1309230",})
awards_keywords.print_coupon({"print count":"1","coupon number":"1309230","include file name":"cds_didntbuy.txt","validate coupon":"No","validate_step":"Yes"})
