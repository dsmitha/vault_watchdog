from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"need database?":"No","expected clu":"773","chain number":"376","cds enabled?":"Yes","data source name":"CDS0250ZP.376.zip","printer type":"CMC6","store number":"0250",})
awards_keywords.cmcset_update({"clu":"1094222,1094199","print flag":"Yes",})
awards_keywords.print_coupon({"print count":"2","include file name":"CDS_deac.txt","coupon number":"1094222","validate coupon":"No",})
awards_keywords.print_coupon({"print count":"3","include file name":"CDS_deac2.txt","coupon number":"1094199","validate coupon":"No",})
awards_keywords.validate_coupon_not_printed({"log file name":"CDS_d~.001","coupon number":"1094222","validate_step":"Yes"})
