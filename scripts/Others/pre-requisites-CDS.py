from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core
import pdb


from framework.reporting import *
import awards_keywords

awards_keywords.a_a_a_initialize_store_system({"expected clu":"7","chain number":"376","need database?":"Yes","data source name":"CDS0250ZP.376.zip","store number":"0250","printer type":"CMC6","cds enabled?":"Yes",})
