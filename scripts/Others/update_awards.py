from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

VM = sys.argv[1]
platform = sys.argv[2]
testcaseid = sys.argv[3]
tesplanid = sys.argv[4]
build_date=None
build_folder = None
if len(sys.argv)>5:
    if sys.argv[5]=="":
        build_date=None
    else:
        build_date=sys.argv[5]
if len(sys.argv)>6:
    if sys.argv[6]=="":
        build_folder=None
    else:
        build_folder=sys.argv[6]
print (build_date)
#pdb.set_trace()
print (build_folder)
#pdb.set_trace()
#testlink.addtestcasetotestplan({"testprojectid":"3","tcexternalid":"AW-18","testplanid":"4",})
awards_keywords.update_awards({"version":"daily","build date":build_date, "build folder":build_folder})
