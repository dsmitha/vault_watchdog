from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
VM = sys.argv[1]
platform = sys.argv[2]
testcaseid = sys.argv[3]
tesplanid = sys.argv[4]
testlink.createbuild({"build notes":"Automated build","testplanid": tesplanid})
