from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.a_a_a_initialize_store_system({"cds enabled?":"Yes","need database?":"YES","printer type":"CMC5","data source name":"BWdb.zip","need databse cleanup?":"&nbsp; No","expected clu":"504",})
awards_keywords.validate_mclu({"mclu":"1281864","validate_step":"Yes"})
