from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"expected clu":"504","printer type":"CMC6","need database?":"YES","cds enabled?":"Yes","need databse cleanup?":"No","data source name":"BWdb.zip",})
awards_keywords.validate_file_contains({"case sensitive?":"False","file name":"1281865.xml","text to validate":"&lt;ImageTag&gt;","regex?":"False",})
awards_keywords.validate_mclu({"mclu":"1281865","validate_step":"Yes"})
