from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"expected clu":"504","need databse cleanup?":"&nbsp; Yes","cds enabled?":"Yes","data source name":"BWdb.zip","printer type":"CMC6","need database?":"YES",})
awards_keywords.validate_file_contains({"filename":"1281866.xml","regex?":"False","case sensitive?":"False","text to validate":"&lt;ImageTag&gt;",})
awards_keywords.validate_mclu({"mclu":"1281866","validate_step":"Yes"})
