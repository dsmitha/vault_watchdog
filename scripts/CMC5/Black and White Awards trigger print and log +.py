from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.a_a_a_initialize_store_system({"data source name":"BWdb.zip","cds enabled?":"Yes","expected clu":"&nbsp; 504","printer type":"CMC5","need databse cleanup?":"&nbsp; No","need database?":"YES",})
awards_keywords.print_coupon({"print count":"","coupon number":"","include file name":"","validate_step":"Yes"})
#awards_keywords.extract_and_report_system_logs({"cds enabled?":"Yes",})
