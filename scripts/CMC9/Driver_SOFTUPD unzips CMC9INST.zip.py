from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core
from framework.reporting import *
import awards_keywords
platformid=sys.argv[2]

# Script install CMC9 printer drivr using softup

awards_keywords.copy_zip_to_comm({"file name":"CMC9inst.zip","file path":r"I:\sqa\shared\automation\CMC9"})
awards_keywords.validate_file_not_exists({"file name":r"C:\strsytm\comm\cmc9inst.zip"})
awards_keywords.file_exists({"file name":r"c:\strsystm\comm\CMC9DRV.FLG")}
awards_keywords.file_exists({"file name":r"c:\strsystm\comm\CMC9FRM.FLG")}
awards_keywords.file_exists({"filename":r"c:\Funai\cmc9drv"})
awards_keywords.file_exists({"filename":r"c:\Funai\cmc9frm"})
awards_keywords.file_exists({"filename":r"c:\strsystm\install\cmc9inst.zip"})
awards_keywords.file_exists({"filename":r"c:\strsystm\backup\cmc9drv.zip"})
awards_keywords.file_exists({"filename":r"c:\strsystm\backup\cmc9frm.zip"})
