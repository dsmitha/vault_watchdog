from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
# This is script is to print coupon with QR code and validate the coupons are printed properly
'''awards_keywords.shutdown_awards({})
awards_keywords.update_ini_file_before_print_coupon({"value":"Rotate=True","sectionname":"UPCA","ini filename":"c:\strsystm\data\store.ini",})
awards_keywords.start_awards({})'''

#update CMC to make all coupons non printable
awards_keywords.cmcset_update({"clu":"1315116","print flag":"Yes","validation flag":"Yes",})

awards_keywords.WEBPOS_start_capture_log({})
# Print coupon
awards_keywords.print_coupon({"validate barcode":"yes","validate coupon":"No","include file name":"EAN_CMC9.txt","print count":"1","coupon number":"1315116",})
#validate the barcodes are printed properly with the color
awards_keywords.validate_bar_code({"barcode color":"blue","barcode type":"QRCode","include barcode file name":"QRCode.png","validate_step":"Yes"})
awards_keywords.WEBPOS_Stop_capture_log({})
