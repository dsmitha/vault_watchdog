from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core
from framework.reporting import *
import awards_keywords

# Script to Validate the info displayed in LAne info page from displayed
awards_keywords.capture_lane_mapping_scren({"lane info":["Mac Address","Lane","Tid","IpAddress","PrinterName","Location"]}),
