from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core
from framework.reporting import *
import awards_keywords
#Scrit to Delete Lane from Display
awards_keywords.del_lane_display({})
mac_address=awards_keywords.get_mac_address()
awards_keywords.validate_section_in_ini_file({"ini file path":"C:\strsystm\data\store.ini","section name":"Printers","1":"*","validate_step":"Yes"})
awards_keywords.validate_section_in_ini_file({"ini file path":"C:\strsystm\data\store.ini","section name":"LaneMap",mac_address:"","validate_step":"Yes"})
