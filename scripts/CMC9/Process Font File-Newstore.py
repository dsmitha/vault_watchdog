from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core
from framework.reporting import *
import awards_keywords

#Process the Fontfile.upd using newstore
awards_keywords.newstore_setup({"country":"united states","locale":"us","printer_type":"CMC9","printer":"CMC9"})
awards_keywords.file_exists({"filename":r"c:\strsystm\DB\FONTFILE.DAT"})
awards_keywords.file_exists({"filename":r"c:\strsystm\DB\FONTFILE.IMG"})
