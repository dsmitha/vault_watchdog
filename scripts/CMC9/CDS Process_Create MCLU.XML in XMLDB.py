from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
VM = sys.argv[1]
platformid = sys.argv[2]
testcaseid = sys.argv[3]
testplanid = sys.argv[4]
# Script to load cds file and verify 1230001.xml file is processed
testlink.addtestcasetotestplan({"testprojectid":"3","testplanid":testplanid,"tcexternalid":"AW-26","platformid":platformid})
#awards_keywords.a_a_a_initialize_store_system({"big database?":"No","cds enabled?":"Yes","need databse cleanup?":"No","chain number":"376","printer type":"CMC6","expected clu":"7","data source name":"CDS0250ZP.376.zip","store number":"0250","need database?":"No",})
awards_keywords.shutdown_awards({})
awards_keywords.update_ini_file_before_print_coupon({"value":"Enabled:Yes","sectionname":"CDS","ini filename":"c:\strsystm\data\store.ini",})
awards_keywords.start_awards({})
awards_keywords.force_load({"cds enabled?":"Yes","big database?":"No","expected clu":"17","validate_step":"Yes"})
awards_keywords.validate_file_contains(r"C:\stsrtsystm\XMLDB\12300001.xml","PrinterType=CMC9",False,True)
