from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core
from framework.reporting import *
import awards_keywords
platformid=sys.argv[2]
#Script to get the driver version of the installed printer from the display screen
awards_keywords.capture_driver_version()
awards_keywords.validate_section_in_ini_file({"ini file path":"C:\strsystm\data\store.ini","section name":"Softwareversion","printer":"3.12.1202","validate_step":"Yes"})
#awards_keywords.validate_section_in_ini_file({"ini file path":"C:\strsystm\data\store.ini","section name":"LaneMap",mac_address:'2, 4, 1, CMC6, 00-00-48-31-80-01, 192.168.192.1, "", "",""',"validate_step":"Yes"})
