from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core
from framework.reporting import *
import awards_keywords
# Copy CMC9.zip file from central location and run newstore to install driver
awards_keywords.copy_zip_to_comm({"file name":"CMC9inst.zip","file path":r"I:\sqa\shared\automation\CMC9"})
awards_keywords.newstore_setup({"country":"united states","locale":"us","printer_type":"CMC6","printer":"CMC6"})
awards_keywords.file_exists({"file name":r"c:\strsystm\comm\CMC9DRV.FLG")}
awards_keywords.file_exists({"file name":r"c:\strsystm\comm\CMC9FRM.FLG")}
awards_keywords.file_exists({"filename":r"c:\Funai\cmc9drv"})
awards_keywords.file_exists({"filename":r"c:\Funai\cmc9frm"})
awards_keywords.file_exists({"filename":r"c:\strsystm\install\cmc9inst.zip"})
