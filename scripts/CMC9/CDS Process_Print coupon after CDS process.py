from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
VM = sys.argv[1]
platformid = sys.argv[2]
testcaseid = sys.argv[3]
testplanid = sys.argv[4]
# Script to load CDS DB and verify print coupon s printing coupons properly
#awards_keywords.a_a_a_initialize_store_system({"big database?":"No","cds enabled?":"Yes","need databse cleanup?":"No","chain number":"376","printer type":"CMC6","expected clu":"7","data source name":"CDS0250ZP.376.zip","store number":"0250","need database?":"No",})
awards_keywords.shutdown_awards({})
awards_keywords.update_ini_file_before_print_coupon({"value":"Enabled:Yes","sectionname":"CDS","ini filename":"c:\strsystm\data\store.ini",})
awards_keywords.start_awards({})
#Load DB
awards_keywords.force_load({"cds enabled?":"Yes","big database?":"No","expected clu":"17","validate_step":"Yes"})
#Verify MCLU is loaded
awards_keywords.validate_mclu({"mclu":"1303278"})
awards_keywords.cmcset_update({"clu":"1303278","print flag":"Yes",})
#Print coupon and verify coupons printed propely from awards console and also from log
awards_keywords.print_coupon({"include file name":"cds_AFT.txt","print count":"2","coupon number":"1303278","validate coupon":"No","validate_step":"Yes"})
