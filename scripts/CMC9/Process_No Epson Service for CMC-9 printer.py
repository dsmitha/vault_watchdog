from pathlib import Path
import os,sys,time

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
VM = sys.argv[1]
platform = sys.argv[2]
testcaseid = sys.argv[3]
testplanid = sys.argv[4]
print (sys.argv)
print ("in teat case %s"%sys.argv[2])

# Script to verify CMCEpStatSrv,CMCFwUpdSrv,CMCHotSwapSrv are not running
awards_keywords.validate_process_is_not_running({"process name":"CMCEpStatSrv.exe"})
awards_keywords.validate_process_is_not_running({"process name":"CMCFwUpdSrv.exe"})
awards_keywords.validate_process_is_not_running({"process name":"CMCHotSwapSrv.exe"})
