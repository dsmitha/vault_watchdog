from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords


awards_keywords.cmcset_update({"clu":"1315112","print flag":"Yes",})
awards_keywords.WEBPOS_start_capture_log({})
awards_keywords.print_coupon({"validate coupon":"No","validate barcode":"Yes","include file name":"EAN_CMC9.txt","coupon number":"1315112","print count":"1",})

awards_keywords.validate_bar_code({"include barcode file name":"EAN_horz.png","barcode color":"blue","barcode type":"EAN","validate_step":"Yes"})
awards_keywords.WEBPOS_Stop_capture_log({})
