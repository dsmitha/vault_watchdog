from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
#Script to print UPCA coupon and verify barcode is prited vertically
awards_keywords.shutdown_awards({})
awards_keywords.update_ini_file_before_print_coupon({"value":"Rotate=True","sectionname":"UPCA","ini filename":"c:\strsystm\data\store.ini",})
awards_keywords.start_awards({})
#awards_keywords.copy_ini_from_comm({"filename":"Store_vert.ini"})

awards_keywords.cmcset_update({"clu":"1315109","print flag":"Yes",})
awards_keywords.WEBPOS_start_capture_log({})
awards_keywords.print_coupon({"coupon number":"1315109","print count":"1","validate barcode":"yes","include file name":"EAN_CMC9.txt",})
awards_keywords.WEBPOS_Stop_capture_log({})
pdb.set_trace()
awards_keywords.validate_bar_code({"barcode color":"blue","include barcode file name":"UPCA_vert.png","barcode type":"UPCA","validate_step":"Yes"})
