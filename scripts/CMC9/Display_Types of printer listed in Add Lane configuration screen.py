from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core
from framework.reporting import *
import awards_keywords
#Script to verify all lane types are printed while adding printer from display
awards_keywords.get_pritnter_types_from_dispaly({"printers":["CMC6","CMC7","CMC5","CMC4","Sesenta1","StdPrinter","CMC5E02C","CMC8","CMC9"],"lanenum":"012","termid":"0012"})
