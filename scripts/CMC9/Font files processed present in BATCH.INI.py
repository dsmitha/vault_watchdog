from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core
from framework.reporting import *
import awards_keywords
platformid=sys.argv[2]

awards_keywords.validate_section_in_ini_file({"ini file path":"C:\strsystm\data\batch.ini","section name":"Processfontfile","InputFiles":'"FONTUPD.ZIP", "FONTIMG.*", "FONTDAT.*", "CMC6TTFDAT.*", "CMC6TTFIMG.*", "CMC7TTFDAT.*","CMC7TTFIMG.*", "CMC8TTFDAT.*","CMC8TTFIMG.*", "CMC9TTFIMG.*", "CMC9TTFDAT.*"',"validate_step":"Yes"})
