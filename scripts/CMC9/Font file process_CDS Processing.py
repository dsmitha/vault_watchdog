from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core
from framework.reporting import *
import awards_keywords
platformid=sys.argv[2]
# copy the font file and run force load job to process fontupd file and check files are processed and verify font upd is deleted from comm folder
#new files Fontfile.DAT and FontFile.IMG files re created under c:\strsystm\DB folder

awards_keywords.validate_section_in_ini_file({"ini file path":"C:\strsystm\data\batch.ini","section name":"Processfontfile","InputFiles":'"FONTUPD.ZIP", "FONTIMG.*", "FONTDAT.*", "CMC6TTFDAT.*", "CMC6TTFIMG.*", "CMC7TTFDAT.*","CMC7TTFIMG.*", "CMC8TTFDAT.*","CMC8TTFIMG.*", "CMC9TTFIMG.*", "CMC9TTFDAT.*"',"validate_step":"Yes"})
awards_keywords.copy_zip_to_comm({"file name":"Fontupd.zip","file path":r"I:\SQA\shared\Automation\CMC9"})
awards_keywords.force_load({"big database?":"No"})
awards_keywords.validate_file_not_exists({"file name":r"C:\strsytm\comm\fontupd.zip"})
awards_keywords.validate_file_not_exists({"file name":r"c:\strsystm\comm\cmc9ttfdat.USA"})
awards_keywords.validate_file_not_exists({"file name":r"c:\strsystm\comm\cmc9ttfimg.USA"})
awards_keywords.file_exists({"filename":r"c:\strsystm\DB\FONTFILE.DAT"})
awards_keywords.file_exists({"filename":r"c:\strsystm\DB\FONTFILE.IMG"})
