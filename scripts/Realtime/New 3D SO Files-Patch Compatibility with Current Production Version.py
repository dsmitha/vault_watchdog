from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
VM = sys.argv[1]
platformid = sys.argv[2]
testcaseid = sys.argv[3]
testplanid = sys.argv[4]

testlink.addtestcasetotestplan({"testprojectid":"3","testplanid":testplanid,"tcexternalid":"AW-340","platformid":platformid})

#awards_keywords.a_a_a_initialize_store_system({"expected clu":"773","cds enabled?":"No","data source name":"SO0198ZP.198","chain number":"198","printer type":"CMC4","store number":"0198","big database?":"No","need database?":"YES",})
awards_keywords.set_store_and_chain_number({"store number":"0007","chain number":"099",})
#awards_keywords.clean_logs({})
awards_keywords.copy_ic_file({"destination file name":"SO0007ZP.099","original file name":"SO0007ZP.099",})
awards_keywords.run_batch_database({})
awards_keywords.tail_file({"regex? (true/false)":"False","file name":"C:\strsystm\REPORTS\DATABASE.RPT","expected value":"FINISHIT: PROCESSING FINISHED","delay before":"1020",})
awards_keywords.validate_amount_of_clus({"expected clus":"228","validate_step":"Yes"})
awards_keywords.validate_shuttle_ini({"last shipment processed":"3983","validate_step":"Yes"})
