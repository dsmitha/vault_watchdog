from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
VM = sys.argv[1]
platformid = sys.argv[2]
testcaseid = sys.argv[3]
testplanid = sys.argv[4]

testlink.addtestcasetotestplan({"testprojectid":"3","testplanid":testplanid,"tcexternalid":"AW-339","platformid":platformid})

awards_keywords.set_store_and_chain_number({"chain number":"099","store number":"0007",})
#awards_keywords.clean_logs({})
awards_keywords.copy_ic_file({"destination file name":"SO0007ZP.099","original file name":"FSO0007ZP.099",})
awards_keywords.run_batch_database({})
awards_keywords.tail_file({"expected value":"FINISHIT: PROCESSING FINISHED","delay before":"1020","file name":"C:\strsystm\REPORTS\DATABASE.RPT","regex? (true/false)":"False",})
awards_keywords.validate_amount_of_clus({"expected clus":"228","validate_step":"Yes"})
awards_keywords.validate_shuttle_ini({"last shipment processed":"3982","validate_step":"Yes"})
