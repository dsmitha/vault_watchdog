from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.go_to_multitasking_store_system({})
awards_keywords.shutdown_awards({})
awards_keywords.new_store({"printer":"CMC6","country":"Japan","locale":"JP",})
awards_keywords.a_a_a_initialize_store_system({"printer type":"CMC6","expected clu":"773","store number":"0250","big database?":"No","chain number":"376","cds enabled?":"Yes","data source name":"CDS0250ZP.376.zip","need database?":"No",})
awards_keywords.validate_realtime_section_in_ini_file({"ini file path":"C:\strsystm\data\store.ini","rtpos_enabled":"No","zeromq_tran_address":"tcp://127.0.0.1:8000","validate_step":"Yes"})
awards_keywords.extract_and_report_system_logs({"cds enabled?":"Yes","validate_step":"Yes"})
