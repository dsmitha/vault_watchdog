from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

VM = sys.argv[1]
platformid = sys.argv[2]
testcaseid = sys.argv[3]
testplanid = sys.argv[4]

testlink.addtestcasetotestplan({"testprojectid":"3","testplanid":testplanid,"tcexternalid":"AW-28","platformid":platformid})
awards_keywords.validate_realtime_section_in_ini_file({"zeromq_tran_address":"tcp://127.0.0.1:8001","rtpos_enabled":"No","ini file path":"C:\strsystm\data\store.ini","validate_step":"Yes"})
