from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

#awards_keywords.a_a_a_initialize_store_system({"printer type":"CMC6","need database?":"No","big database?":"No","chain number":"376","expected clu":"499","cds enabled?":"Yes","data source name":"CDS0250ZP.376.zip","store number":"0250",})
awards_keywords.go_to_multitasking_store_system({})
awards_keywords.validate_process_is_running_in_awards({"process name":"COUPAPP","validate_step":"Yes"})
awards_keywords.validate_process_is_running({"process name":"Coupapp.Exe","validate_step":"Yes"})
awards_keywords.go_to_multitasking_store_system({})
awards_keywords.shutdown_awards({})
awards_keywords.validate_process_is_not_running({"process name":"Coupapp.Exe","validate_step":"Yes"})
awards_keywords.validate_process_is_not_running({"process name":"RTTRPROC.Exe","validate_step":"Yes"})
awards_keywords.start_awards({})
