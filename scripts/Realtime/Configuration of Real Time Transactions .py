from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

VM = sys.argv[1]
platformid = sys.argv[2]
testcaseid = sys.argv[3]
testplanid = sys.argv[4]

#testlink.addtestcasetotestplan({"testprojectid":"3","testplanid":testplanid,"tcexternalid":"AW-28","platformid":platformid})
awards_keywords.upgrade_firstupd({"version":"dailytask"})
awards_keywords.validate_realtime_section_in_ini_file({"zeromq_tran_address":"tcp://127.0.0.1:8001","rtpos_enabled":"No","ini file path":"C:\strsystm\data\store.ini","validate_step":"Yes"})
awards_keywords.shutdown_awards({})
awards_keywords.validate_process_is_not_running({"process name":"Coupapp.Exe","validate_step":"Yes"})
awards_keywords.validate_process_is_not_running({"process name":"RTTRPROC.Exe","validate_step":"Yes"})
awards_keywords.start_awards({})
awards_keywords.validate_mclu({"mclu":"1326084"})
awards_keywords.cmcset_update({"clu":"1326084","print flag":"Yes",})
awards_keywords.print_coupon({"coupon number":"1326084","print count":"1","include file name":"cds_id.txt","validate coupon":"No","validate_step":"Yes"})
awards_keywords.WEBPOS_start_capture_log({})
awards_keywords.cmcset_update({"clu":"1326084","print flag":"Yes",})
awards_keywords.print_coupon({"coupon number":"1326084","print count":"1","include file name":"cds_id.txt","validate coupon":"No","validate_step":"Yes"})
