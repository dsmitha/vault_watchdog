from pathlib import Path
import os,sys
if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
VM = sys.argv[1]
platformid = sys.argv[2]
testcaseid = sys.argv[3]
testplanid = sys.argv[4]

testlink.addtestcasetotestplan({"testprojectid":"3","testplanid":testplanid,"tcexternalid":"AW-221","platformid":platformid})

awards_keywords.cmcset_update({"oclu":"73","print flag":"Yes",})
awards_keywords.validate_logrec({"print count":"1","clu":"73","coupon number":"1310570","include file name":"mfd2.txt","validate_step":"Yes"})
awards_keywords.start_awards({})
