from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
#pdb.set_trace()
testlink.addtestcasetotestplan({"testprojectid":"3","testplanid":testplanid,"tcexternalid":"AW-851","platformid":platformid})
awards_keywords.validate_section_in_ini_file({"ini file path":"C:\strsystm\data\store.ini","section name":"Processes","webposstatemanager":"","validate_step":"Yes"})
awards_keywords.validate_section_in_ini_file({"ini file path":"C:\strsystm\data\system.ini","section name":"WEBPOS","StateManage":"","validate_step":"Yes"})
awards_keywords.validate_processs_running({"process name":"WEBPOSSTATEMANAGER.EXE"})
awards_keywords.validate_file_exists({"file name":r"C:\strsystm\dll\WebPosstatemgrFactory.dll"})
awards_keywords.validate_file_exists({"file name":r"C:\strsystm\dll\WebPosstatemgr_Generic.dll"})
awards_keywords.validate_file_exists({"file name":r"C:\strsystm\bin\WebPosStateManager.exe"})
