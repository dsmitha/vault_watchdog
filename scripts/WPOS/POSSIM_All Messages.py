from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
#pdb.set_trace()
testlink.addtestcasetotestplan({"testprojectid":"3","testplanid":testplanid,"tcexternalid":"AW-1102","platformid":platformid})
awards_keywords.WEBPOS_start_capture_log({})
#pdb.set_trace()
awards_keywords.start_POS_APP({})
awards_keywords.WEBPOS_Stop_capture_log({})
awards_keywords.validate_messages_in_log({})
awards_keywords.WEBPOS_start_capt_debug_log({})
awards_keywords.WEBPOS_Stop_capture_log({})
#awards_keywords.validate_messages_in_debuglog({})
#awards_keywords.compare_log_files({})
awards_keywords.validate_WEBPOS_Log({})
