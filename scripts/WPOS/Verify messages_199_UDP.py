from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

awards_keywords.add_firewall_exception({"protocol":"UDP 199"})
awards_keywords.WEBPOS_start_capture_log({})
#pdb.set_trace()
awards_keywords.start_POS_APP({})


awards_keywords.WEBPOS_Stop_capture_log({})

awards_keywords.validate_messages_in_log({})

awards_keywords.validate_WEBPOS_Log({})
