from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core
from framework.awards.settings import machine_settings as ms

from framework.reporting import *
import awards_keywords
)
ip_address=""
awards_keywords.update_ini_file_before_print_coupon({"value":"reporting=Yes","sectionname":"DepartmentCodes","ini filename":"c:\strsystm\data\store.ini",})
awards_keywords.update_ini_file_before_print_coupon({"value":"AllowDeptPurchaseFakeUpc=Yes","sectionname":"Generic","ini filename":"c:\strsystm\data\webposstatemanager.ini",})
awards_keywords.update_ini_file_before_print_coupon({"value":"AllowRegularDeptPurchaseFakeUpc=Yes","sectionname":"Generic","ini filename":"c:\strsystm\data\webposstatemanager.ini",})
awards_keywords.WEBPOS_start_capture_log({})
awards_keywords.start_POS_APP({})
awards_keywords.WEBPOS_Stop_capture_log({})
awards_keywords.shutdown_awards()
awards_keywords.validate_webpos_log({"messages":["Deparmentcode"]})
awards_keywords.start_awards()
