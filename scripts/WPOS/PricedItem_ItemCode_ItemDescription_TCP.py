from pathlib import Path
import os,sys,time

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords

if len(sys.argv)>6:
    if sys.argv[6]=="":
        build_date=None
    else:
        build_folder=sys.argv[6]

awards_keywords.WEBPOS_start_capture_log({})
awards_keywords.start_POS_APP({})
awards_keywords.WEBPOS_message_caputre_from_screen({"messages":["Priced item"]})

awards_keywords.WEBPOS_Stop_capture_log({})
awards_keywords.validate_messages_in_log({"messages":["Priced item"]})
awards_keywords.validate_WEBPOS_Log({})

awards_keywords.validate_messages_in_log({})

awards_keywords.validate_WEBPOS_Log({})
