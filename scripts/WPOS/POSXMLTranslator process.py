from pathlib import Path
import os,sys

if r"I:\SQA\shared\Automation\Projects\Awards" in sys.path:
    sys.path.remove(r"I:\SQA\shared\Automation\Projects\Awards")
sys.path.append (str(Path(__file__).parent.parent.parent))

import framework.testlink as testlink
import framework.awards.core


from framework.reporting import *
import awards_keywords
#pdb.set_trace()
testlink.addtestcasetotestplan({"testprojectid":"3","testplanid":testplanid,"tcexternalid":"AW-429","platformid":platformid})
awards_keywords.validate_section_in_ini_file({"ini file path":"C:\strsystm\data\store.ini","section name":"Processes","POSXML":"","validate_step":"Yes"})
awards_keywords.validate_section_in_ini_file({"ini file path":"C:\strsystm\data\system.ini","section name":"Processes","POSXML":"","validate_step":"Yes"})
