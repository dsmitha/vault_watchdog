# File: Awards Main
#
# Filename:
# > awards_main.py
#
# Description:
# This file will execute the test case that is invoked from ALM. This test case
# in ALM needs to carry a VAPI XP script as follows:
# > ' Author: Danilo Guimarães (danilo.guimares@catalinamarketing.com)
# > Sub Test_Main(Debug, CurrentTestSet, CurrentTSTest, CurrentRun)
# >
# >   Dim wsh, executionStatus, scriptDirectory, testsFolder, frameworkFolder
# >
# >   ' Change those as needed:
# >   testsFolder = "I:\SQA\shared\Automation\Projects\Awards\"
# >   frameworkFolder = "I:\SQA\shared\Automation\Projects\Awards\"
# >   scriptDirectory = ""
# >
# >   ' Clear the VAPI-XP Output
# >   TDOutput.Clear
# >
# >   ' Set the pythonpath, we need that for the framework to function properly
# >   Set wsh = CreateObject("Wscript.shell")
# >
# >   ' Set the working directory. This is where your script will be.
# >   wsh.currentDirectory = testsFolder
# >
# >   ' If the variable CurrentTestSet is an object, that means this test case is
# >   ' being executed from a test set in the Lab module.
# >   if IsObject(CurrentTestSet) Then
# >
# >      ' Update the test run status & post it
# >      CurrentRun.Status = "Not Completed"
# >      CurrentRun.Post
# >
# >      ' Run the test script with parameters
# >      wsh.Run "cmd /k set PYTHONPATH=" & frameworkFolder & " && python ""awards_main.py"" " & ThisTest.ID & " " & CurrentTestSet.ID & " """ & ThisTest.Name & """ " & CurrentRun.ID & " " & CurrentTSTest.Instance & " && exit", , true
# >   else
# >
# >      ' Run the test script without parameters -- will not update ALM
# >      wsh.Run "cmd /k set PYTHONPATH=" & frameworkFolder & " && python ""awards_main.py"" && exit", , true
# >   End If
# >
# >   ' Finalize the script
# >   Set wsh = Nothing
# >
# > End Sub
#
# ALM Integration:
# The awards_main.py will get the following parameters from the ALM call:
# - Test ID
# - Test Set ID
# - Test Name
# - Run ID
# - Test Instance ID
#
# Those parameters are going to be used to call the ALM functions in order to
# load the steps from the test case in ALM. A parser is created, so it parses
# those steps and store the list of functional calls that need to be done.
# When invoking the method run_main, each of those functions are going to be
# invoked and executed.

import pdb
import os,sys
# Adding Lib folder path to sys path for importing library files
dir_path=(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(os.path.join(dir_path,'framework','awards','Libs'))
# Framework imports
from framework.awards.parser import *
from framework.reporting import *
from framework.alm import *
#
# Get the test case id, so we can load it:
test_id = sys.argv[1]
#test_id = 228

# Create the parser
parser = ALMTestParser()
#
# Load the test case from ALM
parser.load_test_case(test_case_id=test_id)

# Start a new iteration in the report
start_iteration(parser.test_case.description)

'''fh= open(r"I:\SQA\shared\\Automation\Projects\\Awards_TL\scripts\Address2\\%s.py"%sys.argv[3],"w")
fh.write("\n \nfrom framework.reporting import *\nimport awards_keywords \n\n")'''
# Run the main (parses the steps from the test case in ALM)
parser.run_main()#fh)
#fh.close()
#
# Close the connection with ALM
parser.test_case.close_connection()
