# File: Awards Validations
#
# Filename:
# > awards_validations.py
#
# Description:
# This module contains all functions used to run validations for the Awards
# automation. Those validation functions are used to make sure that the behavior
# of the system is correct accordingly to the actions tahat have been executed.
# Those validation functions will run the minium amount of actions in their
# code, just the enough to get to the validation point.
#
# Notes:
# - EVERY test case should have a validation point. Else, the test case is
#   meaningless.
# - EVERY validation point on screen must have a screenshot. Use the function
#   *take_screenshot()* for that purpose.
#
# Naming Convention:
# All function should start with the word *validate* followed by whatever is
# going to be validated:
# - validate_system_is_running
# - validate_build_date
import pdb
import configparser
# Framework imports
import framework.awards.utils as utils
from framework.reporting import *
from framework.utils import *
from framework.awards.defaults import *
from framework.awards.settings import machine_settings as ms

# NAV imports
import awards_navigation
import awards_actions
import awards_utils

# Config imports
from config import awards_version
from config import os_version
from config import build_version

# Python imports
import re
import datetime
import os
import codecs
import sys
#import filess to compare coupon number in print coupon
dir_path=(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(os.path.join(dir_path,'framework','awards','Libs'))
#dir_path=(os.path.dirname(os.path.realpath(__file__)))
import pytesseract
pytesseract.tesseract_cmd=os.path.join(dir_path,'framework','awards','Libs','tesseract')
from PIL import Image

#Import library file for barcode verification
#Author: Smitha D
import cv2
import numpy as np
from matplotlib import pyplot as plt
import webcolors
# Function: validate_system_is_running
# Validates that the awards application has been initialized properly.
# In case everything is ok, adds this information to the report header.
#
# Process:
# Navigate to the multitasking store system. Verify that
# "<--------CMC Is Now Operational" can be found on screen.
#
# Screen:
# Any
#
# Author:
# Danilo Guimaraes
def validate_system_is_running(timeout=DEFAULT_TIMEOUT):

    start_step("Validate that the system has been initialized sucessfully.")

    # Navigate to the multitasking store systems to validate it
    awards_navigation.to_multitastking_store_system()

    # Set the end time
    end_time = et(timeout)

    # Check if the sucess message is displayed on screen
    while not utils.find_text_in_console("(<-+CMC Is Now Operational|1 *Ready)",True) and ct() < end_time:
        time.sleep(3)
        awards_navigation.to_multitastking_store_system()
        utils.send_keys("p[enter]")
    # Find the CMC is now operational on screen
    if ct() > end_time:
        awards_navigation.to_multitastking_store_system()
        utils.send_keys("p[enter]")
        if not utils.find_text_in_console("(<--------CMC Is Now Operational)|(1 *Ready)",True):
            fail_step("[validate_system_is_running] 'CMC Is Now Operational' or '1  Ready' has not been found on screen.")
            utils.take_screenshot()
            utils.finish_execution()

    # take a screenshot
    utils.take_screenshot()

    pass_step("'CMC Is Now Operational' or '1  Ready' has been found on screen.")

# Function: validate_awards_version
# Validates that the awards version is correct. The awards version to be
# validated needs to be retrieved from the config.py .
#
# Process:
# Retrieves the file at DEFAULT_STORE_INI_DEST_PATH and verify for the Value
# "Version" in the section "SoftwareVersion".
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_awards_version():

    start_step("Validate that the Awards Version is '%s'." % awards_version)

    # Open the information panel
    awards_version_from_file = utils.get_ini_file_value(DEFAULT_STORE_INI_DEST_PATH, "SoftwareVersion", "Version")

    # Find the build date on screen
    if not tu(awards_version_from_file) == tu(awards_version):
        fail_step("[validate_awards_version] Awards version doesn't match. Version found: %s." % awards_version_from_file)
        utils.finish_execution()

    # Append the version to the report header
    add_header_info("Awards Version", awards_version_from_file)
    pass_step("Awards Version is '%s' as expected." % awards_version_from_file)

    return awards_version_from_file

# Function: validate_os_version
# Validates that the OS version is correct. The OS version to be
# validated needs to be retrieved from the config.py .
# In case everything is ok, adds this information to the report header.
#
# Process:
# Retrieves the file at DEFAULT_STORE_INI_DEST_PATH and verify for the Value
# "OSVersion" in the section "SoftwareVersion".
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_os_version():

    start_step("Validate that the OS Version is is '%s'." % os_version)

    # Open the information panel
    os_version_from_file = utils.get_ini_file_value(DEFAULT_STORE_INI_DEST_PATH, "SoftwareVersion", "OSVersion")

    # Find the build date on screen
    if not tu(os_version_from_file) == tu(os_version):
        fail_step("[validate_os_version] OS version doesn't match. Version found: %s." % os_version_from_file)
        utils.finish_execution()

    # Append the version to the report header
    add_header_info("OS Version", os_version_from_file)
    pass_step("OS Version is '%s' as expected." % os_version_from_file)

    return os_version_from_file

# Function: validate_build_date
# Validates that the os build date is correct. The os build date to be
# validated needs to be retrieved from the config.py .
# In case everything is ok, adds this information to the report header.
#
# Process:
# Retrieves the file at DEFAULT_STORE_INI_DEST_PATH and verify for the Value
# "OSBuildDate" in the section "SoftwareVersion".
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_build_date():

    start_step("Validate that the Build date is '%s'." % build_version)

    # Open the information panel
    build_date_from_file = utils.get_ini_file_value(DEFAULT_STORE_INI_DEST_PATH, "SoftwareVersion", "OSBuildDate")

    # Find the build date on screen
    if not tu(build_date_from_file) == tu(build_version):
        fail_step("[validate_build_date] Build date doesn't match. Date found: %s." % build_date_from_file)
        utils.finish_execution()

    # Append the version to the report header
    add_header_info("Build Date", build_date_from_file)
    pass_step("Build date is '%s' as expected." % build_date_from_file)

    return build_date_from_file

# Function: validate_clus
# Validates that the amount of CLUS loaded in the system matches the expected.
#

# Parameters:
# - clus_number: the amount of CLUS expected to be loaded in the system, in
# string format.
#
# Process:
# Press [f6] in the awards box until it shows the amount of CLUs loaded. Then,
# capture it from screen and compare with what has been passed as parameter.
#
# Screen:
# Should be at a awards session screen.
#
# Author:
# Danilo Guimaraes
def validate_clus(clus_number="0", fail_test = True):

    start_step("Validate that the CLUS value is %s." % clus_number)

    # Press F6 and wait untils 'CLUS' is displayed on screen
    end_time = et(DEFAULT_TIMEOUT)
    while not utils.find_text_in_console(r"\d+ *?CLUS", True) and ct() <= end_time:
        utils.send_keys("[f6]")
        time.sleep(1)
        utils.wait_for_text_in_console("CLUS")

    # Check if the time has expired
    if ct() > end_time:
        fail_step("[validate_clu] Could not find the CLUS value on screen.")
        utils.take_screenshot()
        utils.finish_execution()

    # Get the CLUS Value
    clus_found = utils.match_text_in_console(r"\d+ +CLUS")
    clus_found = re.findall(r"\d+", clus_found)[0]

    # Compare the CLUS value
    if tu(str(clus_found)) != tu(str(clus_number)):
        if fail_test:
            fail_step("[validate_clu] CLUS value doesn't match. Expected: %s, found: %s." % (str(clus_number), str(clus_found)) )
            utils.take_screenshot()
            utils.finish_execution()
        else:
            add_sub_step("[validate clu] CLUs doesn't match. The test will keep going as it has been explicitely passed a parameter to not fail under this condition.")
            utils.take_screenshot()
            return False

    # take a screenshot
    utils.take_screenshot()

    pass_step("%s CLUS have been found." % clus_found)

    return True

# Function: validate_store_number
# Validates that the store number in the store.ini matches the expected.
#
# Parameters:
# - store_number: the store number expected for that ini file.
#
# Process:
# Retrieve the file DEFAULT_STORE_INI_DEST_PATH. After that, look for the value
# "Number" at the section "Store".
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_store_number(store_number="9008"):

    start_step("Validate that the store number is '%s'." % store_number)

    # Open the information panel
    store_number_from_file = utils.get_ini_file_value(DEFAULT_STORE_INI_DEST_PATH, "Store", "Number")

    # Find the build date on screen
    if not tu(str(store_number_from_file)) == tu(str(store_number)):
        fail_step("Store number doesn't match. Number found: %s." % store_number_from_file)
        utils.finish_execution()

    pass_step("Store Number is '%s'." % store_number)

# Function: validate_chain_number
# Validates that the chain number in the store.ini matches the expected.
#
# Parameters:
# - chain_number: the chain number expected for that ini file.
#
# Process:
# Retrieve the file DEFAULT_STORE_INI_DEST_PATH. After that, look for the value
# "Number" at the section "Chain".
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_chain_number(chain_number="399"):

    start_step("Validate that the chain number is '%s'." % chain_number)

    # Open the information panel
    chain_number_from_file = utils.get_ini_file_value(DEFAULT_STORE_INI_DEST_PATH, "chain", "Number")

    # Find the build date on screen
    if not tu(str(chain_number_from_file)) == tu(str(chain_number)):
        fail_step("Chain number doesn't match. Number found: %s." % chain_number_from_file)
        utils.finish_execution()

    pass_step("Chain Number is '%s'." % chain_number)

# Function: validate_cds_is_enabled
# Validates that the CDS Enabled flag in the store.ini matches the expected.
#
# Parameters:
# - expected_value: "YES" or "NO". Defaults to "NO".
#
# Process:
# Retrieve the file DEFAULT_STORE_INI_DEST_PATH. After that, look for the value
# "Enabled" at the section "CDS".
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_cds_is_enabled(expected_value="NO"):

    start_step("Validate that the CDS Enabled flag is set to '%s'." % expected_value)

    # Open the information panel
    cds_flag_from_file = utils.get_ini_file_value(DEFAULT_STORE_INI_DEST_PATH, "CDS", "Enabled")
    print(cds_flag_from_file)

    if not tu(cds_flag_from_file) == tu(expected_value):
        fail_step("CDS Enabled flag doesn't match. Value found: %s, expected: %s." % (cds_flag_from_file, expected_value))
        utils.finish_execution()

    pass_step("CDS Flag is '%s'." % expected_value)

# Function: validate_cds_database_load
# Tail the file DEFAULT_CDS_DATABASE_REPORT_PATH until it finds
# "FYI - FINISHIT: PROCESSING FINISHED".
#
# Process:
# Runs a tail() function for this file in order to find
# "FYI - FINISHIT: PROCESSING FINISHED".
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_cds_database_load():

    start_step("Validate that the CDS force load job has worked properly.")

    # Check if any matches have been found
    if not utils.tail(DEFAULT_CDS_DATABASE_REPORT_PATH, ".*FYI - PROCCDSDB: PROCESS FINISHED\"", True):
    #if not utils.tail(DEFAULT_CDS_DATABASE_REPORT_PATH, ".*FYI - FINISHIT: PROCESSING FINISHED\"", True):
        fail_step("[validate_cds_force_load_job] Could not find any success load message at the report.")
        utils.finish_execution()

    # Get the file contents from the database report
    file_contents = utils.get_file_contents(DEFAULT_CDS_DATABASE_REPORT_PATH)
    entry = re.findall(".*FYI - PROCCDSDB: PROCESS FINISHED.*", file_contents)[0]
    #entry = re.findall(".*FYI - FINISHIT: PROCESSING FINISHED.*", file_contents)[0]

    pass_step("Right entry has been found in the file: <br>%s." % file_contents)

# Function: validate_error_in_cds_database_log
# Validates if a given error message can be found in the CDS database log. It is
# expected to find it.
#
# Parameters:
# - error_message: error message to be looked for. Can be a regular expression.
# - regex_flag: must be True in case the error message is a regular expression.
#
# Process:
# Will retrieve the CDS database log and look for the error message passed as
# parameter. In case it doesn't find it, will log an error.
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_error_in_cds_database_log(error_message, regex_flag = False):

    start_step("Validate that the CDS database log has the following error message: '%s'" % error_message)

    # Check if any matches have been found
    if not utils.tail(DEFAULT_CDS_DATABASE_REPORT_PATH, error_message, regex_flag):
        fail_step("[validate_error_in_database_log] Could not find any error message like '%s' in the report." % error_message)
        utils.finish_execution()

    # Get the file contents from the database report
    file_contents = utils.get_file_contents(DEFAULT_CDS_DATABASE_REPORT_PATH)

    if regex_flag:
        entry = re.findall(error_message, file_contents)[0]
    else:
        expression = ".*?%s.*?" % error_message
        entry = re.findall(error_message, file_contents)[0]

    pass_step("Right entry has been found in the file: <br>%s." % entry)

# Function: validate_error_in_so_database_log
# Validates if a given error message can be found in the SO database log. It is
# expected to find it.
#
# Parameters:
# - error_message: error message to be looked for. Can be a regular expression.
# - regex_flag: must be True in case the error message is a regular expression.
#
# Process:
# Will retrieve the SO database log and look for the error message passed as
# parameter. In case it doesn't find it, will log an error.
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_error_in_so_database_log(error_message, regex_flag = False):

    start_step("Validate that the CDS database log has the following error message: '%s'" % error_message)

    # Check if any matches have been found
    if not utils.tail(DEFAULT_CDS_DATABASE_REPORT_PATH, error_message, regex_flag):
        fail_step("[validate_error_in_database_log] Could not find any error message like '%s' in the report." % error_message)
        utils.finish_execution()

    # Get the file contents from the database report
    file_contents = utils.get_file_contents(DEFAULT_CDS_DATABASE_REPORT_PATH)

    if regex_flag:
        entry = re.findall(error_message, file_contents)[0]
    else:
        expression = ".*?%s.*?" % error_message
        entry = re.findall(error_message, file_contents)[0]

    pass_step("Right entry has been found in the file: <br>%s." % entry)

# Function: validate_cds_database_load
# Tail the file DEFAULT_DATABASE_REPORT_PATH until it finds
# "FYI - FINISHIT: PROCESSING FINISHED".
#
# Process:
# Runs a tail() function for this file in order to find
# "FYI - FINISHIT: PROCESSING FINISHED".
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_so_database_load():

    start_step("Validate that the SO force load job has worked properly.")

    if not utils.tail(DEFAULT_DATABASE_REPORT_PATH, ".*FYI - FINISHIT: PROCESSING FINISHED", True, REBOOT_INIT_TIME):
        fail_step("[validate_so_database_load] Could not find any success load message at the report.")
        utils.finish_execution()

    # Log an entry
    file_contents = utils.get_file_contents(DEFAULT_DATABASE_REPORT_PATH)
    entry = re.findall(".*FYI - FINISHIT: PROCESSING FINISHED.*", file_contents)[0]
    pass_step("Right entry has been found in the file: <br>%s." % entry)

    return True

# Function: validate_triggered_coupon_amount
# Finds the amount of coupons triggered on screen. Then, it checks if this
# amount matches with the amount of coupons expected to be found on screen.
#
# Parameters:
# - coupon_amount: how many coupons are expected to be displayed on screen.
#
# Process:
# Get the amount of coupons triggered, then compares to the amount of coupons
# passed as parameter.
#
# Screen:
# Should be at the awards session screen.
#
# Author:
# Danilo Guimaraes
def validate_triggered_coupon_amount(coupon_amount = 1):

    start_step("Validate that the system has %d coupons triggered." % coupon_amount)

    # Get the coupon amount from screen
    end_time = et(DEFAULT_TIMEOUT)
    coupon_amount_from_screen = awards_utils.get_triggered_coupon_amount()
    while coupon_amount_from_screen == None and ct() < end_time:
        time.sleep(1)
        coupon_amount_from_screen = awards_utils.get_triggered_coupon_amount()

    # Check if it amount from screen matches with the expected

    if ct() > end_time:
        fail_step("[validate_triggered_coupon_amount] The expected amount of triggered coupons could not be captured from the screen." )
        utils.take_screenshot()
        utils.finish_execution()

    if coupon_amount != coupon_amount_from_screen:
        fail_step("[validate_triggered_coupon_amount] The expected amount of triggered coupons doesn't match. Expected: '%s', found: '%s'." % (coupon_amount, coupon_amount_from_screen) )
        utils.take_screenshot()
        utils.finish_execution()

    # take a screenshot
    utils.take_screenshot()

    # Prepare the log
    coupon_amount = int(coupon_amount_from_screen) - int(coupon_amount)

    pass_step("%s coupons have been triggered so far." % (str(coupon_amount_from_screen)))

# Function: validate_coupon_print_in_log
# Looks for the coupon number in the file. If found, checks if the row where the
# coupon number is has "cs=Ok".
#
# Parameters:
# - log_file: log file where it is expected to to find the coupon print feedback.
# - coupon_number: number of the coupon expected to be found on screen.
#
# Process:
# Retrieve the file passed as parameter and try to match the following regular
# expression: r"\n.*coupon=%s .+cs=Ok.+\n" % coupon_number
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_coupon_print_in_log(log_file, coupon_number):

    start_step("Validate that the proper coupon print can be found in the print log.")

    # Get the file and match the entry

    coupon_number = tu(coupon_number)
    #print (coupon_number)

    log_file = utils.get_file_contents("C:\strsystm\log\%s" % log_file)
    expression = r"\n.*coupon=%s .+cs=Ok.+\n" % coupon_number
    log_row = re.findall(expression, log_file)
    #
    # Get the log row with the right coupon number
    if len(log_row) == 0:
        awards_navigation.to_multitastking_store_system()
        awards_actions.start_awards()
        fail_step("[validate_coupon_print_in_log] Could not find a log entry for coupon '%s'." % coupon_number)
        utils.finish_execution()

    # We got the right entry, now make sure CS=OK
    log_row1 = log_row[0]

    print (log_row1.find("cs=Ok"))
    find_cs=log_row1.find("cs=Ok")

    if  log_row1.find("cs=Ok")== -1:
        awards_navigation.to_multitastking_store_system()
        awards_actions.start_awards()
        fail_step("[validate_coupon_print_in_log] The right entry for coupon '%d' has been found in the report but 'CS' is not 'OK'.")
        utils.finish_execution()
    #
    pass_step("Right entry has been found: <br>%s." % log_row)

def validate_blind_coupon_print_in_log(log_file, coupon_number):

    start_step("Validate that the proper coupon print can be found in the print log.")

    # Get the file and match the entry
    coupon_number = tu(coupon_number)
    log_file = utils.get_file_contents("C:\strsystm\log\%s" % log_file)
    expression = r"\n.*coupon=%s .+cs=Blind.+\n" % coupon_number
    log_row = re.findall(expression, log_file)

    # Get the log row with the right coupon number
    if len(log_row) == 0:
        awards_actions.start_awards()
        fail_step("[validate_coupon_print_in_log] Could not find a log entry for coupon '%s'." % coupon_number)
        utils.finish_execution()

    # We got the right entry, now make sure CS=OK
    log_row = log_row[0]
    if log_row.find("cs=Blind") == -1:
        awards_actions.start_awards()
        fail_step("[validate_coupon_print_in_log] The right entry for coupon '%d' has been found in the report but 'CS' is not 'Blind'." % coupon_number)
        utils.finish_execution()

    pass_step("Right entry has been found: <br>%s." % log_row)

# Function: validate_coupon_not_printed_in_log
# Validates in the log that a coupon has not been printed.
#
# Parameters:
# - log_file: log file where it is not expected to find the coupon print feedback.
# - coupon_number: number of the coupon that is not suppose dto be printed.
#
# Process:
# Retrieve the file passed as parameter and try to match the following regular
# expression: r"\n.*coupon=%s .+cs=Ok.+\n" % coupon_number. If it matches,
# logs an error.
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_coupon_not_printed_in_log(log_file, coupon_number):

    start_step("Validate that the coupon %s print cannot be found in the print log." % coupon_number)

    # Get the file and match the entry
    coupon_number = tu(coupon_number)
    log_file = utils.get_file_contents("C:\strsystm\log\%s" % log_file)
    expression = r"\n.*coupon=%s.+\n" % coupon_number
    log_row = re.findall(expression, log_file)

    # Get the log row with the right coupon number
    if len(log_row) > 0:
        fail_step("[validate_coupon_not_print_in_log] Could find a log entry for coupon '%s': <br> %s" % (coupon_number, log_row[0]) )
        utils.finish_execution()

    pass_step("No entry has has been found for coupon %s." % coupon_number)

# Function: validate_mclu
# Validate tha a mclu exists in the system.
#
# Parameters:
# - mclu: Number of the MCLU to be validated.
#
# Process:
# In the awards system, type dmc <mclu> [enter] and make sure the MCLU
# information shows up on screen.
#
# Screen:
# Should be in an Awards session.
#
# Author:
# Danilo Guimaraes
def validate_mclu(mclu):

    start_step("Validate that the following MCLU can be found in the system: %s" % mclu)

    awards_navigation.to_multitastking_store_system()
    awards_utils.run_dmc(mclu)

    # take a screenshot
    utils.take_screenshot()

    pass_step("MCLU %s has been found in the system." % mclu)

# Function: validate_mclu_negative
# Validate tha a mclu does not exist in the system.
#
# Parameters:
# - mclu: Number of the MCLU to be validated.
#
# Process:
# In the awards system, type dmc <mclu> [enter] and make sure the MCLU
# information does not show up on screen.
#
# Screen:
# Should be in an Awards session.
#
# Author:
# Danilo Guimaraes
def validate_mclu_negative(mclu):

    start_step("Validate that the following MCLU cannot be found in the system: %s" % mclu)

    awards_navigation.to_multitastking_store_system()
    utils.send_keys("dmc %s [enter]" % tu(mclu))

    if utils.wait_for_text_in_console("MCLU Number", False, 5):
        fail_step("[validate_mclu_negative] 'MCLU number has been found on screen.")
        utils.take_screenshot()
        utils.finish_execution()

    # take a screenshot
    utils.take_screenshot()

    pass_step("MCLU %s has not been found in the system." % mclu)

# Function: validate_cds_section_in_ini_file
# Validate that the CDS sesion exists within the ini file passed as parameter.
# Then, it verifies the values within the CDS section match the parameters.
#
# Parameters:
# - filename: Name of the ini file to be validated.
# - values: A dictionary containing the name of the field to be looked for into
#   the ini file and its respective value.
#
# Process:
# For each parameter, gets the INI file value and compare with the parameter.
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes, Ajay Keerthi G
def validate_cds_section_in_ini_file(filename, values):

    start_step("Validate the file '%s' contains the right values for the CDS section." % filename)

    for field in values:

        # Clean the parameter stuff in case this is a parameter dictionary
        clean_field = field.strip()
        if clean_field != "validate_step":
            # Check each of the fields
            awards_utils.verify_ini_file_value(filename, "CDS", clean_field, values[field])

# Function: validate_cds_section_not_in_ini_file
# Validate that the CDS sesion does not exists within the ini file passed as
# parameter.
#
# Process:
# Get the ini file and validates there is no [CDS] section in the file.
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_cds_section_not_in_ini_file(filename):

    start_step("Validate the file '%s' does not contains the CDS section." % filename)

    if not awards_utils.verify_section_not_in_ini_file(filename, "CDS"):
        fail_step("[validate_cds_section_not_in_ini_file] Could find '[CDS]' into the file '%s'." % filename)
        utils.finish_execution()

    pass_step("File '%s' does not contains the CDS section." % filename)

# Function: validate_jpgs_in_images_folder
# Validates that the images folder contains the right amount of files.
#
# Parameters:
# -amount_of_images: the amount of images expected in the folder.
#
# Process:
# Run a dir in the images folder and compare the amount of images in it.
#
# Screen:
# Needs to be in a DOS session.
#
# Author:
# Danilo Guimaraes
def validate_jpgs_in_images_folder(amount_of_images):

    start_step("Validate the images folder has '%s' images." % amount_of_images)

    utils.send_keys(r"cls [enter]")
    utils.send_keys(r"dir %s [enter]" % DEfAULT_IMAGES_PATH)
    if not utils.wait_for_text_in_console("%s File(s)" % str(amount_of_images), False, 30):
        fail_step("[validate_jpgs_in_images_folder] Could not find '%s' files in the images directory." % str(amount_of_images) )
        utils.take_screenshot()
        utils.finish_execution()

    # take a screenshot
    utils.take_screenshot()
    pass_step("'%s' images have been found in the system." % str(amount_of_images))

# Function: validate_xmls_in_xmldb_folder
# Validates that the XML folder contains the right amount of files.
#
# Parameters:
# -amount_of_file: the amount of files expected in the folder.
#
# Process:
# Run a dir in the XMLDB folder and compare the amount of images in it.
#
# Screen:
# Needs to be in a DOS session.
#
# Author:
# Danilo Guimaraes
def validate_xmls_in_xmldb_folder(amount_of_files):

    start_step("Validate the XMLDB folder has '%s' XMLs." % amount_of_files)

    utils.send_keys(r"cls [enter]")
    utils.send_keys(r"dir %s [enter]" % DEFAULT_XMLS_PATH)
    if not utils.wait_for_text_in_console("%s File(s)" % str(amount_of_files), False, 30):
        fail_step("[validate_xmls_in_xmldb_folder] Could not find '%s' files in the images directory." % str(amount_of_files) )
        utils.take_screenshot()
        utils.finish_execution()

    # take a screenshot
    utils.take_screenshot()
    pass_step("'%s' XMLs have been found in the XMLDB folder." % str(amount_of_files))

# Function: validate_quickcash_in_dw_log
# Will validate that the QuickCash print entries can be found in the DW log.
#
# Parameters:
# - log_file: full path to the log file in the awards box
# - coupon_number: number of the coupon that should've been printed
# - upc_number: name of the upc to be looked at in the log
#
# Process:
# Matches a regular expression with the number of the coupon, cs=Ok and having
# the [QuickCash] entries.
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_quickcash_in_dw_log(log_file, coupon_number, upc_number):

    start_step("Validate that the coupon %s (Quick Cash) can be found in the DW log." % coupon_number)

    # Get the file and match the entry
    coupon_number = tu(coupon_number)
    upc_number = tu(upc_number).zfill(12)

    log_file = utils.get_file_contents(log_file)
    expression = r"\n.*[QCPrint] coupon=%s.*cs=Ok.*\n.*[QcLineItem] upc=%s.*\n.*[QcLineItemInfo].*\n" % (coupon_number, upc_number)
    log_row = re.findall(expression, log_file)

    # Get the log row with the right coupon number
    if len(log_row) == 0:
        fail_step("[validate_quickcash_in_dw_log] Could not find a log entry for coupon '%s'." % coupon_number )
        utils.finish_execution()

    pass_step("A QuickCash entry has been found for coupon %s:<br>%s" % (coupon_number, upc_number) )

# Function: validate_coupon_data_dmc
# Validates the coupon information in the DMC screen.
#
# Parameters:
# - coupon_number: number of the coupon to be validated.
# - coupon_class: class that the coupon is supposed to have.
# - coupon_type: type that the coupon is supposed to have.
# - print_priority: print priority the coupon is supposed to have.
#
# Process:
# For each of the parameters passed, run the command dmc <coupon number> and
# validates that the field is correct. As the dmc screen goes away after a few
# seconds, it will run the DMC command each time it needs to validate something.
#
# Screen:
# Needs to be at the awards screen
#
# Author:
# Danilo Guimaraes
def validate_coupon_data_dmc(coupon_number, coupon_class, coupon_type, print_priority):

    start_step("Validate the coupon data in DMC for coupon '%s'.")

    # Prepare the Parameters
    coupon_number = tu(coupon_number).zfill(7)
    coupon_class = tu(coupon_class)
    coupon_type = tu(coupon_type)
    print_priority = tu(print_priority)

    # Start checking the values on screen
    awards_utils.run_dmc(coupon_number)
    if not utils.find_text_on_screen("MCLU Number.*= %s" % coupon_number, True):
        fail_step("[validate_coupon_data_dmc] Requested DMC for '%s' but something else showed up." % coupon_number)

    # Always send dmc [enter] to refresh the screen
    awards_utils.run_dmc(coupon_number)
    if not utils.find_text_on_screen("Coupon Class.*= %s" % coupon_class, True):
        value_from_screen = utils.match_text_on_screen("Coupon Class.*= %s" % coupon_class, True)
        fail_step("[validate_coupon_data_dmc] 'Coupon class' found is '%s' while expecting '%s'." % (value_from_screen, coupon_class) )

    # Get the Coupon Class
    awards_utils.run_dmc(coupon_number)
    if not utils.find_text_on_screen("Print Priority.*= %s" % print_priority, True):
        value_from_screen = utils.match_text_on_screen("Print Priority.*= %s" % print_priority, True)
        fail_step("[validate_coupon_data_dmc] 'Print Priority' found is '%s' while expecting '%s'." % (value_from_screen, print_priority) )

    # Take a screenshot
    awards_utils.run_dmc(coupon_number)
    utils.take_screenshot()

    pass_step("All data has been validated sucessfully.")

# Function: validate_coupon_data_da
# Validates the coupon information in the DA screen.
#
# Parameters:
# - award_number: number of the award to be validated
# - treshold_seq: treshold seq the award is supposed to have
# - coup_seq: coupon sequence the award is supposed to have
# - description: description the award is supposed to have
# - coupon_number: number of the coupon the award is supposed to have
#
# Process:
# For each of the parameters passed, run the command da <award number> and
# validates that the field is correct. As the dmc screen goes away after a few
# seconds, it will run the DA command each time it needs to validate something.
#
# Screen:
# Needs to be at the awards screen
#
# Author:
# Danilo Guimaraes
def validate_coupon_data_da(award_number, treshold_seq, coup_seq, description, coupon_number):

    start_step("Validate the coupon data in DA for coupon '%s'.")

    # Prepare the Parameters
    award_number = tu(award_number)
    treshold_seq = tu(treshold_seq)
    coup_seq = tu(coup_seq)
    description = tu(description)
    coupon_number = tu(coupon_number)

    # Start checking the values on screen
    awards_utils.run_da(award_number)
    if not utils.find_text_on_screen("Award: %s" % award_number, True):
        fail_step("[validate_coupon_data_da] Requested DA for '%s' but something else showed up." % coupon_number)
        utils.take_screenshot()
        utils.finish_execution()

    awards_utils.run_da(award_number)
    if not utils.find_text_on_screen("Treshold Seq: %s" % treshold_seq, True):
        fail_step("[validate_coupon_data_da] Treshold seq is not '%s'." % treshold_seq)
        utils.take_screenshot()
        utils.finish_execution()

    awards_utils.run_da(award_number)
    if not utils.find_text_on_screen("Coup Seq: %s" % coup_seq, True):
        fail_step("[validate_coupon_data_da] Coup seq is not '%s'." % coup_seq)
        utils.take_screenshot()
        utils.finish_execution()

    awards_utils.run_da(award_number)
    if not utils.find_text_on_screen("Description: %s" % description, True):
        fail_step("[validate_coupon_data_da] Description is not '%s'." % description)
        utils.take_screenshot()
        utils.finish_execution()

    awards_utils.run_da(award_number)
    if not utils.find_text_on_screen("Coupon: %s" % coupon_number, True):
        fail_step("[validate_coupon_data_da] Coupon is not '%s'." % coupon_number)
        utils.take_screenshot()
        utils.finish_execution()

    # Take a screenshot
    awards_utils.run_da(award_number)
    utils.take_screenshot()

    pass_step("All data has been validated sucessfully.")

# Function: validate_realtime_section_in_ini_file
# Validate that the realtime sesion exists within the ini file passed as parameter.
# Then, it verifies the values within the reatime section match the parameters.
#
# Parameters:
# - filename: Name of the ini file to be validated.
# - values: A dictionary containing the name of the field to be looked for into
#   the ini file and its respective value.
#
# Process:
# For each parameter, gets the INI file value and compare with the parameter.
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_realtime_section_in_ini_file(filename, values):

    start_step("Validate the file '%s' contains the right values for the REALTIME section." % filename)

    for field in values:

        # Clean the parameter stuff in case this is a parameter dictionary
        clean_field = field.strip()

        # Check each of the fields
        awards_utils.verify_ini_file_value(filename, "REALTIME", clean_field, values[field])

# Function: validate_file_exists
# Validates that a given award has the proper NBIC condition.
#
# Parameters:
# - award_number: Number of the award to be validated
# - expected_condition: Expected NBIC condition to be validated
#
# Process:
# Will run a DA and see if the text NBIC shows on screen. If so, will send DA
# again and will verify if the expected condition for NBIC is correct.
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_nbic_condition(award_number, expected_condition):

    start_step("Validate the coupon data in DA for coupon '%s'.")

    # Prepare the Parameters
    award_number = tu(award_number)

    # Start checking the values on screen
    awards_utils.run_da(award_number)
    if not utils.find_text_on_screen("Award: %s" % award_number, True):
        fail_step("[validate_coupon_data_da] Requested DA for '%s' but something else showed up." % coupon_number)
        utils.take_screenshot()
        utils.finish_execution()

        # Start checking the values on screen
        awards_utils.run_da(award_number)
        if not utils.find_text_on_screen("NBIC"):
            fail_step("[validate_coupon_data_da] Requested DA for '%s' and no NBIC condition has showed up." % coupon_number)
            utils.take_screenshot()
            utils.finish_execution()

    awards_utils.run_da(award_number)
    if not utils.find_text_on_screen("NBIC Condition: %s" % expected_condition, True):
        fail_step("[validate_coupon_data_da] NBIC expected condition is not '%s'." % treshold_seq)
        utils.take_screenshot()
        utils.finish_execution()

    # Get a screenshot
    awards_utils.run_da(award_number)
    utils.take_screenshot()

    pass_step("All data has been validated sucessfully.")

# Function: validate_file_exists
# Validates that a given file exists in the Awards box.
#
# Parameters:
# - filename: Name of the ini file to be validated
#
# Process:
# Sends the TRapi special message to verify if the file exist.
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_file_exists(filename):

    start_step("Validate that the file '%s' exists in the Awards box." % filename)

    if not utils.check_file_exists(filename):
        fail_step("[validate_file_exists] File '%s' does not exists in the Award box." % filename)
        utils.finish_execution()

    pass_step("File has been found in the Awards box.")

# Function: validate_file_does_not_exists
# Validates that a given file does not exist in the Awards box.
#
# Parameters:
# - filename: Name of the ini file to be validated
#
# Process:
# Sends the TRapi special message to verify if the file exist.
#
# Screen:
# N/A
#
# Author:
# Danilo Guimaraes
def validate_file_does_not_exists(filename):

    start_step("Validate that the file '%s' does not exists in the Awards box." % filename)

    if utils.check_file_exists(filename):
        fail_step("[validate_file_exists] File '%s' exists in the Award box." % filename)
        utils.finish_execution()

    pass_step("File has not been found in the Awards box.")

def validate_process_is_running(process_name):

    start_step("Validate that the process '%s' is running." % process_name)
    if not utils.check_process_is_running(process_name):
        fail_step("Process '%s' is not running." % process_name)
        utils.finish_execution()

    pass_step("Process '%s' is running." % process_name)

def validate_process_is_not_running(process_name):

    start_step("Validate that the process '%s' is NOT running." % process_name)
    if utils.check_process_is_running(process_name):
        fail_step("[validate_process_is_not_running] Process '%s' is running." % process_name)
        utils.finish_execution()

    pass_step("Process '%s' is not running." % process_name)

def validate_process_is_running_in_awards(process_name):

    start_step("Validate that the process '%s' is running in the Awards system." % process_name)

    utils.send_keys("[f10]")

    if not utils.wait_for_text_in_console(process_name, False, 5):
        fail_step("[validate_process_is_running_in_awards] Process '%s' is not running." % process_name)
        utils.take_screenshot()
        utils.finish_execution()

    utils.take_screenshot()
    pass_step("Process '%s' is running in Awards." % process_name)

def validate_list_of_files_exist(folder, files):

    start_step("Validate the following list of files exist at '%s': %s" % (folder, files))

    # Get the list of files
    files = files.split(",")
    for i, file in enumerate(files):
        files[i] = file.strip()

    # Verify every file
    for f in files:
        if not utils.check_file_exist("%s\%s" % (folder, files)):
            fail_step("[validate_list_of_files_exist] File '%s' does not exists in the Award box at the folder '%s'." % (files, folder))
            utils.finish_execution()

def validate_file_contains(filename, text_to_validate, case_sensitive_flag = True, regex = False,xmllog=True):

    start_step("Validate the file '%s' contains the following string:<br>%s" % (filename, text_to_validate))

    # Get the file contents
    folder=DEFAULT_XMLS_PATH
    if xmllog==True:
        file_name=DEFAULT_XMLS_PATH+"\\"+filename
    else:
        file_name=filename
    file_contents = utils.get_file_contents(file_name)
    #DEFAULT_XMLS_PATH
    # Handle the strings in case this is case sensitive
    if not case_sensitive_flag:
        file_contents = file_contents.upper()
        text_to_validate = text_to_validate.upper()

    # Run the validation

    if regex:

        if len(re.findall(text_to_validate, file_contents)) > 0:
            return True
    else:
        #print (file_contents)
        print(text_to_validate)

        if file_contents.find(text_to_validate) != -1:
            return True

    fail_step("[validate_file_contains] File '%s' does not contains '%s'.<br>File contents:<br>%s" % (filename, folder, file_contents))
    utils.finish_execution()

def validate_file_not_contains(filename, text_to_validate, case_sensitive_flag = True, regex = False,xmllog=True):

    start_step("Validate the file '%s' does not contains the following string:<br>%s" % (filename, text_to_validate))

    # Get the file contents

    file_contents = utils.get_file_contents(filename)

    # Handle the strings in case this is case sensitive
    if not case_sensitive_flag:
        file_contents = file_contents.upper()
        text_to_validate = text_to_validate.upper()

    # Run the validation
    if regex:
        if len(re.findall(text_to_validate, file_contents)) == 0:
            return True
    else:
        if file_contents.find(text_to_validate) == -1:
            return True

    fail_step("[validate_file_not_contains] File '%s' contains '%s'.<br>File contents:<br>%s" % (filename, text_to_validate, file_contents))
    utils.finish_execution()

def validate_shuttle_ini(expected_last_shipment_processed, expected_clus, expected_messages, expected_pictures, expected_triggers):

    # Get the data from the INI file
    last_shipment_processed = utils.get_ini_file_value(DEFAULT_SHUTTLE_INI_DEST_PATH, "Shuttle", "LASTSHIPMENTPROCESSED")
    clus = utils.get_ini_file_value(DEFAULT_SHUTTLE_INI_DEST_PATH, "Shuttle", "CLUS")
    messages = utils.get_ini_file_value(DEFAULT_SHUTTLE_INI_DEST_PATH, "Shuttle", "MESSAGES")
    pictures = utils.get_ini_file_value(DEFAULT_SHUTTLE_INI_DEST_PATH, "Shuttle", "PICTURES")
    triggers = utils.get_ini_file_value(DEFAULT_SHUTTLE_INI_DEST_PATH, "Shuttle", "TRIGGERS")

    # Start validating the data
    if not tu(expected_last_shipment_processed) == tu(last_shipment_processed):
        fail_step("[validate shuttle ini] Validation failed. 'LASTSHIPMENTPROCESSED' is '%s'. Expected: '%s'" % (last_shipment_processed, expected_last_shipment_processed))
        utils.finish_execution()

    if not tu(expected_clus) == tu(clus):
        fail_step("[validate shuttle ini] Validation failed. 'CLUS' is '%s'. Expected: '%s'" % (clus, expected_clus))
        utils.finish_execution()

    if not tu(expected_messages) == tu(messages):
        fail_step("[validate shuttle ini] Validation failed. 'MESSAGES' is '%s'. Expected: '%s'" % (messages, expected_messages))
        utils.finish_execution()

    if not tu(expected_pictures) == tu(pictures):
        fail_step("[validate shuttle ini] Validation failed. 'PICTURES' is '%s'. Expected: '%s'" % (pictures, expected_pictures))
        utils.finish_execution()

    if not tu(expected_triggers) == tu(triggers):
        fail_step("[validate shuttle ini] Validation failed. 'TRIGGERS' is '%s'. Expected: '%s'" % (triggers, expected_triggers))
        utils.finish_execution()

    pass_step("Success. Values:<br>LASTSHIPMENTPROCESSED: %s<br>CLUS: %s<br>MESSAGES: %s<br>PICTURES: %s<br>TRIGGERS: %s" % (last_shipment_processed, clus, messages, pictures, triggers))

def validate_awards_update(new_version=None):
    start_step("Validate the software has been updated sucessfully.")

    # Get the contents of the report file
    file_contents = utils.get_file_contents(DEFAULT_SOFTWARE_UPDATE_REPORT_PATH)
    print(file_contents)
    # Check if we can find the message
    matches = re.findall("SUCCESSFULLY UPGRADED FROM", file_contents)
    print (matches)


    # Check if we have matches
    if len(matches) == 0:
        fail_step("[validate_awards_update] Could not capture the sucess upgrade message from the file %s." % DEFAULT_SOFTWARE_UPDATE_REPORT_PATH)
        sys.exit(0)

    # Get the first entry from the matches
    log_entry = matches[0]

    # if we have any value in new_version, we should look for it in the report
    if new_version is not None:
        if log_entry.find(new_version) == -1:
            fail_step("[validate_awards_update] Could capture the log but the version in it doesn't matches the expected: '%s'.<br>Log entry: %s" % (new_version, log_entry) )
            sys.exit(0)

    pass_step("The right log entry has been found.<br>%s" % log_entry)

# Function:Validate_coupon_number_in_image
# Validates that a given  coupon number is printed properly on the coupon image
#
# Parameters:
# - couon_number: Coupon number for validating printed number on the coupon image_file
#- Validate_bc- if Validate_bc = Yes the deletion of image will not be excuted. Images will be
#   retained in the temp folder for the validation of barcode
# Process:
# Coupon image will be copied from SUT to local temp folder.
# Image will be parsed using  PIL library and find the coupon number on the image
#
# Screen:
# N/A
#
# Author:
# Smitha D

def Validate_coupon_number_in_image(coupon_number, validate_bc):

    start_step("Validate that the Coupon number is displayed properly on the coupon")
    awards_navigation.to_new_dos_session()

    local_temp_path = "%s_%s" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    flag=0
    awards_utils.prepare_sut_temp_directory(local_temp_path)

    #image_file=os.path.join(DEFAULT_COUPON_IMAGE_PATH,""


    awards_actions.copy_coupon_from_SUT_local()

    for file in os.listdir(local_temp_path):
        print (file)
        file_name=tu(file)
        print(file_name)


        if file_name.endswith(".PNG") or file.endswith(".PNG"):

            print(local_temp_path+"\\"+file)


            #Image.open(local_temp_path+"\\"+file).rotate(270,expand=1).show()
            image_content=pytesseract.image_to_string(Image.open(local_temp_path+"\\"+file).convert("L").rotate(270,expand=1))#res.png"))
            image_content1=pytesseract.image_to_string(Image.open(local_temp_path+"\\"+file).rotate(270,expand=1))
            print (image_content)
            str(coupon_number).replace("6","0").replace("9","0")

            expression = r"\n.*coupon=%s .+cs=Ok.+\n" % coupon_number

            coupon_data1=re.findall(coupon_number,image_content)
            coupon_data2=re.findall(coupon_number,image_content1)
            coupon_data = re.findall(str(coupon_number).replace("6","0").replace("9","0").replace("8","3"), image_content)
            print (coupon_data)
            print (coupon_data1)

            if len(coupon_data)!=0 or len(coupon_data1)!=0 or len(coupon_data2)!=0:
                flag=1
                file_name=file.split(".")

                copy_file_name=DEFAULT_EVIDENCE_STORAGE_PATH+r"\\Coupon_images\\"+file_name[0]+"_"+datetime.datetime.now().strftime("%Y_%m_%d_%H_%M")+"."+file_name[1]

                utils.copy_local_file(local_temp_path+"\\"+file,copy_file_name)
                break
    if flag!=1:
        fail_step("[Validate_coupon_number_in_image] Searching coupon number %s is not found on the couponimage"%coupon_number)
        utils.take_screenshot()
        utils.finish_execution()
    if validate_bc == "NO":

        awards_utils.clean_sut_temp_directory(r'C:\Strsystm\PrnSimImageDump')
    image_path=DEFAULT_EVIDENCE_STORAGE_PATH+"\\Coupon_images\\"+file
    print(image_path)
    pass_step("<br>Coupon number found on image</br> <IMG SRC=%s>"%copy_file_name)
# Function:closest_colour
# Validates barcode is printed properly on the printed coupon
#
# Parameters:
# - barcode_file: Path of a file used for comaprison of barcode.
#- barcode_type : Type of barcode verifying
# Process:
# Coupon image will be copied from SUT to local temp folder.
# Barcode image will be copied under data_barcode folder and will be used for the comparison
# Cv2 libray file is used for the comparios of the image.
# Color, size and patteren of the barcode will be validated
#
# Screen:
# N/A
#
# Author:
# Smitha D
def closest_colour(requested_colour):
    min_colours = {}
    for key, name in webcolors.css3_hex_to_names.items():
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - requested_colour[0]) ** 2
        gd = (g_c - requested_colour[1]) ** 2
        bd = (b_c - requested_colour[2]) ** 2
        min_colours[(rd + gd + bd)] = name
    return min_colours[min(min_colours.keys())]

# Function:get_colour_name
# Validates barcode is printed properly on the printed coupon
#
# Parameters:
# - barcode_file: Path of a file used for comaprison of barcode.
#- barcode_type : Type of barcode verifying
# Process:
# Coupon image will be copied from SUT to local temp folder.
# Barcode image will be copied under data_barcode folder and will be used for the comparison
# Cv2 libray file is used for the comparios of the image.
# Color, size and patteren of the barcode will be validated
#
# Screen:
# N/A
#
# Author:
# Smitha D
def get_colour_name(requested_colour):
    try:
        closest_name = actual_name = webcolors.rgb_to_name(requested_colour)
    except ValueError:
        closest_name = closest_colour(requested_colour)
        actual_name = None
    return actual_name, closest_name

# Function:validate_color_barcode
# Validates barcode is printed properly on the printed coupon
#
# Parameters:
# - barcode_file: Path of a file used for comaprison of barcode.
#- barcode_type : Type of barcode verifying
# Process:
# Coupon image will be copied from SUT to local temp folder.
# Barcode image will be copied under data_barcode folder and will be used for the comparison
# Cv2 libray file is used for the comparios of the image.
# Color, size and patteren of the barcode will be validated
#
# Screen:
# N/A
#
# Author:
# Smitha D

def validate_color_barcode(filename,color_name):

    start_step("Validate color of the barcode is %s"%color_name)
    img=Image.open(filename)
    avg_color=np.average(img,axis=0)

    print (avg_color)
    flag_acol=0


    flag_ccol=0
    print (tu(color_name))

    for col in avg_color:
        actual_name, closest_name = get_colour_name(col)
        print (actual_name)
        print( closest_name)

        if actual_name!=None:
            if tu(color_name) =="BLACK":

                if len(re.findall(tu(color_name),tu(actual_name)))>0 or  len(re.findall("GRAY",tu(actual_name)))>0 or  len(re.findall("GREY",tu(actual_name)))>0 :
                    flag_acol=1
                    break
                else:

                    if len(re.findall(tu(color_name),tu(actual_name)))>0:
                        flag_acol=1
                        break
        else:

            pass

        if closest_name!=None:
            if tu(color_name) =="BLACK":

                if len(re.findall(tu(color_name),tu(closest_name)))>0 or  len(re.findall("GRAY",tu(closest_name)))>0 or  len(re.findall("GREY",tu(closest_name)))>0 :
                    flag_acol=1
                    break
            else:


                if len(re.findall(tu(color_name),tu(closest_name)))>0:

                    print (closest_name)

                    flag_ccol=1
                    break
        else:


            pass
    print("flag_acol=%s flag_ccol=%s"%(flag_acol,flag_ccol))

    if flag_acol==1 or flag_ccol==1:
            add_sub_step("Color of the barcode is %s"%color_name)
            return True
    else:
            add_sub_step("Color of the barcode is not %s"%color_name)
            return False
# Function:validate_barcode
# Validates barcode is printed properly on the printed coupon
#
# Parameters:
# - barcode_file: Path of a file used for comaprison of barcode.
#- barcode_type : Type of barcode verifying
# Process:
# Coupon image will be copied from SUT to local temp folder.
# Barcode image will be copied under data_barcode folder and will be used for the comparison
# Cv2 libray file is used for the comparios of the image.
# Color, size and patteren of the barcode will be validated
#
# Screen:
# N/A
#
# Author:
# Smitha D

def validate_barcode(barcode_file, barcodetype,params):
    start_step("Validate for the proper display of bar code type %s " %barcodetype)
    print (params.keys())

    awards_navigation.to_new_dos_session()
    local_temp_path="%s_%s_barcode" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    template_file=DEFAULT_AWARDS_BARCODE_FILE_REPO+"\\"+barcode_file
    print(template_file)


    awards_actions.copy_coupon_from_SUT_local(local_temp_path)


    color_barcode=False
    flag=0
    for file in os.listdir(local_temp_path):
        print (file)
        file_name=tu(file)
        print(file_name)


        if tu(file_name).endswith(".PNG"):
            img_rgb= cv2.imread(local_temp_path+"\\"+file)

            img=img_rgb.copy()
            img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)


            template = cv2.imread(template_file,0)
            w, h = template.shape[::-1]

            res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
            threshold = 0.8
            loc = np.where( res >= threshold)

            barcode_image_file=DEFAULT_EVIDENCE_STORAGE_PATH+r"\\Coupon_images\\"+file.split(".")[0]+"_barcode"+datetime.datetime.now().strftime("%Y_%m_%d_%H_%M")+"."+file.split(".")[1]
            copy_file_name=DEFAULT_EVIDENCE_STORAGE_PATH+r"\\Coupon_images\\"+file.split(".")[0]+datetime.datetime.now().strftime("%Y_%m_%d_%H_%M")+"."+file.split(".")[1]
            utils.copy_local_file(local_temp_path+"\\"+file,copy_file_name)
            print (barcode_image_file)

            if len(loc[::-1][0]) == 0:
                flag=0
            else:
                flag=1




                corp_img=img[loc[0][0]:loc[0][0]+h,loc[1][0]:loc[1][0]+w]
                cv2.imwrite(barcode_image_file,corp_img)
                #utils.copy_local_file(local_temp_path+"\\"+file,copy_file_name)


                color_barcode=validate_color_barcode(barcode_image_file,params["barcode color"])
                print(color_barcode)

                break


    if flag!=1 or color_barcode != True:

        fail_step("<br> The Bar code was not found as expectedon coupon </br><IMG SRC=%s>" %(copy_file_name))
        #utils.take_screenshot()

        utils.finish_execution()
    else:

        pass_step("<br>Barcode is verified on Coupon image</br> <IMG SRC=%s> "%copy_file_name)
    awards_utils.clean_sut_temp_directory(r'C:\Strsystm\PrnSimImageDump')
    image_path=DEFAULT_EVIDENCE_STORAGE_PATH+"\\Coupon_images\\"+file
    print(image_path)


    #return actual_name, closest_name

# Function: validate_xmllog
# Validates the string in XML log. String will be passed as parameter text_to_validate
#
# Parameters:
# - params  - Parameter
#
# Process:
# String will be searched in the XML LOG onf sut machine
#
# Screen:
# N/A
#
# Author:
# Smitha D


def validate_xmllog(params):

    start_step("Validate XML Log for the text %s" %params['text_to_validate'])
    if "file_name" not in params or params["file_name"]=="":
        params["file_name"] = ""
    if params["file_name"]=="":
        utils.send_keys(r"cd c:\strsystm\log [enter]")
        utils.send_keys(r"cls [enter]")
        utils.send_keys(r"dir *.xml /b /od [enter]")
        file_name=" "
        file_name=utils.get_latest_file()
        print (file_name)
        find_log=0

        #print (params['text_to_validate'])

        if file_name!=None:
            utils.send_keys(r"READXMLLOG %s >log.txt[enter]"%file_name.strip())

            time.sleep(3)
    file_content=utils.get_file_contents(r"C:\strsystm\log\%s" % "log.txt")
    print (file_content)


        #print ( params['text_to_validate'])


    validate_text=params['text_to_validate']
    print(type(validate_text))
    #for validate_text in params['text_to_validate'].split(','):
    print (validate_text)



    val_txt=".*"+validate_text.replace(r'&nbsp;',' ').replace('\\','_').replace('(','_').replace(')','_')
    #val_txt=r"136366.XML"
    print (val_txt)
    strfilevalues = file_content.replace('\\','_').replace('(','_').replace(')','_')
    find_log=re.findall(val_txt,file_content.replace('\\','_').replace('(','_').replace(')','_'))
    print (find_log)

    if len(find_log)==0:

        fail_step("[validate_xmllog] Searching string %s is not found on the screen" %params['text_to_validate'])
        utils.take_screenshot()
        utils.finish_execution()
    else:
        pass_step("String %s is found in XML log file" %params['text_to_validate'])
        awards_navigation.to_multitastking_store_system()

# Function: update_store_ini_single_section
# Validation fo changing a single section in Store.ini file
#
# Parameters:
# - params  - Parameters in the test case
#
# Process:
# copy the storfix.ini file from the central location.
#Get the value from test cases about the section to be updated in the store.ini file
#Copy store.ini file from SUT. Compare section in stroefix.ini and store.ini files are same or not
# If the values under the section are same then change the values in store.ini file
#
# Screen:
# N/A
#
# Author:
# Smitha D


def update_store_ini_single_section(params):
    start_step("Update store.ini file with new file for section %s"%(params["section name"]))
    awards_actions.copy_store_ini_from_SUT_local()
    config1 = configparser.ConfigParser(strict=False)
    config1.optionxform = lambda option: option
    config2= configparser.ConfigParser(strict=False)
    config2.optionxform = lambda option: option
    local_temp_path="%s_%s"%(DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    config2.read(local_temp_path+r"\store_temp.ini")
    section_name={}

    config3=configparser.ConfigParser(strict=False)
    config3.optionxform = lambda option: option

    config3.read(local_temp_path+r"\storefix.ini")

    for section in config3.sections():
    #print (section)
        for key2 in config3.options(section):
        #print (config[section][key2].split(",")[0])
            if len(config3[section][key2].split(",")) >1:

                    section_name[(config3[section][key2]).split(",")[1]]=(config3[section][key2]).split(",")[0]
                    if tu(config3[section][key2].split(",")[0])==tu(params["section name"]):
                        section_update=(config3[section][key2]).split(",")[1]

    config1.read(local_temp_path+r"\store.ini")
    for section1 in config1.sections():

            if tu(section1)==tu(params["section name"]):
                print(section1)

                for key2 in config2.options(section_update):
                    for key1 in config1.options(section1):
                        if tu(key1) == tu(key2):
                            print(key2)
                            config2[section_update][key2].replace('%','%%')
                            config1[section1][key1].replace('%','%%')
                            print (config2[section_update][key2])

                            if tu(config2[section_update][key2]).replace('"',"") == tu(config1[section1][key1]).replace('"',""):

                                if len(re.findall('"',config2[section_update][key2]))>0:
                                    config1[section1][key1].replace('"',"")
                                    value='"'+config1[section1][key1].replace('"',"")+'1"'
                                    config1.set(section1,key1,value)
                                else:
                                    config1.set(section1,key1,config2[section_update][key2]+"1")

    			#print(config[section][key])
    #print (config.inline_comments())
    with open(local_temp_path+r"\next.ini","w")as f:

        config1.write(f)
    pass_step("updated store.ini file with new value")
    return section_update

#Function: update_ini_file

def update_ini_file(params):
    start_step("update store.ini file with new value")

    awards_actions.copy_store_ini_from_SUT_local()
    config1 = configparser.ConfigParser(strict=False)
    config1.optionxform = lambda option: option
    config2= configparser.ConfigParser(strict=False)
    config2.optionxform = lambda option: option
    local_temp_path="%s_%s"%(DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    config1.read(local_temp_path+r"\store.ini")

#Function: update_store_ini_file
# Validation fo changing a single section in Store.ini file
#
# Parameters:
# - params  - Parameters in the test case
#
# Process:
# copy the storfix.ini file from the central location.
# Copy store.ini file from SUT. Compare all section in stroefix.ini and store.ini files are same or not
# If the values under the section are same then change the values in store.ini file
#
# Screen:
# N/A
#
# Author:
# Smitha D


def update_store_ini_file():

    start_step("update store.ini file with new value")

    awards_actions.copy_store_ini_from_SUT_local()
    config1 = configparser.ConfigParser(strict=False)
    config1.optionxform = lambda option: option
    config2= configparser.ConfigParser(strict=False)
    config2.optionxform = lambda option: option
    local_temp_path="%s_%s"%(DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    config1.read(local_temp_path+r"\store.ini")
    config2.read(local_temp_path+r"\store_temp.ini")
    config2.optionxform = str
    section_name={}
    config=configparser.ConfigParser(strict=False)
    config.optionxform = lambda option: option
    config.read(local_temp_path+r"\storefix.ini")
    for section in config.sections():
    #print (section)
        for key2 in config.options(section):
        #print (config[section][key2].split(",")[0])
            if len(config[section][key2].split(",")) >1:

                    section_name[(config[section][key2]).split(",")[1]]=(config[section][key2]).split(",")[0]

    for section2 in config2.sections():
        for section1 in config1.sections():
            if section2 in section_name.keys():
                if tu(section1)==tu(section_name[section2]):
                    print(section1)

                    for key2 in config2.options(section2):
                        for key1 in config1.options(section1):
                            if tu(key1) == tu(key2):
                                print(key2)
                                config2[section2][key2].replace('%','%%')
                                config1[section1][key1].replace('%','%%')
                                print (config2[section2][key2])
                                if tu(config2[section2][key2]).replace('"',"") == tu(config1[section1][key1]).replace('"',""):
                                    if len(re.findall('"',config2[section2][key2]))>0:
                                        config1[section1][key1].replace('"',"")
                                        value='"'+config1[section1][key1].replace('"',"")+'1"'
                                        config1.set(section1,key1,value)
                                    else:
                                        config1.set(section1,key1,config2[section2][key2]+"1")


    with open(local_temp_path+r"\next.ini","w")as f:
        config1.optionxform = str
        config1.write(f)
    pass_step("updated store.ini file with new value")

#Function:compare_store_ini_single_section
# Validation fo changing a single section in Store.ini file
#
# Parameters:
# - params  - Parameters in the test case
#
# Process:
# copy the storfix.ini file from the central location.
# Copy store.ini file from SUT. Compare a  section mentioned  in stroefix.ini and store.ini files are same or not
# If the values under the section are same then change the values in store.ini file
#
# Screen:
# N/A
#
# Author:
# Smitha D


def compare_store_ini_single_section(params,section_updates):
    start_step("Verify store.ini file is updated after running the job")

    awards_actions.copy_store_ini_from_SUT_local()

    local_temp_path = "%s_%s" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))

    config1 = configparser.ConfigParser(strict=False)
    config1.optionxform = lambda option: option
    config2 = configparser.ConfigParser(strict=False)
    config2.optionxform = lambda option: option

    section_name={}
    section_name={}
    section_map={}
    command={}
    config=configparser.ConfigParser(strict=False)
    config.optionxform = lambda option: option
    config.read(local_temp_path+r"\storefix.ini")
    for section in config.sections():
    #print (section)
        if tu(section)=="COMMANDS":
            sec_cmd=section
        for key2 in config.options(section):
        #print (config[section][key2].split(",")[0])

            if len(config[section][key2].split(",")) >1:
                section_map[section]=config[section][key2].split(",")[1]
                section_name[(config[section][key2]).split(",")[1]]=config[section][key2].split(",")[0]
                if len(config[section][key2].split(",")[1].split("\n"))>0:
                    if tu(config[section][key2].split(",")[1].split("\n")[0]) == tu(section_updates) :
                        section_compare=config[section][key2].split(",")[1].split("\n")[0]
                        section_update=config[section][key2].split(",")[1].split("\n")[0]
                else:
                    if tu(config[section][key2].split(",")[1].split("\n")[0]) == tu(section_updates) :
                        section_compare=config[section][key2].split(",")[1]
                        section_update=config[section][key2].split(",")[1]

            else:
                if tu(config[section][key2].split(",")[0]) == tu(section_updates):
                    section_update=config[section][key2].split(",")[0]
                section_map[section]=section

    for key in config.options(sec_cmd):
        if len(config[sec_cmd][key].split(",")) >1:
            print(config[sec_cmd][key].split(",")[1])

            command[section_map[config[sec_cmd][key].split(",")[1].replace(" ","")]]=config[sec_cmd][key].split(",")[0]



    fh=open(local_temp_path+r"\store.ini","r")

    fh_array=fh.readlines()

    for i in range(len(fh_array)):

        if len(re.findall("%",fh_array[i])) >0:
            print(fh_array[i])

            str(fh_array[i]).replace('%','%%')
    fh.close()
    fh2=open(local_temp_path+r"\store_new.ini",'w')

    for i in range (len(fh_array)):


            fh2.write(fh_array[i])
            #fh2.write("\n")

    fh2.close()

    config1.read(local_temp_path+r"\store_new.ini")

    config2.read(local_temp_path+r"\store_temp.ini")
    flag=0
    flag_sec=0

    for section in config1.sections():
        print (section)
        print (params["section name"])
        print(section_compare)

        if  tu(section_updates) == tu(section):

            for key1 in config2.options(section_update):
                flag_sec=0

                for key2 in config1.options(section):
                    print(key1)
                    print(key2)

                    if tu(key1) == tu(key2):

                        flag_sec=1
                        if tu(config2[section_update][key2]).replace('"',"") != tu(config1[section][key1]).replace('"',""):
                            fail_step("[validate_storefix_singlesection]%s in section %s  is not updated"% (params["section name"],key2))
                            flag_val=1
                        else:

                            add_sub_step("[validate_storefix_singlesection]%s section   is updated with %s =%s"% (params["section name"],key2,config1[section][key2]))
                        break

                if flag_sec!=1:

                        flag=1
                        fail_step("[Validate_storefix_singlesection] %s is not there in %s section"%(key2,params["section name"]))


    if flag==1:
        fail_step("[validate_storefix_singlesection] Store.ini is not updated properly")
    else:
        pass_step("[validate_storefix_singlesection]Store.ini file is updated successfully")
    				#print(config[section][key])
    #print (config.inline_comments())


# Function: compare_store_ini_file
# Validation fo changing a single section in Store.ini file
#
# Parameters:
# None
#
# Process:
# copy the storfix.ini file from the central location.
# Copy store.ini file from SUT. Compare all section in stroefix.ini and store.ini files are same or not
# If the values under the section are same then store.ini file is updated propely
#
# Screen:
# N/A
#
# Author:
# Smitha D


def compare_store_ini_file(updated_setions):
    start_step("Verfy store.ini file is updated after running the job")
    awards_actions.copy_store_ini_from_SUT_local()

    local_temp_path = "%s_%s" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    config1 = configparser.ConfigParser(strict=False)
    config1.optionxform = lambda option: option
    config2= configparser.ConfigParser(strict=False)
    config2.optionxform = lambda option: option

    section_name={}
    section_map={}
    command={}
    config=configparser.ConfigParser(strict=False)
    config.optionxform = lambda option: option
    config.read(local_temp_path+r"\storefix.ini")
    for section in config.sections():
    #print (section)
        for key2 in config.options(section):
        #print (config[section][key2].split(",")[0])

            if len(config[section][key2].split(",")) >1:
                section_map[section]=config[section][key2].split(",")[1]
                section_name[(config[section][key2]).split(",")[1]]=(config[section][key2]).split(",")[0]
            else:
                section_map[section]=section

    for key in config.options("Commands"):

        if len(config["Commands"][key].split(",")) >1:
            if len(config["Commands"][key].split(",")[1].split("\n"))>1:
                command[section_map[config["Commands"][key].split(",")[1].split("\n")[0].replace(" ","")]]=config["Commands"][key].split(",")[0]
                if len(config["Commands"][key].split(",")[1].split("\n")[1].split("="))>1:
                        print(config["Commands"][key].split(",")[-1])


                        command[section_map[config["Commands"][key].split(",")[-1]]]=config["Commands"][key].split(",")[1].split("\n")[1].split("=")[1].split(",")[0]

            else:

                command[section_map[config["Commands"][key].split(",")[1].replace(" ","")]]=config["Commands"][key].split(",")[0]
    print(command)

    fh=open(local_temp_path+r"\store.ini","r")

    fh_array=fh.readlines()

    for i in range(len(fh_array)):

        if len(re.findall("%",fh_array[i])) >0:
            print(fh_array[i])

            str(fh_array[i]).replace('%','%%')
    fh.close()
    fh2=open(local_temp_path+r"\store_new.ini",'w')

    for i in range (len(fh_array)):


            fh2.write(fh_array[i])
            #fh2.write("\n")

    fh2.close()

    config1.read(local_temp_path+r"\store_new.ini")

    config2.read(local_temp_path+r"\store_temp.ini")
    flag=0
    flag_sec=0
    flag_secname=True
    for section2 in config2.sections():
        flag_sec=0


        if section2 in command.keys():

            if tu(command[section2])=="UPDATESECTION" or tu(command[section2])=="REMOVESECTION":
                print (command[section2])

                flag_sec=1
        print (updated_setions)
        print(section2)

        if section2 in section_name.keys() and section2 in updated_setions:

            for section1 in config1.sections():



                if tu(section_name[section2])==tu(section1):

                    print(section1)

                    flag_sec=1
                    for key1 in config2.options(section2):
                        flag_key=0
                        keyflag=True
                        flag=True
                        for key2 in config1.options(section1):

                            if tu(key1)==tu(key2):
                                flag_key=1

                                print(config2[section2][key2])

                                print(config1[section1][key1])


                                val1=config2[section2][key2].replace('"',"")
                                val2=config1[section1][key1].replace('"',"")

                                if tu(val1) != tu(val2):
                                    #
                                    flag = False
                                    fail_step("[validate_storefix]%s in section %s  is not updated with value is %s"% (key2,section1,val1))

                                else:


                                    add_sub_step("[validate_storefix] %s section is updated with value %s = %s"%(section_name[section2],key1,val2))
                                break

                        if flag_key!=1:
                            keyflag = False
                            fail_step("[validate_storefix]%s  is not there  in section %s  is not updated"% (key1 ,section1))

                    break

            if flag_sec==0:
                flag_secname=False

                fail_step("[validate_storefix] %s in Store.ini is not updated" %section_name[section2])



    print (flag+keyflag+flag_secname)
    if flag == False or keyflag == False or flag_secname==False:
        fail_step("Store.ini file is not updated properly. see details for results")
    else:
        pass_step("Store.ini file is updated successfully")
# Function: update_section_ini_file
# Validation fo changing a single section in Store.ini file
#
# Parameters :Params
#
# Process:
# update a section mentioned in the test case in a file
#
# Screen:
# N/A
#
# Author:
# Smitha D
def update_section_ini_file(ini_filepath,section,value,copy_file="No"):
        ##
        start_step("Update %s ini filefor %s section"%(ini_filepath,section))

        local_temp_path = "%s_%s" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))

        print (ini_filepath.replace("\\","\\\\"))
        print (copy_file)

        #
        ini_filename=os.path.split(ini_filepath)[-1]#.split("\\")[-
        if tu(copy_file)=="YES":

            awards_utils.prepare_local_temp_directory(local_temp_path)
            awards_actions.copy_ini_from_SUT_local(ini_filepath.replace("\\","\\\\"))
            #
        print (ini_filepath.replace("\\","\\\\"))
        fh=open(local_temp_path+r"\\\\"+ini_filename,"r")

        fh_array=fh.readlines()


        for i in range(len(fh_array)):

            if len(re.findall("%",fh_array[i])) >0:
                print(fh_array[i])

                str(fh_array[i]).replace('%','%%')
            if len(re.findall("\t",fh_array[i])) >0:
                print(fh_array[i])


                str(fh_array[i]).replace(' ','')
        fh.close()
        fh2=open(local_temp_path+r"\\\\"+ini_filename,'w')

        for i in range (len(fh_array)):


                fh2.write(fh_array[i])
                #fh2.write("\n")

        fh2.close()

        config = configparser.ConfigParser(strict=False)

        config.optionxform = lambda option: option



        config.read(local_temp_path+r"\\\\"+ini_filename)


        for file_section in config.sections():
            print(file_section)

            if tu(file_section) == tu(section):

                config.set(file_section,value.split("=")[0],value.split("=")[1])

        with open(local_temp_path+r"\\"+ini_filename,"w")as f:

            config.write(f)
        pass_step("Updated Store.ini file succesfully")


# Function: parse_single_section_ini_file
# Validation fo changing a single section in Store.ini file
#
# Parameters :Params
#
# Process:
# copy the storfix.ini file from the central location.
# Get the section name as a parameter from test cases to be updated
# Update the section with chain number 376 and store number 250
# Save storefix.ini
#
# Screen:
# N/A
#
# Author:
# Smitha D

def parse_single_section_ini_file(params):
    start_step("Update the storefix file with strore number 0250 and chanin numbere 376 for section %s"%params["section name"])
    awards_utils.prepare_local_temp_directory(DEFAULT_LOCAL_TEMP_PATH+"_"+ms.awards_machine_name.replace(" ", "_"))
    awards_actions.copy_store_ini_from_SUT_local()
    print(params["storefix file path"])

    utils.copy_local_file(params["storefix file path"]+r"\storefix.ini", DEFAULT_LOCAL_TEMP_PATH+"_"+ms.awards_machine_name.replace(" ", "_")+r"\storefix.ini")
    local_temp_path = "%s_%s" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    config = configparser.ConfigParser(strict=False)
    config.optionxform = lambda option: option
    config= configparser.SafeConfigParser(comment_prefixes=';')
    config.optionxform = lambda option: option
    fh=open(local_temp_path+r"\storefix.ini","r")

    fh_array=fh.readlines()

    for i in range(len(fh_array)):

        if len(re.findall("%",fh_array[i])) >0:
            #print(fh_array[i])

            str(fh_array[i]).replace('%','%%')
    fh.close()
    fh2=open(local_temp_path+r"\store_org.ini",'w')
    fh1=open(local_temp_path+r"\store_temp.ini","w")
    num=len(fh_array)

    for i in range (len(fh_array)):
        if len(re.findall("Command Data",fh_array[i])) >0:
            num=i
            break
        else:
            print (fh_array[i])

            fh2.write(fh_array[i])
            #fh2.write("\n")
    for j  in range(num,len(fh_array)):
        fh1.write(fh_array[j])
    fh.close()
    fh1.close()
    fh2.close()
    section_name={}
    section_map={}
    config.read(local_temp_path+r"\store_org.ini")
    #config.read("store.ini")

    for section in config.sections():
    #print (section)
        if tu(section) !="COMMANDS":
            for key2 in config.options(section):

                if tu(config[section][key2].split(",")[0]) == tu(params["section name"]):
                    config.set(section,params["store number"]+"_"+params["chain number"],config[section][key2])
                    if len(config[section][key2].split(",")) >1:
                        update_section=(config[section][key2].split(",")[1])

                    else:
                        update_section=(config[section][key2].split(",")[0])


    with open(local_temp_path+r"\storefix.ini","w")as f:

        config.write(f)

    fh_write=open(local_temp_path+r"\storefix.ini","a+")
    fh_tempfile=open(local_temp_path+r"\store_temp.ini","r")
    #print(len(fh_tempfile.readlines()))
    temp_write_array=fh_tempfile.readlines()

    for line in range(len(temp_write_array)):
        fh_write.write(temp_write_array[line])
    fh_write.close()
    pass_step("Updated Storefix file succesfully")
    print(update_section)

    return update_section

# Function: parse_ini_file
# Validation fo changing a single section in Store.ini file
#
# Parameters : None
#
# Process:
# copy the storfix.ini file from the central location.
# Update all the section with chain number 376 and store number 250
# Save storefix.ini
#
# Screen:
# N/A
#
# Author:
# Smitha D

def parse_ini_file(params):
    start_step("Update storefix file for all sections with store number %s and chain number %s"%(params["store number"],params["chain number"]))
    #commneted by ajay as was not needed
    #awards_actions.copy_store_ini_from_SUT_local()
    awards_utils.prepare_local_temp_directory(DEFAULT_LOCAL_TEMP_PATH+"_"+ms.awards_machine_name.replace(" ", "_"))
    print(params["storefix file path"])

    utils.copy_local_file(params["storefix file path"]+r"\storefix.ini", DEFAULT_LOCAL_TEMP_PATH+"_"+ms.awards_machine_name.replace(" ", "_")+r"\storefix.ini")

    local_temp_path = "%s_%s" % (DEFAULT_LOCAL_TEMP_PATH,ms.awards_machine_name.replace(" ", "_"))
    config = configparser.ConfigParser(strict=False)
    config.optionxform = lambda option: option
    fh=open(local_temp_path+r"\storefix.ini","r")

    fh_array=fh.readlines()

    for i in range(len(fh_array)):

        if len(re.findall("%",fh_array[i])) >0:
            print(fh_array[i])

            str(fh_array[i]).replace('%','%%')
    fh.close()
    fh2=open(local_temp_path+r"\store_org.ini",'w')
    fh1=open(local_temp_path+r"\store_temp.ini","w")
    num=len(fh_array)
    print(num)


    for i in range (len(fh_array)):
        if len(re.findall("Command Data",fh_array[i])) >0:
            num=i
            break
        else:
            print (fh_array[i])

            fh2.write(fh_array[i])

    print (num)
    print(len(fh_array))


    for j  in range(num,len(fh_array)):

        print(j)
        print(fh_array[j])
        fh1.write(fh_array[j])


    fh1.close()
    fh2.close()


    config.read(local_temp_path+r"\store_org.ini")
    #config.read("store.ini")
    cfgfile = open(local_temp_path+r"\storefix.ini",'w')
    section_exist=[]
    updated_setion=[]

    for section in config.sections():


        for key in config.options(section):

            if key.upper() == "ALL_STORES":
                if len((config[section][key]).split(",")) >1:
                    updated_setion.append((config[section][key]).split(",")[1])
                else:
                    updated_setion.append((config[section][key]).split(",")[0])
                break
            elif re.findall("ALL_",key.upper()):
                config.set(section,"ALL_"+params["chain number"],config[section][key])

            else:
                if (config[section][key]).split(",")[0] in section_exist:
                    pass
                else:

                    section_exist.append((config[section][key]).split(",")[0])
                    config.set(section,params["store number"]+"_"+params["chain number"],config[section][key])
                    if len((config[section][key]).split(",")) >1:
                        updated_setion.append((config[section][key]).split(",")[1])


    with open(local_temp_path+r"\storefix.ini","w")as f:

        config.write(f)

    fh_write=open(local_temp_path+r"\storefix.ini","a+")
    fh_tempfile=open(local_temp_path+r"\store_temp.ini","r")

    temp_write_array=fh_tempfile.readlines()

    for line in range(len(temp_write_array)):
        fh_write.write(temp_write_array[line])
    fh_write.close()
    pass_step("Updated Storefix file succesfully")
    return (updated_setion)

def validate_cdssection_screen(params):

    start_step("Validate CDS section in awards is displayed properly")
    utils.send_keys("[f1]")

    if tu(params["enabled"]) == "YES":
        if utils.wait_for_text_in_console("CDS Enabled: Yes"):
            pass_step("CDS Enabled displayed as 'Yes' as expected")
        else:
            fail_step("[validate_cds_section_on_screen] CDS is not enabled" )
            utils.take_screenshot()
            utils.finish_execution()

    elif tu(params["enabled"]) == "NO":

        if utils.wait_for_text_in_console("CDS Enabled: No"):
            pass_step("CDS Enabled displayed as 'No' as expected")
        else:
            fail_step("[validate_cds_section_on_screen] CDS is enabled" )
            utils.take_screenshot()
            utils.finish_execution()

## Function name - Validate vault is running
# Description: Function will find the heart beet on the console
# Screen:
# N/A
#
# Author:
# Smitha D
def validate_vault_is_running(timeout=DEFAULT_TIMEOUT):
    start_step("Validate vault is running")
    awards_navigation.to_vault_session()
    text_console=utils.get_console_text()
    print("%s"%text_console.encode('utf-8'))

    end_time = et(timeout)
    while not utils.find_text_in_console("console - \x03 ",True) and ct() < end_time:

            time.sleep(3)
    if ct() > end_time:
        fail_step("[validate vault is running] Not able to find \x03 on screen")
    else:
        pass_step("[validate vault is runnng] Vault is running ")
#Function Name:
#Description: Validate the log file for printing coupon
#Author:
#Smitha D
def validate_vaut_coupon_print_in_log():
    start_step("Validate that the proper coupon print can be found in the print log.")

    # Get the file and match the entry
    #file_name=
    awards_navigation.to_new_dos_session()
    utils.send_keys(r"copy C:\\vault\\logs\\debug.????-??-??-??.log C:\\vault\\logs\\debug.txt[enter]")
    utils.send_keys("y[enter]")
    utils.send_keys("exit")

    log_file = utils.get_file_contents(r"C:\\vault\\logs\\debug.txt" )
    #print (log_file)
    #expression=r'.*OrderTotalTran.*\n.*"lne":1.*"seq":\d+.*'
    expression=r'.*"lne":1.*"seq":\d+.*'
    log_row = re.findall(expression, log_file)

    # Get the log row with the right coupon number
    if len(log_row) == 0:

        fail_step("[validate_vault_coupon_print_in_log] Could not find a log entry .")
        utils.finish_execution()


    pass_step("Right entry has been found: <br>%s." % log_row)
#Function Name: validate_logrec_in_applog
#Description: Vlidate the recordtype 5a01 in the logfile after print_coupon
#Author:
#Smitha D
def validate_logrec_in_applog(log_file, coupon_number,clu):

    start_step("Validate that the proper coupon print can be found in the print log.")

    # Get the file and match the entry

    coupon_number = tu(coupon_number)
    #print (coupon_number)

    log_file = utils.get_file_contents("C:\strsystm\log\%s" % log_file)
    expression = r".*5a01.*coupon=%s.+clu=%s.+\n" % (coupon_number,clu)
    log_row = re.findall(expression, log_file)
    #
    # Get the log row with the right coupon number
    if len(log_row) == 0:
        awards_actions.start_awards()
        fail_step("[validate_logrec_in_applog] Could not find a log entry for coupon '%s'." % coupon_number)
        utils.finish_execution()

    # We got the right entry, now make sure CS=OK
    log_row1 = log_row[0]

    print (log_row1.find("cs=Ok"))
    find_cs=log_row1.find("5a01")

    if  log_row1.find("5a01")== -1:
        awards_actions.start_awards()
        fail_step("[validate_logrec_in_applog] The right entry for coupon '%d' has been found in the report but 'CS' is not 'OK'.")
        utils.finish_execution()
    #
    pass_step("Right entry has been found: <br>%s." % log_row)

def validate_ini_file(file1,file2):
    start_step("validate store.ini file")
    fh=open(file1,"r")

    fh_array=fh.readlines()

    for i in range(len(fh_array)):

        if len(re.findall("%",fh_array[i])) >0:
            print(fh_array[i])

            fh_array[i]=str(fh_array[i]).replace('%','%%')
    fh.close()
    fh1=open(file1,"w")
    for line in fh_array:
        fh1.write(line)

    fh1.close()
    fh=open(file2,"r")

    fh_array1=fh.readlines()

    for i in range(len(fh_array1)):

        if len(re.findall("%",fh_array1[i])) >0:
            print(fh_array1[i])

            fh_array1[i]=fh_array1[i].replace('%','%%')
            print (fh_array1[i])
    fh.close()

    fh2=open(file2,"w")
    for line in fh_array1:
        fh2.write(line)
    fh2.close()

    config1=configparser.ConfigParser(strict=False)
    config1.optionxform = lambda option: option
    config1.read(file1)
    config2= configparser.ConfigParser(strict=False)
    config2.optionxform = lambda option: option
    config2.read(file2)
    keyflag=0
    flag_secname=0
    flag=0
    for section1 in config1.sections():
        flag_sec=0
        for section2 in config2.sections():
            if tu(section1) == tu(section2):
                flag_sec=1
                for key1 in config1.options(section1):
                    flag_key=0
                    for key2 in config2.options(section2):
                        if tu(key1)==tu(key2):
                            flag_key=1
                            val1=config2[section2][key2].replace('"',"").replace("(","_").replace(")","_").replace(".","_")
                            val2=config1[section1][key1].replace('"',"").replace("(","_").replace(")","_").replace(".","_")

                            if tu(val1) != tu(val2):
                                #
                                flag = False
                                fail_step("%s in section %s  is not updated with value is %s"% (key2,section1,val1))

                            else:


                                add_sub_step("section is updated with value %s = %s"%(key1,val2))
                            break

                    if flag_key!=1:
                        keyflag = False
                        fail_step("%s  is not there  in section %s  is not updated"% (key1 ,section1))

                break

        if flag_sec==0:
            flag_secname=False

            #fail_step("Store.ini is not updated" )



    print (flag+keyflag+flag_secname)
    if flag == False or keyflag == False or flag_secname==False:
        fail_step("Store.ini file is not updated properly. see details for results")
    else:
        pass_step("Store.ini file is updated successfully")

#Function Name: validate_section_in_ini_file
#Description: Validate the section name in the ini file is proper
#Author:
#Smitha D

def validate_section_in_ini_file(filename,section_name, values):

    start_step("Validate the file '%s' contains the right values." % filename)

    print (values)

    for field in values:
        print(field)


        # Clean the parameter stuff in case this is a parameter dictionary
        clean_field = field.strip()

        # Check each of the fields
        awards_utils.verify_ini_file_value(filename, section_name, clean_field, values[field])


#Function Name: validate_section_value_not_in_ini_file
#Description: Validate the section name and values are not in  the ini file
#Author:
#Smitha D
def validate_section_value_not_in_ini_file(filename,section_name,values):
    start_step("Validate the file %s does not have values"%filename,params)
    for field in values:
        clean_field=field.strip()
        awards_utils.verify_value_not_in_ini_file(filename, section_name, clean_field, values[field])
#Function Name: validate_order_CAPT_LOG
#Description: Validate order of the logs captured in CAPT LOG
#Author:
#Smitha D


def validate_order_CAPT_LOG():

        start_step("Validate all messages are captured.")

        # Get the file contents from the database report
        file_contents = utils.get_file_contents(DEFAULT_CAPT_LOG_PATH)
        entry = re.findall("Lane:.*:.*:", file_contents)
        if len(entry)>0:
        #entry = re.findall(".*FYI - FINISHIT: PROCESSING FINISHED.*", file_contents)[0]

            pass_step("Right entry has been found in the file: <br>%s." % file_contents)
        else:
            fail_step("order in the Log file is not ")


#Function Name: validate_messages_CAPT_LOG
#Description: All the messages displayed on screen are  captured in log
#Author:
#Smitha D

def validate_messages_CAPT_LOG(messages):


        start_step("Validate all messages are captured.")
        flag=0

        # Get the file contents from the database report

        file_contents = utils.get_file_contents(DEFAULT_CAPT_LOG_PATH)
        for message in messages:

            entry = re.findall(message, file_contents)
            if entry==None or len(entry)==0 :
                fail_step("%s is not there in log file"%message)
                flag=1
                #utils.finish_execution()
        #entry = re.findall(".*FYI - FINISHIT: PROCESSING FINISHED.*", file_contents)[0]
        if flag !=1:
            pass_step("Right entry has been found in the file: <br>%s." % file_contents)



#Function Name: validate_messages_Not_in_CAPT_LOG
#Description: All the messages are not captured in log when WEBPOs is disabled
#Author:
#Smitha D

def validate_messages_Not_in_CAPT_LOG(messages,filename=DEFAULT_CAPT_LOG_PATH):


        start_step("Validate all messages are captured.")
        flag=0
        #

        # Get the file contents from the database report

        file_contents = utils.get_file_contents(filename)
        #print
        #
        for message in messages:

            entry = re.findall(message, file_contents)
            if entry ==None or len(entry)==0 :
                pass_step("%s is not there in log file"%message)
                flag=1
                #utils.finish_execution()
        #entry = re.findall(".*FYI - FINISHIT: PROCESSING FINISHED.*", file_contents)[0]

        if flag !=1:
            #
            fail_step("Messages are there in the log")

#Function Name: validate_log_files
#Description: Validate 2 log files same
#Author:
#Smitha D
def validate_log_files(file1,file2):
    start_step("Compre log files")
    print(file1)
    print(file2)

    fh1=open(file1,"r+")
    fh2=open(file2,"r")
    lines_file1=fh1.readlines()

    lines_file2=fh2.readlines()
    print (lines_file2)

    for line1 in lines_file1:
        for line2 in lines_file2:
            if line1 != line2:
                fail_step("Files are not mmatching")
                utils.finish_execution()
    fh1.close()
    fh2.close()
    pass_step("Files are same")
#Function name : validat_trans_in_applog
#
#Description: Validate trans is displayed for each order
#
## Author : Smitha D

def validate_trans_applog(filename):
    start_step("Validate transID is displayed for all order")
    file_contents = utils.get_file_contents(filename)
    for line in file_contents:
        if len(re.findall("4607:",line))>0:
            ver=re.findall("trans",line)
            if ver==None or len(ver)<1:
                fail_step("transaction ID is not present for oreder %s"%line)
                utils.finish_execution()
    pass_step("Transtion ID is present for all orders")
#####################################################
#Function NAme: validate_messges_POS_Simulator_log
#####################################################
#DEscription: Validate messages in POS log file
#####################################################
#Author: Smitha D
######################################################



def validate_messges_POS_Simulator_log(filename,message):
    start_step("VerifySimulator log has %s"%message)
    fh=open(fileame)
    ver=re.findall(message,fh)
    if ver == None or len(ver)<1 :
        fail_step("%s not foung in POS Simulator log"%message)
    else:
        pass_step("%sF ound in POS Simulator log"%message)
#####################################################
#Function NAme: validate_tranid_captlog
#####################################################
#DEscription: Comapre transid in POS log and CAPT log
#####################################################
#Author: Smitha D
######################################################



def validate_tranid_captlog(trnaids,filename=DEFAULT_CAPT_LOG_PATH):
    start_step("Validate transID is displayed for all order")
    file_contents = utils.get_file_contents(filename)
    for line in file_contents:
        if len(re.findall("Start order: 123456",line))>0:

            if len(line.split(","))>1:
                if line.split(",")[-1] not in transids:
                    fail_step("transaction ID is not present for oreder %s"%line)
                    utils.finish_execution()
    pass_step("Transtion ID is present for all orders")


#####################################################
#Function NAme: validate_message_in_webposlog
#####################################################
#DEscription: Validate the messages are there in WEBPOS log
#####################################################
#Author: Smitha D
######################################################
def validate_message_in_webposlog(messages,filename=DEFAULT_WEBPOSLOG_PATH):
    flag=0

    start_step("Validate webpos log has all the messages")
    utils.send_keys(r"cd %s[enter]"%filename)
    utils.send_keys(r"copy webpos_*.log webposlog.txt[enter]")
    filename=filename+"\\webposlog.txt"
    file_contents = utils.get_file_contents(filename)
    for message in messages:
        if len(re.findall(message,file_contents))<1:
            flag=1
    if flag==1:
        fail_step("Some messages not found in WEBPOS log")
    else:
        pass_step("All messages are there in WEBPOS log")

#####################################################
#Function NAme: validate_message_not_in_webposlog
#####################################################
#DEscription: Validate the messages are not there in WEBPOS log
#####################################################
#Author: Smitha D
######################################################
def validate_message_not_in_webposlog(messages,filename=DEFAULT_WEBPOSLOG_PATH):
    flag=0

    start_step("Validate webpos log has all the messages")
    utils.send_keys(r"cd %s[enter]"%filename)
    utils.send_keys(r"copy webpos_*.log webposlog.txt[enter]")
    filename=filename+"\\webposlog.txt"
    file_contents = utils.get_file_contents(filename)
    for message in messages:
        if len(re.findall(message,file_contents))>0:
            flag=1
    if flag==1:
        fail_step("Some messages found in WEBPOS log")
    else:
        pass_step("All messages are not there in WEBPOS log")


#####################################################
#Function NAme: validate_Eror_in_Errorlog
#####################################################
#DEscription: Comapre transid in POS log and CAPT log
#####################################################
#Author: Smitha D
######################################################
def validate_Eror_in_Errorlog(filename=DEFAULT_ERROR_LOG_PATH):
    start_step("Validate transID is displayed for all order")
    file_contents = utils.get_file_contents(filename)

    if len(re.findall("Error",file_contents))>0:
        fail_step("Errors are there in Errorlog")
    else:
        pass_step("No errors are there in errorlog")

##################################################################
# Funtion Name:validate_message_config
# DEscription: validate message in config.josn
#Author Smitha D
#####################################################################
def validate_message_config(messages,filename=DEFAULT_CONFIG_JSON_PATH):
    start_step("Validate config.json file")
    file_contents = utils.get_file_contents(filename)
    flag=0
    for message in messages:
        if len(re.findall(message,file_contents))<0:
            flag=1
    if flag!=1:
        pass_step("All the messages are there in config file")
    else:
        fail_step("Some messages are not there in config file")

###########################################################################
#Function Name:Verify_swapfile_error
#Description:Verify Swapfile error is not there in applog
#Author:Smitha D
###########################################################################
def verify_swapfile_error(filename,Error_msg=None,copy_text="No"):
    start_step("Verify swpfile creation error messge is there in applog")
    print (filename)
    #pdb.set_trace()
    log_file = utils.get_file_contents("C:\strsystm\log\%s" % filename)
    if Error_msg==None:
        if len(re.findall("Error",file_contents))>0:
            fail_step("Errors are there in Errorlog")

        # Veriying the error message in the errorlog
    else:

        #pdb.set_trace()
        print(log_file)
        #pdb.set_trace()
        for line in log_file.split("\n"):
                #
            print(line)
            #pdb.set_trace()
            if len(re.findall(Error_msg.replace("::","_"),line.replace("::","_")))>0:

                pdb.set_trace()
                print (copy_text)
                #pdb.set_trace()
                if tu(copy_text)=="YES":
                    filename="%s\%s_memoryusage_%s.txt"%(DEFAULT_COPY_ERROR_LOG,ms.awards_machine_name,datetime.datetime.now().strftime("%Y_%m_%d"))
                    if not os.path.isfile(filename):
                        fh=open(filename,"w")
                    else:
                        fh=open(filename,"a")
                        fh.write("\t"+line)
                        fh.close()

                    fail_step("Error message %s is there in the log file"%Error_msg)
        pass_step("No errors are there in errorlog")
