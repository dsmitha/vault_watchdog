# Set the expected awards, OS and build versions
awards_version ="17.2.26.0"
os_version ="2.0.55.02"
build_version ="08/19/2014"
daily_build_ver ="A - 17.3.18.0"
# Set the framework base folder below
from pathlib import Path
import pdb
base_path=str(Path(__file__).parent)+"\\"
base_path_report = "I:\SQA\shared\Automation\Projects\Awards_TL\\"
print(base_path)

memory_leak_test="yes"
# Set the scripts folder below
script_path = base_path + "scripts/"

# Set the application name
app_name = "Awards"

# Set the application environment
app_environment = "sqa"

# extract logs at the end of the executions
extract_logs_at_end = False

# Keep python open on error
keep_cmd_on_error = False

#Verify step is last step or not
validate_step="No"
# ALM Setup
alm_post_results = True
alm_server_path = 'http://orlpalmv01:8080/qcbin'
alm_username = 'agururaj'
alm_password = ''
alm_domain = 'AUTOMATION'
alm_project = 'Automation_Poc'

# Element repository file path
element_repository_path = base_path + "element_repository.txt"

# Defines the browser type
# Accepted Values:
# Firefox
# Chrome
# IE
browser_type = "chrome"

# Defines if the browser window must remain open after execution
remain_open = False

# Defines if the test should finish on assertions
abort_on_assertion = False

# Defines the path for the report output
report_path = base_path_report  + "reports"

# Set the default report template paths
report_template = base_path + "framework/HTML Report/template.html"
report_template_path = base_path + "framework/HTML Report/"
report_shared_lib_path = base_path + "framework/HTML Report/"
validate_step_VM124="Yes"
validate_step_VM121="Yes"
