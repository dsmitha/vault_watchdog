rem Hello All, this file is the trigger for SKIM execution. Here we mention the
rem testplanid/platforms used and also the VM's it is executed from.

rem cd \
cls
rem #############################################################################################
rem mention the testplanid below when w new Testlink Testplan is created
set strtestplanid=110247
set builddate="17.3.34.0"
set buildfolder="\\stpshared\shared\Store_Systems\public\release_builds\17.3\17.3.34.0"
rem #############################################################################################

set Batfilepath=%~dp0
rem cd %Batfilepath%
rem pause
rem CDS
set strplatformid="CMC6"
echo %buildfolder%
echo %builddate%
pause
set strplatformid="CMC6"
start %Batfilepath%"VMN143.bat" %~n0 %strtestplanid% %strplatformid% %builddate% %buildfolder%
